<div id="footer">
    <div class="a_fBox span-6">
        <div class="a_fBox_top">
            <h3>Twoje konto</h3>
                        <p>- <a href="#">Rejestracja</a></p>
            <p>- <a href="#">Logowanie</a></p>
                        <p>- <a href="#">Zmień hasło</a></p>
        </div>
        <div class="a_fBox_title"></div>
        <p>- <a href="#">Twoje ogłoszenia</a></p>
        <p>- <a href="#">Obserwowane ogłoszenia</a></p>
    </div>
    
    
    <div class="a_fBox span-6">
        <div class="a_fBox_top">
            <h3>Jak dodać ogłoszenie</h3>
            <p>- <a href="#">Jak pisać ogłoszenie?</a></p>
            <p>- <a href="#">Programy dla firm</a></p>
            <p>- <a href="#">Cennik ogłoszeń</a></p>
            <p>- <a href="#">Pomoc</a></p>
        </div>
        <div class="a_fBox_title"></div>
        <h2>+ <a href="http://www.nawynajmij.pl/u">Dodaj ogłoszenie</a></h2>
    </div>
    

    <div class="a_fBox span-6">
        <div class="a_fBox_top">
            <h3>NaWynajmij.pl</h3>
            <p>- <a href="#">O serwisie NaWynajmij.pl</a></p>
            <p>- <a href="#">Regulamin</a></p>
            <p>- <a href="#">Ochrona prywatności</a></p>
            <p>- <a href="#">Nasi partnerzy</a></p>
        </div>
        <div class="a_fBox_title">Dowiedz się więcej:</div>
        <p>- <a href="#">Aukcje</a></p>
        <p>- <a href="#">Fundusz Wynajem Mieszkań</a></p>
    </div>

    
    
    
    <div class="a_fBox span-6 last">
        <div class="a_fBox_top">
            <h3>Kontakt</h3>
            <div class="a_fBox_mail"><a href="mailto:info@nawynajmij.pl">info@nawynajmij.pl</a></div>
        </div>
        <div class="a_fBox_title"></div>
        <p>- <a href="#">Mapa strony</a></p>
    </div>
</div>
</div>
<div id="a_footer_line"></div>
<div id="a_footer2" class="container">
    <div class="span-12">
        Copyright: <a href="#">NaWynajmij.pl</a>
    </div>
    <div class="span-12 last text-right">
        <a href="http://ernes.pl/">made by ernes.pl</a>
    </div>
</div>


</body></html>