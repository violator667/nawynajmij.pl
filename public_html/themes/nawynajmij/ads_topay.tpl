<div class="span-6">
{include file="account_menu.tpl"}

</div>
<div class="span-18 last"><div class="span-18 last">
<h1>{$title}</h1>
        {if $error}
          {foreach from=$error item=err}
            <div class="error">{$err.0}</div>
          {/foreach}
        {/if}
        {if $notice}
          {foreach from=$notice item=ntc}
            <div class="success">{$ntc.0}</div>
          {/foreach}
        {/if}
<div class="clearfix row-header">
	<div class="span-2">
		<div>Numer ogłoszenia</div>
	</div>	
	<div class="span-4">
		<div>Tytuł</div>
	</div>
	<div class="span-2">
		<div title="Ogłoszenie wyróżnione">Wyróżnienie</div>
	</div>
	<div class="span-2">
		<div title="Ogłoszenie promowane">Promowanie</div>
	</div>
	<div class="span-5">
		<div>Typ płatności / czas</div>
	</div>
	<div class="span-3 last">
		<div>Zatwierdź</div>
	</div>
</div>
{foreach from=$ad_list item=ad}
{cycle values='row-a,row-b' assign=cell} 
<div class="clearfix row {$cell}">
	<div class="span-2"><center>{$ad.aid}</center></div>
	<div class="span-4">
		<a href="{router->linkTo module=ads action=showad id=$ad.aid link=$ad.link}">{$ad.title}</a>	</div>
	<div class="span-2 cell-c">
    {if $ad.highlighted eq 1}
      TAK
    {else}
      nie
    {/if}
	</div>
	<div class="span-2 cell-c">
    {if $ad.positioned eq 1}
      TAK
    {else}
      nie
    {/if}
	</div>
	<div class="span-5 cell-c">
	<form action="user/payment" method="post">
	<input type="hidden" name="aid" value="{$ad.aid}">	
	<input type="hidden" name="high" value="{$ad.highlighted}">	
	<input type="hidden" name="posit" value="{$ad.positioned}">	
	<input type="hidden" name="auction" value="{$ad.auction}">	
      <select name="payment_method">
        {foreach from=$ad.payment_methods item=pay}
          <option value="{$pay.id}">
            {if $pay.type eq "cash"}
              Przelew: {$pay.days} dni : {$pay.price} zł
            {elseif $pay.type eq "subscription"}
              ABONAMENT: {$pay.days} dni : {$pay.price} zł
            {else}
              SMS: {$pay.days} dni : {$pay.price} zł
            {/if}
          </option>
        {/foreach}
      </select>

	</div>
	<div class="span-3 last">
		<div><input type="submit" value="opłać"></form> <a href="{router->linkTo module=user action=adedit id=$ad.aid}"><img src="themes/nawynajmij/obraz/icon-edit.gif" alt="Edytuj" title="Edytuj ogłoszenie"/></a> <a href="{router->linkTo module=user action=addelete id=$ad.aid}" onclick="return confirm('Usunąć ogłoszenie bezpowrotnie?');"><img src="themes/nawynajmij/obraz/icon-delete.gif" alt="Usuń" title="Usuń ogłoszenie"/></a></div>
	</div>
</div>
{/foreach}

</div></div><div class="clearfix"></div>
<script>
(function() {
});
</script>