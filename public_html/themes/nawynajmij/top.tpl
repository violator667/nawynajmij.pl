                       <body>
                            <div class="container">
    <div class="clearfix" id="a_header">
        <div class="span-8"><a href="{$site_url}"><img src="themes/nawynajmij/obraz/logo.png" id="a_logo" alt="NaWynajmij.pl - serwis z nieruchomościami na wynajem."></a></div>
        <div class="span-16 last np" id="a_header_menu">
            <div class="cell-r">
                                {if $user.role > 0 }
                                  <a href="{router->linkTo module=user action=index}"><span>Witaj, {$user.username}  Twoje konto</span></a>
                                {else}
                                  <a href="{router->linkTo module=user action=index}"><span>Zaloguj się / Zarejestruj</span></a>
                                {/if}
                                  <a href="#">Pomoc</a>
                                  <a href="#">Kontakt</a>
                                {if $user.role > 0 }
                                  <a href="{router->linkTo module=user action=logout}">Wyloguj się</a>
                                {/if}
                            </div>
        </div>
    </div>
    <div id="a_search">
        <form method="post" action="category/search">
            <input type="text" name="search" id="a_search_input" placeholder="wpisz miejscowość, dzielnicę lub ulicę" value="{$searchWord}">
            <button type="submit" value="Szukaj" id="a_search_button"></button>
        </form>
    </div>               
{include file="breadcrumbs.tpl"}
