<div class="catWrap clearfix">
    <div class="catMenuWrap span-5">
		{include file="menucat.tpl"}     	
	 </div>
    <div class="catListWrap span-19 last">
        {if $error}
          {foreach from=$error item=err}
            <div class="error">{$err.0}</div>
          {/foreach}
        {/if}
        {if $notice}
          {foreach from=$notice item=ntc}
            <div class="success">{$ntc.0}</div>
          {/foreach}
        {/if}
		<div class=" positioned">
      {if $positionedAds}
      {foreach from=$positionedAds item=ad}
      	{if $ad.highlighted eq 1}
      	<div class="a_adoffer clearfix highlighted">
      	{else}
      	<div class="a_adoffer clearfix">
      	{/if}
  				<div class="a_thumb span-5">
      			<a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}"><img src="{$ad.photo}" width="176px"></a>
  				</div>
 			<div class="span-10">
 				{if $ad.highlighted eq 1}
        		<div class="a_title_bold"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">{$ad.title}</a>
        		{else}
        		<div class="a_title"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">{$ad.title}</a>
        		{/if}
        		<div class="a_titleAddress">Adres: {$ad.city}</div>
        		</div>

    			<div class="a_shortDesc span-10">
             {$ad.text_raw}
    			</div>
 			 </div>
  			<div class="span-3 last a_offer_right">
  			{if $ad.auction eq 1}
          {if $ad.auction_array.current_bid >= $ad.price}
            <div class="a_price"><span>{$ad.auction_array.current_bid}</span> zł</div>
          {else}
            <div class="a_price"><span>{$ad.price}</span> zł</div>
          {/if}
        {else}
          <div class="a_price"><span>{$ad.price}</span> zł</div>
        {/if}
    			<div class="a_offerAttr a_priceType"><span>{$ad.price_type_str}</span></div>
  			</div>
  			<a class="a_offerMore" href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">zobacz ofertę</a>
  			<div class="a_adCreatedAt">
    			Dodano: <span title="{$ad.created_datetime}">{$ad.created_date}</span>
 			</div>
			</div>
		{/foreach}   
      {/if}          
      </div>
    </div>


</div><div class="clearfix"></div>
