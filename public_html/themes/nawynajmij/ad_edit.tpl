<div class="span-6">
{include file="account_menu.tpl"}

</div>
<div class="span-18 last"><div class="span-18 last">
<h1>{$title}</h1>
{if $error}
  {foreach from=$error item=err}
    <div class="error">{$err.0}</div>
  {/foreach}
{/if}
{if $notice}
  {foreach from=$notice item=ntc}
    <div class="success">{$ntc.0}</div>
  {/foreach}
{/if}
			<form action="{$site_url}user/doedit/" method="post">
			<input type="hidden" name="token" value="{$token}">
			<input type="hidden" name="edit" value="{$id}">
			<table class="table">
					<tbody>
						<tr class="success">
							<td width="40%"><b>Telefon kontaktowy:</b></td>
							<td width="60%"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="tel" size="50" value="{$tel}"></td>
							<td width="60%"></td>
						</tr>
						<tr class="success">
							<td width="40%"><b>Email kontaktowy:</b></td>
							<td width="60%"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="contactEmail" size="50" value="{$email}"></td>
							<td width="60%"></td>
						</tr>
						<tr class="success">
							<td width="40%"><b>Treść:</b></td>
							<td width="60%"></td>
						</tr>
						<tr>
							<td width="40%"><textarea id="adText" name="adText">{$adText}</textarea></td>
							<td width="60%"></td>
						</tr>
						<tr class="active">
							<td width="40%"><input type="submit" value="zmień dane"></td>
							<td width="60%"></td>
						</tr>
					</tbody>
			</table>
			</form>

</div></div><div class="clearfix"></div>