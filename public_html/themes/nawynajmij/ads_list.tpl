<div class="span-6">
{include file="account_menu.tpl"}

</div>
<div class="span-18 last"><div class="span-18 last">
<h1>{$title}</h1>
        {if $error}
          {foreach from=$error item=err}
            <div class="error">{$err.0}</div>
          {/foreach}
        {/if}
        {if $notice}
          {foreach from=$notice item=ntc}
            <div class="success">{$ntc.0}</div>
          {/foreach}
        {/if}
<div class="clearfix row-header">
	<div class="span-2">
		<div>Numer ogłoszenia</div>
	</div>	
	<div class="span-4">
		<div>Tytuł</div>
	</div>
	<div class="span-2">
		<div title="Ogłoszenie wyróżnione">Wyróżnienie</div>
	</div>
	<div class="span-2">
		<div title="Ogłoszenie promowane">Promowanie</div>
	</div>
	<div class="span-2">
		<div>Wyświetleń</div>
	</div>
	<div class="span-2">
		<div>Data dodania</div>
	</div>
	<div class="span-2">
		<div>Data wygaśnięcia</div>
	</div>
	<div class="span-2 last">
		<div>Opcje</div>
	</div>
</div>
{foreach from=$ad_list item=ad}
{cycle values='row-a,row-b' assign=cell} 
<div class="clearfix row {$cell}">
	<div class="span-1">
		<!-- <input type="checkbox" name="topay[73]" value="1" checked="checked"/> -->&nbsp;
	</div>
	<div class="span-1">{$ad.aid}</div>
	<div class="span-4">
		<a href="{router->linkTo module=ads action=showad id=$ad.aid link=$ad.link}">{$ad.title}</a>	</div>
	<div class="span-2 cell-c">
    {if $ad.highlighted eq 1}
      TAK
    {else}
      nie
    {/if}
	</div>
	<div class="span-2 cell-c">
    {if $ad.positioned eq 1}
      TAK
    {else}
      nie
    {/if}
	</div>
	<div class="span-2 cell-c">
		{$ad.ad_views}
	</div>
	<div class="span-2">
								{$ad.created}	
					</div>
	<div class="span-2">
									{$ad.expires}
					</div>
	<div class="span-2 last">
	{if $ad.auction eq 1}
	 <a href="{router->linkTo module=user action=auction id=$ad.aid}"><img src="themes/nawynajmij/obraz/icon-auction.png" width="14" height="14" alt="Aukcja" title="Aukcja"/></a>
	&nbsp;&nbsp;
	{/if}
		<a href="{router->linkTo module=user action=adedit id=$ad.aid}"><img src="themes/nawynajmij/obraz/icon-edit.gif" alt="Edytuj" title="Edytuj ogłoszenie"/></a>
	&nbsp;&nbsp;
    {if $ended}
      <a href="{router->linkTo module=user action=adreset id=$ad.aid}" onclick="return confirm('Zamierzasz wystawić ponownie ogłoszenie - zostaniesz przeniesiony na stronę z ogłoszeniami do opłacenia.');"><img src="themes/nawynajmij/obraz/icon-repeat.png" alt="Wystaw ponownie" title="Wystaw ponownie"/></a>
    {else}
      <a href="{router->linkTo module=user action=adstop id=$ad.aid}" onclick="return confirm('Zakończyć ogłoszenie?');"><img src="themes/nawynajmij/obraz/icon-delete.gif" alt="Zakończ" title="Zakończ ogłoszenie"/></a>
    {/if}
	</div>
</div>
{if $ended}
{else}
  <div class="clearfix row {$cell}">
    <div class="span-4">Przedłuż ogłoszenie: </div>
    <div class="span-14 last">
    <form action="user/payment" method="post">
    <input type="hidden" name="aid" value="{$ad.aid}">
    <input type="hidden" name="high" value="{$ad.highlighted}">	
    <input type="hidden" name="posit" value="{$ad.positioned}">	
        <select name="payment_method">
          {foreach from=$ad.payment_methods item=pay}
            <option value="{$pay.id}">
              {if $pay.type eq "cash"}
                Przelew: {$pay.days} dni : {$pay.price} zł
              {elseif $pay.type eq "subscription"}
                ABONAMENT: {$pay.days} dni : {$pay.price} zł
              {else}
                SMS: {$pay.days} dni : {$pay.price} zł
              {/if}
            </option>
          {/foreach}
        </select>
        <input type="submit" value="przedłuż"></form>
  </div></div>
  {/if}
{/foreach}


</div></div><div class="clearfix"></div>
