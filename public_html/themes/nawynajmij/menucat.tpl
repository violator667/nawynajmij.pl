{if $mainCategories}
  <ul id="a_catMenu">
    {foreach from=$mainCategories item=main}
        <li>
        <a href="{router->linkTo module=category action=show cid=$main.cid page=1 link=$main.link}">{$main.name} ({$main.ad_num})</a>
        </li>
    {/foreach}
  </ul>
{else}
  {if $subParent.0.name}
    <a class="subCat" href="{router->linkTo module=category action=show cid=$subParent.0.cid page=1 link=$subParent.0.link}">{$subParent.0.name}</a>
  {/if}
  {if $subCategories}
  <ul id="a_catMenu">
    {foreach from=$subCategories item=sub}
      
      {if $activeCategory eq $sub.cid}
        <li class="active">
          <a href="{router->linkTo module=category action=show cid=$sub.cid page=1 link=$sub.link}">{$sub.name} ({$sub.ad_num})</a>
        {if $activeChildren}
          <ul>
          {foreach from=$activeChildren item=act}
            <a href="{router->linkTo module=category action=show cid=$act.cid page=1 link=$act.link}">{$act.name} ({$act.ad_num})</a>
          {/foreach}
          </ul>
        {/if}
          
        </li>
      {else}
        <li>
        <a href="{router->linkTo module=category action=show cid=$sub.cid page=1 link=$sub.link}">{$sub.name} ({$sub.ad_num})</a>
        </li>
      {/if}
    {/foreach}
  </ul>
  {else}
  {/if}
{/if}
<ul id="a_catMenuAukcje">
  <li class="a_catMenu"><a href="{router->linkTo module=category action=show auction=1 page=1 link=aukcje}">Aukcje</a></li>
</ul>
<div id="a_add"><img src="themes/nawynajmij/obraz/add.png"></div>

