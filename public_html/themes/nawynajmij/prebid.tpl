<div class="span-6">
{include file="account_menu.tpl"}

</div>
<div class="span-18 last"><div class="span-18 last">
<h1>Aukcja: {$ad_name}</h1>
{if $error}
  {foreach from=$error item=err}
    <div class="error">{$err.0}</div>
  {/foreach}
{/if}
{if $notice}
  {foreach from=$notice item=ntc}
    <div class="success">{$ntc.0}</div>
  {/foreach}
{/if}
			<form action="{$site_url}user/bid/" method="post">
			<input type="hidden" name="token" value="{$token}">
			<input type="hidden" name="ad_id" value="{$ad_id}">
			<input type="hidden" name="ad_name" value="{$ad_name}">
			<input type="hidden" name="ad_link" value="{$ad_link}">
			<table class="table">
					<tbody>
						<tr class="success">
							<td width="100%" colspan="2"><b>Twoja maksymalna oferta</b></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="bid" size="7" value="{$bid}"> zł</td>
							<td width="80%"><input type="submit" value="Złóż ofertę"></td>
						</tr>

					</tbody>
			</table>
			</form>

</div></div><div class="clearfix"></div>