<div class="catWrap clearfix">
    <div class="catMenuWrap span-5">
		{include file="menucat.tpl"}     	
	 </div>
<div class="span-19 last a_offer">
        {if $ad.status eq 2}
          <div class="alert">
            <h2>Ta oferta wygasła</h2>
            {if $similar}
              Oto podobne ogłoszenia które są jeszcze aktualne:<br/>
              {foreach from=$similar item=simi}
                <li><a href="{router->linkTo module=ads action=showad id=$simi.aid link=$simi.link}">{$simi.title}</a>
              {/foreach}
            {/if}
          </div>
        {/if}
        <h2 class="span-15">{$ad.title}</h2>
        <div class="span-3 last text-blue text-right">nr oferty: {$ad.aid} / odsłon: {$ad.ad_views}</div>
        <div class="clearfix"></div>
        <div class="span-12 a_offerimages">
                     {if $adPhotos|@count gt 0}
                      {foreach from=$adPhotos name=photo item=photo}
                        {if $smarty.foreach.photo.first}
                          <div class="a_first">
                            <a href="{$site_url}upload/{$photo.file}" data-lightbox="ogloszenie"><img src="{$site_url}upload/{$photo.file}" rel="lightbox[{$ad.aid}]" width="435" alt=""></a>
                          </div>
                        {else}
                          {if $smarty.foreach.photo.last}
                            <div class="l last">
                              <a href="{$site_url}upload/{$photo.file}" data-lightbox="ogloszenie"><img src="{$site_url}upload/{$photo.file}" rel="lightbox[{$ad.aid}]" width="105" alt=""></a>
                            </div>
                          {else}
                            <div class="l">
                              <a href="{$site_url}upload/{$photo.file}" data-lightbox="ogloszenie"><img src="{$site_url}upload/{$photo.file}" rel="lightbox[{$ad.aid}]" width="105" alt=""></a>
                            </div>

                          {/if}
                        {/if}
                        {/foreach}
                    {else}
                      <div class="a_first">
                            <a href="{$site_url}themes/nawynajmij/obraz/camera.png" data-lightbox="ogloszenie"><img src="{$site_url}themes/nawynajmij/obraz/camera.png" rel="lightbox[{$ad.aid}]" width="435" alt=""></a>
                      </div>
                    {/if}      </div>                
        <div class="span-6 last a_offerAttrBox">
            <p class="a_offerAttrTitle">Informacje o ofercie</p>
            <dl class="clearfix">
                <dt class="a_row_a">Cena</dt>
                {if $ad.auction eq 1}
                  {if $adAuction.current_bid >= $ad.price}
                    <dd class="a_row_a" alt="{$adAuction.current_bid} zł {$ad.price_str}" title="{$adAuction.current_bid} zł {$ad.price_str}">{$adAuction.current_bid} zł {$ad.price_str}</dd>
                  {else}
                    <dd class="a_row_a" alt="{$ad.price} zł {$ad.price_str}" title="{$ad.price} zł {$ad.price_str}">{$ad.price} zł {$ad.price_str}</dd>
                  {/if}
                {else}
                  <dd class="a_row_a" alt="{$ad.price} zł {$ad.price_str}" title="{$ad.price} zł {$ad.price_str}">{$ad.price} zł {$ad.price_str}</dd>
                {/if}
                <dt class="a_row_b">Województwo</dt>
                <dd class="a_row_b">{$ad.region_str}</dd>
                <dt class="a_row_a">Miejscowość</dt>
                <dd class="a_row_a">{$ad.city}</dd>
                <dt class="a_row_b">Dodano</dt>
                <dd class="a_row_b">{$ad.created}</dd>
                <dt class="a_row_a">Wygasa</dt>
                <dd class="a_row_a">{$ad.expires}</dd>
                {if $ad.city_district}
                  <dt class="a_row_b">Dzielnica</dt>
                  <dd class="a_row_b">{$ad.city_district}</dd>
                {/if}
                {foreach from=$adAttribute item=attr}
                  {if $attr.hide !=1 }
                    
                      {cycle values='a_row_a,a_row_b' assign=cell} 
                      <dt class="{$cell}">{$attr.name}</dt>
                      <dd class="{$cell}">{$attr.value}</dd>

                  {/if}
                {/foreach}
            </dl>
        </div>
     </div>
            <div class="span-19 last a_offer">
                <div class="span-14 a_offer_body">
                    <p class="a_offerTitle">Opis oferty</p>
                    <hr class="space">
                                                
                        <div class="justif">
                        {$ad.text}
                        </div>
                        <hr class="space">
                        <hr class="space">
                        <hr class="space">
                        <p class="a_offerTitle">Lokalizacja</p>
                        <p>{$ad.region_str} / {$ad.city}
                        {if $ad.city_district}
                          / {$ad.city_district}
                        {/if}
                        </p>
                        <div class="a_map">
                            <div id="map_canvas" style="width: 503px; height: 303px; position: relative; overflow: hidden; -webkit-transform: translateZ(0px); background-color: rgb(229, 227, 223);">
							<div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;">
							<div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(http://maps.gstatic.com/mapfiles/openhand_8_8.cur) 8 8, default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1;"><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: -115px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: 141px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: -115px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: 141px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: -115px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: 141px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: -115px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: 141px;"></div></div></div></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1;"><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: -115px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: 141px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: -115px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: 141px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: -115px;"></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: 141px;"></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: -115px;"></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: 141px;"></div></div></div></div><div style="position: absolute; z-index: 0; left: 0px; top: 0px;"><div style="overflow: hidden; width: 503px; height: 303px;"><img src="./Wynajmij Wynajmę pokój_files/StaticMapService.GetMapImage" style="width: 503px; height: 303px;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1;"><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: 141px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: -115px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(1)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: -115px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(2)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: 141px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(3)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: -115px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(4)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: -115px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(5)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: 141px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(6)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: 141px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(7)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="http://maps.google.com/maps?ll=54.194322,16.171491&z=15&t=m&hl=pl-PL&gl=US&mapclient=apiv3" title="Kliknij, aby wyświetlić ten obszar w serwisie Mapy Google" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 62px; height: 26px; cursor: pointer;"><img src="./Wynajmij Wynajmę pokój_files/google_white2.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 62px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 157px; bottom: 0px; width: 144px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Dane mapy</a><span>Dane do Mapy ©2014 Google</span></div></div></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); -webkit-box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 102px; top: 62px; background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Dane mapy</div><div style="font-size: 13px;">Dane do Mapy ©2014 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="./Wynajmij Wynajmę pokój_files/mapcnt3.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Dane do Mapy ©2014 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; position: absolute; -webkit-user-select: none; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a href="http://www.google.com/intl/pl-PL_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Warunki korzystania z programu</a></div></div><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; display: none; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a target="_new" title="Zgłoś do Google błędy na mapie drogowej lub na zdjęciach." href="http://maps.google.com/maps?ll=54.194322,16.171491&z=15&t=m&hl=pl-PL&gl=US&mapclient=apiv3&skstate=action:mps_dialog$apiref:1&output=classic" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Zgłoś błąd w mapach</a></div></div><div class="gmnoprint" draggable="false" controlwidth="32" controlheight="84" style="margin: 5px; -webkit-user-select: none; position: absolute; left: 0px; top: 0px;"><div controlwidth="32" controlheight="40" style="cursor: url(http://maps.gstatic.com/mapfiles/openhand_8_8.cur) 8 8, default; position: absolute; left: 0px; top: 0px;"><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img src="./Wynajmij Wynajmę pokój_files/cb_scout2.png" draggable="false" style="position: absolute; left: -9px; top: -102px; width: 1028px; height: 214px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="./Wynajmij Wynajmę pokój_files/cb_scout2.png" draggable="false" style="position: absolute; left: -107px; top: -102px; width: 1028px; height: 214px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="./Wynajmij Wynajmę pokój_files/cb_scout2.png" draggable="false" style="position: absolute; left: -58px; top: -102px; width: 1028px; height: 214px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="./Wynajmij Wynajmę pokój_files/cb_scout2.png" draggable="false" style="position: absolute; left: -205px; top: -102px; width: 1028px; height: 214px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></div><div class="gmnoprint" controlwidth="0" controlheight="0" style="opacity: 0.6; display: none; position: absolute;"><div title="Obróć mapę o 90 stopni" style="width: 22px; height: 22px; overflow: hidden; position: absolute; cursor: pointer;"><img src="./Wynajmij Wynajmę pokój_files/mapcnt3.png" draggable="false" style="position: absolute; left: -38px; top: -360px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></div><div class="gmnoprint" controlwidth="20" controlheight="39" style="position: absolute; left: 6px; top: 45px;"><div style="width: 20px; height: 39px; overflow: hidden; position: absolute;"><img src="./Wynajmij Wynajmę pokój_files/mapcnt3.png" draggable="false" style="position: absolute; left: -39px; top: -401px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div title="Powiększ" style="position: absolute; left: 0px; top: 2px; width: 20px; height: 17px; cursor: pointer;"></div><div title="Pomniejsz" style="position: absolute; left: 0px; top: 19px; width: 20px; height: 17px; cursor: pointer;"></div></div></div><div class="gmnoprint" style="margin: 5px; z-index: 0; position: absolute; cursor: pointer; right: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Pokaż mapę ulic" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 1px 6px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; border: 1px solid rgba(0, 0, 0, 0.14902); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 30px; font-weight: 500; background-color: rgb(255, 255, 255); background-clip: padding-box;">Mapa</div><div style="z-index: -1; padding-top: 2px; -webkit-background-clip: padding-box; border-width: 0px 1px 1px; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: rgba(0, 0, 0, 0.14902); border-bottom-color: rgba(0, 0, 0, 0.14902); border-left-color: rgba(0, 0, 0, 0.14902); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 20px; text-align: left; display: none; background-color: white; background-clip: padding-box;"><div draggable="false" title="Pokaż mapę ulic z terenem" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-top-left-radius: 1px; border-top-right-radius: 1px; border-bottom-right-radius: 1px; border-bottom-left-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="./Wynajmij Wynajmę pokój_files/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Teren</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Pokaż zdjęcia satelitarne" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 1px 6px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; border-width: 1px 1px 1px 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-top-color: rgba(0, 0, 0, 0.14902); border-right-color: rgba(0, 0, 0, 0.14902); border-bottom-color: rgba(0, 0, 0, 0.14902); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 38px; background-color: rgb(255, 255, 255); background-clip: padding-box;">Satelita</div><div style="z-index: -1; padding-top: 2px; -webkit-background-clip: padding-box; border-width: 0px 1px 1px; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: rgba(0, 0, 0, 0.14902); border-bottom-color: rgba(0, 0, 0, 0.14902); border-left-color: rgba(0, 0, 0, 0.14902); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 20px; text-align: left; display: none; background-color: white; background-clip: padding-box;"><div draggable="false" title="Powiększ, aby wyświetlić widok pod kątem 45 stopni" style="color: rgb(184, 184, 184); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap; display: none; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(241, 241, 241); border-top-left-radius: 1px; border-top-right-radius: 1px; border-bottom-right-radius: 1px; border-bottom-left-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="./Wynajmij Wynajmę pokój_files/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">45°</label></div><div draggable="false" title="Pokaż zdjęcia z nazwami ulic" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-top-left-radius: 1px; border-top-right-radius: 1px; border-bottom-right-radius: 1px; border-bottom-left-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="./Wynajmij Wynajmę pokój_files/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Etykiety</label></div></div></div></div></div></div>
<script type="text/javascript" src="js/js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
    var geocoder;
    var map;
    {if $adAttribute.attr_street }
      var address = 'Polska, {$ad.region_str}, {$ad.city}, {$adAttribute.attr_street.value}';
    {else}
      var address = 'Polska, {$ad.region_str}, {$ad.city},';
    {/if}
    function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(52.40828351070693, 16.933321952819824);
        var myOptions = {
          zoom: 15,
          center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
      }
        
    $(document).ready(function() {
    	initialize();
    	    geocoder.geocode( { 'address': address}, function(results, status) {
    	      if (status == google.maps.GeocoderStatus.OK) {
    	        map.setCenter(results[0].geometry.location);
    	        var marker = new google.maps.Marker({
    	            map: map, 
    	            position: results[0].geometry.location
    	        });
    	      }
    	    });
    });
    </script>
                        </div>
                </div>
    <div class="span-5 last a_offer_contact">
        <p class="a_offerAttrTitle">Kontakt</p>
        {if $ad.status eq 2}
          oferta wygasła
        {else}
          <div class="a_offerTel"><small>tel.</small> {$ad.phone_number|substr:0:3}<span class="tnum">pokaż numer</span></div>
          <div class="a_offerTel2"><small>wyświetleń</small> {$ad.phone_views}</div>
        {/if}
        <hr class="space">
        {if $error}
          {foreach from=$error item=err}
            <div class="error">{$err.0}</div>
          {/foreach}
        {/if}
        {if $notice}
          {foreach from=$notice item=ntc}
            <div class="success">{$ntc.0}</div>
          {/foreach}
        {/if}
        {if $ad.status eq 2}
        {else}
        <form method="post" class="np" action="ads/contact" id="formularz-kontaktowy">
        <input type="hidden" name="aid" value="{$ad.aid}">
        <input type="hidden" name="token" value="{$token}">
            <label for="zapytanie" class="text-blue">Zadaj pytanie:</label>
                                    <div>
                                        <textarea id="zapytanie" name="zapytanie"></textarea>
                                    </div>
                                                                        <label for="zapytanieemail" class="text-blue">Twój E-mail</label>
                                    <div>
                                        <input id="zapytanieemail" name="zapytanieemail" style="" type="text" class="a_zapytanieText" value="">
                                    </div>
                                                                        <label for="zapytanietel" class="text-blue">Twój numer telefonu:</label>
                                    <div>
                                        <input id="zapytanietel" name="zapytanietel" style="" type="text" class="a_zapytanieText" value="">
                                    </div>
                                    
                                    <div class="text-right"><input type="submit" value="Wyślij zapytanie"></div>
                                    
                                </form>
          {/if}    
                                <ul class="a_offerMenu clearfix">
                                <li><a class="a_ico a_printIco" rel="nofollow" id="printbutton" href="{router->linkTo module=ads action=printad id=$ad.aid}">Drukuj ogłoszenie</a></li>
                                <li><a class="a_ico a_abuseIco" rel="nofollow" href="#">Zgłoś nadużycie</a></li>
                                {if $ad.status eq 1}
                                <li><a class="a_ico a_watchlistIco" href="{router->linkTo module=ads action=watchad id=$ad.aid}">
                                {if $isWatched eq "no"}
                                Dodaj do obserwowanych
                                {else}
                                Usuń z obserwowanych
                                {/if}
                                </a></li>
                                {/if}
                                                                </ul>
    </div>

                <div class="clearfix"></div>

                </div>
            </div>
<div class="clearfix"></div>

{if ($ad.auction eq 1) }
  {if $auction_id == 0}
    <div class="span-19 last a_offer">
      <h2 class="span-15">Aukcja:</h2>
      <div class="clearfix"></div>
    Aukcja dla tego ogłoszenia jeszcze się nie rozpoczęła.
    </div>
  {else}
    <div class="span-19 last a_offer">
      <h2 class="span-15">Aukcja:</h2>
      <div class="clearfix"></div>
      {if $adAuction.min_price > 0}
        {if $adAuction.current_bid < $adAuction.min_price}
          cena minimalna nie została osiągnięta<br/>
        {/if}
      {/if}
      {if $user.role == 0 }
        <form action="{router->linkTo module=ads action=showad id=$ad.aid link=$ad.link}" method="post">
          <input type="hidden" name="ad_id" value="{$ad.aid}">
          <input type="hidden" name="ad_name" value="{$ad.title}">
          <input type="hidden" name="ad_link" value="{$ad.link}">
        <table border="0" width="100%" cellspacing="0" cellpadding="3">
          <tr>
            <td width="15%"><b>Aktualna cena:</b></td>
            <td width="15%">{if $adAuction.current_bid eq 0} {$adAuction.start_price} {else} {$adAuction.current_bid} {/if} zł</td>
            <td width="15%"><b>Twoja oferta:</b></td>
            <td width="15%"><input type="text" name="bid" size="5" value="{$adAuctionNextBid}"> zł</td>
            <td width="40%"><input type="submit" value="Licytuj" disabled> Musisz być zalogowany aby licytować.
          </tr>
        </table>
      {else}
        <form action="user/prebid/" method="post">
          <input type="hidden" name="ad_id" value="{$ad.aid}">
          <input type="hidden" name="ad_name" value="{$ad.title}">
          <input type="hidden" name="ad_link" value="{$ad.link}">
        <table border="0" width="100%" cellspacing="0" cellpadding="3">
          <tr>
            <td width="15%"><b>Aktualna cena:</b></td>
            <td width="15%">{if $adAuction.current_bid eq 0} {$adAuction.start_price} {else} {$adAuction.current_bid} {/if} zł</td>
            <td width="15%"><b>Twoja oferta:</b></td>
            <td width="15%"><input type="text" name="bid" size="5" value="{$adAuctionNextBid}"> zł</td>
            <td width="40%"><input type="submit" value="Licytuj">
          </tr>
        </table>
        </form>
      {/if}
      <table border=0 width=100% cellspacing=0 cellpadding=3>
      <tr>
        <td width="50%"><b>Licytujący</b></td>
        <td width="20%"><b>Oferta</b></td>
        <td width="30%"><b>Data</b></td>
      </tr>
      {if $adAuction.current_bid_id > 0}
        <tr>
        {if $adAuction.current_bid >= $adAuction.min_price}
          <td width="50%"><b>{$adAuctionWinner.email}</b> {if $user.role eq "99"} /{$adAuctionWinner.bidder_email}/{/if}</td>
        {else}
          <td width="50%">{$adAuctionWinner.email} {if $user.role eq "99"} /{$adAuctionWinner.bidder_email}/{/if}</td>
        {/if}
        <td width="20%">
            <b>{$adAuction.current_bid} zł</b> {if $user.role eq "99"} /{$adAuctionWinner.max_bid} zł/{/if}
        </td>
        <td width="30%">{$adAuctionWinner.bid_date}</td>
      </tr>
      {/if}
      {foreach $adBids as $key => $value}
      {if $value.id neq $adAuction.current_bid_id}
        {if $value.max_bid <= $adAuction.current_bid}
          <tr>
            <td width="50%">{$value.email} {if $user.role eq "99"} /{$value.bidder_email}/{/if}</td>
            <td width="20%">
                {$value.max_bid} zł
            </td>
            <td width="30%">{$value.bid_date}</td>
          </tr>
         {/if}
      {/if}
      {/foreach}
        </table>
    </div>
  {/if}
{/if}
