<div class="span-6">
{include file="account_menu.tpl"}

</div>
<div class="span-18 last"><div class="span-18 last">
<h1>{$title}</h1>

<div class="clearfix row-header">
  <div class="span-2">
    <div>Numer ogłoszenia</div>
  </div>
	<div class="span-14">
		<div>Tytuł</div>
	</div>	
	<div class="span-2 last">
		<div>Opcje</div>
	</div>
</div>
{foreach from=$ad_list item=ad}
{cycle values='row-a,row-b' assign=cell} 
<div class="clearfix row {$cell}">
  <div class="span-2">
    <div><center>{$ad.aid}</center></div>
  </div>
	<div class="span-14">

		<a href="{router->linkTo module=ads action=showad id=$ad.aid link=$ad.link}">{$ad.title}</a>	
	</div>
	
	<div class="span-2 last">
      <a href="{router->linkTo module=ads action=watchad id=$ad.aid}" onclick="return confirm('Napewno usunąć ogłoszenie z obserwowanych?');"><img src="themes/nawynajmij/obraz/icon-delete.gif" alt="Usuń z obserwowanych" title="Usuń z obserwowanych"/></a>
	</div>
</div>
{/foreach}

</div></div><div class="clearfix"></div>
