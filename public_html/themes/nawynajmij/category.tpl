<div class="catWrap clearfix">
    <div class="catMenuWrap span-5">
		{include file="menucat.tpl"}     	
	 </div>
    <div class="catListWrap span-19 last">
    <h1>{$categoryName}</h1>
        {if $error}
          {foreach from=$error item=err}
            <div class="error">{$err.0}</div>
          {/foreach}
        {/if}
        {if $notice}
          {foreach from=$notice item=ntc}
            <div class="success">{$ntc.0}</div>
          {/foreach}
        {/if}
    <div class="filter">
            <div id="filterHead" class="hide">
                <div id="filterHeadInfo">
                </div>
            <a href="#" class="changeFilter">Zmień filtrowanie i sortowanie</a>
                
            </div>
              <form method="post" action="{router->linkTo module=category action=filter}">
              <input type="hidden" name="categoryId" value="{$categoryId}">
              <input type="hidden" name="auction" value="{$auctionParam}">
                <div id="filterCnt" class="">
                    <h3>Filtruj ogłoszenia</h3>
                    <div class="clearfix">
                        <div class="span-3 colborder">
                            <label for="regionId">Województwo</label>
                            <select style="width:110px;" name="regionId" id="regionId">
                                <option>wszystkie</option>
                                <option value="1"{$filterSelect1}>dolnośląskie</option>
                                <option value="2"{$filterSelect2}>kujawsko-pomorskie</option>
                                <option value="3"{$filterSelect3}>lubelskie</option>
                                <option value="4"{$filterSelect4}>lubuskie</option>
                                <option value="5"{$filterSelect5}>łódzkie</option>
                                <option value="6"{$filterSelect6}>małopolskie</option>
                                <option value="7"{$filterSelect7}>mazowieckie</option>
                                <option value="8"{$filterSelect8}>opolskie</option>
                                <option value="9"{$filterSelect9}>podkarpackie</option>
                                <option value="10"{$filterSelect10}>podlaskie</option>
                                <option value="11"{$filterSelect11}>pomorskie</option>
                                <option value="12"{$filterSelect12}>śląskie</option>
                                <option value="13"{$filterSelect13}>świętokrzyskie</option>
                                <option value="14"{$filterSelect14}>warmińsko-mazurskie</option>
                                <option value="15"{$filterSelect15}>wielkopolskie</option>
                                <option value="16"{$filterSelect16}>zachodniopomorskie</option>
                             </select>
                        </div>

                        <div class="span-4 colborder">
                            <label for="city">Miasto</label>
                            <input style="width:150px;" class="text" value="{$filterCity}" type="text" name="city" id="city">
                        </div>

                        <div class="span-3 colborder">
                            <label for="cityRegion">Dzielnica</label>
                            <input style="width:110px;" class="text" value="{$filterCityDistrict}" type="text" name="cityDistrict" id="cityDistrict">
                        </div>
                        <div class="span-4 last">
                            <label for="priceFrom">Cena od/do</label>
                            <div>
                                <input style="width:60px;" class="text" value="{$filterPriceMin}" type="text" name="priceMin" id="priceMin">
                                <input style="width:60px;" class="text" value="{$filterPriceMax}" type="text" name="priceMax" id="priceMax">
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div>
                                                    <label for="order">Sortuj oferty po</label>
                            <div>
                                <select id="order" name="order">
                                    <option value="0">Dacie malejąco</option>
                                    <option value="1">Dacie rosnąco</option>
                                    <option value="2">Cenie malejąco</option>
                                    <option value="3">Cenie rosnąco</option>
                                    <option value="4">Tytule Z do A</option>
                                    <option value="5">Tytule A do Z</option>

                                </select>
                            </div>
                                            </div>

                    <input type="submit" value="Filtruj ogłoszenia"></form>
                    <form action="{$site_url}category/show/cid/{$categoryId}/" method="get"><input type="submit" value="wyczyść filtrowanie"></form>
                </div>
        </div>
		<div class=" positioned">
		<div class="pager">
      {if $currentPage neq 1 && $currentPage neq 0}
        <a class="prev" href="{router->linkTo module=category action=show cid={$categoryId} page={$prevPage}}">poprzednia strona</a>
      {/if}
      {if $currentPage lt $totalPages}
        <a class="next" href="{router->linkTo module=category action=show cid={$categoryId} page={$nextPage}}">następna strona</a>
      {/if}
		</div>
      {if $positionedAds}
      {foreach from=$positionedAds item=ad}
      	{if $ad.highlighted eq 1}
      	<div class="a_adoffer clearfix highlighted">
      	{else}
      	<div class="a_adoffer clearfix">
      	{/if}
  				<div class="a_thumb span-5">
      			<a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}"><img src="{$ad.photo}" width="176px"></a>
  				</div>
 			<div class="span-10">
 				{if $ad.highlighted eq 1}
        		<div class="a_title_bold"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">{$ad.title}</a>
        		{else}
        		<div class="a_title"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">{$ad.title}</a>
        		{/if}
        		<div class="a_titleAddress">Adres: {$ad.city}</div>
        		</div>

    			<div class="a_shortDesc span-10">
             {$ad.text_raw}
    			</div>
 			 </div>
  			<div class="span-3 last a_offer_right">
  			{if $ad.auction eq 1}
          {if $ad.auction_array.current_bid >= $ad.price}
            <div class="a_price"><span>{$ad.auction_array.current_bid}</span> zł</div>
          {else}
            <div class="a_price"><span>{$ad.price}</span> zł</div>
          {/if}
        {else}
          <div class="a_price"><span>{$ad.price}</span> zł</div>
        {/if}
    			<div class="a_offerAttr a_priceType"><span>{$ad.price_type_str}</span></div>
  			</div>
  			<a class="a_offerMore" href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">zobacz ofertę</a>
  			<div class="a_adCreatedAt">
    			Dodano: <span title="{$ad.created_datetime}">{$ad.created_date}</span>
 			</div>
			</div>
		{/foreach}   
      {/if}
      
      {if $normalAds}
      {foreach from=$normalAds item=ad}
      	{if $ad.highlighted eq 1}
      	<div class="a_adoffer clearfix highlighted">
      	{else}
      	<div class="a_adoffer clearfix">
      	{/if}
  				<div class="a_thumb span-5">
      			<a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}"><img src="{$ad.photo}" width="176px"></a>
  				</div>
 			<div class="span-10">
 				{if $ad.highlighted eq 1}
        		<div class="a_title_bold"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">{$ad.title}</a>
        		{else}
        		<div class="a_title"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">{$ad.title}</a>
        		{/if}
        		<div class="a_titleAddress">Adres: {$ad.city}</div>
        		</div>

    			<div class="a_shortDesc span-10">
             {$ad.text}
    			</div>
 			 </div>
  			<div class="span-3 last a_offer_right">
    			<div class="a_price"><span>{$ad.price}</span> zł</div>
    			<div class="a_offerAttr a_priceType"><span>{$ad.price_type_str}</span></div>
  			</div>
  			<a class="a_offerMore" href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}">zobacz ofertę</a>
  			<div class="a_adCreatedAt">
    			Dodano: <span title="{$ad.created_datetime}">{$ad.created_date}</span>
 			</div>
			</div>
		{/foreach}   
      {/if}                    
      </div>
    </div>


</div><div class="clearfix"></div>
