<div class="span-6">
{include file="account_menu.tpl"}

</div>
<div class="span-18 last"><div class="span-18 last">
<h1>{$title}</h1>
{if $error}
  {foreach from=$error item=err}
    <div class="error">{$err.0}</div>
  {/foreach}
{/if}
{if $notice}
  {foreach from=$notice item=ntc}
    <div class="success">{$ntc.0}</div>
  {/foreach}
{/if}
			<form action="{$site_url}user/updatePassword" method="post">
			<input type="hidden" name="token" value="{$token}">
			<table class="table">
					<tbody>
						<tr class="success">
							<td width="40%"><b>Hasło</b></td>
							<td width="60%"></td>
						</tr>
						<tr>
							<td width="40%">Obecne hasło:</td>
							<td width="60%"><input type="password" name="current_pass" size="50"></td>
						</tr>
						<tr>
							<td width="40%">Nowe hasło:</td>
							<td width="60%"><input type="password" name="new_pass" size="50"></td>
						</tr>
						<tr>
							<td width="40%">Powtórz nowe hasło:</td>
							<td width="60%"><input type="password" name="new_pass2" size="50"></td>
						</tr>
						<tr class="active">
							<td width="40%"><input type="submit" value="zmień hasło"></td>
							<td width="60%"></td>
						</tr>
					</tbody>
			</table>
			</form>

</div></div><div class="clearfix"></div>