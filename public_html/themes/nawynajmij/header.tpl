<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<base href="{$CNF_SITE_URL}">
<title>{$title|default:$breadcrumbsTitle|default:$wdname}</title>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oxygen:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="themes/nawynajmij/css/screen.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="themes/nawynajmij/css/lightbox.css" type="text/css">


<!--[if lt IE 8]>
<link rel="stylesheet" href="themes/nawynajmij/css/blueprint/ie.css" type="text/css" media="screen, projection">
<![endif]-->
<link rel="stylesheet" href="themes/nawynajmij/css/nw.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="themes/nawynajmij/css/slider.css" type="text/css" media="screen, projection">
<link rel="stylesheet" href="themes/nawynajmij/css/print.css" type="text/css" media="print">
<meta name="robots" content="INDEX,FOLLOW">
<script type="text/javascript" async="" src="themes/nawynajmij/js/ga.js"></script>
<script type="text/javascript" src="themes/nawynajmij/js/jquery.js"></script>
<script type="text/javascript" src="themes/nawynajmij/js/slides.min.jquery.js"></script>
<script type="text/javascript" src="themes/nawynajmij/js/lightbox.min.js"></script>
<script type="text/javascript" src="themes/nawynajmij/js/nw.js"></script>
<script type="text/javascript" src="themes/nawynajmij/js/cookie.js"></script>
<script type="text/javascript">

jQuery(document).ready(function(){
	jQuery.fn.cookiesEU({
    text: 'Używamy informacji zapisanych za pomocą plików cookies w celu zapewnienia maksymalnej wygody w korzystaniu z naszego serwisu. Mogą też korzystać z nich współpracujące z nami firmy badawcze oraz reklamowe. Jeżeli wyrażasz zgodę na zapisywanie informacji zawartej w cookies kliknij na „zamknij” w prawym górnym rogu tej informacji. Jeśli nie wyrażasz zgody, ustawienia dotyczące plików cookies możesz zmienić w swojej przeglądarce.',
		close:		'Zamknij'
	});  
});

</script>
{if $addAd eq true}
<script type="text/javascript" src="themes/nawynajmij/js/addad.js"></script>
<script src="themes/nawynajmij/js/jquery.form.js"></script>
<link rel="stylesheet" type="text/css" href="themes/nawynajmij/cleditor/jquery.cleditor.css" />
<script type="text/javascript" src="themes/nawynajmij/cleditor/jquery.cleditor.min.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
            $("#adText").cleditor({
                width: 400, // width not including margins, borders or padding
                height: 250, // height not including margins, borders or padding
                controls: // controls to add to the toolbar
                    "bold italic underline | " +
                    "color removeformat | undo redo | ",
                   
                colors: // colors in the color popup
                    "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                    "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                    "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                    "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                    "666 900 C60 C93 990 090 399 33F 60C 939 " +
                    "333 600 930 963 660 060 366 009 339 636 " +
                    "000 300 630 633 330 030 033 006 309 303",
                fonts: // font names in the font popup
                    "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
                    "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
                sizes: // sizes in the font size popup
                    "1,2,3,4,5,6,7",
                styles: // styles in the style popup
                    [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
                    ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
                    ["Header 6","<h6>"]],
                useCSS: false, // use CSS to style HTML when possible (not supported in ie)
                docType: // Document type contained within the editor
                    '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
                docCSSFile: // CSS file used to style the document contained within the editor
                    "",
                bodyStyle: // style to assign to document body contained within the editor
                    "margin:4px; font:10pt Arial,Verdana; cursor:text"
            });
        });
    </script>
<!--
<link rel="stylesheet" href="themes/nawynajmij/tinyeditor/tinyeditor.css">
<script src="themes/nawynajmij/tinyeditor/tiny.editor.packed.js"></script>
-->
{/if}



<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-23153593-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();
</script>

<script type="text/javascript">
    $(function() {
        $("#slides").slides({
            preload: true,
            preloadImage: 'themes/nawynajmij/obraz/loading.gif',
            play: 5000,
            pause: 2500,
            hoverPause: true
        });
    });
</script>
{if $offerDetail}
<script type="text/javascript">
  $(function() {
    $('.a_offerTel').click(function() {
      
      $.ajax({
        dataType: "json",
        url: "ads/phone/id/{$offerDetail}",
        
      }).done(function( data ) {
        $('.a_offerTel').html('<small>tel.</small> ' +data.phone_number );
      });
         
    });
  });
</script>
{/if}
{if $print}
<script type="text/javascript">
<!--
window.print();
//-->
</script>
{/if}
</head>
 