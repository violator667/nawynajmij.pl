<div class="span-6">
{include file="account_menu.tpl"}
</div>
<div class="span-18 last"><div class="span-10">
<h1>Dodaj ogłoszenie</h1>
{if $error}
  {foreach from=$error item=err}
    <div class="error">{$err.0}</div>
  {/foreach}
{/if}
{if $notice}
  {foreach from=$notice item=ntc}
    <div class="success">{$ntc.0}</div>
  {/foreach}
{/if}
<form lang="pl" accept-charset="UTF-8" action="{router->linkTo module=ads action=add}" method="post" id="form-ogloszeniaDodaj">
	
<input type="hidden" name="draftid" value="{$draftId}" id="draftid">
<input type="hidden" name="selectedCategory" value="{$selectedCategory}" id="selectedCategory">
<input type="hidden" name="selectedCategoryName" value="{$selectedCategoryName}" id="selectedCategoryName">
<input type="hidden" name="formPhoto1" value="{$formPhoto1}" id="formPhoto1">
<input type="hidden" name="formPhoto2" value="{$formPhoto2}" id="formPhoto2">
<input type="hidden" name="formPhoto3" value="{$formPhoto3}" id="formPhoto3">
<input type="hidden" name="formPhoto4" value="{$formPhoto4}" id="formPhoto4">
<input type="hidden" name="formPhoto5" value="{$formPhoto5}" id="formPhoto5">
{foreach from=$selectedSubCat item=selSub}
  {$count = $selSub@iteration}
  <input type="hidden" id="selCat{$count}" value="{$selSub}">
{/foreach}
<div id="draft-143" class="draftWrap">
<div class="error" id="formerror">Musisz wypełnić wszystkie pola oznaczone na czerwono</div>
<span class="toolbox">
	 <a href="{router->linkTo module=ads action=preview id=$draftId}" target="podglad" id="preview"><img title="Podgląd ogłoszenia" alt="Podgląd" src="themes/nawynajmij/obraz/icon-search.gif"> Podgląd </a>
</span>
<span class="toolbox">	 
	  <a onclick="return confirm('Czy na pewno wyczyścić formularz?');" href="{router->linkTo module=user action=draft delete=$draftId}"><img title="Wyczyść formularz ogłoszenia" alt="Usuń" src="themes/nawynajmij/obraz/icon-delete.gif">Wyczyść</a>
</span>
	<div>
		<label for="catId">Wybierz kategorię <span class="required">*</span></label>
		<div class="error_hidden" id="catId_err">Musisz wybrać kategorię</div>
	</div>
	<div class="selectCat" id="seleCatFirst">
	</div>
	<div class="selectCat">
				<select name="selectCategory1" id="selectCategory1" class="catSel">
				</select>
	</div>
	<div class="selectCat">
				<select name="selectCategory2" id="selectCategory2"  class="catSel">
				</select>
	</div>
	<div class="selectCat">
				<select name="selectCategory3" id="selectCategory3"  class="catSel">
				</select>
	</div>
	<div class="selectCat">
				<select name="selectCategory4" id="selectCategory4"  class="catSel">
				</select>
	</div>
	<div class="selectCat">
				<select name="selectCategory5" id="selectCategory5"  class="catSel">
				</select>
	</div>
	<div class="selectCat">
				<select name="selectCategory6" id="selectCategory6" class="catSel">
				</select>
	</div>

	<div>
		<label for="regionId">Województwo <span class="required">*</span></label>
		<div class="error_hidden" id="regionId_err">Musisz wybrać województwo</div>
	</div>
	<div>
	
		<select name="regionId" id="regionId" style="width:300px;">
		<option></option>
				<option value="1"{$regionId1}>dolnośląskie</option>
				<option value="2"{$regionId2}>kujawsko-pomorskie</option>
				<option value="3"{$regionId3}>lubelskie</option>
				<option value="4"{$regionId4}>lubuskie</option>
				<option value="5"{$regionId5}>łódzkie</option>
				<option value="6"{$regionId6}>małopolskie</option>
				<option value="7"{$regionId7}>mazowieckie</option>
				<option value="8"{$regionId8}>opolskie</option>
				<option value="9"{$regionId9}>podkarpackie</option>
				<option value="10"{$regionId10}>podlaskie</option>
				<option value="11"{$regionId11}>pomorskie</option>
				<option value="12"{$regionId12}>śląskie</option>
				<option value="13"{$regionId13}>świętokrzyskie</option>
				<option value="14"{$regionId14}>warmińsko-mazurskie</option>
				<option value="15"{$regionId15}>wielkopolskie</option>
				<option value="16"{$regionId16}>zachodniopomorskie</option>
				</select>
		
	</div>
		
	
	<div>
		<label for="city">Miasto <span class="required">*</span></label>
		<div class="error_hidden" id="city_err">Musisz wskazać miasto</div>
	</div>
	<div>
				<input class="text" value="{$city}" type="text" name="city" id="city" >
	</div>

	
	<div>
		<label for="cityRegion">Dzielnica</label>
	</div>
	<div>
				<input class="text" value="{$cityDistrict}" type="text" name="cityDistrict" id="cityDistrict">
	</div>
		<div>
		<label for="tel">Telefon kontaktowy<span class="required">*</span></label>
		<div class="error_hidden" id="tel_err">Musisz podać telefon</div>
	</div>
	<div>
				<input class="text" value="{$tel}" type="text" name="tel" id="tel">
	</div>
		<div>
		<label for="contactEmail">E-mail<span class="required">*</span></label>
		<div class="error_hidden" id="contactEmail_err">Musisz podać prawidłowy email</div>
	</div>
	<div>
				<input class="text" value="{$contactEmail}" type="text" name="contactEmail" id="contactEmail">
	</div>
		
	<div>
		<label for="price">Cena w zł <span class="required">*</span></label>
		<div class="error_hidden" id="price_err">Musisz podać cenę</div>
	</div>
	<div class="">
				<input style="width:90px;text-align:right;" class="text" value="{$price}" type="text" name="price" id="price">
		
			</div>
	
	<div>
		<label for="priceType">Cena za <span class="required">*</span></label>
		<div class="error_hidden" id="priceType_err">Musisz wybrać</div>
	</div>
	<div>
	
		<select name="priceType" id="priceType" style="width:300px;">
				<option value="0"></option>
				<option value="1"{$priceSelect1}>godzinę</option>
				<option value="2"{$priceSelect2}>dzień</option>
				<option value="3"{$priceSelect3}>tydzień</option>
				<option value="4"{$priceSelect4}>miesiąc</option>
				<option value="5"{$priceSelect5}>kwartał</option>
				<option value="6"{$priceSelect6}>pół roku</option>
				<option value="7"{$priceSelect7}>rok</option>
				<option value="8"{$priceSelect8}>m2</option>
				<option value="9"{$priceSelect9}>całość</option>
				<option value="10{$priceSelect10}">ha</option>
				</select>
	</div>
	<div id="attrPlaceHolder"></div>	
	<div id="attrWrap">
		{foreach from=$attr_array item=attr}
		<input type="hidden" id="a_{$attr.aid}" value="{$attr.value}">
      <div class="attr">
        <label for="{$attr.aid}" class="attr">{$attr.name}
          {if $attr.required eq 1}
            <span class="required attr">*</span>
          {/if}
        </label>
        <div class="error_hidden" id="{$attr.aid}_err">To pole jest wymagane</div>
      </div>
      <div class="attr">
				<input class="attr" value="{$attr.value}" type="text" name="{$attr.aid}" id="{$attr.aid}">
      </div>
		{/foreach}
	</div>
	
	
	<div>
		<label for="ad">Treść ogłoszenia <span class="required">*</span></label>
		<div class="error_hidden" id="adText_err">Musisz podać treść ogłoszenia.</div>
	</div>
	<div>
				<textarea id="adText" name="adText">{$adText}</textarea>
</div>
	<h2>Dołącz zdjęcia</h2>
	<div>
	{include file="upload.tpl"}
	</div>
	
	
	<h2>Rodzaj ogłoszenia</h2>
	<p>Możesz bardziej uwidocznić swoje ogłoszenie zaznaczając poniżej opcję wyróżnienia lub promowania, albo obie jednocześnie. Opcje są dodatkowo płatne. Koszt wyróżnienia i promowania można sprawdzić w <a href="{router->linkTo module=cms action=showpage tid=cennik}">cenniku.</a></p>
	<div>
		<div class="span-1">
				<input type="checkbox" id="highlighted" name="highlighted" value="1"{$highlighted}>
		</div>
		<div class="span-9 last">
			<label for="highlighted">Wyróżnione kolorem</label>
			<p>Ogłoszenie, którego tytuł na liście ogłoszeń jest pogrubiony i podświetlony innym kolorem.</p>
		</div>
		<div class="span-1">
							<input type="checkbox" id="positioned" name="positioned" value="1"{$positioned}>
		
		</div>
		<div class="span-9 last">
			<label for="positioned">Promowane</label>
			<p>Ogłoszenie pozycjonowane na liście ogłoszeń i wyświetlane na stronie głównej serwisu Nawynajmij.pl. <span class="quiet">Wymagane 1 zdjęcie.</span></p>
			
		</div>
		<div class="span-1">
				<input type="checkbox" id="auction" name="auction" value="1"{$auction}>
		</div>
		<div class="span-9 last">
			<label for="highlighted">Aukcja</label>
			<p>Ogłoszenie wraz z którym uruchomiona zostaje aukcja.</p>
		</div>
			</div>
	
	
	
		
					
		<hr>
		<div style="padding-bottom:10px;">
			<input type="submit" value="Zapisz" id="save"><span id="saveInfo"></span>
			<span class="toolbox">
			<a onclick="return confirm('Czy na pewno chcesz usunąć szkic?');" href="{router->linkTo module=user action=draft delete=$draftId}">
				<img title="Usuń szkic" alt="Usuń" src="themes/nawynajmij/obraz/icon-delete.gif">
				Wyczyść formularz ogłoszenia
			</a></span>
		</div>
		<div>
			<input type="submit" name="nextstep" value="Zachowaj do opłacenia" id="publish">
					</div>
		
		
	
		
</div></div>

<div class="span-8 last">
	<h2>Bez limitu w abonamencie</h2>
	<p>W ofercie abonamentowej dodajesz ogłoszenia za darmo bez limitu oraz wybierasz długość wyświetlania ogłoszenia. Sprawdź szczegóły na <a href="/oferta-dla-firm.html">oferta abonamentowa</a>. </p>
	<p>
		
		<button style="margin-bottom:5px;width:300px;" onclick="window.location='/cennik.html';" class="button btn-c2a" title="Abonament" type="button"><span><span>Zobacz cennik ogłoszeń</span></span></button>
	</p>
	<h2>Autozapisywanie</h2>
	<p>Wszystko co wprowadzasz jest zapisywane w tle. Dzięki temu możesz w każdej chwili powrócić do wprowadzania ogłoszenia. Ostatni zapis: <span class="autoSaveInfo"></span></p>
	<h2>Jak pisać ogłoszenia?</h2>
	<p>
	</p><ul><li>Tytuł ogłoszenia zostanie wygenerowany automatycznie na podstawie wybranej kategorii.</li><li>Pola oznaczone czerwoną gwiazdką są obowiązkowe. Uzupełnij je odpowiednią treścią.</li><li>Podaj cenę wynajmu oferowanego przedmiotu lub usługi oraz wybierz właściwe kryterium dotyczące rozliczania (np. za dobę, za m2).</li><li>Treść ogłoszenia powinna zawierać informacje prawdziwe, dokładnie opisujące oferowany przedmiot pod wynajem lub usługę. Aby ogłoszenie było bardziej przejrzyste skorzystaj z edytora tekstu, wypunktuj i pogrub wybrane fragmenty, a każdą nową myśl zacznij od kolejnego akapitu. Dodaj zdjęcia, wtedy ogłoszenie będzie chętniej przeglądane. W trakcie pisania możesz skorzystać z podglądu ogłoszenia.</li><li>W celu umożliwienia kontaktu możesz podać swój numer telefonu.</li></ul>
	<p></p>
	<h2>Jak dodać zdjęcia?</h2>
	<p>Korzystając  z przycisku  "Wybierz zdjęcia" dodaj do ogłoszenia fotografie znajdujące się na Twoim komputerze. Maksymalnie można wstawić 5 zdjęć o wielkości nie przekraczające 1 MB każde. Zdjęcia muszą być zapisane w formacie .jpg, .png. W celu zmiany rozmiaru, rozdzielczości i formatu zdjęcia można posłużyć się programem IrfanView dostępnym za darmo w Internecie.</p>
</div></div>
<div class="clearfix"></div>
{if $validator_script}
<script type="text/javascript">
  {$validator_script}
</script>
{/if}