<div class="span-6">
{include file="account_menu.tpl"}

</div>
<div class="span-18 last"><div class="span-18 last">
<h1>Aukcja: {$ad_name}</h1>
{if $error}
  {foreach from=$error item=err}
    <div class="error">{$err.0}</div>
  {/foreach}
{/if}
{if $notice}
  {foreach from=$notice item=ntc}
    <div class="success">{$ntc.0}</div>
  {/foreach}
{/if}

{if $bid_error}			
<div class="error">{$bid_error}</div>
{/if}

{if $bid_info}			
<div class="success">{$bid_info}</div>
{/if}
</div></div><div class="clearfix"></div>