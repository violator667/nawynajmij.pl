<div class="span-6">
{include file="account_menu.tpl"}

</div>
<div class="span-18 last"><div class="span-18 last">
<h1>Ustawienia aukcji</h1>
{if $error}
  {foreach from=$error item=err}
    <div class="error">{$err.0}</div>
  {/foreach}
{/if}
{if $notice}
  {foreach from=$notice item=ntc}
    <div class="success">{$ntc.0}</div>
  {/foreach}
{/if}
      {if $auction_info }
        <div class="success">{$auction_info}</div>
      {/if}

			<form action="{$site_url}user/addauction/" method="post">
			<input type="hidden" name="token" value="{$token}">
			<input type="hidden" name="ad_id" value="{$ad_id}">
			<table class="table">
					<tbody>
						<tr class="success">
							<td width="40%"><b>Kwota startowa licytacji:</b></td>
							<td width="60%"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="start_price" size="30" value="{$start_price}"{$form_disabled}></td>
							<td width="60%">Od tej kwoty rozpocznie się licytacja.</td>
						</tr>
						<tr class="success">
							<td width="40%"><b>Kwota minimalna:</b></td>
							<td width="60%"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="min_price" size="30" value="{$min_price}"{$form_disabled}></td>
							<td width="60%">Pozostaw 0 aby wyłączyć kwotę minimalną.</td>
						</tr>
						<tr class="active">
							<td width="40%"><input type="submit" value="zapisz aukcję"{$form_disabled}></td>
							<td width="60%"></td>
						</tr>
					</tbody>
			</table>
			</form>
</div></div><div class="clearfix"></div>