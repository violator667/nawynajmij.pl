<!--
{$error}
<form action={$site_url}user/login method=post>
<input type=hidden name=login value=true>
<input type=hidden name=redirect value=test>
<input type=hidden name=token value="{$token}">
<input type=text name=email> <input type=password name=password> <input type=submit value="{$lang.ButtonLogIn}">
</form>
-->
{if $error}
          {foreach from=$error item=err}
            <div class="error">{$err.0}</div>
          {/foreach}
{/if}
{if $notice}
          {foreach from=$notice item=ntc}
            <div class="success">{$ntc.0}</div>
          {/foreach}
{/if}
<div class="span-12 a_login a_login_left">
    <h1>Zaloguj się</h1>
    <div class="a_loginContent">
        <div class="text-center"><img src="themes/nawynajmij/obraz/home_login.png"></div>
        <p>Mam już konto NaWynajmij.pl</p>

        <form action="{router->linkTo module=user action=login}" method="post" id="form-zaloguj">
        <input type=hidden name=login value=true>
        <input type=hidden name=redirect value=user/index>
        <input type=hidden name=token value="{$token}">
                        <div class="a_label">
                <label for="userLogin">Twój e-mail (Login)</label>
            </div>
            <div class="a_input">
                <input class="text" value="" type="text" name="email" id="userLogin">
            </div>
                        <div class="a_label">
                <label for="userPassword">Hasło</label>
            </div>
            <div class="a_input">
                <input class="text" type="password" name="password" id="userPassword">
            </div>
                        <input type="submit" value="Zaloguj się">
        </form>

                    <hr class="space"><a href="#" onclick="$('#recoverPass').slideDown();
                $(this).hide();
                return false;">Zapomniałem hasła</a>
            <div class="hide" id="recoverPass">
                <h3>Zapomniałeś hasła?</h3>
                <p>Jeśli zapomniałeś hasła do konta, wpisz swój adres e-mail:
                </p><form action="{$site_url}user/lostpassstart" method="post">
                    <input type="text" class="text" name="userLogin" value="">
                    <input type="submit" value="Przypomnij hasło">
                </form><p></p>
            </div>
                            </div>
</div>
<div class="span-12 last a_login a_login_right">
    <h1>Rejestracja</h1>
    <div class="a_loginContent">
        <div class="text-center"><img src="themes/nawynajmij/obraz/rejestruj.png"></div>
        
        <p>Jeśli nie posiadasz konta w serwisie <a href="#">NaWynajmij.pl</a> możesz je utworzyć poniżej podając adres e-mail jako login oraz hasło. Konto umożliwi Ci umieszczanie ofert wynajmu i usług w serwisie.</p>
        <p>Rejestracja jest darmowa. Zwróć uwagę, aby adres e-mail był poprawny. </p>
        <form action="{$site_url}user/register" method="post" id="form-rejestracja">
        <input type=hidden name=doRegister value=true>
        <input type=hidden name=redirect value=index>
        <input type=hidden name=token value="{$token}">

                        <div class="a_label">
                <label for="userLogin2">Twój e-mail (Login) <span class="required">*</span></label>
            </div>
            <div class="a_input">
                <input class="text" value="" type="text" name="email" id="userLogin2">
            </div>
            
            <div class="a_label">
                <label for="userPass">Hasło <span class="required">*</span></label>
            </div>
            <div class="a_input">
                <input class="text" type="password" name="password" id="userPass">
            </div>
            
            <div class="a_label">
                <label for="userPass2">Powtórz hasło <span class="required">*</span></label>
            </div>
            <div class="a_input">
                <input class="text" type="password" name="rep_password" id="userPass2">
            </div>
                        <div>
                <p class="text-blue">
                    <input id="akceptuje" type="checkbox" name="akceptuje" value="tak">
                    <label for="akceptuje">Oświadczam, że zapoznałem / zapoznałam się z treścią</label>&nbsp;<a href="http://www.nawynajmij.pl/regulamin.html">Regulaminu</a> <label for="akceptuje">i w pełni go akceptuję.</label> <span class="required">*</span>
                </p>
            </div>
                        <input type="submit" value="Zarejestruj się">
        </form>
    </div>
</div>
<div class="clearfix"></div><div class="clearfix"></div>