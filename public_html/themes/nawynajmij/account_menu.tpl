<h3>Menu</h3>
<ul class="menu konto">
	<li><a href="{router->linkTo module=user action=index}">Dodaj ogłoszenie</a></li>
	<li><a href="{router->linkTo module=user action=ads type=active}">Ogłoszenia wystawione ({$ads_num})</a></li>
	<li><a href="{router->linkTo module=user action=ads type=watched}">Obserwowane ogłoszenia ({$ads_num_watched})</a></li>
	<li><a href="{router->linkTo module=user action=ads type=topay}">Ogłoszenia do opłacenia ({$ads_num_to_pay})</a></li>
	<li><a href="{router->linkTo module=user action=ads type=ended}">Ogłoszenia zakończone ({$ads_num_ended})</a></li>
</ul>

{if $user.subscription eq 1}
<h3>Abonament</h3>
Pozostało ogłoszeń:<br/>
Standard: {$user.free_standard}<br/>
Wyróżnionych: {$user.free_highlighted}<br/>
Promowanych: {$user.free_positioned}<br/>
Mix: {$user.free_mix}<br/>
<br/>{/if}
<h3>Konto</h3>
<ul class="menu konto">
	<li><a href="{router->linkTo module=user action=settings}">Zmień hasło</a></li>
	<li><a href="{router->linkTo module=user action=logout}">Wyloguj się</a></li>
</ul>
