<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <base href="{$CNF_SITE_URL}">
        <title>{$title|default:$breadcrumbsTitle|default:$wdname}</title>

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Oxygen:400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

                <link rel="stylesheet" href="themes/nawynajmij/css/blueprint/screen.css" type="text/css" media="screen, projection">
                    <link rel="stylesheet" href="themes/nawynajmij/css/blueprint/print.css" type="text/css" media="print">
                        <!--[if lt IE 8]>
                          <link rel="stylesheet" href="themes/nawynajmij/css/blueprint/ie.css" type="text/css" media="screen, projection">
                        <![endif]-->
                        <link rel="stylesheet" href="themes/nawynajmij/css/nw.css" type="text/css" media="screen, projection" />
                        <link rel="stylesheet" href="themes/nawynajmij/css/slider.css" type="text/css" media="screen, projection" />

                        <meta name="robots" content="{$headRobots|default:'INDEX,FOLLOW'}" />

                        <script type="text/javascript" src="themes/nawynajmij/js/jquery.js"></script>
                        <script type="text/javascript" src="themes/nawynajmij/js/slides.min.jquery.js"></script>
                        <script type="text/javascript" src="themes/nawynajmij/js/nw.js"></script>
                        {if $cleditor}
                            <link rel="stylesheet" type="text/css" href="themes/nawynajmij/js/cleditor/jquery.cleditor.css" />
                            <script type="text/javascript" src="themes/nawynajmij/js/cleditor/jquery.cleditor.js"></script>
                            {if $cleditorbb}
                                <script type="text/javascript" src="themes/nawynajmij/js/cleditor/jquery.cleditor.bbcode.min.js"></script>
                            {/if}
                        {/if}
                        {if $swfupload}
                            <script type="text/javascript" src="themes/nawynajmij/js/swfupload/swfupload.js"></script>
                            <script type="text/javascript" src="themes/nawynajmij/js/swfupload/plugins/swfupload.queue.js"></script>
                        {/if}

                        {if $lightbox}
                            <script type="text/javascript" src="themes/nawynajmij/js/jquery.lightbox-0.5.pack.js"></script>
                            <link rel="stylesheet" type="text/css" href="themes/nawynajmij/css/jquery.lightbox-0.5.css" media="screen, projection" />
                        {/if}
                        {if $jsTree}
                            <script type="text/javascript" src="themes/nawynajmij/js/jquery.jstree.js"></script>
                            <script type="text/javascript" src="themes/nawynajmij/js/jquery.hotkeys.js"></script>
                            <script type="text/javascript" src="themes/nawynajmij/js/jquery.cookie.js"></script>
                        {/if}
                        {*
                        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
                        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
                        *}
                        <link rel="alternate" type="application/rss+xml" title="RSS NaWynajmij.pl" href="{router->linkTo action=Rss}" />

                        <script type="text/javascript">
                            var _gaq = _gaq || [];
                            _gaq.push(['_setAccount', 'UA-23153593-1']);
                            _gaq.push(['_trackPageview']);

                            (function() {
                                var ga = document.createElement('script');
                                ga.type = 'text/javascript';
                                ga.async = true;
                                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                                var s = document.getElementsByTagName('script')[0];
                                s.parentNode.insertBefore(ga, s);
                            })();
                        </script>

                        <script type="text/javascript">
                            $(function() {
                                $("#slides").slides({
                                    preload: true,
                                    preloadImage: 'themes/nawynajmij/img2/loading.gif',
                                    play: 5000,
                                    pause: 2500,
                                    hoverPause: true
                                });
                            });
                        </script>

                        </head>
                        <body>
                            {if $user.isAdmin}
                                <div style="border-bottom: 1px #6d767b solid; background-color:#f8f6f2;" class="np">
                                    <div style="width:950px;margin:0 auto;padding:3px;">
                                        Witaj administratorze, przejdź do <a href="{router->linkTo action=Admin}">panelu administracyjnego</a>.
                                    </div>
                                </div>
                            {/if}