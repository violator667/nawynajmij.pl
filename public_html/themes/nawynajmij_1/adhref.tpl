{strip}
{if $ad.catSingular}
	{eval var=$ad.catSingular assign=urlNice}
{else}
	{assign var=urlNice value=$ad.catSingular|cat:' '|cat:$ad.city|cat:' '|cat:$ad.cityRegion}
{/if}
{assign var=urlNice value=$urlNice|urlNice}
<a href="{router->linkTo action=Oferta urlNice=$urlNice adId=$ad.adId}">
{if $ad.catSingular}
	{eval var=$ad.catSingular}
{else}
	{$ad.catSingular} {$ad.city|ucwords}{if $ad.cityRegion|ucwords}, {$ad.cityRegion|ucwords}{/if}
{/if}
</a>
{/strip}