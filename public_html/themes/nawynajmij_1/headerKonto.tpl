<div class="span-6">
<h3>Menu</h3>
<ul class="menu konto">
	<li><a href="{router->linkTo action=KontoOgloszeniaDodaj}">Dodaj ogłoszenie</a></li>
	<li><a href="{router->linkTo action=KontoOgloszenia}">Twoje ogłoszenia ({$adUserStat.ads})</a></li>
	<li><a href="{router->linkTo action=KontoObserwowane}">Obserwowane ogłoszenia ({$adUserStat.monitored})</a></li>
	<li><a href="{router->linkTo action=KontoOgloszeniaOplac}">Ogłoszenia do opłacenia ({$adUserStat.topay})</a></li>
	<li><a href="{router->linkTo action=KontoOgloszeniaZakonczone}">Ogłoszenia zakończone ({$adUserStat.expired})</a></li>
	{*<li><a href="{router->linkTo action=KontoAbonament}">Abonament</a></li>*}
</ul>

<h3>Konto</h3>
<ul class="menu konto">
	<li><a href="{router->linkTo action=KontoBaner}">Baner w ogłoszeniach</a></li>
	<li><a href="{router->linkTo action=KontoHaslo}">Zmień hasło</a></li>
	<li><a href="{router->linkTo action=LoginOut}">Wyloguj się</a></li>
</ul>

</div>
<div class="span-18 last">