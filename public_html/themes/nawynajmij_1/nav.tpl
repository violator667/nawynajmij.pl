{if $navArray}
<div class="pager{if $pagerClass} {$pagerClass}{/if}">
	
	{assign var=i value=0}
		
	{foreach from=$navArray key=k item=v}
		{if $k=='ak'}
				<a class="active">{$v}</a>
		{else}
			{math equation="x + y" x=$i y=1 assign=i}
			{if $v=='0' AND $k!=='<'}
				<a href="{eval var=$navUrl}">{$k}</a>
			{else}
				{if $v!='...' AND $k!=='>' AND $k!=='<'}
					{if $v==$pager.allPages}
					<a href="{eval var=$navUrl1}">{$k}</a>
					{else}
					<a href="{eval var=$navUrl}">{$k}</a>
					{/if}
				{elseif $k=='>'}
					{assign var=next value=$v}
				{elseif $k=='<'}
					{if $v==$pager.allPages}
					<a class="prev" href="{eval var=$navUrl1}">Wstecz</a>
					{else}
					<a class="prev" href="{eval var=$navUrl}">Wstecz</a>
					{/if}
				{else}
					{if $v!='...'}
						<a class="active">{$v}</a>
					{else}
						<a class="mid">{$v}</a>
					{/if}
				{/if}
			{/if}
		{/if}
	{foreachelse}
		<a class="active">{$v}</a>
	{/foreach}
	{if $next}
		{assign var=v value=$next}
			{if $v==$pager.allPages}
			<a class="next" href="{eval var=$navUrl1}">Dalej</a>
			{else}
			<a class="next" href="{eval var=$navUrl}">Dalej</a>
			{/if}
	{/if}
</div>
<div class="clear"></div>
{/if}