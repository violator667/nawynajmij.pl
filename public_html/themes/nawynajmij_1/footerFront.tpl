<div class="clearfix"></div>
<div id="footer">
    <div class="a_fBox span-6">
        <div class="a_fBox_top">
            <h3>Twoje konto</h3>
            {if !$user.isLoged}
            <p>- <a href="{router->linkTo action=Login}">Rejestracja</a></p>
            <p>- <a href="{router->linkTo action=Login}">Logowanie</a></p>
            {/if}
            <p>- <a href="{router->linkTo action=KontoHaslo}">Zmień hasło</a></p>
        </div>
        <div class="a_fBox_title"></div>
        <p>- <a href="{router->linkTo action=KontoOgloszenia}">Twoje ogłoszenia</a></p>
        <p>- <a href="{router->linkTo action=KontoObserwowane}">Obserwowane ogłoszenia</a></p>
    </div>
    
    
    <div class="a_fBox span-6">
        <div class="a_fBox_top">
            <h3>Jak dodać ogłoszenie</h3>
            <p>- <a href="{router->linkTo action=Strona pageSlug='jak-dodac-ogloszenie'}">Jak pisać ogłoszenie?</a></p>
            <p>- <a href="{router->linkTo action=Strona pageSlug='oferta-dla-firm'}">Programy dla firm</a></p>
            <p>- <a href="{router->linkTo action=Strona pageSlug=cennik}">Cennik ogłoszeń</a></p>
            <p>- <a href="{router->linkTo action=Strona pageSlug=pomoc}">Pomoc</a></p>
        </div>
        <div class="a_fBox_title"></div>
        <h2>+ <a href="{router->linkTo action=Konto}">Dodaj ogłoszenie</a></h2>
    </div>
    

    <div class="a_fBox span-6">
        <div class="a_fBox_top">
            <h3>NaWynajmij.pl</h3>
            <p>- <a href="{router->linkTo action=Strona pageSlug='o-serwisie'}">O serwisie NaWynajmij.pl</a></p>
            <p>- <a href="{router->linkTo action=Strona pageSlug=regulamin}">Regulamin</a></p>
            <p>- <a href="{router->linkTo action=Strona pageSlug='ochrona-prywatnosci'}">Ochrona prywatności</a></p>
            <p>- <a href="{router->linkTo action=Strona pageSlug='nasi-partnerzy'}">Nasi partnerzy</a></p>
        </div>
        <div class="a_fBox_title">Dowiedz się więcej:</div>
        <p>- <a href="#">Aukcje</a></p>
        <p>- <a href="#">Fundusz Wynajem Mieszkań</a></p>
    </div>

    
    
    
    <div class="a_fBox span-6 last">
        <div class="a_fBox_top">
            <h3>Kontakt</h3>
            <div class="a_fBox_mail"><span>@</span><a href="mailto:info@nawynajmij.pl">info@nawynajmij.pl</a></div>
        </div>
        <div class="a_fBox_title"></div>
        <p>- <a href="{router->linkTo action=KategorieMapa}">Mapa strony</a></p>
    </div>
</div>
</div>
<div id="a_footer_line"></div>
<div id="a_footer2" class="container">
    <div class="span-12">
        Copyright: <a href="http://nawynajmij.pl">NaWynajmij.pl</a>
    </div>
    <div class="span-12 last text-right">
        Design: <a href="http://princessdesign.com.pl">princessdesign.com.pl</a>
    </div>
</div>