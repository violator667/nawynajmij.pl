<div class="span-6">
<h3>Menu Administratora</h3>
<ul class="menu">
	<li><a href="{router->linkTo action=Admin}">Kokpit</a></li>
	<li><a href="{router->linkTo action=AdminOgloszenia}">Ogłoszenia</a></li>
	<li><a href="{router->linkTo action=AdminUzytkownicy}">Użytkownicy</a></li>
	<li><a href="{router->linkTo action=AdminKategorie}">Kategorie</a></li>
	<li><a href="{router->linkTo action=AdminAttr}">Atrybuty</a></li>
	<li><a href="{router->linkTo action=AdminAbonamenty}">Abonamenty</a></li>
	<li><a href="{router->linkTo action=AdminFaktury}">Faktury</a></li>
	<li><strong>CMS</strong></li>
	<li><a href="{router->linkTo action=AdminCmsStrony}">Strony</a></li>
	<li><a href="{router->linkTo action=AdminCmsEmail}">Szablony E-mail</a></li>
	<li><strong>BACKUP</strong></li>
	<li><a href="{router->linkTo action=AdminBackup}">Backup</a></li>
</ul>


</div>
<div class="span-18 last">