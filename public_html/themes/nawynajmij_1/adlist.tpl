{if $ads or $flash.$filterNamespace}

    {include file="nav.tpl" navArray=$pager.man pagerClass="pagerTop"}

    {if $filter}

        <div class="filter{if $flash.$filterNamespace} filter-active{/if}{if $filterError} filter-error{/if}">
            <div id="filterHead">
                <div id="filterHeadInfo">
                    Znalezionych ofert: {$all}
                </div>
                {if $flash.$filterNamespace}
                    Filtr jest <strong>włączony</strong>,
                    {if $flash.$filterNamespace.value.regionId}
                        Województwo: {foreach from=$regions item=region}{if $flash.$filterNamespace.value.regionId==$region.regionId}{$region.region}{/if}{/foreach}
                    {/if}
                    {if $flash.$filterNamespace.value.city}
                        Miasto: {$flash.$filterNamespace.value.city|ss|escape}
                    {/if}
                    {if $flash.$filterNamespace.value.cityRegion}
                        Dzielnica: {$flash.$filterNamespace.value.cityRegion|ss|escape}
                    {/if}
                    {if $flash.$filterNamespace.value.priceFrom}
                        Cena od: {$flash.$filterNamespace.value.priceFrom|number_format:2:",":" "} zł
                    {/if}
                    {if $flash.$filterNamespace.value.priceTo}
                        Cena do: {$flash.$filterNamespace.value.priceTo|number_format:2:",":" "} zł
                    {/if}
                    <a href="{$filterResetUrl}">Resetuj</a> | <a href="#" class="changeFilter">Zmień filtrowanie</a>
                {else}
                    <a href="#" class="changeFilter">Zmień filtrowanie i sortowanie</a>
                {/if}

            </div>
            <form method="post" action="{$filterUrl|default:'#'}">
                <div id="filterCnt" class="hide">
                    <h3>Filtruj ogłoszenia</h3>
                    <div class="clearfix">
                        <div class="span-3 colborder">
                            <label for="regionId">Województwo</label>
                            <select style="width:110px;" name="regionId" id="regionId">
                                <option></option>
                                {foreach from=$regions item=region}
                                    <option value="{$region.regionId}"{if $flash.$filterNamespace.value.regionId==$region.regionId} selected="selected"{/if}>{$region.region}</option>
                                {/foreach}
                            </select>
                        </div>

                        <div class="span-4 colborder">
                            <label for="city">Miasto</label>
                            <input style="width:150px;" class="text" value="{$flash.$filterNamespace.value.city|ss|escape}" type="text" name="city" id="city" />
                        </div>

                        <div class="span-3 colborder">
                            <label for="cityRegion">Dzielnica</label>
                            <input style="width:110px;" class="text" value="{$flash.$filterNamespace.value.cityRegion|ss|escape}" type="text" name="cityRegion" id="cityRegion" />
                        </div>
                        <div class="span-4 last">
                            <label for="priceFrom">Cena od/do</label>
                            <div>
                                <input style="width:60px;" class="text" value="{$flash.$filterNamespace.value.priceFrom|ss|escape}" type="text" name="priceFrom" id="priceFrom" />
                                <input style="width:60px;" class="text" value="{$flash.$filterNamespace.value.priceTo|ss|escape}" type="text" name="priceTo" id="priceTo" />
                            </div>
                        </div>
                    </div>

                    <hr />
                    <div>
                        {if $flash.$filterNamespace.value.search}
                            <label for="search2">Szukaj ofert po słowach kluczowych</label>
                            <div>
                                <input style="width:300px;" class="text" value="{$flash.$filterNamespace.value.search|ss|escape}" type="text" name="search" id="search2" />
                            </div>
                        {else}
                            <label for="order">Sortuj oferty</label>
                            <div>
                                <select id="order" name="order">
                                    <option>Dacie malejąco</option>
                                    <option value="1"{if $flash.$filterNamespace.value.order==1} selected="selected"{/if}>Dacie rosnąco</option>
                                    <option value="2"{if $flash.$filterNamespace.value.order==2} selected="selected"{/if}>Cenie malejąco</option>
                                    <option value="3"{if $flash.$filterNamespace.value.order==3} selected="selected"{/if}>Cenie rosnąco</option>

                                </select>
                            </div>
                        {/if}
                    </div>

                    <input type="submit" value="Filtruj ogłoszenia" />

                </div>
            </form>
        </div>


        <script type="text/javascript" language="JavaScript">
            {literal}
                $('.changeFilter').click(function() {
                    $('#filterHead').addClass('hide');
                    $('#filterCnt').removeClass('hide');
                });
            {/literal}
        </script>
    {/if}
    {*        {$ads|@var_dump}*}
    {foreach name='ads' from=$ads item=ad}
        {if $ad.catSingular}
            {eval var=$ad.catSingular assign=urlNice}
        {else}
            {assign var=urlNice value=$ad.catSingular|cat:' '|cat:$ad.city|cat:' '|cat:$ad.cityRegion}
        {/if}
        {assign var=urlNice value=$urlNice|urlNice}
        <div class="{if $ad.positioned} positioned{/if}">
            <div class="a_adoffer clearfix{if $ad.highlighted} highlighted{/if}">
                <div class="a_thumb span-5">
                    <a href="{router->linkTo action=Oferta urlNice=$urlNice adId=$ad.adId}"><img src="{$ad.thumb|default:$config.defaultThumb3}" width="176px" /></a>
                </div>
                <div class="span-10">
                    <div class="a_title">
                        {$attr = $ad.attr|@unserialize}
                        <a href="{router->linkTo action=Oferta urlNice=$urlNice adId=$ad.adId}">{strip}{if $ad.catSingular}
                            {eval var=$ad.catSingular}
                            {else}
                                {$ad.catSingular} {$ad.city|ucwords}{if $ad.cityRegion|ucwords}, {$ad.cityRegion|ucwords}{/if}
                                {/if}{/strip}</a>
                                    <div class="a_titleAddress">Adres: {strip}{eval var=$ad.city}{/strip}</div>
                                </div>

                                <div class="a_shortDesc span-10">
                                    {$ad.ad|stripbbcode|mb_substr:0:250}
                                </div>
                            </div>
                            <div class="span-3 last a_offer_right">
                                <div class="a_price"><span>{$ad.price|number_format:2:",":" "}</span> zł</div>
                                <div class="a_offerAttr a_priceType"><span>
                                        {assign var=priceTypes value=['hour'=>'za godzinę','day'=>'za dzień','week'=>'za tydzień','month'=>'za miesiąc','year'=>'za rok','all'=>'całość', 'ha'=>'za hektar', 'm2'=>'za m2']}
                                        {$priceTypes.{$ad.priceType}}
                                    </span></div>
                                
                                {if $attr.at_pok}<div class="a_offerAttr">{eval $attr.at_pok}
                                        {if $attr.at_pok == 1}
                                            pokój
                                        {elseif $attr.at_pok<5}
                                            pokoje
                                        {else}
                                            pokoi
                                        {/if}
                                    </div>{/if}
                                    {if $attr.at_area}<div  class="a_offerAttr">{eval $attr.at_area} m<sup>2</sup></div>{/if}

                                    {if $monitorRemove}
                                        <a href="{router->linkTo action=KontoObserwowaneUsun adId=$ad.adId}" class="a_deleteFromWatchlist">Usuń z obserwowanych</a>
                                    {/if}
                                </div>
                                <a class='a_offerMore' href="{router->linkTo action=Oferta urlNice=$urlNice adId=$ad.adId}">zobacz ofertę</a>
                                <div class="a_adCreatedAt">
                                    Dodano: <span title="{$ad.adCreatedAt|date_format:'%y-%m-%d %H:%M:%S'}">

                                        {if $ad.adCreatedAt > $smarty.now-3600}
                                            {$ad.adCreatedAt|date_format:'%y-%m-%d %H:%M:%S'|date2date}
                                        {elseif $ad.adCreatedAt|date_format:'%y-%m-%d' == $smarty.now|date_format:'%y-%m-%d'}
                                            {$ad.adCreatedAt|date_format:'%H:%M:%S'}
                                        {else}
                                            {$ad.adCreatedAt|date_format:'%Y-%m-%d'}
                                        {/if}
                                    </span>
                                </div>
                            </div>
                        </div>
                        {/foreach}

                            {include file="nav.tpl" navArray=$pager.man pagerClass="pagerBottom"}
                            {/if}