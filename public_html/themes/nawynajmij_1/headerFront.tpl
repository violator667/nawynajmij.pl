<div class="container">
    <div class="clearfix" id="a_header">
        <div class="span-8"><a href="{router->linkTo action=Index}"><img src="themes/nawynajmij/img2/logo.png" id='a_logo' alt="NaWynajmij.pl - serwis z nieruchomościami na wynajem." /></a></div>
        <div class="span-16 last np" id='a_header_menu'>
            <div class="cell-r">
                {if $user.role > 0 }
                    <a href="{router->linkTo action=Konto}"><span>Twoje konto</span></a>
                {else}
                    <a href="{router->linkTo action=Login}"><span>Zaloguj się / Zarejestruj</span></a>
                {/if}
                <a href="{router->linkTo action=Strona pageSlug=pomoc}">Pomoc</a>
                <a href="{router->linkTo action=Strona pageSlug=kontakt}">Kontakt</a>
                {if $user.role > 0 }
                    <a href="{router->linkTo action=LoginOut}">Wyloguj się</a>
                {/if}
            </div>
        </div>
    </div>
    <div id='a_search'>
        <form method="post" action="{router->linkTo action=Szukaj}">
            <input type="text" name="search" id='a_search_input' placeholder="wpisz miejscowość, dzielnicę lub ulicę" value="" />
            <button type="submit" value="Szukaj" id='a_search_button'></button>
        </form>
    </div>               

{include file="breadcrumbs.tpl"}

