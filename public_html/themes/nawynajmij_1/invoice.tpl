<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Faktura VAT {$invoice.invoiceNumber}/{$invoice.invoiceYear}</title>
<style type="text/css">
{literal}
.itemlist {border: 1px #999999 solid;}
.itemlist th {background-color:#bbbbbb;}
.itemlist td {padding:5px; border-right:1px #cccccc solid;}
.itemlist .last {border:0px;}
.tr {text-align:right;}
.tc {text-align:center;}

{/literal}
</style>
</head>
<body>
<div style="padding-left:55%;">
	<div>{$invoice.invoiceSellerCodepost}, {$invoice.invoiceDate|date_format:"%d-%m-%Y"} r.</div>
	<div>Data sprzedaży: {$invoice.invoiceDate|date_format:"%d-%m-%Y"} r.</div>
</div>
<div>
	<h2>{$invoice.invoiceSellerName}</h2>
	<div>{$invoice.invoiceSellerStreet}</div>
	<div>{$invoice.invoiceSellerCode} {$invoice.invoiceSellerCodepost}</div>
	<div>NIP {$invoice.invoiceSellerNIP}</div>
</div>
<div style="text-align:center;">
<h1>Faktura VAT {$invoice.invoiceNumber}/{$invoice.invoiceYear}</h1>
oryginał / kopia
</div>


<table style="width:100%;margin-top:5%;">
<tr>
	<td>Sprzedawca:</td>
	<td>Nabywca:</td>
</tr>
<tr>
	<td><strong>{$invoice.invoiceSellerName}</strong></td>
	<td><strong>{$invoice.invoiceBuyerName}</strong></td>
</tr>
<tr>
	<td>{$invoice.invoiceSellerStreet}</td>
	<td>{$invoice.invoiceBuyerStreet}</td>
</tr>
<tr>
	<td>{$invoice.invoiceSellerCode} {$invoice.invoiceSellerCodepost}</td>
	<td>{$invoice.invoiceBuyerCode} {$invoice.invoiceBuyerCodepost}</td>
</tr>
<tr>
	<td>NIP {$invoice.invoiceSellerNIP}</td>
	<td>NIP {$invoice.invoiceBuyerNIP}</td>
</tr>
</table>


<table style="width: 100%;margin-top:5%;" class="itemlist">
<thead>
	<tr>
		<th style="width:5%;">Lp.</th>
		<th>Nazwa usługi</th>
		<th style="width:15%;">Cena netto</th>
		<th style="width:15%;">Wartość netto</th>
		<th style="width:10%;">Stawka VAT</th>
		<th style="width:10%;">Kwota VAT</th>
		<th style="width:15%;">Wartośc brutto</th>
	</tr>
</thead>
<tbody>
	{foreach name=ii from=$invoice.items item=item}
	<tr>
		<td class="tr">{$smarty.foreach.ii.index+1}</td>
		<td>{$item.invoiceItemLabel}</td>
		<td class="tr">{($item.invoiceItemPrice/100)|floatval|number_format:2:",":" "} zł</td>
		<td class="tr">{($item.invoiceItemPrice/100)|floatval|number_format:2:",":" "} zł</td>
		<td class="tr">{$item.invoiceItemVAT*100-100}%</td>
		<td class="tr">{((($item.invoiceItemPrice/100*$item.invoiceItemVAT)-$item.invoiceItemPrice/100))|number_format:2:",":" "} zł</td>
		<td class="tr last">{($item.invoiceItemPrice/100*$item.invoiceItemVAT)|number_format:2:",":" "} zł</td>
	</tr>
	
	{$sum.{$item.invoiceItemVAT*100-100}.netto=$sum.{$item.invoiceItemVAT*100-100}.netto+$item.invoiceItemPrice}
	{$sum.{$item.invoiceItemVAT*100-100}.vat=$sum.{$item.invoiceItemVAT*100-100}.vat+((($item.invoiceItemPrice*$item.invoiceItemVAT)-$item.invoiceItemPrice))}
	{$sum.{$item.invoiceItemVAT*100-100}.brutto=$sum.{$item.invoiceItemVAT*100-100}.brutto+($item.invoiceItemPrice*$item.invoiceItemVAT)}
	{$sumbrutto=$sumbrutto+($item.invoiceItemPrice*$item.invoiceItemVAT)}
	
	{/foreach}
</tbody>
</table>
<table style="margin-top:1%;width:100%;border:0px;" class="itemlist">
	<thead>
		<tr>
			<th style="background-color:none;"></th>
			<th style="width:15%;">NETTO</th>
			<th style="width:10%;">STAWKA VAT</th>
			<th style="width:10%;">VAT</th>
			<th style="width:15%;">BRUTTO</th>
		</tr>
	</thead>
	<tbody>
	{foreach from=$sum key=vat item=vatsum}
		<tr>
			<td class="last"></td>
			<td class="tr ">{($vatsum.netto/100)|number_format:2:",":" "} zł</td>
			<td class="tr ">{$vat}%</td>
			<td class="tr ">{($vatsum.vat/100)|number_format:2:",":" "} zł</td>
			<td class="tr last">{($vatsum.brutto/100)|number_format:2:",":" "} zł</td>
		</tr>
	{/foreach}
	</tbody>
</table>

<div style="padding-top:5%;">
	<div>Do zapłaty: {($sumbrutto/100)|number_format:2:",":" "} zł (słownie: {($sumbrutto/100)|number_format:2:",":" "|slownie})</div>
	<div>Sposób zapłaty: Płatność on-line</div>
</div>

<table style="width:100%;margin-top:15%;">
<tr>
	<td class="tc">............................................................................</td>
	<td class="tc">............................................................................</td>
</tr>
<tr>
	<td class="tc">Odbiorca faktury</td>
	<td class="tc">Wystawca faktury</td>
</tr>
</table>

</body>
</html>