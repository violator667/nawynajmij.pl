 <div class="box light nav-acount">
  <h5>Ogłoszenia:</h5>
  <nav>
    <ul>
      <li><a href="{router->linkTo module=user action=index}">Dodaj ogłoszenie</a>
      </li>
      <li><a href="{router->linkTo module=user action=ads type=active}">Ogłoszenia wystawione</a>
      </li>
      <li><a href="{router->linkTo module=user action=ads type=watched}">Obserwowane ogłoszenia</a>
      </li>
      <li><a href="{router->linkTo module=user action=ads type=topay}">Ogłoszenia do opłacenia</a>
      </li>
      <li><a href="{router->linkTo module=user action=ads type=ended}">Ogłoszenia zakończone</a>
      </li>
      <li><a href="{router->linkTo module=user action=userauctions}">Aukcje</a>
      </li>
    </ul>
  </nav>
</div>
{if $user.subscription eq 1}
<div class="box light nav-acount">
  <h5>Abonament:</h5>
  <nav>
    <ul>
      <li>Standard: {$user.free_standard}</li>
      <li>Wyróżnionych: {$user.free_highlighted}</li>
      <li>Promowanych: {$user.free_positioned}</li>
      <li>Mix: {$user.free_mix}</li>
    </ul>
  </nav>
</div>
{/if}

<div class="box light nav-acount">
  <h5>Konto:</h5>
  <nav>
    <ul>
      <li><a href="{router->linkTo module=user action=settings}">Zmień hasło</a>
      </li>
      <li><a href="{router->linkTo module=user action=logout}">Wyloguj się</a>
      </li>
    </ul>
  </nav>
</div>