$(document).ready(function(){
  var siteurl = 'http://playground.nawynajmij.pl/';
  
  //hide all 
  $('catSel').hide();
  
  //show first
  $('#selectCategory1').show();
  
  var tmpVal = true;
  
  //actual selected category
  var cid = $('#selectedCategory').val();

 
 $('#publish').click(function( event ) {
  event.preventDefault();
  saveDraft();
  $('#publish').delay(500).fadeOut('1000',function () {
    window.location.href=siteurl+'user/index/add/new';
   })
 });
 
 $('#preview').click(function( event ) {
  event.preventDefault();
  saveDraft();
  setTimeout(function() {
      //window.location.href=$('#preview').attr('href');
      window.open($('#preview').attr('href'),'_blank');
  }, 500);
  
 });
 
 $('#preview').click(function( event ) {
  saveDraft();
 });
 //load main cat
  if( $('#selectedCategory').val()!=0 ) {
    //hide all 
    $('catSel').hide();
    $('#seleCatFirst').html('wybrana kategoria: <strong>'+$('#selectedCategoryName').val()+'</strong> wybierz ponownie by zmienić: ');
    $('#seleCatFirst').show();
    //var tmpNum = $('#selectedCategory').val();
    selectCat( 0, "#selectCategory1","#selectCategory2", 1 );
   // $('#selectedCategory').val( tmpNum );
  }else{
    //$('catSel').hide();
    //$('#selectCategory1').show();
    console.log( 'load main cat 2' );
    selectCat( 0, "#selectCategory1","#selectCategory2", 1 );
  }
  
  setInterval( "saveDraft()", 30000 );
  
  $('#positioned').click(function() {
    if( eval($('#uploadnumber').val()) == 0 ) {
      $('#positioned').prop("checked",false);
      $(".quiet").fadeOut(300);
      $(".quiet").css('color', 'red');
      $(".quiet").fadeIn(300).fadeOut(300).fadeIn(300).fadeOut(300).fadeIn(300);
    }else{
      $(".quiet").fadeOut(300);
    }
  });
  
  $('#selectCategory1').change(function(e) {
    $('#seleCatFirst').hide();
    selectCat( $('#selectCategory1').val(), "#selectCategory1","#selectCategory2", 2 );
  });
  $('#selectCategory2').change(function(e) {
    selectCat( $('#selectCategory2').val(), "#selectCategory2","#selectCategory3", 3 );
  });
  $('#selectCategory3').change(function(e) {
    selectCat( $('#selectCategory3').val(), "#selectCategory3","#selectCategory4", 4 );
  });
  $('#selectCategory4').change(function(e) {
    selectCat( $('#selectCategory4').val(), "#selectCategory4","#selectCategory5", 5 );
  });
  $('#selectCategory5').change(function(e) {
    selectCat( $('#selectCategory5').val(), "#selectCategory5","#selectCategory6", 6 );
  });
 
  //getParents( $('#selectedCategory').val() );

  $('#save').click(function( event ) {
  		event.preventDefault();
  		saveDraft();
  	});
 
 
 
  function hideHigher( id )
  {
    var max = 6;
    for( var i=id; i<=max; i++ ) {
      if( i>1) {
        $('#selectCategory'+i).empty();
        $('#selectCategory'+i).hide();
      }
    }
  }
  
   
  
  function getParents( cid )
  {
   $.get( siteurl + "category/selectAllParents/cid/" + cid,
    function(data) {
      for (var i=1; i<=data.length; i++) {
        var n = i - 1;
        var tmpName = "#selectCategory"+i;
        var tmpOption = tmpName+' option[value='+data[i].parent_cid+']';
        $( tmpName ).show();
        $( tmpOption ).prop('selected', true );
        $( tmpName ).trigger('change');
      }
    }, "json");
  }
  
  function loadAttributes( cid )
  {
    console.log( 'Load ATTRIBUTES: ' + cid );
    //get all class in case of validation errors
    var Unvalid = new Array();
    $('.attr .unvalidated').each(function() {
      Unvalid.push(this.id);
    });
    $('.attr').remove();
    $.get( siteurl +  "category/selectAttributes/cid/" + cid,
    function(data) {
         console.log( data );
         var arrt = $('#attrWrap');
      for (var i=0; i<data.length; i++) { 
        
        var attr_value = '#a_'+data[i]['aid'];
        attr_value = $(attr_value);
        attr_value = attr_value.val();
        if (typeof attr_value === "undefined") {
          attr_value = ' ';
        }
        //arrt.append('<div class="attr">');
        arrt.append('<div class="form-group attr">');
       

        if( data[i]['required'] == 1 ) {
          arrt.append('<label class="attr">'+data[i]['name']+'<sup>*</sup></label>');
        }else{
          arrt.append('<label class="attr">'+data[i]['name']);
        }
       
        var tmpN = data[i]['aid'];
        var tmpStatus = false;
        for( var n=0; n<Unvalid.length; n++ ) {
          if( Unvalid[n] == tmpN ) {
            tmpStatus = true;
          }
        }
        //arrt.append('</div><div class="attr">');
        if( tmpStatus === true ) {
           //arrt.append('<input class="attr unvalidated" value="'+attr_value+'" type="text" name="'+data[i]['aid']+'" id="'+data[i]['aid']+'">');
           arrt.append('<input class="form-control unvalidated attr" value="'+attr_value+'" type="text" name="'+data[i]['aid']+'" id="'+data[i]['aid']+'">');
        }else{
           arrt.append('<input class="form-control attr" value="'+attr_value+'" type="text" name="'+data[i]['aid']+'" id="'+data[i]['aid']+'">');
        }
       
        arrt.append('</div>');
        //console.log( data[i].name );
      }

    }, "json");
  }
  
  function selectCat( cid, select, target, targetNum )
  {
    console.log( 'start selectCat' )
    console.log( 'cid: '+cid+' | select: '+select+' | target: '+target+' | targetNum: '+targetNum ); 
    hideHigher( targetNum );
    if( cid > 0 ) {
      $('#selectedCategory').val( cid );
      console.log( 'wybrano kategorie: '+$('#selectedCategory').val() );
    }else{
      console.log( 'wybrano kategorie: '+$('#selectedCategory').val() );
    }
    loadAttributes( $('#selectedCategory').val() );
    $.get( siteurl +  "category/selectCategory/cid/" + cid,
    function(data) {
      if ( cid > 0 ) {
        console.log( 'loding select:' +target );
        var sel = $( target );
      }else{
        console.log( 'loding select:' +select );
        var sel = $( select );
        var self = true;
      }
        sel.empty();
        sel.show();
        sel.append('<option value="'+cid+'" selected>[wybierz kategorie]</option>');
      for (var i=0; i<data.length; i++) {
        sel.append('<option value="' + data[i].cid + '">' + data[i].name + '</option>');
        console.log( data[i].name );
        
      }

     
    }, "json");
  }

  
 });
 
 

function saveDraft() {
    var draft = $('#draftid').val();
    var date = new Date();
    //$('#save').val('Zapisuje');
    //$('#save').prop("disabled", true);
    //$('#publish').prop("disabled",true);
    $('#save').fadeOut();
    $('#publish').fadeOut();
    var saveDraft = $.post( 'http://playground.nawynajmij.pl/user/savedraft/draft_id/'+draft, $('#form-ogloszeniaDodaj').serialize()
    ).success(function( data ){
        console.log($('#form-ogloszeniaDodaj'));
        $('#save').fadeIn();
        $('#publish').fadeIn();
        $('#saveInfo').fadeOut();
        $('#saveInfo').fadeIn();
        $('#saveInfo').fadeOut();
        $('#saveInfo').fadeIn();
        $('#saveInfoMsg').html( 'Zapisano: ' + date.getUTCDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear() + ' ' + date.getHours() + ':' + date.getUTCMinutes() + ':' + date.getSeconds() +'<br/><br/>' );
        console.log( 'save success' );
        console.log(saveDraft.responseText);
    }).error(function (data ) {
      console.log( 'save error' );
    });
}

function setValue( source, target, change )
{
      console.log( 'start setValue' )
      console.log( 'setValue src:' + source + ' tar: ' + target );
  		var value = $(source).val();
  		if( value == null || value == '' ) {
  	  }else{
  			var tmpName = target+' option[value='+value+']';
  			console.log( tmpName );
  			
  			$( target ).show();
        $( target ).each(function( index ) {
        });
       
        $(tmpName).attr('selected', true);
        if( change == true ) {
          $( target ).trigger('change');
        }
  		}
}