      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>
<!-- content -->
<table class="ads-table" width="100%">
  <thead class="ads">
    <tr>
       <th>Numer ogłoszenia</th>
       <th>Tytuł</th>
       <th>Opcje</th>
    </tr>
  </thead>
  <tbody>
  {foreach from=$ad_list item=ad}
    <tr>
       <td>{$ad.aid}</td>
       <td><a href="{router->linkTo module=ads action=showad id=$ad.aid link=$ad.link}">{$ad.title}</a></td>
       <td><a class="button-small" href="{router->linkTo module=ads action=watchad id=$ad.aid}" onclick="return confirm('Napewno usunąć ogłoszenie z obserwowanych?');">Usuń</a></td>
    </tr>
    {/foreach}
  </tbody>
</table>
<!-- /content -->
</div>
</div>
</div>
</div>