      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>
<!-- content -->
{if $bid_error}			
<div class="error">{$bid_error}</div>
{/if}

{if $bid_info}			
<div class="success">{$bid_info}</div>
{/if}
<a class="button" href="{router->linkTo module=ads action=showad id=$ad_id link=$ad_link}">Powrót do ogłoszenia</a>
<!-- /content -->
</div>
</div>
</div>
</div>