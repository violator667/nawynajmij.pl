<div class="container">
        <div class="row">
          <div id="content">  
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>
<!-- content -->
    {$content}
<!-- /content -->
</div>
</div>
</div>
</div>
