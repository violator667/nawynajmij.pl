<!DOCTYPE html>
<html lang="pl-PL">
  <head>
    <title>{$title|default:$breadcrumbsTitle|default:$wdname}</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <base href="{$CNF_SITE_URL}">
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,700,400&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <!--script(src='js/script.js')-->
    <link href="themes/nawynajmij_pl/css/style.css" rel="stylesheet">
    <link href="themes/nawynajmij_pl/css/lightbox.css" rel="stylesheet" >
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="themes/nawynajmij_pl/js/jquery.js"></script>
    <script type="text/javascript" src="themes/nawynajmij_pl/js/nw.js"></script>
    <script type="text/javascript" src="themes/nawynajmij_pl/js/cookie.js"></script>
    <script type="text/javascript" src="themes/nawynajmij_pl/js/lightbox.min.js"></script>
    <script type="text/javascript">

    jQuery(document).ready(function(){
      jQuery.fn.cookiesEU({
        text: 'Używamy informacji zapisanych za pomocą plików cookies w celu zapewnienia maksymalnej wygody w korzystaniu z naszego serwisu. Mogą też korzystać z nich współpracujące z nami firmy badawcze oraz reklamowe. Jeżeli wyrażasz zgodę na zapisywanie informacji zawartej w cookies kliknij na „zamknij” w prawym górnym rogu tej informacji. Jeśli nie wyrażasz zgody, ustawienia dotyczące plików cookies możesz zmienić w swojej przeglądarce.',
        close:		'Zamknij'
      });  
    });

    </script>
    {if $addAd eq true}
    <script type="text/javascript" src="themes/nawynajmij_pl/js/addad.js"></script>
    <script src="themes/nawynajmij_pl/js/jquery.form.js"></script>
    <link rel="stylesheet" type="text/css" href="themes/nawynajmij_pl/cleditor/jquery.cleditor.css" />
    <script type="text/javascript" src="themes/nawynajmij_pl/cleditor/jquery.cleditor.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            
            $("#adText").cleditor({
                width: 370, // width not including margins, borders or padding
                height: 250, // height not including margins, borders or padding
                controls: // controls to add to the toolbar
                    "bold italic underline | " +
                    "color removeformat | undo redo | ",
                   
                colors: // colors in the color popup
                    "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                    "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                    "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                    "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                    "666 900 C60 C93 990 090 399 33F 60C 939 " +
                    "333 600 930 963 660 060 366 009 339 636 " +
                    "000 300 630 633 330 030 033 006 309 303",
                fonts: // font names in the font popup
                    "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
                    "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
                sizes: // sizes in the font size popup
                    "1,2,3,4,5,6,7",
                styles: // styles in the style popup
                    [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
                    ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
                    ["Header 6","<h6>"]],
                useCSS: false, // use CSS to style HTML when possible (not supported in ie)
                docType: // Document type contained within the editor
                    '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
                docCSSFile: // CSS file used to style the document contained within the editor
                    "",
                bodyStyle: // style to assign to document body contained within the editor
                    "margin:4px; font:10pt Arial,Verdana; cursor:text"
            });
        });
    </script>
   
    {/if}
    
    {if $offerDetail}
    <script type="text/javascript">
      $(function() {
        $('#abuseLink').click(function(event) {
          event.preventDefault();
          $('.abuseBox').show();
        });
        $('#abuse-close').click(function(event) {
          event.preventDefault();
          $('.abuseBox').hide();
        });
        $('.a_offerTel').click(function() {
          
          $.ajax({
            dataType: "json",
            url: "ads/phone/id/{$offerDetail}",
            
          }).done(function( data ) {
            $('.a_offerTel').html( data.phone_number );
          });
             
        });
      });
    </script>
    {/if}
    {if $print}
    <script type="text/javascript">
    <!--
    window.print();
    //-->
    </script>
    {/if}
  </head>
  <body>
 