      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>
<!-- content -->
<table class="ads-table" width="100%">
  <thead class="ads">
    <tr>
       <th colspan="2">Zmiana hasła dla konta {$user.username}</th>
    </tr>
  </thead>
  <tbody>
    <tr>
       <td width="30%">Obecne Hasło:</td>
       <td><input type="password" name="current_pass" class="form-control"></td>
    </tr>
    <tr>
       <td width="30%">Nowe hasło:</td>
       <td><input type="password" name="new_pass" class="form-control"></td>
    </tr>
    <tr>
       <td width="30%">Powtórz nowe hasło:</td>
       <td><input type="password" name="new_pass2" class="form-control"></td>
    </tr>
    <tr>
       <td width="30%"></td>
       <td><button type="submit" class="button">zmień hasło</button></td>
    </tr>
  </tbody>
</table>
<!-- /content -->
</div>
</div>
</div>
</div>