<body>
    <div id="all">
      <header id="header">
        <div class="container">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><a href="{router->linkTo module=homepage action=index}" id="logo"><img src="themes/nawynajmij_pl/img/logo.png"></a></div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
              <div class="ico-tel"></div>
              <div id="questions">Masz pytania<br>Zadzwoń</div>
              <div id="tel-number">506 900 909</div>
            </div>
          </div>
        </div>
        <div id="baner-home" class="container-fluid">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <nav id="nav-top">
                  <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                      <ul class="list-inline">
                                        <li><a href="{router->linkTo module=homepage action=index}">Strona główna</a>
                                        </li>
                                        {if $auctionParam eq 1}
                                        <li><a href="{router->linkTo module=category action=show}">Nieruchomości</a>
                                        </li>
                                        <li class="active"><a href="{router->linkTo module=category action=show auction=1}">Aukcje</a>
                                        </li>
                                        {else}
                                        <li class="active"><a href="{router->linkTo module=category action=show}">Nieruchomości</a>
                                        </li>
                                        <li><a href="{router->linkTo module=category action=show auction=1}">Aukcje</a>
                                        </li>
                                        {/if}
                                        
                                        <li><a href="{router->linkTo module=cms action=contact}">Kontakt</a>
                                        </li>
                      </ul>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right">
                    {if $user.role > 0 }
                      <a href="{router->linkTo module=user action=index}" class="button login">{$user.username}</a>
                    {else}
                      <a href="{router->linkTo module=user action=index}" class="button login">Logowanie</a>
                      <a href="{router->linkTo module=user action=index}" class="button button-light">Dodaj ogłoszenie</a>
                    {/if}
                    {if $user.role > 0 }
                      <a href="{router->linkTo module=user action=logout}" class="button login">Wyloguj</a>
                      
                    {/if}
                    </div>
                  </div>
                </nav>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="formBlue">
                   <form method="post" action="{router->linkTo module=category action=filter}">
                   <input type="hidden" name="categoryId" value="{$categoryId}">
                   <input type="hidden" name="auction" value="{$auctionParam}">
                    <div class="row form-group">
                      <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <p>Lokalizacja:</p>
                        <input type="text" placeholder="Wpisz miasto" class="form-control" value="{$filterCity}" name="city" id="city">
                      </div>
                      <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <p>Województwo</p>
                        <select class="form-control" name="regionId" id="regionId">
                                <option>wszystkie</option>
                                <option value="1"{$filterSelect1}>dolnośląskie</option>
                                <option value="2"{$filterSelect2}>kujawsko-pomorskie</option>
                                <option value="3"{$filterSelect3}>lubelskie</option>
                                <option value="4"{$filterSelect4}>lubuskie</option>
                                <option value="5"{$filterSelect5}>łódzkie</option>
                                <option value="6"{$filterSelect6}>małopolskie</option>
                                <option value="7"{$filterSelect7}>mazowieckie</option>
                                <option value="8"{$filterSelect8}>opolskie</option>
                                <option value="9"{$filterSelect9}>podkarpackie</option>
                                <option value="10"{$filterSelect10}>podlaskie</option>
                                <option value="11"{$filterSelect11}>pomorskie</option>
                                <option value="12"{$filterSelect12}>śląskie</option>
                                <option value="13"{$filterSelect13}>świętokrzyskie</option>
                                <option value="14"{$filterSelect14}>warmińsko-mazurskie</option>
                                <option value="15"{$filterSelect15}>wielkopolskie</option>
                                <option value="16"{$filterSelect16}>zachodniopomorskie</option>
                             </select>
                      </div>
                      <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                        <p>Sortuj oferty</p>
                        <select id="order" name="order" class="form-control">
                                    <option value="0">po dacie malejąco</option>
                                    <option value="1">po dacie rosnąco</option>
                                    <option value="2">po cenie malejąco</option>
                                    <option value="3">po cenie rosnąco</option>
                                    <option value="4">po tytule Z do A</option>
                                    <option value="5">po tytule A do Z</option>

                                </select>
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                        <p class="od">Cena [zł]</p>
                        <p class="do-cena">&nbsp;</p>
                        <p class="od">Dzielnica</p>
                      </div>
                      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                        <input type="text" placeholder="od:" class="form-control od" value="{$filterPriceMin}" name="priceMin" id="priceMin">
                        <input type="text" placeholder="do:" class="form-control do-cena" value="{$filterPriceMax}" name="priceMax" id="priceMax">
                        <input placeholder="wpisz dzielnicę" class="form-control district" value="{$filterCityDistrict}" type="text" name="cityDistrict" id="cityDistrict">
                      </div>
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <button type="submit" class="button">Szukaj</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>