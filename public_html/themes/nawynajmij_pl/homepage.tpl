<div class="container">
        <div class="row">
          <div id="content">
            <div class="container">
              <!-- here -->
              <div class="row">
                <div id="gal-list" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <!-- ADS -->
                  {if $positionedAds}
                    {foreach from=$positionedAds item=ad}
                      {if $ad.highlighted eq 1}
                        <div class="row highlighted">
                      {else}
                        <div class="row">
                      {/if}
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><img src="{$ad.photo}" width="155px"></div>
                      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {if $ad.highlighted eq 1}
                              <h4>{if $ad.auction eq 1}<font color=red>[AUKCJA]</font>{/if} {$ad.title}</h4>
                            {else}
                              <h5>{if $ad.auction eq 1}<font color=red>[AUKCJA]</font>{/if} {$ad.title}</h5>
                            {/if}
                            <div class="title">{$ad.city}
                              <ul>
                                <li>{$ad.city_district}</li>
                                <li>Dodano: {$ad.created_date}</li>
                              </ul>
                              <p>{$ad.text_raw}</p>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="line"></div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <h6>
                            {if $ad.auction eq 1}
                              {if $ad.auction_array.current_bid >= $ad.price}
                                {$ad.auction_array.current_bid}
                              {else}
                                {$ad.price}
                              {/if}
                            {else}
                              {$ad.price}
                            {/if}
                            zł <small>{$ad.price_type_str}</small></h6>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}" class="rm">Zobacz całe ogłoszenie</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="lineDuble"></div>
                    {/foreach}   
                  {/if}
                </div>
              </div>
              <!-- /here -->
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <h3 class="text-center">Najlepsze okazje</h3>
                  <div class="lineDuble"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 best">
                  <h6>Lorem ipsum dolor</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices lorem ac odio laoreet eleifend.</p><a href="#">sprawdź</a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 best">
                  <h6>Lorem ipsum dolor</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices lorem ac odio laoreet eleifend.</p><a href="#">sprawdź</a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 best">
                  <h6>Lorem ipsum dolor</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices lorem ac odio laoreet eleifend.</p><a href="#">sprawdź</a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 best">
                  <h6>Lorem ipsum dolor</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices lorem ac odio laoreet eleifend.</p><a href="#">sprawdź</a>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <div class="lineDuble"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 best">
                  <h6>Lorem ipsum dolor</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices lorem ac odio laoreet eleifend.</p><a href="#">sprawdź</a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 best">
                  <h6>Lorem ipsum dolor</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices lorem ac odio laoreet eleifend.</p><a href="#">sprawdź</a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 best">
                  <h6>Lorem ipsum dolor</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices lorem ac odio laoreet eleifend.</p><a href="#">sprawdź</a>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 best">
                  <h6>Lorem ipsum dolor</h6>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ultrices lorem ac odio laoreet eleifend.</p><a href="#">sprawdź</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>