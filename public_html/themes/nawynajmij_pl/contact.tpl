<div class="container">
        <div class="row">
          <div id="content" class="contact">
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
              <h3>Dane kontaktowe:</h3>
              <p class="strong">Lorem ipsum dolor sit</p>
              <p class="ico ico-loc-small">ul.Lorem ipsum 11<br>00-000 Lorem ipsum</p>
              <p class="ico ico-mail-small"><a href="mailto:biuro@lorem ipsumg.pl">biuro@lorem ipsumg.pl</a></p>
              <p class="ico ico-tel-small">+48 000 000 000</p>
              <div id="box-contact">
                <h4>Napisz do nas:</h4>
                <form>
                  <div class="form-group">
                    <label>Imię i nazwisko:</label>
                    <input type="text" placeholder="Imię i nazwisko" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Telefon:</label>
                    <input type="text" placeholder="Telefon" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Adres email:</label>
                    <input type="email" placeholder="Adres email" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Treść wiadomości:</label>
                    <textarea placeholder="Treść wiadomości" class="form-control"></textarea>
                  </div>
                  <div class="form-group form-inline">
                    <label class="chapta"><img src="themes/nawynajmij_pl/img/chapta.png"></label>
                    <input type="text" class="form-control input-sm">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="button">Wyślij wiadomość</button>
                  </div>
                </form>
              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d10074.49198383472!2d17.4749638!3d50.856665250000006!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spl!2spl!4v1423604186960" width="293" height="790" frameborder="0" style="border:0"></iframe>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <div id="right">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box-blue">
                      <h2>Porównaj kredyty</h2>
                      <h4>Skorzystaj z kalkulatora</h4>
                      <p>Oblicz swoje raty</p><a href="#" class="button button-light">Sprawdź</a>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box-add">
                      <h6>Miejsce na reklamę</h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>