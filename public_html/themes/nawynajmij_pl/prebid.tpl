      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>
<!-- content -->
<table class="ads-table" width="100%">
  <thead class="ads">
    <tr>
       <th colspan="3">Twoja oferta w aukcji {$ad_name}</th>
    </tr>
  </thead>
  <tbody>
      <form action="{$site_url}user/bid/" method="post">
			<input type="hidden" name="token" value="{$token}">
			<input type="hidden" name="ad_id" value="{$ad_id}">
			<input type="hidden" name="ad_name" value="{$ad_name}">
			<input type="hidden" name="ad_link" value="{$ad_link}">
    <tr>
			<td width="30%"><input type="text" name="bid" size="7" class="form-control" value="{$bid}"></td>
			<td width="10%">ZŁ</td>
			<td width="70%"><button type="submit" class="button">Złóż ofertę</button></td>
    </tr>
    <tr>
      <td colspan="3">Wpisz maksymalną kwotę jaką jesteś skłonny zapłacić.</td>
    </tr>
  </tbody>
</table>
<!-- /content -->
</div>
</div>
</div>
</div>
