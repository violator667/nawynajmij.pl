<div class="container">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
{if $error}
          {foreach from=$error item=err}
            <div class="error">{$err.0}</div>
          {/foreach}
{/if}
{if $notice}
          {foreach from=$notice item=ntc}
            <div class="success">{$ntc.0}</div>
          {/foreach}
{/if}
</div>
</div>
<div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="box light">
                <h3>Zaloguj się</h3>
                <form action="{router->linkTo module=user action=login}" method="post" id="form-zaloguj">
                <input type=hidden name=login value=true>
                <input type=hidden name=redirect value=user/index>
                <input type=hidden name=token value="{$token}">
                  <div class="form-group">
                    <label>Twój email</label>
                    <input type="email" placeholder="Email" class="form-control" name="email">
                  </div>
                  <div class="form-group">
                    <label>Hasło</label>
                    <input type="password" placeholder="Hasło" class="form-control" name="password">
                  </div>
                  <div class="form-group text-right">
                    <button type="submit" class="button">Zaloguj się</button>
                  </div>
                </form>
                <div class="line"></div>
                <a href="#" id="recoverPassLink">Zapomniałem hasła</a>
                <div id="recoverPass">
                  <h3>Zapomniałeś hasła?</h3>
                  <p>Jeśli zapomniałeś hasła do konta, wpisz swój adres e-mail:
                  </p><form action="{$site_url}user/lostpassstart" method="post">
                  <div class="form-group">
                      <input type="email" placeholder="Email" class="form-control" name="userLogin">
                  </div>
                    <div class="form-group text-right">
                      <button type="submit" class="button">Przypomnij hasło</button>
                    </div>
                  </form>
                </div>
             </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
              <div class="box dark">
                <h3>Rejestracja</h3>
                <p>Jeśli nie posiadasz konta w serwisie NaWynajmij.pl możesz je utworzyć poniżej podając adres e-mail jako login oraz hasło.<br>Konto umożliwi Ci umieszczanie ofert wynajmu i usług w serwisie.</p>
                <p>Rejestracja jest darmowa. Zwróć uwagę, aby adres e-mail był poprawny.</p>
                <div class="line"></div>
                <form action="{$site_url}user/register" method="post" id="form-rejestracja">
                <input type=hidden name=doRegister value=true>
                <input type=hidden name=redirect value=index>
                <input type=hidden name=token value="{$token}">
                  <div class="form-group">
                    <label>Tówj email</label>
                    <input type="email" placeholder="Email" class="form-control" name="email">
                  </div>
                  <div class="form-group">
                    <label>Hasło</label>
                    <input type="password" placeholder="Hasło" class="form-control" name="password">
                  </div>
                  <div class="form-group">
                    <label>Powtórz hasło</label>
                    <input type="password" placeholder="Powtórz hasło" class="form-control" name="rep_password">
                  </div>
                  <div class="form-group checkbox">
                    <label><input type="checkbox" name="akceptuje" value="tak"> Oświadczam, że zapoznałem / zapoznałam się z treścią Regulaminu i w pełni go akceptuję.</label>
                  </div>
                  <div class="form-group text-right">
                    <button type="submit" class="button button-light">Zarejestruj się</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
