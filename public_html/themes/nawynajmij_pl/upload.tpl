<form name="upload" id="upload" action="user/upload" method="post" enctype="multipart/form-data">
  <input type="file" name="file" id="upfile"><br>
</form>
<div class="progress">
    <div class="bar"></div >
    <div class="percent"></div >
    <input type="hidden" id="uploadnumber" value="{$uploadNumber}">
</div>
<div class="fileContainer">
  <div class="innerFile1"></div>
  {foreach from=$photoList item=photo}
    {$count = $photo@iteration+1}
    <div class="innerFile{$count} uplFile"><img class="uplMiniature" src="upload/mini/{$photo}"></div>
  {/foreach}
    <div class="uplFileEnd"></div>
</div>
<div id="status"></div>
    
<script>
    (function() {
        
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');
    var uplNumber = $('#uploadnumber');

    $('#upfile').change(function() {
      $('#upload').submit();
    });
    
    //$('#upfile').on('change', function () {
    //  $('#upload').submit();
    //});
    
    if( uplNumber.val() >= 5 ) {
    $('#upfile').hide();
    }
       
    $('#upload').ajaxForm({
        beforeSend: function( xhr ) {
            status.empty();
            var percentVal = '';
            bar.width(percentVal)
            percent.html(percentVal);
            if( uplNumber.val() >= 5 ) {
              xhr.abort();
              status.html( 'Dodano już maksymalną liczbę plików.' );
              $('#upfile').hide();
            }else{
              if( uplNumber.val() > 0 ) {
                $('#positioned').prop("disabled", false);
              }
            }
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
            console.log(percentVal, position, total);
        },
        success: function() {
            var percentVal = '100%';
            bar.width(percentVal)
            percent.html(percentVal);
            console.log('success');
            var tmpNum = eval(uplNumber.val());
            tmpNum = tmpNum+1;
            uplNumber.val( tmpNum );
            console.log( 'upload num = '+uplNumber.val() );
            $('#upfile').val('');
        },
        error: function() {
            console.log('error');
        },
      complete: function(xhr) {
        console.log('complete');
        console.log(xhr);
        //status.html(xhr.responseText);
        var tmpNum = eval(uplNumber.val());
        var formPhoto = '#formPhoto'+tmpNum;
        console.log( 'formPhoto: '+formPhoto);
        var jsonResponse = JSON.parse(xhr.responseText);
        console.log( jsonResponse );
        if( jsonResponse.error != "null" ) {
          percent.html('<strong>Błąd:</strong> '+jsonResponse.error);
          var tmpNum = eval(uplNumber.val());
          tmpNum = tmpNum-1;
          uplNumber.val( tmpNum );
          console.log( 'upload num = '+uplNumber.val() );
        }else{
          console.log( 'jRESPONSE: '+jsonResponse.error );
          $(formPhoto).val( jsonResponse.file );
          var tmpNum = eval(uplNumber.val());
          tmpNum = tmpNum+1;
          var tmpClass = '.innerFile'+uplNumber.val();
          var tmpClass2 = 'innerFile'+tmpNum;
          $(tmpClass).after( '<div class="'+tmpClass2+' uplFile"><img class="uplMiniature" src=upload/mini/'+jsonResponse.file+'></div>');
          percent.html('');
          console.log( 'LAST LINE' );
        }
      }
    }); 

    })();       
</script>