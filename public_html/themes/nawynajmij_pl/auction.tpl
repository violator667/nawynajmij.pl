      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              {if $auction_info }
                <div class="success">{$auction_info}</div>
              {/if}
              </div>
<!-- content -->
<form action="{$site_url}user/addauction/" method="post">
			<input type="hidden" name="token" value="{$token}">
			<input type="hidden" name="ad_id" value="{$ad_id}">
<table class="ads-table" width="100%">
  <thead class="ads">
    <tr>
       <th colspan="2">Ustawienia aukcji</th>
    </tr>
  </thead>
  <tbody>
    <tr>
       <td width="30%">Kwota startowa licytacji:</td>
       <td><input type="text" name="start_price" class="form-control" value="{$start_price}"{$form_disabled}></td>
    </tr>
    <tr>
       <td colspan="2">Od tej kwoty rozpocznie się licytacja.</td>
    </tr>
    
    <tr>
       <td width="30%">Kwota minimalna:</td>
       <td><input type="text" name="min_price" class="form-control" value="{$min_price}"{$form_disabled}></td>
    </tr>
    <tr>
       <td colspan="2">Pozostaw 0 aby wyłączyć kwotę minimalną.</td>
    </tr>
    
    <tr>
       <td width="30%"></td>
       <td><button type="submit" class="button"{$form_disabled}>Zapisz aukcję</button></td>
    </tr>
  </tbody>
</table>
<!-- /content -->
</div>
</div>
</div>
</div>
