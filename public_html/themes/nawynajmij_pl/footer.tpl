      <footer id="footer">
        <div class="container">
          <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <h6>Twoje konto</h6>
              <ul>
                      {if $user.role eq 0}
                      <li><a href="{router->linkTo module=user action=index}">Rejestracja</a>
                      </li>
                      <li><a href="{router->linkTo module=user action=index}">Logowanie</a>
                      </li>
                      {else}
                      <li><a href="{router->linkTo module=user action=settings}">Zmień hasło</a>
                      </li>
                      {/if}
              </ul>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <h6>Jak dodać ogłoszenie:</h6>
              <ul>
                      <li><a href="#">Jak pisać ogłoszenie?</a>
                      </li>
                      <li><a href="#">Programy dla firm</a>
                      </li>
                      <li><a href="#">Cennik ogłoszeń</a>
                      </li>
                      <li><a href="#">Pomoc</a>
                      </li>
              </ul>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <h6>NaWynajmij.pl</h6>
              <ul>
                      <li><a href="#">O serwisie NaWynajmij.pl</a>
                      </li>
                      <li><a href="#">Regulamin</a>
                      </li>
                      <li><a href="#">Ochrona prywatności</a>
                      </li>
                      <li><a href="#">Nai partnerzy</a>
                      </li>
              </ul>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <h6>Kontakt:</h6>
              <p class="ico ico-mail-small"><a href="mailto:info@nawynajmij.pl">info@nawynajmij.pl</a></p>
              <p class="ico ico-tel-small">506 900 909</p>
            </div>
          </div>
        </div>
        <div class="line"></div>
        <div class="container">
          <div class="row">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <ul>
                      <li><a href="{router->linkTo module=user action=ads type=active}">Twoje ogłoszenia</a>
                      </li>
                      <li><a href="{router->linkTo module=user action=ads type=watched}">Obserwowane ogłoszenia</a>
                      </li>
              </ul>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><a href="{router->linkTo module=user action=index}" class="button">Dodaj ogłoszenie</a></div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <h6>Dowiedz się więcej:</h6>
              <ul>
                      <li><a href="#">Aukcje</a>
                      </li>
                      <li><a href="#">Fundusz Wynajem Mieszkań</a>
                      </li>
              </ul>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
              <h6>Mapa strony</h6>
              <ul>
                      <li><a href="{router->linkTo module=homepage action=index}">Strona główna</a>
                      </li>
                      <li><a href="#">Nieruchomości</a>
                      </li>
                      <li><a href="#">Firma</a>
                      </li>
                      <li><a href="#">Referencje</a>
                      </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="line"></div>
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 copwrite">
              <p>Copyright © 2014 by nawynajmij.pl. Wszelkie prawa zastrzeżone</p>
              <p>Projekt i realizacja:<a href="http://Ernes.pl">Ernes.pl</a></p>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </body>
</html>