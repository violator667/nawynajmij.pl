      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>
<!-- content -->
<table class="ads-table" width="100%">
  <thead class="ads">
    <tr>
       <th>Numer ogłoszenia</th>
       <th>Tytuł</th>
       <th>Wyróżnione</th>
       <th>Promowane</th>
       <th>Wyświetleń</th>
       <th>Data dodania</th>
       <th>Data wygaśnięcia</th>
       <th>Opcje</th>
    </tr>
  </thead>
  <tbody>
  {foreach from=$ad_list item=ad}
    <tr>
       <td>{$ad.aid}</td>
       <td><a href="{router->linkTo module=ads action=showad id=$ad.aid link=$ad.link}">{$ad.title}</a></td>
       <td>{if $ad.highlighted eq 1}
              TAK
            {else}
              nie
            {/if}
       </td>
       <td>{if $ad.positioned eq 1}
              TAK
            {else}
              nie
            {/if}
       </td>
       <td>{$ad.ad_views}</td>
       <td>{$ad.created}</td>
       <td>{$ad.expires}</td>
       <td>
        {if $ad.auction eq 1}
          <a class="button-small" href="{router->linkTo module=user action=auction id=$ad.aid}">Aukcja</a>
        {/if}
        <a class="button-small" href="{router->linkTo module=user action=adedit id=$ad.aid}">Edytuj</a>
        {if $ended}
          <a class="button-small" href="{router->linkTo module=user action=adreset id=$ad.aid}" onclick="return confirm('Zamierzasz wystawić ponownie ogłoszenie - zostaniesz przeniesiony na stronę z ogłoszeniami do opłacenia.');">Wystaw</a>
        {else}
          <a class="button-small" href="{router->linkTo module=user action=adstop id=$ad.aid}" onclick="return confirm('Zakończyć ogłoszenie?');">Zakończ</a>
        {/if}
       </td>
    </tr>
    {if $ended}
    {else}
      <tr>
        <td colspan="2">Przedłuż ogłoszenie: </td>
        <td colspan="2">
        <form action="user/payment" method="post">
        <input type="hidden" name="aid" value="{$ad.aid}">
        <input type="hidden" name="high" value="{$ad.highlighted}">	
        <input type="hidden" name="posit" value="{$ad.positioned}">	
            <select name="payment_method" class="form-control">
              {foreach from=$ad.payment_methods item=pay}
                <option value="{$pay.id}">
                  {if $pay.type eq "cash"}
                    Przelew: {$pay.days} dni : {$pay.price} zł
                  {elseif $pay.type eq "subscription"}
                    ABONAMENT: {$pay.days} dni : {$pay.price} zł
                  {else}
                    SMS: {$pay.days} dni : {$pay.price} zł
                  {/if}
                </option>
              {/foreach}
            </select>
        </td>
        <td colspan="4"><button type="submit" class="button">Przedłuż</button></form></td>
      </tr>
    {/if}
    {/foreach}
  </tbody>
</table>
<!-- /content -->
</div>
</div>
</div>
</div>