<div class="container">
       {if $ad.status eq 2}
        <div class="row">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br/>
              <div class="alert">
                <h3>Ta oferta wygasła</h3>
                {if $similar}
                   Oto podobne ogłoszenia które są jeszcze aktualne:<br/>
                  {foreach from=$similar item=simi}
                    <li><a href="{router->linkTo module=ads action=showad id=$simi.aid link=$simi.link}">{$simi.title}</a>
                  {/foreach}
                {/if}
              </div>
            </div>
          </div>
        </div>
        {/if}
        <div class="row">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="bg-gray">
                <div id="gallery">
                  <div class="img">
                  {if $adPhotos|@count gt 0}
                    {foreach from=$adPhotos name=photo item=photo}
                      {if $smarty.foreach.photo.first}
                          <a href="{$site_url}upload/{$photo.file}" data-lightbox="ogloszenie"><img src="{$site_url}upload/{$photo.file}" rel="lightbox[{$ad.aid}]" width="478" alt="" class="bigimg"></a>
                        <ul>
                        {else}
                          {if $smarty.foreach.photo.last}
                            <li><a href="{$site_url}upload/{$photo.file}" data-lightbox="ogloszenie"><img src="{$site_url}upload/{$photo.file}" rel="lightbox[{$ad.aid}]" width="95" alt=""></a></li>
                            
                            </ul>
                          {else}
                            <li><a href="{$site_url}upload/{$photo.file}" data-lightbox="ogloszenie"><img src="{$site_url}upload/{$photo.file}" rel="lightbox[{$ad.aid}]" width="95" alt=""></a></li>
                          {/if}
                        {/if}
                    {/foreach}
                  {else}
                  <a href="{$site_url}themes/nawynajmij/obraz/camera.png" data-lightbox="ogloszenie"><img src="{$site_url}themes/nawynajmij/obraz/camera.png" rel="lightbox[{$ad.aid}]" width="478" alt=""></a>
                  {/if}
                  </div>
                  <div class="capition">
                    <h4>{$ad.title} {if $ad.auction eq 1}<font color=red>[AUKCJA]</font>{/if}</h4>
                    <table class="table">
                      <tr>
                        <th class="awarded">Szczegóły oferty:</th>
                        <th>nr {$ad.aid}</th>
                      </tr>
                      <tr>
                        <td class="tdlabel">Data publikacji:</td>
                        <td>{$ad.created}</td>
                      </tr>
                      <tr>
                        <td class="tdlabel">Data zakończenia:</td>
                        <td>{$ad.expires}</td>
                      </tr>
                      <tr>
                        <td class="tdlabel">CENA:</td>
                        <td class="tdvalue">
                          {if $ad.auction eq 1}
                            {if $adAuction.current_bid >= $ad.price}
                              <h4>{$adAuction.current_bid} zł {$ad.price_str}</h4>
                            {else}
                              <h4>{$ad.price} zł {$ad.price_str}</h4>
                            {/if}
                          {else}
                            <h4>{$ad.price} zł {$ad.price_str}</h4>
                          {/if}
                        </td>
                      </tr>
                      <tr>
                        <td class="tdlabel">LOKALIZACJA:</td>
                        <td class="tdvalue">{$ad.region_str}, {$ad.city}
                          {if $ad.city_district}
                            , {$ad.city_district}
                          {/if}
                        </td>
                      </tr>
                      <tr>
                        <td class="tdlabel">KATEGORIA:</td>
                        <td class="tdvalue"><a href="{router->linkTo module=category action=show cid=$adCategoryId link=$adCategoryLink}">{$adCategoryName}</a></td>
                      </tr>
                      
                      {foreach from=$adAttribute item=attr}
                        {if $attr.hide !=1 }
                          <tr>
                            <td class="tdlabel">{$attr.name}</td>
                            <td class="tdvalue">{$attr.value}</td>
                          </tr>
                        {/if}
                      {/foreach}
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              <h2>Opis oferty</h2>
              <article>
                <p>{$ad.text}</p>
              </article>
              <h2>Lokalizacja</h2>
              <div class="offer-map">
              <div id="map_canvas" style="width: 624px; height: 230px; position: relative; overflow: hidden; -webkit-transform: translateZ(0px); background-color: rgb(229, 227, 223);">
							<div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;">
							<div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(http://maps.gstatic.com/mapfiles/openhand_8_8.cur) 8 8, default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1;"><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: -115px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: 141px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: -115px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: 141px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: -115px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: 141px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: -115px;"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: 141px;"></div></div></div></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1;"><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: -115px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: 141px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: -115px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: 141px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: -115px;"></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: 141px;"></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: -115px;"></div><div style="width: 256px; height: 256px; overflow: hidden; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: 141px;"></div></div></div></div><div style="position: absolute; z-index: 0; left: 0px; top: 0px;"><div style="overflow: hidden; width: 503px; height: 303px;"><img src="./Wynajmij Wynajmę pokój_files/StaticMapService.GetMapImage" style="width: 503px; height: 303px;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1;"><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: 141px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 4px; top: -115px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(1)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: -115px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(2)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: -252px; top: 141px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(3)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: -115px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(4)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: -115px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(5)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 260px; top: 141px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(6)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div><div style="width: 256px; height: 256px; -webkit-transform: translateZ(0px); position: absolute; left: 516px; top: 141px; opacity: 1; transition: opacity 200ms ease-out; -webkit-transition: opacity 200ms ease-out;"><img src="./Wynajmij Wynajmę pokój_files/vt(7)" draggable="false" style="width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; -webkit-transform: translateZ(0px);"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="-webkit-transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="http://maps.google.com/maps?ll=54.194322,16.171491&z=15&t=m&hl=pl-PL&gl=US&mapclient=apiv3" title="Kliknij, aby wyświetlić ten obszar w serwisie Mapy Google" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 62px; height: 26px; cursor: pointer;"><img src="./Wynajmij Wynajmę pokój_files/google_white2.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 62px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 157px; bottom: 0px; width: 144px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Dane mapy</a><span>Dane do Mapy ©2014 Google</span></div></div></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); -webkit-box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 102px; top: 62px; background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Dane mapy</div><div style="font-size: 13px;">Dane do Mapy ©2014 Google</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="./Wynajmij Wynajmę pokój_files/mapcnt3.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Dane do Mapy ©2014 Google</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; position: absolute; -webkit-user-select: none; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a href="http://www.google.com/intl/pl-PL_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Warunki korzystania z programu</a></div></div><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; display: none; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right;"><a target="_new" title="Zgłoś do Google błędy na mapie drogowej lub na zdjęciach." href="http://maps.google.com/maps?ll=54.194322,16.171491&z=15&t=m&hl=pl-PL&gl=US&mapclient=apiv3&skstate=action:mps_dialog$apiref:1&output=classic" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Zgłoś błąd w mapach</a></div></div><div class="gmnoprint" draggable="false" controlwidth="32" controlheight="84" style="margin: 5px; -webkit-user-select: none; position: absolute; left: 0px; top: 0px;"><div controlwidth="32" controlheight="40" style="cursor: url(http://maps.gstatic.com/mapfiles/openhand_8_8.cur) 8 8, default; position: absolute; left: 0px; top: 0px;"><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img src="./Wynajmij Wynajmę pokój_files/cb_scout2.png" draggable="false" style="position: absolute; left: -9px; top: -102px; width: 1028px; height: 214px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="./Wynajmij Wynajmę pokój_files/cb_scout2.png" draggable="false" style="position: absolute; left: -107px; top: -102px; width: 1028px; height: 214px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="./Wynajmij Wynajmę pokój_files/cb_scout2.png" draggable="false" style="position: absolute; left: -58px; top: -102px; width: 1028px; height: 214px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div style="width: 32px; height: 40px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="./Wynajmij Wynajmę pokój_files/cb_scout2.png" draggable="false" style="position: absolute; left: -205px; top: -102px; width: 1028px; height: 214px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></div><div class="gmnoprint" controlwidth="0" controlheight="0" style="opacity: 0.6; display: none; position: absolute;"><div title="Obróć mapę o 90 stopni" style="width: 22px; height: 22px; overflow: hidden; position: absolute; cursor: pointer;"><img src="./Wynajmij Wynajmę pokój_files/mapcnt3.png" draggable="false" style="position: absolute; left: -38px; top: -360px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></div><div class="gmnoprint" controlwidth="20" controlheight="39" style="position: absolute; left: 6px; top: 45px;"><div style="width: 20px; height: 39px; overflow: hidden; position: absolute;"><img src="./Wynajmij Wynajmę pokój_files/mapcnt3.png" draggable="false" style="position: absolute; left: -39px; top: -401px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div title="Powiększ" style="position: absolute; left: 0px; top: 2px; width: 20px; height: 17px; cursor: pointer;"></div><div title="Pomniejsz" style="position: absolute; left: 0px; top: 19px; width: 20px; height: 17px; cursor: pointer;"></div></div></div><div class="gmnoprint" style="margin: 5px; z-index: 0; position: absolute; cursor: pointer; right: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Pokaż mapę ulic" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 1px 6px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; border: 1px solid rgba(0, 0, 0, 0.14902); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 30px; font-weight: 500; background-color: rgb(255, 255, 255); background-clip: padding-box;">Mapa</div><div style="z-index: -1; padding-top: 2px; -webkit-background-clip: padding-box; border-width: 0px 1px 1px; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: rgba(0, 0, 0, 0.14902); border-bottom-color: rgba(0, 0, 0, 0.14902); border-left-color: rgba(0, 0, 0, 0.14902); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 20px; text-align: left; display: none; background-color: white; background-clip: padding-box;"><div draggable="false" title="Pokaż mapę ulic z terenem" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-top-left-radius: 1px; border-top-right-radius: 1px; border-bottom-right-radius: 1px; border-bottom-left-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="./Wynajmij Wynajmę pokój_files/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Teren</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Pokaż zdjęcia satelitarne" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 1px 6px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; border-width: 1px 1px 1px 0px; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-top-color: rgba(0, 0, 0, 0.14902); border-right-color: rgba(0, 0, 0, 0.14902); border-bottom-color: rgba(0, 0, 0, 0.14902); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 38px; background-color: rgb(255, 255, 255); background-clip: padding-box;">Satelita</div><div style="z-index: -1; padding-top: 2px; -webkit-background-clip: padding-box; border-width: 0px 1px 1px; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: rgba(0, 0, 0, 0.14902); border-bottom-color: rgba(0, 0, 0, 0.14902); border-left-color: rgba(0, 0, 0, 0.14902); -webkit-box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 20px; text-align: left; display: none; background-color: white; background-clip: padding-box;"><div draggable="false" title="Powiększ, aby wyświetlić widok pod kątem 45 stopni" style="color: rgb(184, 184, 184); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap; display: none; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(241, 241, 241); border-top-left-radius: 1px; border-top-right-radius: 1px; border-bottom-right-radius: 1px; border-bottom-left-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="./Wynajmij Wynajmę pokój_files/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">45°</label></div><div draggable="false" title="Pokaż zdjęcia z nazwami ulic" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 3px 8px 3px 3px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-top-left-radius: 1px; border-top-right-radius: 1px; border-bottom-right-radius: 1px; border-bottom-left-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="./Wynajmij Wynajmę pokój_files/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Etykiety</label></div></div></div></div></div></div>
                <script type="text/javascript" src="js/js"></script>
                <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
                    <script type="text/javascript">
                      var geocoder;
                      var map;
                      {if $adAttribute.attr_street }
                        var address = 'Polska, {$ad.region_str}, {$ad.city}, {$adAttribute.attr_street.value}';
                      {else}
                        var address = 'Polska, {$ad.region_str}, {$ad.city},';
                      {/if}
                      function initialize() {
                          geocoder = new google.maps.Geocoder();
                          var latlng = new google.maps.LatLng(52.40828351070693, 16.933321952819824);
                          var myOptions = {
                            zoom: 15,
                            center: latlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                          }
                          map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                        }
                          
                      $(document).ready(function() {
                        initialize();
                            geocoder.geocode( { 'address': address}, function(results, status) {
                              if (status == google.maps.GeocoderStatus.OK) {
                                map.setCenter(results[0].geometry.location);
                                var marker = new google.maps.Marker({
                                    map: map, 
                                    position: results[0].geometry.location
                                });
                              }
                            });
                      });
                      </script>
              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <div id="box-offer">
                <h4>Zapytaj o ogłoszenie:</h4>
                {if $ad.status neq 2}
                  <form method="post" class="np" action="ads/contact" id="formularz-kontaktowy">
                  <input type="hidden" name="aid" value="{$ad.aid}">
                  <input type="hidden" name="token" value="{$token}">
                {/if}
                {if $error}
                  {foreach from=$error item=err}
                    <div class="form-group">
                      <div class="error">{$err.0}</div>
                    </div>
                  {/foreach}
                {/if}
                {if $notice}
                  {foreach from=$notice item=ntc}
                    <div class="form-group">
                      <div class="success">{$ntc.0}</div>
                    </div>
                  {/foreach}
                {/if}
                  <div class="form-group">
                    <label>Telefon:</label>
                    {if $ad.status eq 2}
                      <input type="text" placeholder="Telefon" class="form-control" name="zapytanietel" disabled>
                    {else}
                      <input type="text" placeholder="Telefon" class="form-control" name="zapytanietel">
                    {/if}
                  </div>
                  <div class="form-group">
                    <label>Adres email:</label>
                    {if $ad.status eq 2}
                      <input type="email" placeholder="Adres email" class="form-control" name="zapytanieemail" disabled>
                    {else}
                      <input type="email" placeholder="Adres email" class="form-control" name="zapytanieemail">
                    {/if}
                  </div>
                  <div class="form-group">
                    <label>Treść wiadomości:</label>
                    {if $ad.status eq 2}
                      <textarea placeholder="Treść wiadomości" class="form-control" name="zapytanie" disabled></textarea>
                    {else}
                      <textarea placeholder="Treść wiadomości" class="form-control" name="zapytanie"></textarea>
                    {/if}
                  </div>
                  <div class="form-group">
                  {if $ad.status eq 2}
                    <button type="submit" class="button" disabled>Ta oferta wygasła</button>
                  {else}
                    <button type="submit" class="button">Wyślij wiadomość</button></form>
                  {/if}
                  </div>
                
              </div>

              <div id="box-blueDark">
                <h4>Lub zadzwoń:</h4>
                {if $ad.status eq 2}
                  <h3>Ta oferta wygasła</h3>
                {else}
                  <h2 class="a_offerTel">{$ad.phone_number|substr:0:3} <span class="button button-gray button-number">pokaż numer</span></h2>
                  <p>Wyświetleń numeru: {$ad.phone_views}</p>
                {/if}
              </div>
              <br/>
              <div id="box-blueDark">
                <!-- abuse -->
                  <div id="box-blueDark" class="abuseBox">
                  <h4>Zgłoś nadużycie w ogłoszeniu</h4>
                    <div class="form-group">
                      <textarea name="abuse-text" class="form-control-textarea" placeholder="Opisz nadużycie" required></textarea>
                    </div>
                    <div class="form-group">
                      <input type="text" name="abuse-email" class="form-control" placeholder="Twój adres email" value="{$user.email}" required>
                    </div>
                    <div class="form-group abuseRight">
                      <button type="submit" class="button button-light" id="abuse-close">zamknij</button> <button type="submit" class="button button-gray">zgłoś nadużycie</button>
                    </div>
                  </div>
                <!-- /abuse -->
                <ul class="ad_buttons">
                  <li class="ad_print"><a id="printbutton" href="{router->linkTo module=ads action=printad id=$ad.aid}">Drukuj</a></li>
                  <li class="ad_abuse"><a id="abuseLink" href="#">Zgłoś nadużycie</a></li>
                  {if $user.role > 0}
                    <li class="ad_watch"><a href="{router->linkTo module=ads action=watchad id=$ad.aid}">
                      {if $isWatched eq "no"}
                        Dodaj do obserwowanych
                      {else}
                        Usuń z obserwowanych
                      {/if}
                      </a></li>
                  {/if}
                </ul>
              </div>



            </div>
          </div>
          
          {if ($ad.auction eq 1) }
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div id="auction" class="bg-gray">
                <h4>Aukcja:</h4>
                
                
                {if $auction_id == 0}
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      Aukcja dla tego ogłoszenia jeszcze się nie rozpoczęła.
                    </div>
                  </div>
                {else}
                  {if $adAuction.min_price > 0}
                    {if $adAuction.current_bid < $adAuction.min_price}
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            Cena minimalna nie została osiągnięta<br/><br/>
                          </div>
                        </div>
                    {/if}
                  {/if}
                  
                  {if $user.role == 0}
                    <div class="row">
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <p>Aktualna cena:<span>{if $adAuction.current_bid eq 0} {$adAuction.start_price} {else} {$adAuction.current_bid} {/if} zł</span></p>
                      </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <p>Twoja oferta:
                        <input type="text" placeholder="{$adAuctionNextBid}" class="form-control" value="{$adAuctionNextBid}" name="bid"> zł
                        </p>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><button type="submit" class="button" disabled>Licytuj</button><span>Musisz być zalogowany aby licytować</span></div>
                    </div>
                  {else}
                    <form action="user/prebid/" method="post">
                    <input type="hidden" name="ad_id" value="{$ad.aid}">
                    <input type="hidden" name="ad_name" value="{$ad.title}">
                    <input type="hidden" name="ad_link" value="{$ad.link}">
                    <div class="row">
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <p>Aktualna cena:<span>{if $adAuction.current_bid eq 0} {$adAuction.start_price} {else} {$adAuction.current_bid} {/if} zł</span></p>
                      </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <p>Twoja oferta:
                        <input type="text" placeholder="{$adAuctionNextBid}" class="form-control" value="{$adAuctionNextBid}" name="bid"> zł
                        </p>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"><button type="submit" class="button">Licytuj</button><span></span></div>
                    </div>
                  {/if}
                {/if}
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table class="table">
                      <tr>
                        <th>Licytujący:</th>
                        <th class="price">Oferta:</th>
                        <th class="date">Data:</th>
                      </tr>
                      {if $adAuction.current_bid_id > 0}
                      <tr>
                        {if $adAuction.current_bid >= $adAuction.min_price}
                          <td><b>{$adAuctionWinner.email}</b> {if $user.role eq "99"} /{$adAuctionWinner.bidder_email}/{/if}</td>
                        {else}
                          <td>{$adAuctionWinner.email} {if $user.role eq "99"} /{$adAuctionWinner.bidder_email}/{/if}</td>
                        {/if}
                        <td class="price">
                            <b>{$adAuction.current_bid} zł</b> {if $user.role eq "99"} /{$adAuctionWinner.max_bid} zł/{/if}
                        </td>
                        <td class="date">{$adAuctionWinner.bid_date}</td>
                      </tr>
                      {/if}
                      {foreach $adBids as $key => $value}
                        {if $value.id neq $adAuction.current_bid_id}
                          {if $value.max_bid <= $adAuction.current_bid}
                            <tr>
                              <td>{$value.email} {if $user.role eq "99"} /{$value.bidder_email}/{/if}</td>
                              <td class="price">
                                  {$value.max_bid} zł
                              </td>
                              <td class="date">{$value.bid_date}</td>
                            </tr>
                           {/if}
                        {/if}
                      {/foreach}
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- /auction -->
          {/if}
        </div>
      </div>