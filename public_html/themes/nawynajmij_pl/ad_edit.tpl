      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>
<!-- content -->
<form action="{$site_url}user/doedit/" method="post">
<input type="hidden" name="token" value="{$token}">
<input type="hidden" name="edit" value="{$id}">
<table class="ads-table" width="100%">
  <thead class="ads">
    <tr>
       <th colspan="2">Edytuj ogłoszenie: {$adTitle}</th>
    </tr>
  </thead>
  <tbody>
    <tr>
       <td width="30%">Telefon kontaktowy:</td>
       <td><input type="text" class="form-control" name="tel" value="{$tel}"</td>
    </tr>
    <tr>
       <td width="30%">Email kontaktowy:</td>
       <td><input type="text" class="form-control" name="contactEmail" value="{$email}"></td>
    </tr>
    <tr>
       <td width="30%">Treść:</td>
       <td><textarea id="adText" name="adText" class="form-control">{$adText}</textarea></td>
    </tr>
    <tr>
       <td width="30%"></td>
       <td><button type="submit" class="button">Zapisz</button></form></td>
    </tr>
  </tbody>
</table>
<!-- /content -->
</div>
</div>
</div>
</div>