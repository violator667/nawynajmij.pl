  <body>
    <div id="all" class="homeSite">
      <header id="header">
        <div class="container">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6"><a href="{router->linkTo module=homepage action=index}" id="logo"><img src="themes/nawynajmij_pl/img/logo.png"></a></div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
              <div class="ico-tel"></div>
              <div id="questions">Masz pytania<br>Zadzwoń</div>
              <div id="tel-number">506 900 909</div>
            </div>
          </div>
        </div>
        <div id="baner-home" class="container-fluid">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <nav id="nav-top">
                  <div class="row">
                    <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                      <ul class="list-inline">
                                        <li class="active"><a href="{router->linkTo module=homepage action=index}">Strona główna</a>
                                        </li>
                                        <li><a href="{router->linkTo module=category action=show}">Nieruchomości</a>
                                        </li>
                                        <li><a href="{router->linkTo module=category action=show auction=1}">Aukcje</a>
                                        </li>
                                        <li><a href="{router->linkTo module=cms action=contact}">Kontakt</a>
                                        </li>
                      </ul>
                    </div>
                    <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 text-right">
                    {if $user.role > 0 }
                      <a href="{router->linkTo module=user action=index}" class="button login">{$user.username}</a>
                    {else}
                      <a href="{router->linkTo module=user action=index}" class="button login">Logowanie</a>
                      <a href="{router->linkTo module=user action=index}" class="button button-light">Dodaj ogłoszenie</a>
                    {/if}
                    {if $user.role > 0 }
                      <a href="{router->linkTo module=user action=logout}" class="button login">Wyloguj</a>
                      
                    {/if}
                      </div>
                  </div>
                </nav>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="formBlue">
                  <p>Wyszukaj</p>
                  <form class="row" method="post" action="category/search">
                    <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                      <input name="search" type="text" placeholder="Wpisz słowo, np. miasto" class="form-control" value="{$searchWord}">
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <button type="submit" class="button">Szukaj</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>