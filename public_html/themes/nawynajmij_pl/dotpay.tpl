      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            
<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>
<!-- content -->
<form action="https://ssl.dotpay.pl/" name="dotpay_form" id="dotpay_form" method="post">
<input type="hidden" name="id" value="{$CNF_DOTPAY_ID}" />
<input type="hidden" name="kwota" value="{$amount}" />
<input type="hidden" name="opis" value="Opłata za ogłoszenie #: {$aid} | zamówienie: {$order_id}" />
<input type="hidden" name="control" value="{$secret}" />
<input type="hidden" name="url" value="{$site_url}" />
<input type="hidden" name="urlc" value="{$site_url}dotpay/get/" />
<input type="hidden" name="przelewyonline" value="1" />
<center><h2>Transakcja w trakcie realizacji zostaniesz automatycznie przeniesiony na stronę DotPay</h2>Jeśli przekierowanie nie nastąpiło w ciągu 5 sekund<br/><br/>
<button type="submit" class="button">kliknij tutaj</button>
</form>
<!-- /content -->
</div>
</div>
</div>
</div>
<script>
jQuery(document).ready(function(){
      $("#dotpay_form").submit(); 
    });
</script>