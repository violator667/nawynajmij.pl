      <div class="container">
        <div class="row">
          <div id="content">
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            {include file="account_menu.tpl"}
            </div>
            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
              <div class="form-group">
              {if $error}
                {foreach from=$error item=err}
                  <div class="error">{$err.0}</div>
                {/foreach}
              {/if}
              {if $notice}
                {foreach from=$notice item=ntc}
                  <div class="success">{$ntc.0}</div>
                {/foreach}
              {/if}
              </div>

              <form class="advert" lang="pl" accept-charset="UTF-8" action="{router->linkTo module=ads action=add}" method="post" id="form-ogloszeniaDodaj">
              <input type="hidden" name="draftid" value="{$draftId}" id="draftid">
              <input type="hidden" name="selectedCategory" value="{$selectedCategory}" id="selectedCategory">
              <input type="hidden" name="selectedCategoryName" value="{$selectedCategoryName}" id="selectedCategoryName">
              <input type="hidden" name="formPhoto1" value="{$formPhoto1}" id="formPhoto1">
              <input type="hidden" name="formPhoto2" value="{$formPhoto2}" id="formPhoto2">
              <input type="hidden" name="formPhoto3" value="{$formPhoto3}" id="formPhoto3">
              <input type="hidden" name="formPhoto4" value="{$formPhoto4}" id="formPhoto4">
              <input type="hidden" name="formPhoto5" value="{$formPhoto5}" id="formPhoto5">
              {foreach from=$selectedSubCat item=selSub}
                {$count = $selSub@iteration}
                <input type="hidden" id="selCat{$count}" value="{$selSub}">
              {/foreach}
              <div class="error" id="formerror">Musisz wypełnić wszystkie pola oznaczone na czerwono</div>
                <div class="form-group">
                  <label>
                    <div class="selectCat" id="seleCatFirst"></div>
                  </label>
                </div>
                
                <div class="form-group">
                  <label>
                    <div class="row">
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">Wybierz kategorię<sup>*</sup></div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 starinfo"><sup>*</sup> pola obowiązkowe</div>
                    </div>
                  </label>
                  <select name="selectCategory1" id="selectCategory1" class="catSel form-control" required="required"></select>
                  <select name="selectCategory2" id="selectCategory2" class="catSel form-control"></select>
                  <select name="selectCategory3" id="selectCategory3" class="catSel form-control"></select>
                  <select name="selectCategory4" id="selectCategory4" class="catSel form-control"></select>
                  <select name="selectCategory5" id="selectCategory5" class="catSel form-control"></select>
                  <select name="selectCategory6" id="selectCategory6" class="catSel form-control"></select>
                    <!-- err info -->
                    <div id="#catId_err" class="errInfo">
                            <p>Musisz wybrać kategorię</p>
                    </div>
                    <!-- /err info -->
                </div>
                <div class="form-group">
                  <label>Województwo<sup>*</sup></label>
                  <select name="regionId" id="regionId" class="form-control">
                  <option></option>
                      <option value="1"{$regionId1}>dolnośląskie</option>
                      <option value="2"{$regionId2}>kujawsko-pomorskie</option>
                      <option value="3"{$regionId3}>lubelskie</option>
                      <option value="4"{$regionId4}>lubuskie</option>
                      <option value="5"{$regionId5}>łódzkie</option>
                      <option value="6"{$regionId6}>małopolskie</option>
                      <option value="7"{$regionId7}>mazowieckie</option>
                      <option value="8"{$regionId8}>opolskie</option>
                      <option value="9"{$regionId9}>podkarpackie</option>
                      <option value="10"{$regionId10}>podlaskie</option>
                      <option value="11"{$regionId11}>pomorskie</option>
                      <option value="12"{$regionId12}>śląskie</option>
                      <option value="13"{$regionId13}>świętokrzyskie</option>
                      <option value="14"{$regionId14}>warmińsko-mazurskie</option>
                      <option value="15"{$regionId15}>wielkopolskie</option>
                      <option value="16"{$regionId16}>zachodniopomorskie</option>
                      </select>
                    <!-- err info -->
                    <div id="#regionId_err" class="errInfo">
                            <p>Musisz wybrać województwo</p>
                    </div>
                    <!-- /err info -->
                </div>
                <div class="form-group">
                  <label>Miasto<sup>*</sup></label>
                  <input type="text" required="required" class="form-control" value="{$city}" name="city" id="city">
                  <!-- err info -->
                    <div id="#city_err" class="errInfo">
                            <p>Musisz wybrać miasto</p>
                    </div>
                    <!-- /err info -->
                </div>
                <div class="form-group">
                  <label>Dzielnica</label>
                  <input type="text" class="form-control" value="{$cityDistrict}" name="cityDistrict" id="cityDistrict">
                </div>
                <div class="form-group">
                  <label>Telefon kontaktowy<sup>*</sup></label>
                  <input type="text" required="required" class="form-control" value="{$tel}" type="text" name="tel" id="tel">
                  <!-- err info -->
                    <div id="#tel_err" class="errInfo">
                            <p>Musisz podać numer telefonu</p>
                    </div>
                    <!-- /err info -->
                </div>
                <div class="form-group">
                  <label>Email<sup>*</sup></label>
                  <input type="email" required="required" class="form-control" value="{$contactEmail}" type="text" name="contactEmail" id="contactEmail">
                  <!-- err info -->
                    <div id="#contactEmail_err" class="errInfo">
                            <p>Musisz podać prawidłowy adres email</p>
                    </div>
                    <!-- /err info -->
                </div>
                <div class="form-group">
                  <label>Cena w zł<sup>*</sup></label>
                  <input type="text" required="required" class="form-control" value="{$price}" name="price" id="price">
                  <!-- err info -->
                    <div id="#price_err" class="errInfo">
                            <p>Musisz podać cenę</p>
                    </div>
                    <!-- /err info -->
                </div>
                <div class="form-group">
                  <label>Cena za<sup>*</sup></label>
                  <select name="priceType" id="priceType" class="gray form-control">
                    <option value="0"></option>
                    <option value="1"{$priceSelect1}>godzinę</option>
                    <option value="2"{$priceSelect2}>dzień</option>
                    <option value="3"{$priceSelect3}>tydzień</option>
                    <option value="4"{$priceSelect4}>miesiąc</option>
                    <option value="5"{$priceSelect5}>kwartał</option>
                    <option value="6"{$priceSelect6}>pół roku</option>
                    <option value="7"{$priceSelect7}>rok</option>
                    <option value="8"{$priceSelect8}>m2</option>
                    <option value="9"{$priceSelect9}>całość</option>
                    <option value="10{$priceSelect10}">ha</option>
                  </select>
                  <!-- err info -->
                    <div id="#priceType_err" class="errInfo">
                            <p>Musisz wybrać</p>
                    </div>
                    <!-- /err info -->
                </div>
                
                <div id="attrPlaceHolder"></div>
                <div id="attrWrap">
                    {foreach from=$attr_array item=attr}
                      <div class="form-group attr">
                        <input type="hidden" id="a_{$attr.aid}" value="{$attr.value}">
                        <label>{$attr.name}<sup>*</sup></label>
                          <input class="form-control" value="{$attr.value}" type="text" name="{$attr.aid}" id="{$attr.aid}">
                           <!-- err info -->
                            <div id="{$attr.aid}_err" class="errInfo">
                            <p>To pole jest wymagane</p>
                            </div>
                          <!-- /err info -->
                      </div>
                    {/foreach}
                </div>
          
                <div class="form-group">
                  <label>Treść ogłoszenia<sup>*</sup></label>
                  <!-- err info -->
                  <div id="adText_err" class="errInfo">
                    <p>Musisz wpisać treść ogłoszenia</p>
                  </div>
                  <!-- /err info -->
                  <textarea required="required" class="form-control" id="adText" name="adText">{$adText}</textarea>
                </div>
                <div class="form-group">
                  <!-- tmp -->
                  <div class="info photoInfo" id="photoInfo">
                    <div class="info-top">
                      <div class="info-content">
                        <p class="awarded">JAK DODAĆ ZDJĘCIA?</p>
                        <p>Korzystając z przycisku "Wybierz zdjęcia" dodaj do ogłoszenia fotografie znajdujące się na Twoim komputerze. Maksymalnie można wstawić 5 zdjęć o wielkości nie przekraczające 1 MB każde. Zdjęcia muszą być zapisane w formacie .jpg, .png. W celu zmiany rozmiaru, rozdzielczości i formatu zdjęcia można posłużyć się programem IrfanView dostępnym za darmo w Internecie.</p>
                      </div>
                    </div>
                    <div class="info-end"></div>
                  </div>
                  <!-- tmp -->
                  <h5>Dodaj zdjęcie</h5><!-- <a href="#" class="button button-gray">Wybierz plik</a>-->
                  
                  <div id="uploadPlace"></div>
                </div>
                <div class="form-group">
                  <h5>Rodzaj ogłoszenia</h5>
                  <p>Możesz bardziej uwidocznić swoje ogłoszenie zaznaczając poniżej opcję wyróżnienia lub promowania, albo obie jednocześnie. Opcje są dodatkowo płatne. Koszt wyróżnienia i promowania można sprawdzić w cenniku.</p>
                </div>
                <div class="form-group checkbox">
                  <label><input type="checkbox" id="highlighted" name="highlighted" value="1"{$highlighted}> Wyróżnione kolorem<br>Ogłoszenie, którego tytuł na liście ogłoszeń jest pogrubiony i podświetlony innym kolorem.</label>
                </div>
                <div class="form-group checkbox">
                  <label><input type="checkbox" id="positioned" name="positioned" value="1"{$positioned}> Promowane<br>Ogłoszenie pozycjonowane na liście ogłoszeń i wyświetlane na stronie głównej serwisu Nawynajmij.pl.<br/><span class="quiet">Wymagane 1 zdjęcie.</span></label>
                </div>
                <div class="form-group checkbox">
                  <label><input type="checkbox" id="auction" name="auction" value="1"{$auction}> Aukcja<br>Ogłoszenie wraz z którym uruchomiona zostaje aukcja.</label>
                  <div class="auctionSpacer"></div>
                  <div class="info auctionInfo" id="auctionInfo">
                    <div class="info-top">
                      <div class="info-content">
                        <p>Wszystkie parametry aukcji takie jak cena startowa, cena minimalna będziesz mógł ustawić w menu "ogłoszenia wystawione" po publikacji ogłoszenia.</p>
                      </div>
                    </div>
                    <div class="info-end"></div>
                  </div>
                </div>
                <div class="form-group text-right">
                  <a class="button button-gray button-margin" href="{router->linkTo module=user action=draft delete=$draftId}">Wyczyść</a>
                  <button type="submit" class="button button-margin" id="save" value="Zapisz">Zapisz</button>
                  <button type="submit" class="button button-margin" name="nextstep" id="publish" value="Zachowaj do opłacenia">Zachowaj do opłacenia</button>
                    <div class="info saveInfo" id="saveInfo">
                      <div class="info-top">
                        <div class="info-content">
                          <p><div id="saveInfoMsg"></div></p>
                        </div>
                      </div>
                      <div class="info-end"></div>
                    </div>
                </div>
              </form>
              <div id="uploadForm">
                {include file="upload.tpl"}
              </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <div class="info">
                <div class="info-top">
                  <div class="info-content">
                    <p>Wszystko co wprowadzasz jest zapisywane w tle. Dzięki temu możesz w każdej chwili powrócić do wprowadzania ogłoszenia.</p>
                  </div>
                </div>
                <div class="info-end"></div>
              </div>
              <h5>Jak pisać ogłoszenia?</h5>
              <ol>
                <li>Tytuł ogłoszenia zostanie wygenerowany automatycznie na podstawie wybranej kategorii.</li>
                <li>Pola oznaczone czerwoną gwiazdką są obowiązkowe. Uzupełnij je odpowiednią treścią.</li>
                <li>Podaj cenę wynajmu oferowanego przedmiotu lub usługi oraz wybierz właściwe kryterium dotyczące rozliczania (np. za dobę, za m2).</li>
                <li>Treść ogłoszenia powinna zawierać informacje prawdziwe, dokładnie opisujące oferowany przedmiot pod wynajem lub usługę. Aby ogłoszenie było bardziej przejrzyste skorzystaj z edytora tekstu, wypunktuj i pogrub wybrane fragmenty, a każdą nową myśl zacznij od kolejnego akapitu. Dodaj zdjęcia, wtedy ogłoszenie będzie chętniej przeglądane. W trakcie pisania możesz skorzystać z podglądu ogłoszenia.</li>
                <li>W celu umożliwienia kontaktu możesz podać swój numer telefonu.</li>
              </ol>
              <!--
              <div class="info addfoto">
                <div class="info-top">
                  <div class="info-content">
                    <p class="awarded">JAK DODAĆ ZDJĘCIA?</p>
                    <p>Korzystając z przycisku "Wybierz zdjęcia" dodaj do ogłoszenia fotografie znajdujące się na Twoim komputerze. Maksymalnie można wstawić 5 zdjęć o wielkości nie przekraczające 1 MB każde. Zdjęcia muszą być zapisane w formacie .jpg, .png. W celu zmiany rozmiaru, rozdzielczości i formatu zdjęcia można posłużyć się programem IrfanView dostępnym za darmo w Internecie.</p>
                  </div>
                </div>
                <div class="info-end"></div>
              </div>
              -->
            </div>
          </div>
        </div>
      </div>
{if $validator_script}
<script>
  {$validator_script}
</script>
{/if}