      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1>{$categoryName}</h1>
          </div>
        </div>
        <div class="row">
          <div id="content">
            <div class="container">
              <div class="row">
                <div id="gal-list" class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                  
                  
                  <!-- ADS -->
                  {if $positionedAds}
                    {foreach from=$positionedAds item=ad}
                      {if $ad.highlighted eq 1}
                        <div class="row highlighted">
                      {else}
                        <div class="row">
                      {/if}
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><img src="{$ad.photo}" width="155px"></div>
                      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {if $ad.highlighted eq 1}
                              <h4>{if $ad.auction eq 1}<font color=red>[AUKCJA] </font>{/if}{$ad.title}</h4>
                            {else}
                              <h5>{if $ad.auction eq 1}<font color=red>[AUKCJA] </font>{/if}{$ad.title}</h5>
                            {/if}
                            <div class="title">{$ad.city}
                              <ul>
                                <li>{$ad.city_district}</li>
                                <li>Dodano: {$ad.created_date}</li>
                              </ul>
                              <p>{$ad.text_raw}</p>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="line"></div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <h6>
                            {if $ad.auction eq 1}
                              {if $ad.auction_array.current_bid >= $ad.price}
                                {$ad.auction_array.current_bid}
                              {else}
                                {$ad.price}
                              {/if}
                            {else}
                              {$ad.price}
                            {/if}
                            zł <small>{$ad.price_type_str}</small></h6>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}" class="rm">Zobacz całe ogłoszenie</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="lineDuble"></div>
                    {/foreach}   
                  {/if}
                  {if $normalAds}
                  {foreach from=$normalAds item=ad}
                      {if $ad.highlighted eq 1}
                        <div class="row highlighted">
                      {else}
                        <div class="row">
                      {/if}
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"><img src="{$ad.photo}" width="155px"></div>
                      <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            {if $ad.highlighted eq 1}
                              <h4>{$ad.title}</h4>
                            {else}
                              <h5>{$ad.title}</h5>
                            {/if}
                            <div class="title">{$ad.city}
                              <ul>
                                <li>{$ad.city_district}</li>
                                <li>Dodano: {$ad.created_date}</li>
                              </ul>
                              <p>{$ad.text_raw}</p>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="line"></div>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <h6>
                            {if $ad.auction eq 1}
                              {if $ad.auction_array.current_bid >= $ad.price}
                                {$ad.auction_array.current_bid}
                              {else}
                                {$ad.price}
                              {/if}
                            {else}
                              {$ad.price}
                            {/if}
                            zł <small>{$ad.price_type_str}</small></h6>
                          </div>
                          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right"><a href="{router->linkTo module=ads action=showad id={$ad.aid} link={$ad.link}}" class="rm">Zobacz całe ogłoszenie</a></div>
                        </div>
                      </div>
                    </div>
                    <div class="lineDuble"></div>
                    {/foreach}  
                  {/if}
                  <!-- /ADS -->
                  <nav class="text-right">
                    <ul class="pagination">
                      {if ($currentPage eq 1) || ($currentPage eq 0)}
                        {if $categoryId > 0}
                          <li class="disabled"><a href="{router->linkTo module=category action=show cid={$categoryId} page={$prevPage}}"><span>poprzednia strona</span></a></li>
                        {else}
                          <li class="disabled"><a href="{router->linkTo module=category action=show page={$prevPage}}"><span>poprzednia strona</span></a></li>
                        {/if}
                      {else}
                        {if $categoryId > 0}
                          <li><a href="{router->linkTo module=category action=show cid={$categoryId} page={$prevPage}}"><span>poprzednia strona</span></a></li>
                        {else}
                          <li><a href="{router->linkTo module=category action=show page={$prevPage}}"><span>poprzednia strona</span></a></li>
                        {/if}
                      {/if}
                      {if $currentPage lt $totalPages}
                        {if $categoryId > 0}
                          <li><a href="{router->linkTo module=category action=show cid={$categoryId} page={$nextPage}}"><span>następna strona</span></a></li>
                        {else}
                          <li><a href="{router->linkTo module=category action=show page={$nextPage}}"><span>następna strona</span></a></li>
                        {/if}
                      {else}
                        {if $categoryId > 0}
                          <li class="disabled"><a href="{router->linkTo module=category action=show cid={$categoryId} page={$nextPage}}"><span>następna strona</span></a></li>
                        {else}
                          <li class="disabled"><a href="{router->linkTo module=category action=show page={$nextPage}}"><span>następna strona</span></a></li>
                        {/if}
                      {/if}
                    </ul>
                  </nav>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                  <div id="right">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {include file="menucat.tpl"}
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="box-blue">
                          <h2>Porównaj kredyty</h2>
                          <h4>Skorzystaj z kalkulatora</h4>
                          <p>Oblicz swoje raty</p><a href="#" class="button button-light">Sprawdź</a>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="box-add">
                          <h6>Miejsce na reklamę</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
