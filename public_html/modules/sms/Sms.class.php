<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Sms.class.php
 * Version	:	1.0
 *
 * Info		:	Module Test
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class Sms {


	public function __construct() {
		
	}
	
	public function __destruct() {
	
	}
	
	public function checkCode( $code )
	{
			$query = "SELECT id FROM ".DB_PREFIX."_sms WHERE code = :code AND used = 0 LIMIT 0,1";
			$params = array(":code" => $code);
			$exec = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
			if( count($exec) == 1 ) {
				return $exec[0][id];
			}else{
				return false;
			}
	}
	
	public function useCode ( $id )
	{
			$query = "UPDATE ".DB_PREFIX."_sms SET used = 1, used_date = NOW() WHERE id = :id";
			$params = array(":id" => array( (int) $id, PDO::PARAM_INT ));
			$exec = Db::getConnection()->putQuery( $query, $params);
	}
	
}
?>
