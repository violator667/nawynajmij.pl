<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	SmsController.class.php
 * Version	:	1.0
 *
 * Info		:	Sms Module Controller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	03.09.2014
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class SmsController Extends BasicController {

  protected $access = 0;	
  protected $access_exceptions = array('actionIndex');
  
	public function __construct() {
		parent::__construct();
	}
	
	
	public function actionIndex()
	{
		registry::storeObject( 'sms','sms' );
		$code_id = registry::getObject( 'sms' )->checkCode( registry::getObject( 'input' )->post[code] );
		if( $code_id > 0 ) {
			registry::getObject( 'sms' )->useCode( $code_id );
				registry::storeObject( 'order','order' );
				$order = registry::getObject( 'order' );
				$order->setOrderId( registry::getObject( 'input' )->post[order_id] );
				$order->setOrderType( 'sms' );
				$order->getOrderData();
				$order->updateStatus( '2' );
				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało aktywowane.' , '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/type/active' );
		}else{
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Błędny kod SMS.' , '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/type/topay' );
		}
	}
	
	
}
?>
