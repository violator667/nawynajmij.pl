<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	  AdsView.class.php
 * Version	:	  1.0
 *
 * Info		  :	  Ads view
 *
 * Author  	: 	Michał‚ Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  01.09.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class AdsView Extends BasicView {
	public function __construct( $input )
	{
		parent::__construct( $input );
		$this->viewMakeDisplay();
	}
  }
?>
 