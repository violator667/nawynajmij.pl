<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	  Ads.class.php
 * Version	:	  1.0
 *
 * Info		  :	  Main ads class 
 *
 * Author  	: 	Michał‚ Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  01.09.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class Ads {
  
  /* get CNF_ADS_MAIN_PAGE number of positioned ads */
   public function getRandomPositionedAds( )
   {
	   registry::storeObject( 'auction', 'auction' );
	   $objAuction = registry::getObject( 'auction' );

    $query = "SELECT * FROM ".DB_PREFIX."_ad WHERE status = :status AND positioned = :positioned AND positioned_expires > :today_now AND expires > :today_now ORDER BY RAND() LIMIT 0, :limit";
    $params = array(":status" => array( (int) 1, PDO::PARAM_INT ) , ":positioned" => array( (int) 1, PDO::PARAM_INT ), ":today_now" => date("Y-m-d H:i:s"), ":limit" => array( (int) registry::getSetting( 'CNF_ADS_MAIN_PAGE' ), PDO::PARAM_INT ) );
	
    $result = Db::getConnection()->getQuery( $query, $params );
	
	/* iterate via $result and handle some data */
	for( $i=0; $i<count($result); $i++ ) {
			/* get price type name */
			$result[$i][price_type_str] = $this->getPriceTypeById( $result[$i][price_type] );
			
			/* manage date to be more polish :) */
			$result[$i][created_date] = date("d-m-Y",strtotime( $result[$i][created]) );
			$result[$i][created_datetime] = date("d-m-Y H:i:s",strtotime( $result[$i][created]) );
			$result[$i][expires_date] = date("d-m-Y",strtotime( $result[$i][expires]) );
			$result[$i][expires_datetime] = date("d-m-Y H:i:s",strtotime( $result[$i][expires]) );
			$result[$i][positioned_expires_date] = date("d-m-Y",strtotime( $result[$i][positioned_expires]) );
			$result[$i][positioned_expires_datetime] = date("d-m-Y H:i:s",strtotime( $result[$i][positioned_expires]) );
			
			/* get rid of html tags & cut text */
	 		$result[$i][text_raw] = strip_tags($result[$i][text],'<br>');
	 		if(strlen($result[$i][text_raw])>registry::getSetting( 'CNF_AD_CUT' ) ) {
	 			$result[$i][text_raw] = substr($result[$i][text_raw], 0, strpos($result[$i][text_raw],' ', registry::getSetting( 'CNF_AD_CUT' )));
	 			$result[$i][text_raw].= " [...]"; 
	 		}
			
			/* get photos */
			registry::storeObject( 'ad','ad' );
			$ad = registry::getObject( 'ad' );
			$photos  = $ad->getAdPhotos( $result[$i][aid] );
			if( count($photos)>0 ) {
					$result[$i][photo] = registry::getSetting( 'CNF_SITE_URL' ).'upload/thumb/'.$photos[0][file];
			}else{
					$result[$i][photo] = registry::getSetting( 'CNF_SITE_URL' ).'themes/'.registry::getSetting( 'CNF_THEME' ).'/img/icons/camera.png';
			}

			/* get auction data */
			if($result[$i]['auction']=="1") {
				$objAuction->getAuctionIdByAdId($result[$i][aid]);
				$result[$i]['auction_array']['current_bid'] = $objAuction->auction['current_bid'];
			}
	}
	#echo "<pre>";
	#print_r($result);
	#echo "</pre>";
    return $result;
   } //end getRandomPositionedAds()
   
   public function getSimilarAds( $howMany, $category_id, $city )
   {
   	$query = "SELECT aid,title,link FROM ".DB_PREFIX."_ad WHERE status = 1 AND city = :city AND category_id = :cid ORDER BY aid DESC LIMIT 0,$howMany";
   	$params = array(":city" => $city, ":cid" => array( (int) $category_id, PDO::PARAM_INT) );
   	$result = Db::getConnection()->getQuery($query,$params);
   	return $result;
   }
	
	public function getRegionById( $rid )	
	{
		$query = "SELECT * FROM ".DB_PREFIX."_region WHERE rid = :rid LIMIT 0,1";
	   $params = array( ":rid" => array( (int) trim($rid), PDO::PARAM_INT ));
	   $result = Db::getConnection()->getQuery( $query, $params );
	   if( $result ) {
	   	return $result[0];
	   }else{
	   	/* no data so return empty string */
	   	return '';
	   }
	}
	
	public function sendContact( $aid,$from_email,$text,$phone )
	{
			registry::storeObject( 'ad', 'ad' );
			$ad = registry::getObject( 'ad' );
			try{
				$ad->getAdById( $aid );
				$email = $ad->email;
				$query = "SELECT * FROM ".DB_PREFIX."_email_template WHERE name = :template_name ORDER BY id DESC LIMIT 0,1";
				$params = array( ":template_name" => "AD_CONTACT" );
				$result = Db::getConnection()->getQuery( $query, $params );
											
				$markers = array("{{email}}","{{content}}","{{title}}","{{phone}}");
				$change = array($from_email,$text,$ad->title,$phone);
				$email_body = str_replace($markers,$change,$result[0][content]);
				
				$Semail = new Mail();
				$Semail->SingleTo = true;
				$Semail->addAddress($email);
				$Semail->AddReplyTo($from_email);
				$Semail->Subject = 'Kontakt w sprawie ogłoszenia '.$ad->title;
				$Semail->AltBody = 'Open in HTML mode';
				$Semail->MsgHTML($email_body);
				$Semail->Send();
				$Semail->ClearAddresses();
				return true;
			}catch(AdException $e) {
				return false;
			}
	}
	
	public function getPriceTypes()
	{
		$query = "SELECT * FROM ".DB_PREFIX."_price_types";
	   $params = array( );
	   $result = Db::getConnection()->getQuery( $query, $params );
	   if( $result ) {
	   	return $result;
	   }else{
	   	return false;
	   }
	}
	
   public function getPriceTypeById( $pid )
   {
	   $query = "SELECT type FROM ".DB_PREFIX."_price_types WHERE pid = :pid LIMIT 0,1";
	   $params = array( ":pid" => array( (int) trim($pid), PDO::PARAM_INT ));
	   $result = Db::getConnection()->getQuery( $query, $params );
	   if( $result ) {
	   	return $result[0][type];
	   }else{
	   	/* no data so return empty string */
	   	return '';
	   }
   } //end getPriceTypeById()
   
	   
   
   public function getUserAds( $user_id, $type, $numOnly = NULL )
   {
   	if( $type == "TO_PAY" ) {
   			if( $numOnly == "true" ) {
   				$query = "SELECT COUNT(aid) as count  FROM ".DB_PREFIX."_ad WHERE user_id = :user_id AND status = 0";
   				$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT) );
   				$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
   			}else{
   				$query = "SELECT * FROM ".DB_PREFIX."_ad WHERE user_id = :user_id AND status = 0";
   				$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT) );
   				$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
   				foreach( $result as $key => $value ) {
   					$payment = registry::getObject( 'user' )->getPaymentOptions( $value[highlighted], $value[positioned], $value[auction] );
   					/*
   					echo "<pre>";
   					print_r($payment);
   					echo "</pre>";
   					*/
   					if( registry::getObject( 'user' )->subscription == "1" && $value[auction]!=1 ) {
   						if( date("Y-m-d H:i:s",time()) < registry::getObject( 'user' )->subscription_end ) {
   							$payment[] = array("id" => "subscription", "type" => "subscription", "days" => "30", "price" => "0.00");
   						}
   					}
   					$result[$key][payment_methods] = $payment;
   				}
   				return $result;
   			}
   			
   	}elseif($type == "ENDED") {
   			if( $numOnly == "true" ) {
   				$query = "SELECT COUNT(aid) as count  FROM ".DB_PREFIX."_ad WHERE user_id = :user_id AND status = 2 AND expires < :date_now ";
   			}else{
   				$query = "SELECT * FROM ".DB_PREFIX."_ad WHERE user_id = :user_id AND status = 2 AND expires < :date_now ";
				}   			
   			$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT), ":date_now" => date("Y-m-d H:i:s") );
   	}elseif($type == "WATCHED") {
   			if( $numOnly == "true" ) {
   				$query = "SELECT COUNT(id) as count  FROM ".DB_PREFIX."_ad_watch WHERE user_id = :user_id";
   				$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT));
   				$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
   			}else{
   				$query = "SELECT * FROM ".DB_PREFIX."_ad_watch WHERE user_id = :user_id";
   				$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT));
   				$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
   				
   				if( $result ) {
   					
   					$ads = array();
   					foreach( $result as $key => $value ) {
   						$q = "SELECT * FROM ".DB_PREFIX."_ad WHERE aid= :aid";
   						$id = $value[ad_id];
   						$p = array(":aid" => array( (int) $id, PDO::PARAM_INT ) );
   						$r = Db::getConnection()->getQuery($q, $p, 0, 1, 1);
   						$ads[] = $r;
   					}
   					
   					/* we must flatten array */
   					foreach( $ads as $key => $value) {
   							$new_ads[$key] = $value[0];
   					}
   					return $new_ads;
   				}else{
   					return array();
   				}
				}   			
   			
   	}else{
   		//active
   			if( $numOnly == "true" ) {
   				$query = "SELECT COUNT(aid) as count  FROM ".DB_PREFIX."_ad WHERE user_id = :user_id AND status = 1 AND expires > :date_now ";
   				$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT), ":date_now" => date("Y-m-d H:i:s") );
   				$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
					
   				return $result[0][count];
   			}else{
   				$query = "SELECT * FROM ".DB_PREFIX."_ad WHERE user_id = :user_id AND status = 1 AND expires > :date_now ";
   				$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT), ":date_now" => date("Y-m-d H:i:s") );
   				$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
   				foreach( $result as $key => $value ) {
   					$payment = registry::getObject( 'user' )->getPaymentOptions( $value[highlighted], $value[positioned], $value[auction] );
   					if( registry::getObject( 'user' )->subscription == "1" ) {
   						if( date("Y-m-d H:i:s",time()) < registry::getObject( 'user' )->subscription_end ) {
   							$payment[] = array("id" => "subscription", "type" => "subscription", "days" => "30", "price" => "0.00");
   						}
   					}
   					$result[$key][payment_methods] = $payment;
   				}
   				return $result;
   			}
   			
   				
   	}
   	if($type == "ENDED" ) {
   		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
   	}
   	
   	if( $numOnly == "true" ) {
   		return $result[0][count];
   	}else{
   		return $result;
   	}
   }
   
  } //end Class
?>
