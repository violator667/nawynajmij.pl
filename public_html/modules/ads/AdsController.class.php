<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	  AdsController.class.php
 * Version	:	  1.0
 *
 * Info		  :	  Ads controller
 *
 * Author  	: 	Michał‚ Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  31.08.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class AdsController Extends BasicController {

  protected $access = 0;	
  protected $access_exceptions = array();
  
	public function __construct() {
		parent::__construct();
	}
	
  public function actionIndex()
  {
  		Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
  }	
  
  public function actionPreview()
  {
  		$this->setViewParam( 'templates',array('header.tpl','top.tpl','ad.tpl','footer.tpl') );

    	$this->cookieError( 'ads' );
    	$this->cookieNotice( 'ads' );
    	$link = registry::getObject( 'router' )->linkTo( array( "module" => "ads", "action" => "preview", "id" => $ad->aid ));
      registry::getObject( 'breadcrumb' )->addElement( array("link" => "$link", "name" => "Podgląd ogłoszenia" ) );
    	
		$user = registry::getObject( 'user' );
		$draft = $user->getDraft( registry::getObject( 'input' )->params[ 'id' ] );
		
		if( !empty($draft) ) {
				$draft_data = unserialize($draft[0][data]);
				/* category menu */
      		registry::storeObject( 'category','category' );
      		$parent  = registry::getObject( 'category' )->getParent( $draft_data[selectedCategory] );
     			$children = registry::getObject( 'category' )->getChildren( $parent[0][cid] );
      		$this->setViewParam( 'offerDetail', $draft_data[selectedCategory] );
      		$this->setViewParam( 'subCategories', $children ); //children
      		$this->setViewParam( 'subParent', $parent ); //parent
      		$this->setViewParam( 'activeCategory', $draft_data[selectedCategory] );
      		$f  = registry::getObject( 'ads')->getRegionById( $draft_data[regionId] );
      		$adArray[region_str] = $f[region];
      		$draft_data[region] = $f[region];
      		$title = registry::getObject( 'category' )->makeTitle( $draft_data, $draft_data[selectedCategory] );
      		
      		$this->setViewParam( 'title', $title );
      		$adArray[title] = $title;
      		$adArray[price] = $draft_data[price];
      		$adArray[city] = $draft_data[city];
      		$adArray[phone_number] = $draft_data[tel];
      		$adArray[city_district] = $draft_data[cityDistrict];

      		$adArray[text] = $draft_data[adText];
      		$adArray[ad_views] = 0;
      		$adArray[phone_views] = 0;
      		$adArray[aid] = 0;
      		
      		/* photos */
      		$adPhotos[] = array( "file" => $draft_data[formPhoto1]);
      		$adPhotos[] = array( "file" => $draft_data[formPhoto2]);
      		$adPhotos[] = array( "file" => $draft_data[formPhoto3]);
      		$adPhotos[] = array( "file" => $draft_data[formPhoto4]);
      		$adPhotos[] = array( "file" => $draft_data[formPhoto5]);
      		foreach( $adPhotos as $key => $value )
				{
						if( empty($value[file]) ) {
							unset($adPhotos[$key]);
						}
				}
				
				$attributes = registry::getObject( 'category' )->getRequiredAttributes( $draft_data[selectedCategory] );
				//echo "<pre>";
				//print_r($attributes);
				//echo "</pre>";
			/* fill attributes with data from the draft */
				$new_attributes = array();
				if( is_array($attributes)) {
				foreach( $attributes as $key => $value )
				{
					$value[value] = $draft_data[$key];
					$new_attributes[$key] = $value;
				}
				}
				//echo "<pre>";
				//print_r($new_attributes);
				//echo "</pre>";
      		$this->setViewParam( 'adPhotos', $adPhotos );
      		$this->setViewParam( 'adAttribute', $new_attributes );
      		$this->setViewParam( 'ad', $adArray );
      		//$this->setViewParam( 'adAttribute', $ad->adAttribute );
      		//$this->setViewParam( 'adPhotos', $ad->getAdPhotos( $ad->aid ) );
      
      registry::getObject( 'category' )->getRequiredAttributes( $ad->category_id );
		}
    	//echo "<pre>";
		//print_r($draft_data);
		//echo "</pre>";
    	$this->loadView( 'Ads' );
  }
  
  public function isAdWatched( $ad_id, $user_id ) 
  {
  		if( $user_id > 1 ) {
  			$query = "SELECT id FROM ".DB_PREFIX."_ad_watch WHERE ad_id = :ad_id AND user_id = :user_id";
  			$params = array(":ad_id" => array( (int) $ad_id, PDO::PARAM_INT ),":user_id" => array( (int) $user_id, PDO::PARAM_INT ) );
  			$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
  			if( $result ) {
  				/* this user is watching this ad */
  				return true;
  			}else{
  				return false;
  			}
  		}
  }
  
  public function actionWatchAd()
  {
  		if( registry::getObject( 'user' )->id > 1 ) {
  				$params = array(":ad_id" => array( (int) registry::getObject( 'input' )->params[id], PDO::PARAM_INT ),":user_id" => array( (int) registry::getObject( 'user' )->id, PDO::PARAM_INT ) );
  				if( $this->isAdWatched( registry::getObject( 'input' )->params[id], registry::getObject( 'user' )->id ) ) {
  						$query = "DELETE FROM ".DB_PREFIX."_ad_watch WHERE ad_id = :ad_id AND user_id = :user_id";
  						registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało usunięte z obserowanych.', '60', '/' );
  						parent::redirect( registry::getObject( 'router' )->linkTo(array("module" => "ads", "action" => "showad", "id" => registry::getObject( 'input' )->params[id], "link" => "usun-z-obserowanych") ) );
  				}else{
  						$query = "INSERT INTO ".DB_PREFIX."_ad_watch (ad_id, user_id) VALUES (:ad_id, :user_id)";
  						registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało dodane do obserowanych.', '60', '/' );
  						parent::redirect( registry::getObject( 'router' )->linkTo(array("module" => "ads", "action" => "showad", "id" => registry::getObject( 'input' )->params[id], "link" => "dodaj-do-obserowanych") ) );
  				}
  				$result = Db::getConnection()->putQuery($query, $params);
  		}
  }  
  
  public function actionShowad()
  {
    $this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','ad.tpl','footer.tpl') );

    $this->cookieError( 'ads' );
    $this->cookieNotice( 'ads' );

    try{
      registry::storeObject( 'ad','ad' );
     	registry::getObject( 'ad' )->getAdById( registry::getObject( 'input' )->params[ 'id' ] );
      $ad = registry::getObject( 'ad' );
      /* got ad object */
      /* breadcrumb */
      $ad_categories = $ad->getCategoriesArray();

      for($c=0; $c<count( $ad_categories ); $c++ ) {
      	$link = registry::getObject( 'router' )->linkTo( array( "module" => "category", "action" => "show", "cid" => $ad_categories[$c][cid], "link" => $ad_categories[$c][link] ));
      	registry::getObject( 'breadcrumb' )->addElement( array("link" => "$link", "name" => $ad_categories[$c][name]) );
      }
      $link = registry::getObject( 'router' )->linkTo( array( "module" => "ads", "action" => "showad", "id" => $ad->aid, "link" => $ad->link ));
      registry::getObject( 'breadcrumb' )->addElement( array("link" => "$link", "name" => $ad->title ) );
      
      /* category menu */
      registry::storeObject( 'category','category' );
      $parent  = registry::getObject( 'category' )->getParent( $ad->category_id );
      $children = registry::getObject( 'category' )->getChildren( $parent[0][cid] );
      $ad->addAdView( $ad->aid );
      $this->setViewParam( 'offerDetail', $ad->aid );
      $this->setViewParam( 'subCategories', $children ); //children
      $this->setViewParam( 'subParent', $parent ); //parent
      $this->setViewParam( 'activeCategory', $ad->category_id );
      $this->setViewParam( 'title', $ad->title );
      $this->setViewParam( 'tplTitle', $ad->title );
      $this->setViewParam( 'ad', $ad->adArray );
      $this->setViewParam( 'adAttribute', $ad->adAttribute );
      $this->setViewParam( 'adPhotos', $ad->getAdPhotos( $ad->aid ) );
		$ad_c = array_reverse($ad_categories);

		$this->setViewParam( 'adCategoryId', $ad_c[0][cid]);
		$this->setViewParam( 'adCategoryName', $ad_c[0][name]);
		$this->setViewParam( 'adCategoryLink', $ad_c[0][link]);
      if( $this->isAdWatched( $ad->aid, registry::getObject( 'user' )->id ) ) {
      	$watched = "yes";
      }else{
      	$watched = "no";
      }
      $this->setViewParam( 'isWatched', $watched );
      registry::getObject( 'category' )->getRequiredAttributes( $ad->category_id );

		//get auction
		if($ad->auction == "1") {
			registry::storeObject('auction','auction');
			$objAuction = registry::getObject('auction');
			$objAuction->getAuctionIdByAdId($ad->aid);

			$this->setViewParam( 'auction_id', $objAuction->auction_id );
			$this->setViewParam( 'auction_end', $objAuction->ended );
			if($objAuction->auction['current_bid']>0) {
				$next_bid = $objAuction->auction['current_bid']+1;
			}else{
				$next_bid = $objAuction->auction['start_price'];
			}
			$this->setViewParam( 'adAuction', $objAuction->auction );
			$this->setViewParam( 'adBids', $objAuction->bids);
			$this->setViewParam( 'adAuctionWinner', $objAuction->getWinner());
			$this->setViewParam( 'adAuctionNextBid', $next_bid);
		}

      /* get similar ads (if ended) */
      if($ad->status == "2" ) {
      	$similar = registry::getObject( 'ads' )->getSimilarAds( 3, $ad->category_id, $ad->city );
      	if( count($similar) > 0 ) {
      		$this->setViewParam( 'similar', $similar );
      	}
      }
     
      

    }catch( AdException $e) {
    	registry::getObject( 'cookie' )->extended_setcookie( 'error', $e->getMessage(), '60', '/' );
		Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
    }
	
    $this->loadView( 'Ads' );
  }
  ####
  public function actionPrintad()
  {
    $this->setViewParam( 'templates',array('header.tpl','print_ad.tpl') );
	 $this->setViewParam( 'print','print' );
    $this->cookieError( 'ads' );
    $this->cookieNotice( 'ads' );

    try{
      registry::storeObject( 'ad','ad' );
     	registry::getObject( 'ad' )->getAdById( registry::getObject( 'input' )->params[ 'id' ] );
      $ad = registry::getObject( 'ad' );
      /* got ad object */
      /* breadcrumb */
      $ad_categories = $ad->getCategoriesArray();

      for($c=0; $c<count( $ad_categories ); $c++ ) {
      	$link = registry::getObject( 'router' )->linkTo( array( "module" => "category", "action" => "show", "cid" => $ad_categories[$c][cid], "link" => $ad_categories[$c][link] ));
      	registry::getObject( 'breadcrumb' )->addElement( array("link" => "$link", "name" => $ad_categories[$c][name]) );
      }
      $link = registry::getObject( 'router' )->linkTo( array( "module" => "ads", "action" => "showad", "id" => $ad->aid, "link" => $ad->link ));
      registry::getObject( 'breadcrumb' )->addElement( array("link" => "$link", "name" => $ad->title ) );
      
      /* category menu */
      registry::storeObject( 'category','category' );
      $parent  = registry::getObject( 'category' )->getParent( $ad->category_id );
      $children = registry::getObject( 'category' )->getChildren( $parent[0][cid] );
      $ad->addAdView( $ad->aid );
      $this->setViewParam( 'offerDetail', $ad->aid );
      $this->setViewParam( 'subCategories', $children ); //children
      $this->setViewParam( 'subParent', $parent ); //parent
      $this->setViewParam( 'activeCategory', $ad->category_id );
      $this->setViewParam( 'title', $ad->title );
      $this->setViewParam( 'ad', $ad->adArray );
      $this->setViewParam( 'adAttribute', $ad->adAttribute );
      $this->setViewParam( 'adPhotos', $ad->getAdPhotos( $ad->aid ) );
      if( $this->isAdWatched( $ad->aid, registry::getObject( 'user' )->id ) ) {
      	$watched = "yes";
      }else{
      	$watched = "no";
      }
      $this->setViewParam( 'isWatched', $watched );
      registry::getObject( 'category' )->getRequiredAttributes( $ad->category_id );
      
      /* get similar ads (if ended) */
      if($ad->status == "2" ) {
      	$similar = registry::getObject( 'ads' )->getSimilarAds( 3, $ad->category_id, $ad->city );
      	if( count($similar) > 0 ) {
      		$this->setViewParam( 'similar', $similar );
      	}
      }
     
      

    }catch( AdException $e) {
    	registry::getObject( 'cookie' )->extended_setcookie( 'error', $e->getMessage(), '60', '/' );
		Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
    }
	
    $this->loadView( 'Ads' );
  }
  ####
  public function actionContact()
  {
  		$input = registry::getObject( 'input' );
  		$router = registry::getObject( 'router' );
  		$validator = registry::getObject( 'validator' );
  		$offer_id = $input->post['aid'];
  		if( !empty($offer_id) ) {
  			if( $validator->isIntPositive( array( "item" => $offer_id ) ) ) {  				$route = array("module" => "ads", "action" => "showad", "id" => $offer_id, "idName" => "id", "link" => "formualrz-kontaktowy");
  				$route = array("module" => "ads", "action" => "showad", "id" => $offer_id, "link" => "formualrz-kontaktowy");
  				if( !empty($input->post['zapytanie']) ) {
	  				if( !empty($input->post['zapytanieemail'])) {
	  					if( $validator->isEmail( array( "item" => $input->post['zapytanieemail'] )) ) {
	  							$phone = $input->post['zapytanietel'];
	  							$text = $input->post['zapytanie'];
	  							$from_email = $input->post['zapytanieemail'];
	  							$aid = $offer_id;
	  							if( registry::getObject( 'ads' )->sendContact( $aid,$from_email,$text,$phone ) ) {
	  								registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Zapytanie zostało wysłane.' , '60', '/');			
									$this->redirect( $router->linkTo( $route ) );
	  							}else{
	  								registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wystąpił błąd podczas wysyłki.' , '60', '/');			
									$this->redirect( $router->linkTo( $route ) );
	  							}
						}else{
							registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Błędny adres email.' , '60', '/');			
							$this->redirect( $router->linkTo( $route ) );
						}
	  				}else{
	  					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie podano adresu email.' , '60', '/');			
						$this->redirect( $router->linkTo( $route ) );
	  				}
  				}else{
  					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie podano treści zapytania.' , '60', '/');			
					$this->redirect( $router->linkTo( $route ) );
  				}
  			}else{
  				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie wybrano ogłoszenia do kontaktu.' , '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL') );
  			}
  		}
  } 
  
  public function actionPhone()
  {
  		$this->setViewParam( 'templates',array('phone_json.tpl') );
  		try{
      registry::storeObject( 'ad','ad' );
     	registry::getObject( 'ad' )->getAdById( registry::getObject( 'input' )->params[ 'id' ] );
     	
     	$ad = registry::getObject( 'ad' );
     	$ad->addPhoneView( $ad->aid ); 
		$phone = json_encode( array("phone_number" => $ad->phone_number) );
     	$this->setViewParam( 'phone_json', $phone );
     	$this->loadView( 'Ads' );
     }catch( AdException $e ) {
     }
  		
  }  
  
  public function noAccess()
  {
  }
}
?>
