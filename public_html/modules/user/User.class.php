<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	User.class.php
 * Version	:	1.1
 *
 * Info		:	Module User
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	07.09.2013
 * Modify : 20.09.2014 - last change: MySQL queries via PDO
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class User Extends BasicUser {

  /**
   * 	Store parameters that was chanded
  **/
  protected $changed_array = array(); 

	public function __construct() {
		parent::__construct();

		$this->id = registry::getObject( 'session' )->getUserId();
		$this->getUser();
	}
	
	public function __destruct() {
		$save_query = "UPDATE ".DB_PREFIX."_users SET ";
		if( registry::getObject( 'session' )->getUserId()!=1 ) {
			if(count($this->changed_array)>0) {
			foreach( $this->changed_array as $value ) {
				$save_query.=$value."='".$this->$value."', ";
			}
			$save_query = substr( $save_query, 0,-2 );
			$save_query.= " WHERE id='".registry::getObject( 'session' )->getUserId()."'";
			Db::getConnection()->runQuery( $save_query );
			}
		}
	}
	
	public function setField( $field, $value ) {
		if( $this->$field!=$value ) {
			array_push( $this->changed_array, $field );
			$this->$field = $value;
		}
	}
	
	public function getField( $field ) {
		return $this->$field;
	}


	/*
	*	Returns access level
	*/	
	public function isLoggedIn() {
		if( registry::getObject( 'session' )->getUserId()=="1" ) {
			return 0;
		}else{
			return registry::getObject( 'session' )->getLogged();
		}
	}
	
	public function checkHash( $inputHash )
	{
		if( $inputHash == $this->hash ) {
			return true;
		}else{
			return false;
		}
	}
	
	
	public function loginValidation()
	{
		/**
		 *	Abstract layer for another method of login (ex. google or facebook)
		 *
		**/
		return $this->loginSystemValidation();
	}
	
	/**
	 * 	Validates login data.
	 *	Returns FALSE if user/password don't match or user ID if it does
	**/

	public function loginSystemValidation()
	{
		$password = md5(WDSALT.registry::getObject( 'input' )->post[ 'password' ]);
		
		if( registry::getSetting( 'CNF_LOGIN_METHOD' )=="username" ) {
      /* not supported in this app */
		}else{
			$email = registry::getObject( 'input' )->post[ 'email' ];
     
       $query = "SELECT id FROM ".DB_PREFIX."_users WHERE status = :status AND email = :email AND password = :password";
       $params = array( ":status" => array( (int) 1, PDO::PARAM_INT ), ":email" => "$email", ":password" => "$password" );
       $result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1 );
		}
		
		if( count($result)>0 ) {
			$this->id = $result[0][ 'id' ];
			$this->getUser();
			return $result[0][ 'id' ];
		}else{
			return FALSE;
		}
		
	}
	/* 
	public function confirmEmailChange()
	{
		if( !empty( registry::getObject( 'input' )->params['hash'] ) && !empty( registry::getObject( 'input' )->params['address'] ) ) {
			$email = registry::getObject( 'input' )->params['address'];
			$hash = registry::getObject( 'input' )->params['hash'];
			$query = "SELECT * FROM ".DB_PREFIX."_user_change_email WHERE email='$email' AND hash='$hash'";
			$exec = Db::getConnection()->runQuery( $query );
			if( $exec->num_rows!=0 ) {
				$result = $exec->fetch_array( MYSQL_ASSOC );
				$update  = "UPDATE ".DB_PREFIX."_users SET email='$email' WHERE id='$result[user_id]'";
				$exec_update = Db::getConnection()->runQuery( $update );
				
				// clean any old request for this user 
				$clean_query = "DELETE FROM ".DB_PREFIX."_user_change_email WHERE user_id='$result[user_id]'";
				$exec_clean = Db::getConnection()->runQuery( $clean_query );
				return TRUE;
			}else{
				return FALSE;
			}
		
		}else{
			return FALSE;
		}
	}
	*/
	
	/*
	public function changeEmail( $email )
	{
		$email = trim( $email );
		if( $this->isEmailUsed( $email )===TRUE ) {
			$hash = md5( WDSALT.time().$email.time() );
			$query = "INSERT INTO ".DB_PREFIX."_user_change_email (user_id,hash,email) VALUES ('$this->id','$hash','$email')";
			$exec = Db::getConnection()->runQuery( $query );
			
			// send email 
			$get_email_body = Db::getConnection()->runQuery( "SELECT * FROM ".DB_PREFIX."_email_template WHERE name='EMAIL_CHANGE'" );
			if($get_email_body->num_rows!=0) {
				$result = $get_email_body->fetch_array( MYSQL_ASSOC );
			}					
			$markers = array("{{email}}","{{site}}","{{verif_link}}");
			$change = array($email,registry::getSetting( 'CNF_SITE_URL' ),registry::getSetting( 'CNF_SITE_URL' ).'user/emailchangeverification/hash/'.$hash.'/address/'.$email);
			$email_body = str_replace($markers,$change,$result[content]);								
			$Semail = new Mail();
			$Semail->SingleTo = true;
			$Semail->addAddress($email);
			$Semail->Subject = 'Zmiana adresu email.';
			$Semail->AltBody = 'Open in HTML mode';
			$Semail->MsgHTML($email_body);
			$Semail->Send();
			$Semail->ClearAddresses();
			//registry::getObject( 'notice' )->storeNotice( 'user',language::lang('user','User','registrationOk') );
			//registry::getObject( 'cookie' )->extended_setcookie( 'notice', language::lang('user','User','emailChangeOk'), '60', '/' );
			//Header("Location:  ".registry::getSetting('CNF_SITE_URL').'/user/settings' );
			return TRUE;
		}else{
			return FALSE;
		}
	}
	*/
	
	public function isEmailUsed( $email ) 
	{
		/*
		*	Check if email is used,
		*	returns FALSE if email is TAKEN or in error 
		*/
		
		$query = "SELECT id FROM ".DB_PREFIX."_users WHERE email = :email";
		$params = array( ":email" => $email );
		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1 );
		
		if( count( $result ) > 0 ) {
      return false;
		}else{
      return true;
		}
	}	
	
	public function registration( $username = NULL, $email, $password, $rep_password )
	{
		if( ( $username == NULL ) && ( registry::getSetting( 'CNF_LOGIN_METHOD' )=="username" )  ) {
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nazwa użytkownika jest wymagana' , '60', '/');
			Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
		}else{
			if( registry::getSetting( 'CNF_LOGIN_METHOD' )!="username" ) {
					if( $this->isEmailUsed( $email )===TRUE ) {
						if( $password == $rep_password ) {
							$hash = md5(WDSALT.rand(0,1000).time());
							if( $CNF_EMAIL_STATUS==0 ) {
								$status = 0;
								/*
								*	Provide email verification
								*/
								
								$query = "SELECT * FROM ".DB_PREFIX."_email_template WHERE name = :template_name ORDER BY id DESC LIMIT 0,1";
								$params = array( ":template_name" => "EMAIL_VERIF" );
								$result = Db::getConnection()->getQuery( $query, $params );
											
								$markers = array("{{email}}","{{site}}","{{verif_link}}");
								$change = array($email,registry::getSetting( 'CNF_SITE_URL' ),registry::getSetting( 'CNF_SITE_URL' ).'user/registerverification/hash/'.$hash.'/address/'.$email);
								$email_body = str_replace($markers,$change,$result[0][content]);								
								$Semail = new Mail();
								$Semail->SingleTo = true;
								$Semail->addAddress($email);
								$Semail->Subject = 'Weryfikacja adresu email';
								$Semail->AltBody = 'Open in HTML mode';
								$Semail->MsgHTML($email_body);
								$Semail->Send();
								$Semail->ClearAddresses();
								//registry::getObject( 'notice' )->storeNotice( 'user',language::lang('user','User','registrationOk') );
								registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Rejestracja zakończona pomyślnie', '60', '/' );
								Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
							}else{
								$status = 1;
							}
							$username = NULL;
							$password = md5(WDSALT.$password);
							$role = 1;
							$banned = 0;
							$registration = date("Y-m-d H:i:s",time());
							$this->registerUser( $username, $email, $password, $status, $role, $banned, $registration, $hash );
						}else{
							//registry::getObject( 'error' )->storeError( 'user',language::lang('user','User','passwordDontMatch') );
							registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Hasła nie są jednakowe' , '60', '/');
							Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
						}
					}else{
						//registry::getObject( 'error' )->storeError( 'user',language::lang('user','User','emailUsed') );
						registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wybrany email znajduje się już w systemie.' , '60', '/');
						Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
					}
			}else{
				//registry::getObject( 'error' )->storeError( 'user',language::lang('user','User','usernameRequired') );
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nazwa użytkownika jest wymagana' , '60', '/');
				Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
			}
		}
			
	}
	
	public function getPaymentOptions( $high, $posit, $auction )
	{
			$query = "SELECT * FROM ".DB_PREFIX."_ad_price WHERE highlighted = :high AND positioned = :posit AND auction = :auction ORDER BY days ASC";
			$params = array(":high" => array( (int) $high, PDO::PARAM_INT ),
							":posit" => array( (int) $posit, PDO::PARAM_INT ),
							":auction" => array( (int) $auction, PDO::PARAM_INT ) );
			$result = Db::getConnection()->getQuery($query, $params );
			if( $result ) {
				return $result;
			}else{
				return array();
			}
	}
	
	public function getPaymentMethod( $id )
   {
  		$query = "SELECT * FROM ".DB_PREFIX."_ad_price WHERE id = :id";
  		$params  = array(":id" => array( (int) $id, PDO::PARAM_INT ) );
  		$result = Db::getConnection()->getQuery($query,$params);
  		return $result;
   }
	
	public function deleteDraft()
	{
		$draft = $this->getLastDraft();
		$draft_id = $draft[0][draft_id];
		
		//$query = "DELETE FROM ".DB_PREFIX."_draft WHERE draft_id = :draft_id AND user_id = :user_id";
		$query = "DELETE FROM ".DB_PREFIX."_draft WHERE user_id = :user_id";
		//$params = array(":draft_id" => array( (int) $draft_id, PDO::PARAM_INT ), ":user_id" => array( (int) $this->id, PDO::PARAM_INT ));
		$params = array(":user_id" => array( (int) $this->id, PDO::PARAM_INT ));
		$result = Db::getConnection()->putQuery($query,$params);
		return true;
	}	
	
	public function getDraft( $draft_id )	
	{
			$query = "SELECT * FROM ".DB_PREFIX."_draft WHERE draft_id = :draft_id AND user_id = :user_id ORDER BY draft_id DESC LIMIT 0,1";
			$params = array(":draft_id" => array($draft_id, PDO::PARAM_INT), ":user_id" => array( (int) $this->id, PDO::PARAM_INT ) );
			$result = Db::getConnection()->getQuery( $query, $params, 0, 1,1 );
			
			return $result;
			
	}
	
	public function makeDraft()	
	{
			$query = "INSERT INTO ".DB_PREFIX."_draft (user_id,save_time) VALUES (:user_id,:save_time)";
			$params = array(":user_id" => array( (int) $this->id, PDO::PARAM_INT ), "save_time" => date("Y-m-d H:i:s") );
			$result = Db::getConnection()->putQuery($query,$params);
	}
	
	public function getLastDraft()
	{
		$query = "SELECT * FROM ".DB_PREFIX."_draft WHERE user_id = :user_id ORDER BY draft_id DESC LIMIT 0,1";
		$params = array(":user_id" => array( (int) $this->id, PDO::PARAM_INT ) );
		$result = Db::getConnection()->getQuery( $query, $params, 0, 1,1 );
		if( $result ) {
			return $result;
		}else{
			$this->makeDraft();
			$this->getLastDraft();
		}
	}
	
	public function saveDraft( $draft_id )
	{
		$draft_data = registry::getObject( 'input' )->post;
		$draft_data = serialize($draft_data);
		$query = "UPDATE ".DB_PREFIX."_draft SET data = :data, save_time = :save_time WHERE draft_id = :draft_id";
		$params = array(":draft_id" => array( (int) $draft_id, PDO::PARAM_INT ), ":data" => $draft_data, ":save_time" => date("Y-m-d H:i:s"));
		$result = Db::getConnection()->putQuery($query,$params);
		
		echo "<pre>";
		//print_r( registry::getObject( 'input' )->post );
		print_r($_POST);
		echo "</pre>";
	}
	
	public function takeOneSubscriptionPoint( $point_type )
	{
		$query  = "UPDATE ".DB_PREFIX."_users SET ";
		if( $point_type == "mix" ) {
				$query.= "free_mix=free_mix-1 ";
		}elseif( $point_type == "posit" ) {
				$query.= "free_positioned=free_positioned-1 ";
		}elseif( $point_type == "high" ) {
				$query.= "free_highlighted=free_highlighted-1 ";
		}else{
			/* standard */
				$query.= "free_standard=free_standard-1 ";
		}
		$query.= "WHERE id = :id";
		$params = array( ":id" => array( (int) $this->id, PDO::PARAM_INT ));
		$exec = Db::getConnection()->putQuery($query,$params);
	}

	public function registrationVerification()
	{
		$hash = registry::getObject( 'input' )->params[ 'hash' ];
		$useremail = registry::getObject( 'input' )->params[ 'address' ];
		
		$query = "SELECT id FROM ".DB_PREFIX."_users WHERE email= :useremail AND hash= :hash AND status = :status";
		//$query = "SELECT id FROM ".DB_PREFIX."_users WHERE email = :useremail ";
		$params  = array( ":useremail" => $useremail, ":hash" => $hash, ":status" => array( 0, PDO::PARAM_INT ) );
		//$params  = array( ":useremail" => $useremail);
		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
		if( count( $result ) > 0 ) {
      	$update = "UPDATE ".DB_PREFIX."_users SET status = :status WHERE id = :id";
    	  	$id  = $result[0][id];
      	$params = array( ":status" => array( (int) 1, PDO::PARAM_INT ), ":id" => array( (int) $id, PDO::PARAM_INT) );
     	 	$sth = Db::getConnection()->putQuery( $update, $params );
     	 	registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Weryfikacja zakończona pomyślnie', '60', '/' );
			Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
		}else{
      	registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wystąpił błąd podczas weryfikacji' , '60', '/');
			Header("Location:  ".registry::getSetting('CNF_SITE_URL') );
		}
		
	}

    public function getUserAuctionsBids($user_email)
    {
        $query = "SELECT auction_id FROM ".DB_PREFIX."_bid WHERE bidder_email = :bidderemail GROUP by auction_id";
        $params  = array( ":bidderemail" => $user_email);
        $bids = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
        $inQuery = '';
        foreach($bids as $bid ) {
            $inQuery.= "id = ".$bid['auction_id']." OR ";
        }
        $inQuery = substr($inQuery, 0, -4);

        $query_1 = "SELECT * FROM ".DB_PREFIX."_auction WHERE ".$inQuery;

        $result = Db::getConnection()->runQuery($query_1);

        $result = $result->fetchAll();
        return $result;
    }
	
	public function passwordRemiderChanger($hash)
	{
		/* check hash */
		$query_1 = "SELECT user_id FROM ".DB_PREFIX."_lost_pass WHERE hash = :hash AND expires > NOW() LIMIT 0,1";
		$params_1 = array(":hash" => $hash);
		$result_1 = Db::getConnection()->getQuery($query_1,$params_1, 0, 1, 1);
		#echo "<pre>";
		#print_r($result_1);
		#echo "</pre>";
		if( count($result_1) > 0 ) {
			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    		$pass = array(); //remember to declare $pass as an array
    		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    		for ($i = 0; $i < 8; $i++) {
        		$n = rand(0, $alphaLength);
        		$pass[] = $alphabet[$n];
   		 }
    			$new_pass = implode($pass); //turn the array into a string
    			$new_pass_hash = md5(WDSALT.$new_pass);
    			
    			/* change password */
    			$query_2 = "UPDATE ".DB_PREFIX."_users SET password = :pass WHERE id = :user_id";
    			$params_2 = array(":pass" => $new_pass_hash, ":user_id" => array( (int) $result_1[0][user_id], PDO::PARAM_INT));
    			$result_2 = Db::getConnection()->putQuery($query_2,$params_2);
    			/* send email */
    			$query_3 = "SELECT email FROM ".DB_PREFIX."_users WHERE id = :user_id";
    			$params_3 = array(":user_id" => array( (int) $result_1[0][user_id], PDO::PARAM_INT));
    			$result_3 = Db::getConnection()->getQuery($query_3,$params_3,0, 1, 1);
    			$email = $result_3[0][email];
    			#echo "<pre>";
				#print_r($result_3);
				#echo "</pre>";
    			###
				$query = "SELECT * FROM ".DB_PREFIX."_email_template WHERE name = :template_name ORDER BY id DESC LIMIT 0,1";
				$params = array( ":template_name" => "USER_LOST_PASS_NEW" );
				$result = Db::getConnection()->getQuery( $query, $params );
											
				$markers = array("{{pass}}");
				$change = array($new_pass);
				$email_body = str_replace($markers,$change,$result[0][content]);								
				$Semail = new Mail();
				$Semail->SingleTo = true;
				$Semail->addAddress($email);
				$Semail->Subject = 'Twoje nowe hasło';
				$Semail->AltBody = 'Open in HTML mode';
				$Semail->MsgHTML($email_body);
				$Semail->Send();
				$Semail->ClearAddresses();
				###
				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Nowe hasło zostało wysłane na Twój email.' , '60', '/');
				Header("Location:  ".registry::getSetting('CNF_SITE_URL').'user/index/' );
		}else{
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wystąpił błąd podczas weryfikacji' , '60', '/');
			Header("Location:  ".registry::getSetting('CNF_SITE_URL').'user/index/' );
		}
	}
	
	public function passwordRemindSender($email)
	{
		/* check email */
		$email = trim($email);
		
		$query_1 = "SELECT id FROM ".DB_PREFIX."_users WHERE email = :email AND status = 1 LIMIT 0,1";
		$params_1 = array(":email" => $email);
		$result_1 = Db::getConnection()->getQuery($query_1,$params_1, 0, 1, 1);
		#echo "<pre>";
		#print_r($result_1);
		#echo "</pre>";
		if(count($result_1) > 0) {
			$hash = md5(WDSALT.time().$email);
			$query_2 = "INSERT INTO ".DB_PREFIX."_lost_pass (ip,date,user_id,hash,expires) VALUES (:ip,NOW(),:user_id,:hash,(CURDATE() + INTERVAL 7 DAY))";
			$params_2 = array(":ip" => registry::getObject('input')->server[REMOTE_ADDR],
									":user_id" => array( (int) $result_1[0][id], PDO::PARAM_INT),
									":hash" => $hash);
		
			$result_2 = Db::getConnection()->putQuery($query_2,$params_2);
			###
			$query = "SELECT * FROM ".DB_PREFIX."_email_template WHERE name = :template_name ORDER BY id DESC LIMIT 0,1";
			$params = array( ":template_name" => "USER_LOST_PASS" );
			$result = Db::getConnection()->getQuery( $query, $params );
											
			$markers = array("{{email}}","{{date}}","{{ip}}","{{link}}");
			$change = array($email,date("d-m-Y H:i:s"),registry::getObject('input')->server[REMOTE_ADDR],registry::getSetting( 'CNF_SITE_URL' ).'user/lostpass/hash/'.$hash.'/');
			$email_body = str_replace($markers,$change,$result[0][content]);								
			$Semail = new Mail();
			$Semail->SingleTo = true;
			$Semail->addAddress($email);
			$Semail->Subject = 'Odzyskiwanie hasła';
			$Semail->AltBody = 'Open in HTML mode';
			$Semail->MsgHTML($email_body);
			$Semail->Send();
			$Semail->ClearAddresses();
			###
			registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Procedura odzyskiwania hasła rozpoczęta - sprawdź swoją skrzynkę email aby poznać szczegóły.' , '60', '/');
			Header("Location:  ".registry::getSetting('CNF_SITE_URL').'user/index/' );
		}else{
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Podano nieprawidłowy lub niepotwierdzony adres email.' , '60', '/');
			Header("Location:  ".registry::getSetting('CNF_SITE_URL').'user/index/' );
		}
	}
}
?>
