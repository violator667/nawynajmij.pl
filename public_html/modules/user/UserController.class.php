<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	UserController.class.php
 * Version	:	1.0
 *
 * Info		:	User Module Controller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	09.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class UserController Extends BasicController {

  protected $access = 1;	
  protected $access_exceptions = array('actionLostpass','actionLostpassstart','actionIndex','actionLoginuser','actionLogin','actionLogout','actionRegister','actionRegisterverification','actionEmailchangeverification');
  
 
	public function __construct() {
		parent::__construct();
	}
	
	public function noAccess()
	{
		registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie masz uprawnień do wyświetlenia tej strony' , '60', '/');
		$this->redirect( registry::getSetting('CNF_SITE_URL') );
	}
	
	public function errorAndNotice()
	{
		/* Cookie Error */
		$cookie_error = registry::getObject( 'cookie' )->get( 'error' ); 
		if( !empty( $cookie_error ) ) {
			registry::getObject( 'error' )->storeError( 'user', $cookie_error );
			registry::getObject( 'cookie' )->unsetcookie( 'error' );
		}
		/* Cookie Notice */
		$cookie_notice = registry::getObject( 'cookie' )->get( 'notice' ); 
		if( !empty( $cookie_notice ) ) {
			registry::getObject( 'notice' )->storeNotice( 'user', $cookie_notice );
			registry::getObject( 'cookie' )->unsetcookie( 'notice' );
		}
	}
	
	public function actionAddelete()
	{
		registry::storeObject( 'ad','ad' );
		$ad = registry::getObject( 'ad' );
		$ad->getAdById( registry::getObject( 'input' )->params[id] );
		if( $ad->isOwner( ) ) {
			if( $ad->status==0 ) {
				$ad->removeAd( registry::getObject( 'input' )->params[id] );
				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało usunięte z systemu.' , '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/type/topay/' );
			}else{
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie można usunąć tego ogłoszenia. <b>Uwaga:</b>Jeżeli dopiero zmieniałeś status ogłoszenia musisz odczekać około 5 minut przed usunięciem ogłoszenia z systemu.' , '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/type/topay/' );
			}
		}else{
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie masz uprawnień do usunięcia tego ogłoszenia.' , '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/type/topay/' ); 
		}
	}
	
	public function actionAdedit()
	{
		$this->errorAndNotice();
		$this->getAccountAdsNumbers();
		$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','ad_edit.tpl','footer.tpl') );
		$this->setViewParam( 'title', 'Edytuj ogłoszenie' );
		$this->setViewParam( 'tplTitle', 'Edytuj ogłoszenie' );
		$this->setViewParam( 'addAd', 'true' );
		registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Twoje konto") );
		registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Ogłoszenia") );
		registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/adedit/id/".registry::getObject( 'input' )->params[id], "name" => "Edytuj ogłoszenie") );
		
		registry::storeObject( 'ad','ad' );
		$ad = registry::getObject( 'ad' );
		$ad->getAdById( registry::getObject( 'input' )->params[id] );
		$email = $ad->email;
		$tel = $ad->phone_number;
		$text = $ad->text;
		$adTitle = $ad->title;
		if( $ad->isOwner( ) ) {
			$this->setViewParam( 'email', $email );
			$this->setViewParam( 'tel', $tel );
			$this->setViewParam( 'adText', $text );
			$this->setViewParam( 'adTitle', $adTitle );
			$this->setViewParam( 'id', registry::getObject( 'input' )->params[id] );
		}
		
		$this->loadView( 'User' );
	}
	
	public function actionDoedit()
	{
		if( registry::getObject( 'input' )->post[edit] ) {
				$validations = array(
										'contactEmail' => 'email',
										'tel' => 'not_empty',
										'adText' => 'not_empty');
				$required = array(
										'tel',
										'contactEmail',
										'adText');
										
				$validator = new Formvalidator($validations, $required, array());
				
				$form_data[contactEmail] = registry::getObject( 'input' )->post[contactEmail];
				$form_data[tel] = registry::getObject( 'input' )->post[tel];
				$form_data[adText] = registry::getObject( 'input' )->post[adText];

				if($validator->validate($form_data)) {
					registry::storeObject( 'ad','ad' );
					$ad = registry::getObject( 'ad' );
					$ad->getAdById( registry::getObject( 'input' )->post[edit] );
					if( $ad->isOwner( ) ) {
						
					
						$data[aid] = registry::getObject( 'input' )->post[edit];
 						$data[field] = "email";
 						$data[value] = array( $form_data[contactEmail], PDO::PARAM_STR );
 						$data[placeholder] = ":email";
						$ad->updateField( $data ); 
					
						$data[aid] = registry::getObject( 'input' )->post[edit];
 						$data[field] = "phone_number";
 						$data[value] = array( $form_data[tel], PDO::PARAM_STR );
 						$data[placeholder] = ":phone_number";
						$ad->updateField( $data );
						
						$data[aid] = registry::getObject( 'input' )->post[edit];
 						$data[field] = "text";
 						$data[value] = array( $form_data[adText], PDO::PARAM_STR );
 						$data[placeholder] = ":ad_text";
						$ad->updateField( $data );
					
						registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało zmienione. Zmiany będą widoczne w przeciągu kilku minut.' , '60', '/');
						$this->redirect( registry::getSetting('CNF_SITE_URL').'user/adedit/id/'.$data[aid] ); 
					}else{
						$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/type/active' ); 
					}
				}else{
					//$this->setViewParam( 'validator_script',$validator->getScript());
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wprowadź poprawne dane.' , '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'user/adedit/id/'.registry::getObject( 'input' )->post[edit] ); 
				}
			
		}
	}
	
	public function actionPayment()
	{
		$this->errorAndNotice();
		$this->getAccountAdsNumbers();
		$user = registry::getObject( 'user' );
		$method = $user->getPaymentMethod( registry::getObject( 'input' )->post[payment_method] );
		
		if( $method[0][type] == "sms" ) {
			$title = "Oplata za ogłoszenie # ".registry::getObject( 'input' )->post[aid];
			registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Twoje konto") );
			registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/ads/type/topay", "name" => $title ) );
			$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','sms.tpl','footer.tpl') );
			$this->setViewParam( 'title', $title );
			$this->setViewParam( 'tplTitle', $title );
			$this->setViewParam( 'amount', $method[0][price]);
			$this->setViewParam( 'aid', registry::getObject( 'input' )->post[aid] );
			registry::storeObject( 'order','order' );
			$order = registry::getObject( 'order' );
			$order->setOrderType( 'sms' );
			$order_id = $order->placeOrder();
			$this->setViewParam( 'order_id', $order_id );
			//echo $order_id;
		}else{
			if( registry::getObject( 'input' )->post[payment_method] != "subscription" ) {
				registry::storeObject( 'order','order' );
				$order = registry::getObject( 'order' );
				$order_id = $order->placeOrder();
				//die( $order_id );
				$order->setOrderId( $order_id );
				$order->getOrderData();
				$secret = $order->returnSecret();
				$title = "Oplata za ogłoszenie # ".registry::getObject( 'input' )->post[aid];
				registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Twoje konto") );
				registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/ads/type/topay", "name" => $title ) );
				$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','dotpay.tpl','footer.tpl') );
				$this->setViewParam( 'title', $title );
				$this->setViewParam( 'tplTitle', $title );
				$this->setViewParam( 'amount', $method[0][price] );
				$this->setViewParam( 'secret', $secret );
				$this->setViewParam( 'aid', registry::getObject( 'input' )->post[aid] );
				$this->setViewParam( 'order_id', $order_id );
			}else{
				registry::storeObject( 'order','order' );
				$user  = registry::getObject( 'user' );
				$inp = registry::getObject( 'input' );
				$order = registry::getObject( 'order' );
				$order->setOrderType( 'subscription' );
				if( ($user->subscription == "1") && ($user->subscription_end > date("Y-m-d H:i:s",time() ) ) ) {
					/* check ad options */
					if( $inp->post[high]=="1" && $inp->post[posit]=="1" ) {
						/* mix */
						if( $user->free_mix > 0 ) {
							$user->takeOneSubscriptionPoint( 'mix' );
							$order_id = $order->placeOrder();
							$order->setOrderId( $order_id );
							$order->getOrderData();
							$order->updateStatus( '2' );
							registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało aktywowane.' , '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/active' );	
						}else{
							registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wyczerpałeś limit dla tego typu ogłoszeń.' , '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/topay' );
						}
					}elseif( $inp->post[high]=="0" && $inp->post[posit]=="1" ) {
						/* posit */
						if( $user->free_positioned > 0 ) {
							$user->takeOneSubscriptionPoint( 'posit' );
							$order_id = $order->placeOrder();
							$order->setOrderId( $order_id );
							$order->getOrderData();
							$order->updateStatus( '2' );
							registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało aktywowane.' , '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/active' );	
						}else{
							registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wyczerpałeś limit dla tego typu ogłoszeń.' , '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/topay' );
						}
					}elseif( $inp->post[high]=="1" && $inp->post[posit]=="0" ) {
						/* high */
						if( $user->free_highlighted > 0 ) {
							/* take one point */
							$user->takeOneSubscriptionPoint( 'high' );
							$order_id = $order->placeOrder();
							$order->setOrderId( $order_id );
							$order->getOrderData();
							$order->updateStatus( '2' );
							registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało aktywowane.' , '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/active' );	
						}else{
							registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wyczerpałeś limit dla tego typu ogłoszeń.' , '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/topay' );
						}
					}else{
						/* standard */
						if( $user->free_standard > 0 ) {
							$user->takeOneSubscriptionPoint( 'standard' );
							$order_id = $order->placeOrder();
							$order->setOrderId( $order_id );
							$order->getOrderData();
							$order->updateStatus( '2' );
							registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało aktywowane.' , '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/active' );	
						}else{
							registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wyczerpałeś limit dla tego typu ogłoszeń.' , '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/topay' );
						}
					}
					
				}else{
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie posiadasz abonamentu.' , '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'user/ads/topay' );
				}
			}
		}
		$this->loadView( 'User' );
	}	
	
	public function actionSettings()
	{
		$this->errorAndNotice();
		$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','user_settings.tpl','footer.tpl') );
		$this->setViewParam( 'title', 'Zmień hasło' );
		$this->setViewParam( 'tplTitle', 'Zmień hasło' );
		registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Twoje konto") );
		registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/settings", "name" => "Zmień hasło") );
		$this->getAccountAdsNumbers();
		$this->loadView( 'User' );
	}
	
	public function actionUpdatePassword()
	{
		if( !empty( registry::getObject( 'input' )->post['current_pass'] ) && !empty( registry::getObject( 'input' )->post['new_pass'] ) && !empty( registry::getObject( 'input' )->post['new_pass2'] ) ) {
			
			$old_pass = registry::getObject( 'input' )->post['current_pass'];
			$new_pass = registry::getObject( 'input' )->post['new_pass'];
			$new_pass2 = registry::getObject( 'input' )->post['new_pass2'];
			
			if( $new_pass!=$new_pass2 ) {
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Hasła nie są jednakowe.' , '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'user/settings' );
			}else{
				$old_pass = md5(WDSALT.$old_pass);
				if( $old_pass == registry::getObject( 'user' )->getField( 'password' ) ) {
					$new_pass = md5(WDSALT.$new_pass2);
					registry::getObject( 'user' )->setField( 'password',$new_pass );
					registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Hasło zostało zmienione.' , '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'user/settings' );
				}else{
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Obecne hasło jest nieprawidłowe. '.$old_pass , '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'user/settings' );
				} 
			}
			
		}else{
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz wypełnić wszystkie pola.' , '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'user/settings' );
		}
	}
	
	public function actionUpdateEmail()
	{
		if( !empty( registry::getObject( 'input' )->post['email'] ) ) {
			if( registry::getObject( 'input' )->post['email']!=registry::getObject( 'user' )->getField( 'email' ) ) {
				if( registry::getObject( 'user' )->changeEmail( registry::getObject( 'input' )->post['email'] )!=FALSE ) {
					registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Na nowy adres email został wysłany link potwierdzający zmianę.' , '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'user/settings' );
				}else{
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wystąpił błąd podczas zmiany adresu email.' , '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'user/settings' );
				}
			}else{
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać inny adres email.' , '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'user/settings' );
			}
		}else{
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać adres email.' , '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'user/settings' );
		}
	}
	
	/**
	 * User authentication
	**/
	public function actionLogin()
	{
   
		if( registry::getObject( 'session' )->getLogged()=="0" ) {
			$validation = registry::getObject( 'user' )->loginValidation();
			registry::getObject( 'user' )->getField( 'role' );
			if( $validation != FALSE ) {
				registry::getObject( 'session' )->updateUserId( $validation );
				registry::getObject( 'session' )->setLoggedIn( registry::getObject( 'user' )->getField( 'role' ) );
				registry::getObject( 'user' )->setField( 'last_login', date("Y-m-d H:i;s") );
				parent::redirect( registry::getSetting( 'CNF_SITE_URL' ).registry::getObject( 'input' )->post[ 'redirect' ] );
			}else{
				if( !empty( registry::getObject( 'input' )->post[ 'login' ] ) ) {
            registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Podano nieprawidłowy email lub hasło' , '60', '/');
					if( empty( registry::getObject( 'input' )->post[ 'redirect' ] ) ) {
						parent::redirect( registry::getSetting( 'CNF_SITE_URL' ) );
					}else{
						parent::redirect( registry::getSetting( 'CNF_SITE_URL' ).registry::getObject( 'input' )->post[ 'redirect' ] );
					}
				}else{
					parent::redirect( registry::getSetting( 'CNF_SITE_URL' ).'/' );
				}
			}
		}else{
			parent::redirect( registry::getSetting( 'CNF_SITE_URL' ) );
		}
	}
	
	public function actionLogout()
	{
		registry::getObject( 'session' )->_session_destroy_method();
		$this->redirect( SITE_URL );
	}
	
	public function actionLoginuser()
	{
			/* tmp login */
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/user/theme') );
      $this->errorAndNotice();
      
		if( registry::getObject( 'session' )->getLogged()=="0" ) {
			$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','login_form.tpl','footer.tpl') );
			registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Logowanie / rejestracja") );
			$this->setViewParam( 'tplTitle', 'Logowanie / rejestracja' );
		}else{
			$this->redirect( registry::getSetting( 'CNF_SITE_URL' ).'user/index' );
		}
		$this->loadView( 'User' );
	}

    public function actionUserauctions()
    {
        $this->setViewParam( 'dir', array(ROOT_PATH.'/modules/user/theme') );
        $this->errorAndNotice();
        $this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','user_auction.tpl','footer.tpl') );
        $this->getAccountAdsNumbers();
        $this->setViewParam( 'tplTitle', 'Aukcje' );
        $bids = registry::getObject( 'user' )->getUserAuctionsBids( registry::getObject( 'user' )->email);
        $this->setViewParam( 'userAu', $bids );


//        echo "<pre>";
//        print_r($bids);
//        echo "</pre>";

        $this->loadView( 'User' );
    }
	
	public function actionIndex()
	{
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/user/theme') );
      $this->errorAndNotice();
      
		if( registry::getObject( 'session' )->getLogged()=="0" ) {		
			//$this->setViewParam( 'templates',array('header.tpl','top.tpl','login_form.tpl','footer.tpl') );
			//registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Logowanie / rejestracja") );
			$this->redirect( registry::getSetting( 'CNF_SITE_URL' ).'user/loginuser' );
		}else{
			$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','new_ad.tpl','footer.tpl') );
			$this->getAccountAdsNumbers();
			$this->setViewParam( 'addAd' , 'true' );
			registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Twoje konto") );
			registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/ads", "name" => "Ogłoszenia") );
			registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Dodaj ogłoszenie") );
			
			registry::storeObject( 'category','category' );
			$category = registry::getObject( 'category' );
			//get the draft
			$draft  = registry::getObject( 'user' )->getLastDraft();
			
			//check owner 
			if( $draft[0][user_id] != registry::getObject( 'user' )->id ) {
					registry::getObject( 'user' )->makeDraft();
					$draft = registry::getObject( 'user' )->getLastDraft();
			}
			$this->setViewParam( 'draftId', $draft[0][draft_id] );
			$draft_data = unserialize( $draft[0][data] );
			//echo "<pre>";
			//print_r($draft_data);
			//echo "</pre>";
			$selSubCat = array($draft_data[selectCategory1], $draft_data[selectCategory2], $draft_data[selectCategory3], $draft_data[selectCategory4], $draft_data[selectCategory5], $draft_data[selectCategory6]);

			$this->setViewParam( 'selectedSubCat',  $selSubCat);
			$this->setViewParam( 'selectedCategory', $draft_data[selectedCategory] );
			$selectedCategory  = $category->getCategory( $draft_data[selectedCategory] );
			$this->setViewParam( 'selectedCategoryName',  $selectedCategory[0][name]);

			/* set data to template */
			$this->setViewParam( 'regionId'.$draft_data[regionId], ' selected');	
			$this->setViewParam( 'city', $draft_data[city] );
			$this->setViewParam( 'cityDistrict', $draft_data[cityDistrict] );
			$this->setViewParam( 'tel', $draft_data[tel] );
			$this->setViewParam( 'contactEmail', $draft_data[contactEmail] );
			$this->setViewParam( 'price', $draft_data[price] );
			$this->setViewParam( 'priceSelect'.$draft_data[priceType], ' selected');
			$this->setViewParam( 'adText', $draft_data[adText] );
			$this->setViewParam( 'formPhoto1', $draft_data[formPhoto1] );
			$this->setViewParam( 'formPhoto2', $draft_data[formPhoto2] );
			$this->setViewParam( 'formPhoto3', $draft_data[formPhoto3] );
			$this->setViewParam( 'formPhoto4', $draft_data[formPhoto4] );
			$this->setViewParam( 'formPhoto5', $draft_data[formPhoto5] );
			$photoList = array();
			$uploadNumber = 0;
			if( !empty($draft_data[formPhoto1])) {
					$uploadNumber=$uploadNumber+1;
					array_push($photoList,$draft_data[formPhoto1] );
			}
			if( !empty($draft_data[formPhoto2])) {
					$uploadNumber=$uploadNumber+1;
					array_push($photoList,$draft_data[formPhoto2] );
			}
			if( !empty($draft_data[formPhoto3])) {
					$uploadNumber=$uploadNumber+1;
					array_push($photoList,$draft_data[formPhoto3] );
			}
			if( !empty($draft_data[formPhoto4])) {
					$uploadNumber=$uploadNumber+1;
					array_push($photoList,$draft_data[formPhoto4] );
			}
			if( !empty($draft_data[formPhoto5])) {
					$uploadNumber=$uploadNumber+1;
					array_push($photoList,$draft_data[formPhoto5] );
			}
			$this->setViewParam( 'photoList', $photoList );
			$this->setViewParam( 'uploadNumber', $uploadNumber );
			
			if($draft_data[highlighted]=="1") {
					$this->setViewParam( 'highlighted', ' checked' );
			}
			if($draft_data[positioned]=="1") {
					$this->setViewParam( 'positioned', ' checked' );
			}
			if($draft_data[auction]=="1") {
				$this->setViewParam( 'auction', ' checked' );
			}

			//$this->setViewParam( '', $draft_data[] );
			/* get attributes */
			$attributes = $category->getRequiredAttributes( $draft_data[selectedCategory] );
			//echo "<pre>";
			//print_r($attributes);
			//echo "</pre>";
			/* fill attributes with data from the draft */
			$new_attributes = array();
			if( is_array($attributes)) {
				foreach( $attributes as $key => $value )
				{
					$value[value] = $draft_data[$key];
					$new_attributes[$key] = $value;
				}
			}
			//echo "<pre>";
			//print_r($new_attributes);
			//echo "</pre>";		
			$this->setViewParam( 'attr_array', $new_attributes );
			
			//ADDING NEW AD
			if( registry::getObject( 'input' )->params[ 'add' ]=="new" ) {
				$validations = array(
					 					 'selectedCategory' => 'number',
										'regionId' => 'number',
										'city' => 'not_empty',
										'tel' => 'not_empty',
										'contactEmail' => 'email',
										'price' => 'number',
										'adText' => 'not_empty');
				$required = array('selectedCategory', 
										'regionId',
										'city',
										'tel',
										'contactEmail',
										'price',
										'adText');
				$sanatize = array();
				
				//iterate thru attributes
				foreach($new_attributes as $attr )
				{
					if( $attr[required]=="1") {
						$validations[$attr[aid]] = "not_empty";
						array_push($required, $attr[aid]);
					}
				}
				//echo "<pre>";
				//print_r($validations);
				//echo "</pre>";
				$validator = new Formvalidator($validations, $required, $sanatize);

				if($validator->validate($draft_data))
				{
    				$draft_data = $validator->sanatize($draft_data);
    				//SAVE
    				registry::storeObject( 'ads','ads' );
    				$region = registry::getObject( 'ads' )->getRegionById( $draft_data[regionId] );
    				$draft_data[region]= $region[region];
    				$priceType = registry::getObject( 'ads' )->getPriceTypeById( $draft_data[priceType] );
    				$draft_data[priceTypeStr] = $priceType;
    				$title = $category->makeTitle( $draft_data, $draft_data[selectedCategory] );
    				$draft_data[title] = $title;
    				$link = registry::getObject( 'router' )->makeLink( $title );
    				$draft_data[link] = $link;
    				$draft_data[adText] = stripslashes($draft_data[adText]);
    				registry::storeObject( 'ad','ad' );
    				$ad = registry::getObject( 'ad' );
    				$ad->add( $draft_data );
    				
    				parent::redirect( registry::getSetting('CNF_SITE_URL').'user/draft/delete/'.$draft_data[draftid].'/new/ad' );
   				//echo "<hr><pre>";
					//print_r($draft_data);
					//echo "</pre>";	
				}else{
    				$this->setViewParam( 'validator_script',$validator->getScript());
				}
			}
		}
		$this->setViewParam( 'tplTitle', 'Dodaj ogłoszenie');
		$this->loadView( 'User' );
	}
	
	public function getAccountAdsNumbers()
	{
			registry::storeObject( 'ads','ads' );
			$ads = registry::getObject( 'ads' );
			
			$this->setViewParam('ads_num', $ads->getUserAds( registry::getObject('user')->id,'ACTIVE', true ) );
			$this->setViewParam('ads_num_to_pay', $ads->getUserAds( registry::getObject('user')->id,'TO_PAY', true ) );
			$this->setViewParam('ads_num_watched', $ads->getUserAds( registry::getObject('user')->id,'WATCHED', true ) );
			$this->setViewParam('ads_num_ended', $ads->getUserAds( registry::getObject('user')->id,'ENDED',true ) );
	}
	
	public function actionAdreset()
	{
		$query ="UPDATE ".DB_PREFIX."_ad SET status = 0 WHERE aid = :aid AND user_id = :user_id";
		$params = array(":aid" => array( (int) registry::getObject( 'input' )->params[id], PDO::PARAM_INT), ":user_id" => array( (int) registry::getObject( 'user' )->id, PDO::PARAM_INT ) );
		$result = Db::getConnection()->putQuery($query,$params);
		registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie przygotowane do ponownego wystawienia - możesz je teraz opłacić.' , '60', '/');
		parent::redirect( registry::getSetting( 'CNF_SITE_URL' ).'user/ads/type/topay/');
	}

	public function actionAdstop()
	{
		$query ="UPDATE ".DB_PREFIX."_ad SET expires = :date_now WHERE aid = :aid AND user_id = :user_id";
		$params = array(":date_now" => date("Y-m-d H:i:s"), ":aid" => array( (int) registry::getObject( 'input' )->params[id], PDO::PARAM_INT), ":user_id" => array( (int) registry::getObject( 'user' )->id, PDO::PARAM_INT ) );
		$result = Db::getConnection()->putQuery($query,$params);
		registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Twoje ogłoszenie zostało zakończone.' , '60', '/');
		parent::redirect( registry::getSetting( 'CNF_SITE_URL' ).'user/ads/type/ended/');
	}	
	
	public function actionAds()	
	{
			$this->errorAndNotice();
			$this->getAccountAdsNumbers();
		
			registry::storeObject( 'ads','ads' );
			$ads = registry::getObject( 'ads' );
			$input = registry::getObject( 'input' );
			registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/index", "name" => "Twoje konto") );
			registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/ads", "name" => "Ogłoszenia") );
			if( $input->params[type]=="topay" ) {
				$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','ads_topay.tpl','footer.tpl') );
				$ad_list = $ads->getUserAds( registry::getObject('user')->id,'TO_PAY' );
				$title = "Ogłoszenia do opłacenia";
				registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/ads/type/topay", "name" => $title) );
				
			}elseif( $input->params[type]=="ended" ) {
				$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','ads_list.tpl','footer.tpl') );
				$ad_list = $ads->getUserAds( registry::getObject('user')->id,'ENDED' );
				$title = "Ogłoszenia zakończone";
				$this->setViewParam( 'ended', "ended" );
				registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/ads/type/ended", "name" => $title) );
				
			}elseif( $input->params[type]=="watched" ) {
				$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','ads_watch.tpl','footer.tpl') );
				$ad_list = $ads->getUserAds( registry::getObject('user')->id,'WATCHED' );
				$title = "Ogłoszenia obserowawane";
				
				registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/ads/type/watched", "name" => $title) );
				
			}else{
				$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','ads_list.tpl','footer.tpl') );
				$ad_list = $ads->getUserAds( registry::getObject('user')->id,'ACTIVE' );
				$title = "Ogłoszenia wystawione";
				registry::getObject( 'breadcrumb' )->addElement( array("link" => "user/ads/type/active", "name" => $title) );
			}
			$this->setViewParam( 'ad_list',$ad_list );
			$this->setViewParam( 'title',$title );
			$this->setViewParam( 'tplTitle', $title);
			$this->loadView( 'User' );
	}
	
	public function actionDraft()
	{
			if( registry::getObject( 'user' )->deleteDraft() ) {
				if( registry::getObject( 'input')->params['new'] ) {
					registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Twoje ogłoszenie zostało zapisane.' , '60', '/');
					parent::redirect( registry::getSetting('CNF_SITE_URL').'user/ads/type/topay' );
				}else{
					parent::redirect( registry::getSetting('CNF_SITE_URL').'user/index' );
				}
			}
	}	
	
	public function actionUpload()
	{
		  logger::saveLog( "error",array( 'Code'=>'100','Message'=>'#### Upload start from ajax? ' ) );
		try {
			$upload = new BasicUpload();
			$upload->addAllowedExtension('jpg');
			$upload->addAllowedExtension('JPG');
			$upload->addAllowedExtension('jpeg');
			$upload->addAllowedExtension('JPEG');
			$upload->addAllowedExtension('png');
			$upload->addAllowedExtension('PNG');
			$upload->upload();
			
		}catch( Exception $e ) {
			$response = json_encode(array("error" => $e->getMessage()));
			echo $response;
		}
 
	}

	public function actionSavedraft()
	{
		registry::getObject( 'user' )->saveDraft( registry::getObject( 'input')->params[draft_id] );
	}
	public function actionRegister()
	{
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/user/theme') );
		
		if( registry::getObject( 'input' )->post['doRegister']==true ) {
		
			$username 	= registry::getObject( 'input' )->post[username];
			$email		= registry::getObject( 'input' )->post[email];
			$password 	= registry::getObject( 'input' )->post[password];
			$rep_password =	registry::getObject( 'input' )->post[rep_password];
			$terms = registry::getObject( 'input' )->post[akceptuje];
			
		
			if( CNF_LOGIN_METHOD=="username" ) {
				if(!empty($username) && !empty($email) && !empty($password) && !empty($rep_password)) {
					$fields_ok = true;
				}else{
					$fields_ok = false;
				}
			}else{
				$username = NULL;
				if(!empty($email) && !empty($password) && !empty($rep_password) && ($terms=="tak") ) {
					$fields_ok = true;
				}else{
					$fields_ok = false;
				}
			}
			if($fields_ok === true) {
				registry::getObject( 'user' )->registration( $username,$email,$password, $rep_password);
			}else{
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wszystkie pola oznaczone gwiazdką są wymagane.' , '60', '/');
				parent::redirect( registry::getSetting('CNF_SITE_URL').'user/index' );
			}
		}
			if( registry::getObject( 'session' )->getLogged()=="0" ) {
        /* no view - template is the same for login and registration */
				//$this->setViewParam( 'templates',array('header.tpl','register.tpl','footer.tpl') );
				parent::redirect( registry::getSetting('CNF_SITE_URL').'user/index' );
			}else{
				parent::redirect( registry::getSetting('CNF_SITE_URL').'user/index' );
			}
			//$this->loadView( 'User' );
	}

	public function actionEmailchangeverification()
	{
		if( !empty( registry::getObject( 'input' )->params['hash'] ) && !empty( registry::getObject( 'input' )->params['address'] ) ) { 
			if( registry::getObject( 'user' )->confirmEmailChange()=="TRUE" ) {
				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Email został zmieniony' , '60', '/');
				parent::redirect( registry::getSetting('CNF_SITE_URL') );
			}else{
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Błędny kod potwierdzający' , '60', '/');
				parent::redirect( registry::getSetting('CNF_SITE_URL') );
			}
		}else{
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Błędny kod potwierdzający' , '60', '/');
			parent::redirect( registry::getSetting('CNF_SITE_URL') );
		}
	}

	public function actionRegisterverification()
	{
		/*
		*	Checks hash only
		*/
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/user/theme') );
		if( !empty( registry::getObject( 'input' )->params['hash'] ) && !empty( registry::getObject( 'input' )->params['address'] ) ) { 
			registry::getObject( 'user' )->registrationVerification();
		}else{
			//registry::getObject( 'error' )->storeError( 'user',language::lang('user','User','registrationVerifError') );
			registry::getObject( 'error' )->storeError( 'user','WTF' );
		}
		if( registry::getObject( 'session' )->getLogged()=="0" ) {
			$this->setViewParam( 'templates',array('header.tpl','register-verif.tpl','footer.tpl') );
		}else{
			parent::redirect( registry::getSetting('CNF_SITE_URL') );
		}
		$this->loadView( 'User' );
	}
	
	public function actionLostpassstart()
	{
		registry::getObject( 'user' )->passwordRemindSender(registry::getObject( 'input')->post[userLogin]);
	}
	
	public function actionLostpass()
	{
			registry::getObject( 'user' )->passwordRemiderChanger(registry::getObject( 'input')->params[hash]);
	}

	public function actionPrebid()
	{
		/* Auction prebid form */
		$this->errorAndNotice();
		$this->getAccountAdsNumbers();
		$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','prebid.tpl','footer.tpl') );
		$this->setViewParam( 'bid', registry::getObject( 'input' )->post['bid']);
		$this->setViewParam( 'ad_id', registry::getObject( 'input' )->post['ad_id']);
		$this->setViewParam( 'ad_name', registry::getObject( 'input' )->post['ad_name']);
		$this->setViewParam( 'ad_link', registry::getObject( 'input' )->post['ad_link']);
		$this->setViewParam( 'tplTitle', 'Licytuj: '.registry::getObject( 'input' )->post['ad_name']);
		registry::getObject( 'breadcrumb' )->addElement( array("link" => "ads/showad/id/".registry::getObject( 'input')->post['ad_id']."/link/".registry::getObject( 'input' )->post['ad_link']."/", "name" => "Aukcja: ".registry::getObject( 'input' )->post['ad_name']) );
		$this->loadView( 'User' );
	}

	public function actionBid()
	{
		$this->errorAndNotice();
		$this->getAccountAdsNumbers();
		$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','bid.tpl','footer.tpl') );
		registry::getObject( 'breadcrumb' )->addElement( array("link" => "ads/showad/id/".registry::getObject( 'input')->post['ad_id']."/link/".registry::getObject( 'input' )->post['ad_link']."/", "name" => "Aukcja: ".registry::getObject( 'input' )->post['ad_name']) );
		$this->setViewParam( 'ad_id', registry::getObject( 'input' )->post['ad_id']);
		$this->setViewParam( 'ad_name', registry::getObject( 'input' )->post['ad_name']);
		$this->setViewParam( 'ad_link', registry::getObject( 'input' )->post['ad_link']);
		$this->setViewParam( 'tplTitle', 'Licytuj: '.registry::getObject( 'input' )->post['ad_name']);
		registry::storeObject('auction','auction');
		$objAuction  = registry::getObject('auction');
		if($objAuction->getAuctionIdByAdId(registry::getObject( 'input' )->post['ad_id'])==true) {
			$bid = $objAuction->bid( registry::getObject('input')->post['bid']);
			if($bid['result']=="ok") {
				$this->setViewParam( 'bid_info', $bid['msg']);
			}else{
				$this->setViewParam( 'bid_error', $bid['msg']);
			}
		}else{
			$this->setViewParam( 'bid_error', 'To ogłoszenie nie posiada aukcji.');
		}
		$this->loadView( 'User' );
	}

	public function actionAddAuction()
	{

		$ad_id = registry::getObject( 'input' )->post['ad_id'];
		registry::storeObject('auction','auction');
		$objAuction  = registry::getObject('auction');
		$objAuction->getAuctionIdByAdId($ad_id);
		//sprawdz teraz - zeby nie sprawdzac dwa razy
		$validator = registry::getObject( 'validator' );
		if(($validator->isIntPositive( array("item" => registry::getObject( 'input' )->post['start_price']) )==false) || (empty(registry::getObject( 'input' )->post['start_price'])) || (registry::getObject( 'input' )->post['start_price']==0)) {
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Błędna kwota startowa licytacji.' , '60', '/');
			Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/auction/id/".registry::getObject( 'input' )->post['ad_id']);
		}else{
			if($objAuction->ad_owner=="0") {
				registry::storeObject( 'ad','ad');
				$ad = registry::getObject( 'ad' );
				try {
					$ad->getAdById( $ad_id );
					if($ad->auction!=1) {
						registry::getObject( 'cookie' )->extended_setcookie( 'error', 'To ogłoszenie nie może posiadać aukcji.' , '60', '/');
						Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/ads/type/active/" );
					}
					if($ad->user_id != registry::getObject( 'user' )->id) {
						registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie masz uprawnień do edycji tej aukcji.' , '60', '/');
						Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/ads/type/active/" );
					}
					$objAuction->makeNewAuction($ad_id, registry::getObject( 'user' )->id, registry::getObject( 'input' )->post['start_price'], registry::getObject( 'input' )->post['min_price']);
					registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Dane zostały zachowane.' , '60', '/');
					Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/auction/id/".registry::getObject( 'input' )->post['ad_id']);
				}catch (Exception $e) {
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie znaleziono ogłoszenia.' , '60', '/');
					Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/ads/type/active/" );
				}
			}else{
				if ($objAuction->auction['user_id'] == registry::getObject('user')->id) {
					$objAuction->makeNewAuction($ad_id, registry::getObject( 'user' )->id, registry::getObject( 'input' )->post['start_price'], registry::getObject( 'input' )->post['min_price']);
					registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Dane zostały zachowane.' , '60', '/');
					Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/auction/id/".registry::getObject( 'input' )->post['ad_id']);
				} else {
					registry::getObject('cookie')->extended_setcookie('error', 'Nie masz uprawnień do edycji tej aukcji.', '60', '/');
					Header("Location:  " . registry::getSetting('CNF_SITE_URL') . "user/ads/type/active/");
				}
			}
		}

	}

	public function actionAuction()
	{
		$this->errorAndNotice();
		$this->getAccountAdsNumbers();
		$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','auction.tpl','footer.tpl') );
		$this->setViewParam( 'ad_id', registry::getObject( 'input' )->params['id'] );
		$this->setViewParam( 'tplTitle', 'Ustawienia aukcji' );
		$this->setViewParam( 'title', 'Ustawienia aukcji' );
		$ad_id = registry::getObject( 'input' )->params['id'];
		registry::storeObject('auction','auction');
		$objAuction  = registry::getObject('auction');
		$objAuction->getAuctionIdByAdId($ad_id);
		if($objAuction->ad_owner=="0") {
			//nie ma wlasciciela - znaczy aukcja nie utworzona
			registry::storeObject( 'ad','ad');
			$ad = registry::getObject( 'ad' );
			try {
				$ad->getAdById( $ad_id );
				if($ad->auction!=1) {
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'To ogłoszenie nie może posiadać aukcji.' , '60', '/');
					Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/ads/type/active/" );
				}
				if($ad->user_id != registry::getObject( 'user' )->id) {
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie masz uprawnień do edycji tej aukcji.' , '60', '/');
					Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/ads/type/active/" );
				}
				$this->setViewParam('start_price', '100');
				$this->setViewParam('min_price', '0');
			}catch (Exception $e) {
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie znaleziono ogłoszenia.' , '60', '/');
				Header("Location:  ".registry::getSetting('CNF_SITE_URL')."user/ads/type/active/" );
			}

		}else {
			//jest wlasciciel - znaczy sprawdz czy prawidlowy
			if ($objAuction->auction['user_id'] == registry::getObject('user')->id) {

				if (($objAuction->auction_id != "0") && ($objAuction->auction['ended'] == "0")) {
					$this->setViewParam( 'auction_info', 'Ta aukcja została już rozpoczęta - edycja danych nie jest możliwa.' );
					$this->setViewParam('start_price', $objAuction->auction['start_price']);
					$this->setViewParam('min_price', $objAuction->auction['min_price']);
					$this->setViewParam('form_disabled', ' disabled');
				} else {
					$this->setViewParam( 'auction_info', 'To są parametry POPRZEDNIEJ (zakończonej: '.$objAuction->auction['end_date'].') aukcji dla tego ogłoszenia. Aby uruchomić aukcję wprowadź nowe (lub zachowaj obecne) parametry a następnie wciśnij "zapisz aukcję"');
					$this->setViewParam('start_price', $objAuction->auction['start_price']);
					$this->setViewParam('min_price', $objAuction->auction['min_price']);

				}
			} else {
				registry::getObject('cookie')->extended_setcookie('error', 'Nie masz uprawnień do edycji tej aukcji.', '60', '/');
				Header("Location:  " . registry::getSetting('CNF_SITE_URL') . "user/ads/type/active/");
			}
		}
		$this->loadView( 'User' );
	}
	
	public function actionTest()
	{
		/**
		 *	This is the way to load another controller (ex. if we need pagination or other subactions)
		**/
		/*$actionController = $this->loadActionController();
		if( is_object( $actionController ) ) {
			$this->setSubcontroller( $actionController );
			$actionController->actionIndex();
		}else{
			Throw new Exception('actionController required but not found.');
		}
		*/
		registry::storeObject('auction','auction');
		$objAuction  = registry::getObject('auction');

		$auction_id = $objAuction->getAuctionIdByAdId(21);

		var_dump(registry::getObject('input')->params['bid']);

		$objAuction->bid( registry::getObject('input')->params['bid']);


		$auction_id = $objAuction->getAuctionIdByAdId(21);
		$winner = $objAuction->getWinner();
		echo 'Wygrywa: '.$winner['bidder_email'];
	}
	
}
?>
