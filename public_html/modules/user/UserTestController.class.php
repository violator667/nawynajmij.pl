<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	UserTestController.class.php
 * Version	:	1.0
 *
 * Info		:	Module User
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	26.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class UserTestController Extends UserController {
  
  /*
  *	Stores patination array
  *	@access protectd
  */
  protected $pagination = array();
  
  public function __construct() {
	parent::__construct();
  }
  
  public function actionIndex()
  {
	echo "index";
	$this->setPagination( array( 'prev' => 'link/to' ) );
  }
  
  public function setPagination( $array )
  {
	$this->pagination = $array;
  }
	
  public function getPagination()
  {
	return $this->pagination;
  }
  
  }
?>
