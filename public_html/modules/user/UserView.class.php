<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	UserView.class.php
 * Version	:	1.0
 *
 * Info		:	View for user
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class UserView Extends BasicView {
	public function __construct( $input )
	{
		parent::__construct( $input );
		$this->viewMakeDisplay();
	}
  }
?>
 