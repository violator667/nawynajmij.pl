<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	HomepageController.class.php
 * Version	:	1.0
 *
 * Info		:	Homepage Module Controller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	29.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class HomepageController Extends BasicController {

  protected $access = 0;	
  protected $access_exceptions = array();
  
	public function __construct() {
		parent::__construct();
	}
	public function actionPhpinfo()
	{
    phpinfo();
	}
  public function actionIndex()
  {
	$this->setViewParam( 'templates',array('header.tpl','top.tpl','homepage.tpl','footer.tpl') );

	$this->cookieError( 'homepage' );
	$this->cookieNotice( 'homepage' );

	registry::storeObject( 'category','category' );
	$categories = registry::getObject( 'category' )->getMainCategories();
	$this->setViewParam( 'mainCategories', $categories );
	
	registry::storeObject( 'ads','ads' );
	$ads = registry::getObject( 'ads' )->getRandomPositionedAds();
	$this->setViewParam( 'positionedAds', $ads );
	
	/*
	echo "<pre>";
	print_r($categories);
	echo "</pre>";
	
	registry::storeObject( 'ad','ad' );
	$ad = registry::getObject( 'ad' )->getAdById('1');
	*/
	
	
	$this->loadView( 'Homepage' );
	
  }	
  
  public function noAccess()
  {
  }
}
?>
