<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	  CategoryController.class.php
 * Version	:	  1.0
 *
 * Info		  :	  Category controller
 *
 * Author  	: 	Michał‚ Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  31.08.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class CategoryController Extends BasicController {

  protected $access = 0;	
  protected $access_exceptions = array();
  
	public function __construct() {
		parent::__construct();
	}
	
  public function actionIndex()
  {
	//$this->setViewParam( 'templates',array('header.tpl','top.tpl','homepage.tpl','footer.tpl') );

//	$this->cookieError( 'category' );
//	$this->cookieNotice( 'category' );
//
//  registry::getObject( 'category')->getAds( 1, $force_limit = NULL );
//
//	$this->loadView( 'Category' );
	$this->redirect(registry::getSetting('CNF_SITE_URL').'category/show');
  }	
  
  public function actionShow()
  {
  
    $this->setViewParam( 'templates',array('header.tpl','top_category.tpl','category.tpl','footer.tpl') );


    $this->cookieError( 'category' );
    $this->cookieNotice( 'category' );
	 $category = registry::getObject( 'category' );
	 if( !empty( registry::getObject( 'input' )->params[cid] ) ) {
		 $category->getCategoryById( registry::getObject( 'input' )->params[cid] );
	 }
	 
    $sponsor_ads = $category->getAds( $category->getQuery(1, "ads", $force_limit = NULL ), "ads" );
    $sponsor_total_num = $category->getAds( $category->getQuery(1, "page", $force_limit = NULL ), "page" );
    $sponsor_total_num = count($sponsor_total_num);
    $sponsor_num = count($sponsor_ads);
    $limit = registry::getSetting( 'CNF_ADS_OTHER_PAGE' );
    if( $sponsor_num > 0 ) {
    	if( $sponsor_num < $limit ) {
    		$normal_limit = $limit-$sponsor_num;
    		$normal_ads = $category->getAds( $category->getQuery(0, "ads", $normal_limit), "ads" );
    	}
    	/* there is no place for normal ads on this page */
    }else{
    	$normal_ads = $category->getAds( $category->getQuery(0, "ads", $normal_limit), "ads" );
    }
    $normal_total_num = $category->getAds( $category->getQuery(0, "page", $force_limit = NULL ), "page" );
    $normal_total_num = count($normal_total_num);
    $total_ads = $sponsor_total_num+$normal_total_num;
	 $config_limit = registry::getSetting( 'CNF_ADS_OTHER_PAGE' );
	 
	 /* pages */
	 $pages = ceil( $total_ads / $config_limit );
	 
	 if( !empty(registry::getObject( 'input' )->params['page']) ) {
	 		if( registry::getObject( 'validator' )->isIntPositive( registry::getObject( 'input' )->params['page'] ) ) {
	 			if( registry::getObject( 'input' )->params['page'] < $pages ) {
	 				$current_page = registry::getObject( 'input' )->params['page'];
	 			}else{
	 				$current_page = $pages;
	 			}
	 		}else{
	 			$current_page = 1;
	 		}
	 }else{
	 	$current_page = 1;
	 } 
    
    $this->setViewParam( 'totalAds', $total_ads );
    $this->setViewParam( 'currentPage', $current_page ); 
    $this->setViewParam( 'prevPage', $current_page-1 ); 
    $this->setViewParam( 'nextPage', $current_page+1 ); 
    $this->setViewParam( 'totalPages', $pages ); //number of pages
	 $this->setViewParam( 'positionedAds', $sponsor_ads );
	 $this->setViewParam( 'normalAds', $normal_ads );
	 
	 registry::storeObject( 'ad','ad' );
	 $ad_categories = registry::getObject( 'ad' )->getCategoriesArray( registry::getObject( 'input')->params[cid] );

		//echo "<pre>";
		//print_r($ad_categories);
		//echo "</pre>";
	
		if( !empty( registry::getObject( 'input' )->params[cid] ) ) {
      	for($c=0; $c<count( $ad_categories ); $c++ ) {
      		$link = registry::getObject( 'router' )->linkTo( array( "module" => "category", "action" => "show", "cid" => $ad_categories[$c][cid], "link" => $ad_categories[$c][link] ));
      		registry::getObject( 'breadcrumb' )->addElement( array( "link" => "$link", "name" => $ad_categories[$c][name] ) );
     		}
     	}else{
     		$link = registry::getObject( 'router' )->linkTo( array( "module" => "category", "action" => "show" ));
     		registry::getObject( 'breadcrumb' )->addElement( array( "link" => "$link", "name" => "Wyszukaj" ) );
     	}
	 
	   registry::storeObject( 'category','category' );
      $parent  = registry::getObject( 'category' )->getParent( registry::getObject( 'input')->params[cid] );
     
      $children = registry::getObject( 'category' )->getChildren( $parent[0][cid] );
      $active_children = registry::getObject( 'category' )->getChildren( registry::getObject( 'input')->params[cid] );
      
      if( !empty( registry::getObject( 'input' )->params[cid] ) ) {
      	$this->setViewParam( 'categoryName', $category->name );
      	$this->setViewParam( 'categoryId', registry::getObject( 'input' )->params[cid] );
      	$this->setViewParam( 'title', WDNAME.' '.$category->name );
      }else{
      	$this->setViewParam( 'searchWord', registry::getObject( 'input' )->params[search] );
      	$this->setViewParam( 'title', WDNAME.' wyszukaj: '.registry::getObject( 'input' )->params[search] );
      	$this->setViewParam( 'categoryName', "Wyszukaj ".registry::getObject( 'input' )->params[search] );
      }
      /* filter vars */
	  $this->setViewParam( 'auctionParam', registry::getObject( 'input' )->params[auction] );
      $this->setViewParam( 'filterCity', registry::getObject( 'input' )->params[city] );
      $this->setViewParam( 'filterCityDistrict', registry::getObject( 'input' )->params[cityDistrict] );
      $this->setViewParam( 'filterPriceMin', registry::getObject( 'input' )->params[priceMin] );
      $this->setViewParam( 'filterPriceMax', registry::getObject( 'input' )->params[priceMax] );
      $this->setViewParam( 'filterSelect'.registry::getObject( 'input' )->params[region], " selected" );
      
	 	$this->setViewParam( 'subCategories', $children ); //children
	 	$this->setViewParam( 'activeChildren',$active_children ); //childrend of active category
      $this->setViewParam( 'subParent', $parent ); //parent
      $this->setViewParam( 'activeCategory', registry::getObject( 'input')->params[cid] );
    $this->loadView( 'Category' );
  }
  
  public function actionFilter()
  {
  		$input = registry::getObject( 'input' );
  		$link = registry::getSetting( 'CNF_SITE_URL' ).'category/show/';

	    if( !empty($input->post[auction]) ) {
			$link.='auction/'.$input->post[auction].'/';
		}
  		if( !empty($input->post[categoryId]) ) {
  			$link.='cid/'.$input->post[categoryId].'/';
  		}
	    if( !empty($input->post[auction]) ) {
			$link.='auction/'.$input->post[auction].'/';
	    }
  		if( !empty($input->post[regionId]) ) {
  			$link.='region/'.$input->post[regionId].'/';
  		}
  		if( !empty($input->post[city]) ) {
  			$link.='city/'.$input->post[city].'/';
  		}
  		if( !empty($input->post[cityDistrict]) ) {
  			$link.='cityDistrict/'.$input->post[cityDistrict].'/';
  		}
  		if( !empty($input->post[priceMin]) ) {
  			$link.='priceMin/'.$input->post[priceMin].'/';
  		}
  		if( !empty($input->post[priceMax]) ) {
  			$link.='priceMax/'.$input->post[priceMax].'/';
  		}
  		if( !empty($input->post[order]) ) {
  				$link.='sort/';
  			if( $input->post[order]=="0" ) {
  				// date DESC
  				$link.='expires/sortDest/desc/';
  			}elseif( $input->post[order]=="1" ) {
  				// date ASC
  				$link.='expires/sortDest/asc/';
  			}elseif( $input->post[order]=="2" ) {
  				// price DESC
  				$link.='price/sortDest/desc/';
  			}elseif( $input->post[order]=="3" ) {
  				// price ASC
  				$link.='price/sortDest/asc/';
  			}elseif( $input->post[order]=="4" ) {
  				// title DESC
  				$link.='title/sortDest/desc/';
  			}elseif( $input->post[order]=="5" ) {
  				// title ASC
  				$link.='title/sortDest/asc/';
  			}else{
  				$link.='expires/sortDest/desc/';
  			}
  		}
  		$this->redirect($link);
  }
  
  public function actionSearch()
  {
  		if( !empty( registry::getObject( 'input' )->post[search] ) ) {
  			$this->redirect( registry::getSetting( 'CNF_SITE_URL' ).'category/show/search/'.registry::getObject( 'input' )->post[search] );
  		}else{
  			$this->redirect( registry::getSetting( 'CNF_SITE_URL' ).'category/show/' );
  		}
  }  
  
  public function actionSelectAllParents()
  {
  		$this->setViewParam( 'templates',array('category_parents_json.tpl') );
  		$cid = registry::getObject( 'input' )->params[cid];
  		if( empty($cid) ) {
  			$cid = 0;
  		}
  		$category = registry::getObject( 'category' );
  		$catParents = $category->getAllParents( $cid, array() );
  		$this->setViewParam( 'category_parents_json', $catParents);
  		$this->loadView( 'Category' );
  }
  
  public function actionSelectAttributes()  
  {
  		$this->setViewParam( 'templates',array('attributes_json.tpl') );
  		$cid = registry::getObject( 'input' )->params[cid];
  		if( empty($cid) ) {
  			$cid = 0;
  		}
  		$category = registry::getObject( 'category' );
  		$cat = $category->getRequiredAttributes( $cid, 'true' );
  		if( $cat!=false ) {
  			$cat = json_encode( $cat );
  		}else{
  			$cat = json_encode( array() );
		}
  		$this->setViewParam( 'attributes_json', $cat);
  		$this->loadView( 'Category' );
  }
  
  public function actionSelectCategory()
  {
  		//make template user category->getCategoryToSelect( $cid )
  		$this->setViewParam( 'templates',array('category_json.tpl') );
  		$cid = registry::getObject( 'input' )->params[cid];
  		if( empty($cid) ) {
  			$cid = 0;
  		}
  		$category = registry::getObject( 'category' );
  		$cat = $category->getCategoryToSelect( $cid );
  		$this->setViewParam( 'category_json', $cat);
  		$this->loadView( 'Category' );
  }
  
  
  
  public function noAccess()
  {
  }
}
?>
