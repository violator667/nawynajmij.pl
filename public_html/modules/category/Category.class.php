<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	  Category.class.php
 * Version	:	  1.0
 *
 * Info		  :	  Basic category class 
 *
 * Author  	: 	Michał‚ Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  07.09.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */
 
 if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
  
 Class Category {
 
 /* the id of the category */
 /* INT 11 */
 public $cid = 0;
 
 /* the name of the category */
 /* VARCHAR 200 */
 public $name;
 
 /* parent_cid 
 *		if it's equal to 0 - the category is a main category
 * 	if it isn't the parent_cid points a category_id of the parent 
 *
 * 	example: Meble (id: 10 ) -> Pokojowe (id: 15) -> Sofy (id: 123)
 *					Meble (parent: 0) -> Pokojowe (parent: 10) -> Sofy (parent: 15) 
 */
 
 /* link
 *		the browser friendly representation of $name
 *		
 *		example: name = Przedłużacze ogrodowe
 *					You should set link to: przedluzacze-ogrodowe
 */
 /* VARCHAR 150 */ 
 public $link;
 
 /* INT 11 */
 public $parent_id = 0;
 
 /* the number of ads in this category without it's subcategories
 *
 *		example: Meble (ad_num: 100 ) -> Pokojowe (ad_num: 50) -> Sofy (ad_num: 25)
 * 	means that there are 175 ads in entire Meble category
 */ 
 /* INT 11 */
 
 public $title_format;
 
 public $ad_num = 0;
 
 /* stores last ad count in this category 
 *
 *		example: 2014-09-01 01:02:03
 *		means that last counting of ads in this category took place at 01:02:03 on Septembre the 1st
 */
 /* DATETIME RRRR-MM-DD HH:MM:SS */
 public $last_count;
 
 public $sort_fields = array( 'created', 'price', 'title' );
 
 public function __construct( )
 {
 }
 
 /* gets the category data */
 /* int $cid */
 public function getCategoryById( $cid )
 {
 		$query = "SELECT * FROM ".DB_PREFIX."_category WHERE cid = :cid";
 		$params = array(":cid" => $cid );
 		
 		$result = Db::getConnection()->getQuery( $query, $params );
 		
 		/* set results */
 		foreach( $result[0] as $key => $value ) {
 			$this->$key = $value;
 		}
 } //end getCategoryById( $cid )
 
 public function getCategoryToSelect( $cid = 0 )
 {
 		
 		$query = "SELECT cid,name FROM ".DB_PREFIX."_category WHERE parent_cid = :cid";
 		$params = array( ":cid" => array( (int) $cid, PDO::PARAM_INT ) );
 		$result = Db::getConnection()->getQuery( $query, $params );
 		if( $result ) {
 			return json_encode( $result );
 		}
 }
 
 public function getMainCategories( )
 {
 		$query = "SELECT * FROM ".DB_PREFIX."_category WHERE parent_cid = :parent_cid ORDER BY :order :direction";
 		$params = array(":parent_cid" => "0", ":order" => "cid", ":direction" => "ASC");
 		
 		$result = Db::getConnection()->getQuery( $query, $params );
 		
 		return $result;
 } //end getMainCategories( )
 
 public function getCategory( $cid )
 {
 	/* returns array with category details */
 	$ad_category_query  = "SELECT cid,name,link,parent_cid,ad_num FROM ".DB_PREFIX."_category WHERE cid = :cid LIMIT 0,1";
 	$ad_category_params = array( ":cid" => array( $cid, PDO::PARAM_INT ) );
 	$result = Db::getConnection()->getQuery( $ad_category_query, $ad_category_params );
 	
 	if($result) {
 		return $result;
	}else{
		return false;
	}
 } //getCategory
 
 public function getChildren( $category_id )
 {
 		$children = array();
 		$query = "SELECT cid,name,link,parent_cid,ad_num FROM ".DB_PREFIX."_category WHERE parent_cid = :parent_cid";
 		$params = array( ":parent_cid" => array( $category_id, PDO::PARAM_INT ) );
 		$result = Db::getConnection()->getQuery( $query,$params );
 		if( $result ) {
 			for( $r=0;$r<count($result);$r++ ) {
 				array_push( $children, $result[$r] );
 			}
 			
 			return $children;
 		}else{
			return array(); 		
 		}
 }
 
 public function getParent( $category_id ) 
 {
 		$query = "SELECT parent_cid FROM ".DB_PREFIX."_category WHERE cid = :cid";
 		$params = array( ":cid" => array( $category_id, PDO::PARAM_INT ) );
 		$result = Db::getConnection()->getQuery( $query,$params );
 		if( $result ) {
 			if( $result[0][parent_cid]!=0 ) {
 				$parent = $this->getCategory( $result[0][parent_cid] );
 				
 				return $parent;
 			}else{
 				#return array(0 => array("cid" => $category_id ) );
 				return array(0 => array("cid" => 0 ) );
 			}
 		}else{
			return array(0 => array("cid" => 0) );
 		}
 }
 
 public function getAllParents( $category_id, $prev_result )
 {
 		$query = "SELECT parent_cid,name FROM ".DB_PREFIX."_category WHERE cid = :cid";
 		$params = array( ":cid" => array( (int) $category_id, PDO::PARAM_INT ));
 		$result = Db::getConnection()->getQuery( $query, $params );
 		if( $result ) {
 			if($result[0][parent_cid]!=0) {
 				$prev_result[] = $result[0];
 				//$prev_result[] = $result[0][parent_cid];
 				$this->getAllParents( $result[0][parent_cid], $prev_result );
 				//return json_encode($prev_result);
 			}else{
 				$prev_result[] = $result[0];
 				//echo "wyjście: ";
 				//echo "<pre>";
 				//	print_r($prev_result);
 				//echo "</pre><hr>";
 				$prev_result = array_reverse($prev_result);
 				//echo json_encode($prev_result);
 				return $prev_result;
 			}
 		}
 		
 }
 
 public function getRequiredAttributes( $category_id, $json = NULL )
 {
 		$query = "SELECT * FROM ".DB_PREFIX."_attribute_category WHERE category = :cid";
 		$params = array( ":cid" => array( $category_id, PDO::PARAM_INT ) );
 		$result = Db::getConnection()->getQuery( $query,$params );
 		if( $result ) {
 				for( $r = 0; $r < count($result); $r++ ) {
 					$query = "SELECT aid,name,required,hide FROM ".DB_PREFIX."_attribute WHERE aid = :aid";
 					$params = array( ":aid" => array( $result[$r][attribute], PDO::PARAM_INT ) );
 					$attr = Db::getConnection()->getQuery( $query, $params );
 					if( $json == NULL ) {
 						$result[$r][name] = $attr[0][aid];
 					}else{
 						$result[$r] = $attr[0];
 					}
 					$res[$result[$r][attribute]] = $attr[0];
 				}
 			if( $json == NULL ) {
 		   	return $res;
 		   }else{
 		   	return $result;
 		   }
 		}else{
 			return false;
 		}
 }
 
 public function pageToLimit( $page ) 
 {
 	$config_limit = registry::getSetting( 'CNF_ADS_OTHER_PAGE' );
 	if( registry::getObject( 'validator' )->isIntPositive( $page ) ) {
 		$page = $page;
 	}else{
 		$page = 1;
 	}
 	if( $page == 1 ) {
 			$starter = 0;
 	}else{
 			$starter = $page*$config_limit-$config_limit;
 	}
 			return $starter;
 }
 
 public function getQuery( $positioned, $type, $force_limit = NULL )
 {
 		/* type "page" - only count query */
 		if($type == "page" ) {
 			$query_basic = "SELECT aid ";
 		}else{
 			$query_basic = "SELECT * ";
 		}
 		$input = registry::getObject( 'input' );
 		$query_basic .= "FROM ".DB_PREFIX."_ad WHERE status = :status AND expires > :expires AND positioned = :positioned";
 		$params[':status'] = array( (int) 1, PDO::PARAM_INT );
 		$params[':expires'] = date("Y-m-d H:i:s", time());
 		if($positioned == 1) {
 			$params[':positioned'] = array( (int) 1, PDO::PARAM_INT );
 			$query_basic.= " AND positioned_expires > :positioned_expires";
 			$params[':positioned_expires'] = date("Y-m-d H:i:s",time());
 		}else{
 			$params[':positioned'] = array( (int) 0, PDO::PARAM_INT );
 		}
 		
 		if( !empty($input->params['cid']) ) {
 			$query_basic.= "  AND category_id = :category_id";
 			$params[':category_id'] = array( (int) $input->params['cid'], PDO::PARAM_INT );
 		}

	    //auction display param
	    if( !empty($input->params['auction']) ) {
			$query_basic.= "  AND auction = :auction";
			$params[':auction'] = array( (int) 1, PDO::PARAM_INT);
		}
 		
 		if( (!empty( $input->params[region] ) ) && (is_numeric($input->params[region])) ) {
 			$query_basic.= " AND region_id = :region";
 			$params[':region'] = array( (int) $input->params[region], PDO::PARAM_INT );
 		}
 		if( !empty( $input->params[city] ) ) {
 			$query_basic.= " AND city = :city";
 			$params[':city'] = trim($input->params[city]);
 		}
 		if( !empty($input->params[cityDistrict] ) ) {
 			$query_basic.= " AND city_district = :city_district";
 			$params[':city_district'] = trim($input->params[cityDistrict]);
		}
		
		if( !empty( $input->params[priceMin] ) ) {
			if( registry::getObject( 'validator' )->isInt( $input->params[priceMin] ) ) {
				$query_basic.= " AND price >= :price_min";
				$params[':price_min'] = array( (int) $input->params[priceMin], PDO::PARAM_INT );
			}
		}
		if( !empty( $input->params[priceMax] ) ) {
			if( registry::getObject( 'validator' )->isInt( $input->params[priceMax] ) ) {
				$query_basic.= " AND price <= :price_max";
				$params[':price_max'] = array( (int) $input->params[priceMin], PDO::PARAM_INT );
			}
		}
		
		/* search */
		if( !empty( $input->params[search] ) )	{
			$search = $input->params[search];
			if( strlen( $search) >= registry::getSetting( 'CNF_SEARCH_MIN_LENGHT' ) ) {
				$query_basic.= " AND ( ( title LIKE :search ) OR ( city LIKE :search ) OR ( city_district LIKE :search ) )";
				$params[':search'] = '%'.$search.'%';
			}
		}	
		
		if( !empty($input->params[sort]) ) {
			$query_basic.= " ORDER BY ";
			
			if( in_array( $input->params[sort],$this->sort_fields ) ) {
				$query_basic.= $input->params[sort];
			}else{
				$query_basic.= $this->sort_fields[0];
			}
	
		}else{
			$query_basic.= " ORDER BY ";
			$query_basic.= $this->sort_fields[0];
		}
		
		if( !empty($input->params[sortDest]) ) {
						
			if($input->params[sortDest] == "asc" ) {
				$query_basic.= " ASC";
			}else{
				$query_basic.= " DESC";
			}
			
		}else{
			$query_basic.= " DESC";
		}
		
		if( !empty($input->params[page]) ) {
			$starter = $this->PageToLimit( $input->params[page] );
		}else{
			$starter = $this->pageToLimit( 1 );
			
		}
		
		if( $force_limit!= NULL ) {
			$limit = $force_limit;
		}else{
			$limit = registry::getSetting( 'CNF_ADS_OTHER_PAGE' );
		}
		
		/* pagination */
		if( $type!="page" ) {
			$query_basic.= " LIMIT :starter,:limit";
			$params[':starter'] = array( (int) $starter, PDO::PARAM_INT );
			$params[':limit'] = array( (int) $limit, PDO::PARAM_INT );
		}
		
		$result = array("query" => $query_basic, "params" => $params );	
		//echo "<pre>";
		//print_r($result);
		//echo "</pre>";	
		
		return $result;
 } //getQuery
 
 public function getAds( $query, $type )
 {
 		registry::storeObject( 'auction', 'auction' );
	 	$objAuction = registry::getObject( 'auction' );
 		$query_basic = $query['query'];
 		$params = $query['params'];
		//echo "<hr>".$query_basic.'<hr>';
		/*
		*/
		//echo $query_basic."<hr>";
		/*
		echo "<pre>";
		print_r($input->params);
		echo "<pre>";
		*//*
		echo "<pre>";
		print_r($params);
		echo "<pre>";
		
		*/
		$result = Db::getConnection()->getQuery( $query_basic,$params, 0, 1,1 );
			if( $type!="page" ) {
			for( $i=0; $i<count($result); $i++ ) {
				/* get price type name */
				registry::storeObject( 'ads','ads' );
				$result[$i][price_type_str] = registry::getObject( 'ads' )->getPriceTypeById( $result[$i][price_type] );
			
				/* manage date to be more polish :) */
				$result[$i][created_date] = date("d-m-Y",strtotime( $result[$i][created]) );
				$result[$i][created_datetime] = date("d-m-Y H:i:s",strtotime( $result[$i][created]) );
				$result[$i][expires_date] = date("d-m-Y",strtotime( $result[$i][expires]) );
				$result[$i][expires_datetime] = date("d-m-Y H:i:s",strtotime( $result[$i][expires]) );
				$result[$i][positioned_expires_date] = date("d-m-Y",strtotime( $result[$i][positioned_expires]) );
				$result[$i][positioned_expires_datetime] = date("d-m-Y H:i:s",strtotime( $result[$i][positioned_expires]) );
				
				
				/* get rid of html tags & cut text */
				$result[$i][text_raw] = $result[$i][text];
	 			$result[$i][text_raw] = strip_tags($result[$i][text_raw]);
	 			if(strlen($result[$i][text_raw])>registry::getSetting( 'CNF_AD_CUT' ) ) {
	 				$result[$i][text_raw] = substr($result[$i][text_raw], 0, strpos($result[$i][text_raw],' ', registry::getSetting( 'CNF_AD_CUT' )));
	 				$result[$i][text_raw].= " [...]"; 
	 			}
	 			
				/* get photos */
				registry::storeObject( 'ad','ad' );
				$ad = registry::getObject( 'ad' );
				$photos  = $ad->getAdPhotos( $result[$i][aid] );
				if( count($photos)>0 ) {
					$result[$i][photo] = registry::getSetting( 'CNF_SITE_URL' ).'upload/thumb/'.$photos[0][file];
				}else{
					$result[$i][photo] = registry::getSetting( 'CNF_SITE_URL' ).'themes/'.registry::getSetting( 'CNF_THEME' ).'/img/icons/camera.png';
				}
				/* get auction data */
				if($result[$i]['auction']=="1") {
					$objAuction->getAuctionIdByAdId($result[$i][aid]);
					$result[$i]['auction_array']['current_bid'] = $objAuction->auction['current_bid'];
				}
			}
			}
		
		
		return $result;
 }
 
 public function getAttributeById( $aid )
 {
	 $query = "SELECT * FROM ".DB_PREFIX."_attribute WHERE aid = :aid";
	 $params = array(":aid" => $aid );
	 $result = Db::getConnection()->getQuery( $query, $params );
	 return $result;
 } 
 
 public function makeTitle( $input_data, $category_id = NULLL )
 {
 		if( $category_id != NULL ) {
 			$this->getCategoryById( $category_id );
 		}
 		$markers = array();
 		$replacement = array();
 		/* make markers and replacement */
 		foreach( $input_data as $key => $value ) {
 			array_push($markers, "{{".$key."}}");
 			array_push($replacement, $value);
 		}
 		/*
 		echo "<pre>";
 		print_r($markers);
 		echo "</pre>";
 		echo "<pre>";
 		print_r($replacement);
 		echo "</pre>";
 		*/
 		$title = str_replace($markers,$replacement,$this->title_format);	
 		return $title;
 }
 
 public function countCategoryAds($category_id)
 {
 		/* check for children */
 		$children_query = "SELECT cid FROM ".DB_PREFIX."_category WHERE parent_cid = :category_id AND parent_cid != 0";
 		$children_params = array(":category_id" => array( (int) $category_id, PDO::PARAM_INT) );
 		$children_exec = Db::getConnection()->getQuery($children_query,$children_params);
 		if( count($children_exec)>0 ) {
			$this->countCategoryAds($children_exec[0][cid]);
			/*			
 			echo "<pre>";
 			print_r($children_exec);
 			echo "</pre>";
 			*/
 		}else{
 			/* this is the last children count */
 			//echo "liczę od cat: ".$category_id."<br>";
 			$this->doCountCategoryAds($category_id);
 		}
 }

 public function doCountCategoryAds($category_id,$count = 0)
 {
 		/* check for parents */
 		$parent_query = "SELECT parent_cid FROM ".DB_PREFIX."_category WHERE cid = :cid";
 		$parent_params = array(":cid" => array( (int) $category_id, PDO::PARAM_INT) );
 		$parent_exec = Db::getConnection()->getQuery($parent_query,$parent_params);
 		
 		$query = "SELECT COUNT(aid) as number FROM ".DB_PREFIX."_ad WHERE status = 1 AND category_id = :category_id";
  		$params = array(":category_id" => array( (int) $category_id, PDO::PARAM_INT) );
  		$result = Db::getConnection()->getQuery($query,$params, 0, 1, 1);
		$count = $count+$result[0][number]; 		
 		
 		$query_save = "UPDATE ".DB_PREFIX."_category SET ad_num = :ad_num, last_count = NOW() WHERE cid = :cid";
  		$params_save = array(":ad_num" => array( (int) $count, PDO::PARAM_INT),
  									":cid" => array( (int) $category_id, PDO::PARAM_INT) );
  		$exec = Db::getConnection()->putQuery($query_save,$params_save);
		if( $parent_exec[0][parent_cid] !=0 ) {
			$this->doCountCategoryAds($parent_exec[0][parent_cid],$count);
		}
 		
 		
 }
 
 public function countCategoryAds1($category_id)
 {
 		
 		
  		$query = "SELECT COUNT(aid) as number FROM ".DB_PREFIX."_ad WHERE category_id = :category_id";
  		$params = array(":category_id" => array( (int) $category_id, PDO::PARAM_INT) );
  		$result = Db::getConnection()->getQuery($query,$params, 0, 1, 1);
  		
  		$query_save = "UPDATE ".DB_PREFIX."_category SET ad_num = :ad_num, last_count = NOW() WHERE cid = :cid";
  		$params_save = array(":ad_num" => array( (int) $result[0][number], PDO::PARAM_INT),
  									":cid" => array( (int) $category_id, PDO::PARAM_INT) );
  		$exec = Db::getConnection()->putQuery($query_save,$params_save);
  		
 } //end countCategoryAds
 
 } //end of Class
?>