<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Cron.class.php
 * Version	:	1.0
 *
 * Info		:	Cron
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2014
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
class Cron {
	
public function __construct() {
		
} //end construct

/* gets expiring ads 
 * gets ads expiring with 3 days 
 */
public function getAdsToNotifyOwner()
{
	$query = "SELECT * FROM ".DB_PREFIX."_ad WHERE status = :status AND expires > (CURDATE() + INTERVAL 3 DAY) AND expires < (CURDATE() + INTERVAL 4 DAY)";
	$params = array(":status" => array( (int) 1, PDO::PARAM_INT));	
	$result = Db::getConnection()->getQuery($query, $params, 0, 1, 1);
	
	return $result;
}

public function endAd()
{
	/* get aid,category_id and auction param of expired ads */
	$cat_query = "SELECT aid,category_id,auction FROM ".DB_PREFIX."_ad WHERE status = 1 AND expires < NOW() GROUP BY category_id";
	$cat_result = Db::getConnection()->getQuery($cat_query,array(), 0, 1, 1);
	
	$query = "UPDATE ".DB_PREFIX."_ad SET status = :status WHERE expires < NOW() AND status!=0";
	$params = array(":status" => array( (int) 2, PDO::PARAM_INT ) );
	$exec = Db::getConnection()->putQuery($query,$params);
	/* recount category */
	registry::storeObject( 'category','category' );
	$cat = registry::getObject( 'category' );
	registry::storeObject( 'auction','auction' );
	$objAuction = registry::getObject( 'auction' );
	foreach( $cat_result as $key => $value ) {
			$cat->countCategoryAds($value['category_id']);
			if($value['auction']==1) {

				$objAuction->getAuctionIdByAdId($value['aid']);
				$objAuction->endAuction();
			}
	}
	
}

public function endSubsctiption()
{
	$query = "UPDATE ".DB_PREFIX."_users SET subscription = 0 WHERE subscription_end < NOW()";
	$exec = Db::getConnection()->putQuery($query,array());
}

public function countAds()
{
	/* count all ads in DB */
	registry::storeObject( 'category','category' );
	$cat = registry::getObject( 'category' );
	
	$query = "SELECT cid FROM ".DB_PREFIX."_category WHERE parent_cid = 0";
	$exec = Db::getConnection()->getQuery($query,array(),0, 1, 1);
	foreach($exec as $key => $value ) {
		//echo $key.' -> '.$value[cid].'<br/>';
		$cat->countCategoryAds($value[cid]);
		//$cat->countCategoryAds(6);
	}
}

public function expireLostPass()
{
	/* deletes all out of date data in _lost_pass */
	$query = "DELETE FROM ".DB_PREFIX."_lost_pass WHERE expires<=NOW()";
	$exec = Db::getConnection()->putQuery($query,array());
}

}
?>
