<?php
/* PHP WHISKY DRINKER
 * 
 * File		:  CronController.class.php
 * Version	:	1.0
 *
 * Info		:	Cron Module Controller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2014
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
class CronController Extends BasicController {

protected $access = 0;	
protected $access_exceptions = array();
public $cron;  
public function __construct() {
	parent::__construct();
	registry::storeObject( 'cron','cron' );
	$this->cron = registry::getObject( 'cron' );
}
	
public function noAccess()
{
		
}
	
public function actionIndex()
{
		/* leave it empty */
}

public function actionCount()
{
	/* use it smart it count ALL ads in ALL category - it could make very hi load */
	$this->cron->countAds();
	
}

public function actionEndsubscription()
{
	/* ends users subscription - run int every 1 minute */
	$this->cron->endSubsctiption();
}

public function actionEndad()
{
	/* RUN INT EVERY 5 MINUTES */
	$this->cron->endAd();

}

public function actionCleanlostpass()
{
		$this->cron->expireLostPass();
}

public function actionNotifyowner()
{
	/* USE IT ONLY ONCE A DAY ! */
	
	$notify_list = $this->cron->getAdsToNotifyOwner();
	if( count($notify_list) > 0 ) {
		/* there are some ads to notify */
		
		/* get email template */
		$query = "SELECT * FROM ".DB_PREFIX."_email_template WHERE name = :template_name ORDER BY id DESC LIMIT 0,1";
		$params = array( ":template_name" => "AD_ENDING" );
		$result = Db::getConnection()->getQuery( $query, $params );
		$markers = array("{{title}}","{{end_date}}");
		
		echo "<pre>";
		print_r($notify_list);
		echo "</pre>";

		foreach( $notify_list as $key => $value ) {
				/* get user email */
				$em_query = "SELECT email FROM ".DB_PREFIX."_users WHERE id = :user_id";
				$em_params = array(":user_id" => array($value[user_id], PDO::PARAM_INT) );
				$em_result = Db::getConnection()->getQuery($em_query,$em_params);
				$user_email = $em_result[0][email];
				
				$change = array($value[title],$value[expires]);
				$email_body = str_replace($markers,$change,$result[0][content]);
				$Semail = new Mail();
				$Semail->SingleTo = true;
				$Semail->addAddress($user_email);
				$Semail->Subject = 'Wygasające ogłoszenie: '.$value[title];
				$Semail->AltBody = 'Open in HTML mode';
				$Semail->MsgHTML($email_body);
				$Semail->Send();
				$Semail->ClearAddresses();
				
				logger::saveLog( "error",array( 'Code'=>'100','Message'=>'###CRON actionNotifyOwner | email: '.$user_email.' | AID: '.$value[aid].' ') );
		}
	}
}	
	
}
?>
