<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	CronView.class.php
 * Version	:	1.0
 *
 * Info		:	View for cron
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2014
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class CronView Extends BasicView {
	public function __construct( $input )
	{
		parent::__construct( $input );
		$this->viewMakeDisplay();
	}
  }
?>
 