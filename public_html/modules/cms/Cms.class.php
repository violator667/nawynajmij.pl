<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Cms.class.php
 * Version	:	1.0
 *
 * Info		:	Module
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2014
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class Cms {


	public function __construct() {
		
	}
	
	public function __destruct() {
	
	}
	
	public function getSite($id)
	{
		$query = "SELECT * FROM ".DB_PREFIX."_cms WHERE link=:link";
		$params = array(":link" => $id);
		$result = Db::getConnection()->getQuery($query,$params);
		return $result;
	}
	
}
?>
