<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	CmsController.class.php
 * Version	:	1.0
 *
 * Info		:	Cms Module Controller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	03.09.2014
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class CmsController Extends BasicController {

  protected $access = 0;	
  protected $access_exceptions = array('actionIndex');
  
	public function __construct() {
		parent::__construct();
	}
	
	
	public function actionShow()
	{
		$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','cms.tpl','footer.tpl') );
		registry::storeObject( 'category','category' );
		$categories = registry::getObject( 'category' )->getMainCategories();
		$this->setViewParam( 'mainCategories', $categories );
		$site = registry::getObject( 'cms')->getSite( registry::getObject( 'input' )->params[tid] );
		if( count($site)>0 ) {
			$this->setViewParam( 'title', $site[0][title] );
			$this->setViewParam( 'content',$site[0][text] );
			$this->setViewParam( 'tplTitle', $site[0][title] );
			$this->loadView( 'Cms' );
		}else{
			parent::redirect( registry::getSetting( 'CNF_SITE_URL' ) );
		}
	}


	public function actionContact()
	{
		$this->setViewParam( 'templates',array('header.tpl','top_konto.tpl','contact.tpl','footer.tpl') );
		$this->setViewParam( 'title', 'Kontakt z nawynajmij.pl' );
		$this->setViewParam( 'tplTitle', 'Kontakt' );
		$this->loadView( 'Cms' );
	}
	
	
}
?>
