<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	CmsView.class.php
 * Version	:	1.0
 *
 * Info		:	View for cms
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	01.09.2014
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class CmsView Extends BasicView {
	public function __construct( $input )
	{
		parent::__construct( $input );
		$this->viewMakeDisplay();
	}
  }
?>
 