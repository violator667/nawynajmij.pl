<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	DotPay.class.php
 * Version	:	1.0
 *
 * Info		:	Gateway DotPay
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	12.01.2014
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class DotPay {

	public $hash;
	public function __construct() {
		
	}
	
	public function makeHash()
	{
		$this->hash = md5( registry::getSetting( 'CNF_DOTPAY_PIN' ).":".registry::getSetting( 'CNF_DOTPAY_ID' ).":".registry::getObject( 'input' )->post[ 'control' ].":".registry::getObject( 'input' )->post[ 't_id' ].":".registry::getObject( 'input' )->post[ 'amount' ].":".registry::getObject( 'input' )->post[ 'email' ].":".registry::getObject( 'input' )->post[ 'service' ].":".registry::getObject( 'input' )->post[ 'code' ].":".registry::getObject( 'input' )->post[ 'username' ].":".registry::getObject( 'input' )->post[ 'password' ].":".registry::getObject( 'input' )->post[ 't_status' ] );
	}
	
	public function getResponse()
	{
		$this->makeHash();
		
		if( ( registry::getObject( 'input' )->server[ 'REMOTE_ADDR' ] == "217.17.41.5" ) || ( registry::getObject( 'input' )->server[ 'REMOTE_ADDR' ] == "195.150.9.37" ) ) {
			if( $this->hash == registry::getObject( 'input' )->post[ 'md5' ] ) {
				$Objorder = new Order();
				$Objorder->getOrderBySecret( registry::getObject( 'input' )->post[ 'control' ] );
				
				$status = registry::getObject( 'input' )->post[ 't_status' ];
				if( $status == "1" ) {
					$Objorder->updateStatus( '1' );
				}elseif( $status == "2" ) {
					$Objorder->updateStatus( '2' );
				}elseif( $status == "3" ) {
					$Objorder->updateStatus( '3' );
				}else{
					Logger::saveLog( "error",array( 'Code'=>'100','Message'=>'#DotPay### Gateway invalid status: '.$status.' for order '.$Objorder->returnOrderId() ) );
				}
			}else{
				Logger::saveLog( "error",array( 'Code'=>'100','Message'=>'#DotPay###Gateway invalid MD5 [remote/local]: '.registry::getObject( 'input' )->post[ 'md5' ].'/'.$this->hash ) );
			}
		}else{
			Logger::saveLog( "error",array( 'Code'=>'100','Message'=>'#DotPay###Gateway invalid SERVER IP: '.registry::getObject( 'input' )->server[ 'REMOTE_ADDR' ] ) );
		}
	}
	
	public function Log()
	{
	}
}
?>
