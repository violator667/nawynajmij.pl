<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	UserController.class.php
 * Version	:	1.0
 *
 * Info		:	User Module Controller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	09.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class DotpayController Extends BasicController {

  protected $access = 0;	
  protected $access_exceptions = array();
  
	public function __construct() {
		parent::__construct();
	}
	
	public function noAccess()
	{
		registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie masz uprawnień do wyświetlenia tej strony' , '60', '/');
		$this->redirect( registry::getSetting('CNF_SITE_URL') );
	}
	
	public function errorAndNotice()
	{
		/* Cookie Error */
		$cookie_error = registry::getObject( 'cookie' )->get( 'error' ); 
		if( !empty( $cookie_error ) ) {
			registry::getObject( 'error' )->storeError( 'user', $cookie_error );
			registry::getObject( 'cookie' )->unsetcookie( 'error' );
		}
		/* Cookie Notice */
		$cookie_notice = registry::getObject( 'cookie' )->get( 'notice' ); 
		if( !empty( $cookie_notice ) ) {
			registry::getObject( 'notice' )->storeNotice( 'user', $cookie_notice );
			registry::getObject( 'cookie' )->unsetcookie( 'notice' );
		}
	}
	
	public function actionGet()
	{
		registry::storeObject( 'dotpay','dotpay' );
		registry::getObject( 'dotpay' )->getResponse();
		echo "OK";
	}
	
	public function actionIndex()
	{
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/dotpay/theme') );
	  //$this->setViewParam( 'templates',array('header.tpl','login_form.tpl','footer.tpl') );
		
		$this->loadView( 'Dotpay' );
	}

	
	
}
?>
