<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	TestTestController.class.php
 * Version	:	1.0
 *
 * Info		:	Test Module Subcontroller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class TestTestController Extends TestController {
  
  /*
  *	Stores patination array
  *	@access protectd
  */
  protected $pagination = array();
  
  public function __construct() {
	parent::__construct();
  }
  
  public function actionIndex()
  {
	echo "Test/index";
	$this->setPagination( array( 'prev' => 'link/to' ) );
  }
  
  public function setPagination( $array )
  {
	$this->pagination = $array;
  }
	
  public function getPagination()
  {
	return $this->pagination;
  }
  
  }
?>
