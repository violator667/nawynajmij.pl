<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	TestController.class.php
 * Version	:	1.0
 *
 * Info		:	Test Module Controller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class TestController Extends BasicController {

  protected $access = 0;	
  protected $access_exceptions = array('actionIndex');
  
	public function __construct() {
		parent::__construct();
	}
	
	
	public function actionIndex()
	{
		echo "index";
	}
	
	public function actionTest2()
	{
		echo __CLASS__;
	}
	public function actionTest()
	{
		/**
		 *	This is the way to load another controller (ex. if we need pagination or other subactions)
		**/
		$actionController = $this->loadActionController();
		if( is_object( $actionController ) ) {
			$this->setSubcontroller( $actionController );
			$actionController->actionIndex();
		}else{
			Throw new Exception('actionController required but not found.');
		}
	}
	
}
?>
