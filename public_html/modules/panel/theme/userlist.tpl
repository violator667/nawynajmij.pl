	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
			<h2>Użytkownicy</h2>
			<br/>
			<table class="table">
        <tbody>
          <tr class="success">
            <td width="10%"><b>Filtruj:</b></td>
            <td width="10%">ID:</td>
            <td width="15%"><form action="{$site_url}panel/users/filter/true" method="post"><input type="text" name="user_id" value="{$filter_user_id}" size="15"></td>
            <td width="10%">Email:</td>
            <td width="15%"><input type="text" name="user_email" value="{$filter_user_email}" size="20"></td>
            <td width="10%">Konto:</td>
            <td width="15%">
              <select name="user_type"><option>wszystkie</option><option value="normal"{$selectNormal}>zwykłe</option><option value="subscription"{$selectSub}>abonament</option></select>
            </td>
            <td width="15%">
              <input type="submit" value="filtruj"></form>
            </td>
          </tr>
        </tbody>
			</table>
				<table class="table">
					<tbody>
						<tr class="success">
						  <td width="5%"><b>#</b></td>
							<td width="25%"><b>Użytkownik:</b></td>
							<td width="15%"><b>Status:</b></td>
							<td width="15%"><b>Abonament:</b></td>
							<td width="20%"><b>Rejestracja / ost. logowanie</b></td>
							<td width="20%"><b>Edycja</b></td>
						</tr>
						{foreach $userlist as $key => $value}
						  <tr>
						  <td width="5%">{$value.id}</td>
							<td width="25%">{$value.email}</td>
							<td width="15%">
							  {if $value.status eq 1}
							    <font color="green">potwierdzony</font>
							  {else}
							    nie potwierdzony
							  {/if}
							  {if $value.role eq 99}
							    <font color="orange"> [admin] </font>
							  {/if}
							</td>
							<td width="15%">
                {if $value.subscription eq 1}
                  tak [do: {$value.subscription_end}]
                {else}
                  nie
                {/if}
							</td>
							<td width="20%">{$value.registration}<br/>{$value.last_login}</td>
							<td width="20%"><a href="{$site_url}panel/user/userid/{$value.id}">[edytuj użytkownika]</a></td>
						  </tr>
						{/foreach}
					</tbody>
				</table>
				<table class="table">
        <tbody>
          <tr class="success">
            <td width="100%"><b>Wyślij email do filtrowanych:</b></td>
          </tr>
          <tr class="success">
            <td width="100%">
              <form method="post">
              <input type="hidden" name="send_email" value="1">
              <textarea name="message" rows="7" cols="80"></textarea>
            </td>
          </tr>
          <tr class="active">
            <td width="100%"><input type="submit" value="wyślij email do filtrowanych"></form></td>
          </tr>
        </tbody>
			</table>
		</div>
	</div>