<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Panel administracyjhny</title>
  <base href={$site_url}modules/panel/theme/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->
	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	{if $adedit }
	<link rel="stylesheet" type="text/css" href="{$site_url}themes/nawynajmij/cleditor/jquery.cleditor.css" />
  <script type="text/javascript" src="{$site_url}themes/nawynajmij/cleditor/jquery.cleditor.min.js"></script>
  <script type="text/javascript">
        $(document).ready(function() {
            $("#adText").cleditor({
                width: 500, // width not including margins, borders or padding
                height: 250, // height not including margins, borders or padding
                controls: // controls to add to the toolbar
                    "bold italic underline | " +
                    "style | color removeformat | undo redo | ",
                   
                colors: // colors in the color popup
                    "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                    "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                    "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                    "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                    "666 900 C60 C93 990 090 399 33F 60C 939 " +
                    "333 600 930 963 660 060 366 009 339 636 " +
                    "000 300 630 633 330 030 033 006 309 303",
                fonts: // font names in the font popup
                    "Arial,Arial Black,Comic Sans MS,Courier New,Narrow,Garamond," +
                    "Georgia,Impact,Sans Serif,Serif,Tahoma,Trebuchet MS,Verdana",
                sizes: // sizes in the font size popup
                    "1,2,3,4,5,6,7",
                styles: // styles in the style popup
                    [["Paragraph", "<p>"], ["Header 1", "<h1>"], ["Header 2", "<h2>"],
                    ["Header 3", "<h3>"],  ["Header 4","<h4>"],  ["Header 5","<h5>"],
                    ["Header 6","<h6>"]],
                useCSS: false, // use CSS to style HTML when possible (not supported in ie)
                docType: // Document type contained within the editor
                    '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
                docCSSFile: // CSS file used to style the document contained within the editor
                    "",
                bodyStyle: // style to assign to document body contained within the editor
                    "margin:4px; font:10pt Arial,Verdana; cursor:text"
            });
        });
    </script>
	{/if}
	
</head>

<body>
<div class="container">
<h1>Panel administracyjny</h1>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<ul class="nav nav-tabs">
				{if $active eq "index"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/index">Kokpit</a>
				</li>
				{if $active eq "category"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/category">Kategorie</a>
				</li>
				{if $active eq "attributes"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/attributes">Atrybuty</a>
				</li>
				{if $active eq "ads"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/ads">Ogłoszenia</a>
				</li>
				{if $active eq "payment"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/payment">Płatności / SMS</a>
				</li>
				{if $active eq "users"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/users/index">Użytkownicy</a>
				</li>
				{if $active eq "cms"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/cms/index">CMS</a>
				</li>
				{if $active eq "email"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/email/index">Wiadomości email</a>
				</li>
				{if $active eq "settings"}
					<li class="active">
				{else}
					<li>
				{/if}
					<a href="{$site_url}panel/settings">Ustawienia</a>
				</li>
				<li class="dropdown pull-right">
					 <a href="#" data-toggle="dropdown" class="dropdown-toggle">Szybkie linki<strong class="caret"></strong></a>
					<ul class="dropdown-menu">
						<li>
							<a href="{$site_url}">Strona główna</a>
						</li>
						<li>
							<a href="{$site_url}user/settings">Ustawienia konta</a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="{$site_url}user/logout">Wyloguj</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<br/>