{if $notice}
	{foreach $notice as $ntc}
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong>Info!</strong> {$ntc.0}
		</div>
	{/foreach}
{/if}

