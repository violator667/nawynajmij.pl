	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
			<h2>Ustawienia</h2>
			<br/>
			<form action="{$site_url}panel/settings" method="post">
			<input type="hidden" name="token" value="{$token}">
			{foreach from=$settings_opt  key=key item=section}
				<table class="table">
					<tbody>
						<tr class="success">
							<td width="40%"><b>{$key}</b></td>
							<td width="60%"></td>
						</tr>
				{foreach $section key=k item=param}
					{if $param.hide neq "TRUE" }
						<tr>
							<td width="40%">
							{if $param.field eq "TF"}
								<select name="{$param.param}">
									{if $param.value eq "TRUE"}
										<option value="TRUE" selected>TRUE</option>
										<option value="FALSE">FALSE</option>
									{else}
										<option value="TRUE">TRUE</option>
										<option value="FALSE" selected>FALSE</option>
									{/if}
								</select>
							{else}
								<input type="text" name="{$param.param}" value="{$param.value}" size="40">
							{/if}
							</td>
							<td width="60%">{$param.comment}</td>
						</tr>
					{else}
						<input type="hidden" name="{$param.param}" value="{$param.value}">
					{/if}
				{/foreach}
						<tr class="active">
							<td width="40%"><input type="submit" value="zapisz"></td>
							<td width="60%"></td>
						</tr>
					</tbody>
			</table>
			{/foreach}
			</form>
			
			<table class="table">
					<tbody>
						<tr class="success">
							<td width="100%"><form action="{$site_url}panel/backup" method="post"><input type="submit" value="wykonaj kopię bazy"></form></td>
						</tr>
			</table>
		</div>
	</div>
