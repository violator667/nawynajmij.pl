	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
<h2>CMS</h2>
			<div class="col-md-6 column">
				<table class="table">
					<tbody>
						<tr class="success">
							<td width="80%"><b>Strony w systemie:</b></td>
							<td width="10%"></td>
							<td width="10%"></td>
						</tr>
						{foreach from=$cmslist item=item}
						<tr>
							<td width="80%">{$item.title}</td>
							<td width="10%"><form action="{$site_url}panel/cms/edit/{$item.id}" method="post"><input type="submit" value="zmień"></form></td>
							<td width="10%"><form action="{$site_url}panel/delcms/id/{$item.id}" method="post">
							<input type="submit" value="usuń" onClick="return confirm('Czy bezpowrotnie usunąć tą stronę?')"></form></td>
						</tr>
						{/foreach}
					</tbody>
				</table>
			</div>
			<div class="col-md-6 column">
			  <table class="table">
				<tbody>
				  <tr class="success">
            {if $action eq "edit"}
              <form action="{$site_url}panel/editcms/id/{$id}" method="post" id="cms">
            {else}
              <form action="{$site_url}panel/addcms/" method="post" id="cms">
            {/if}
            <input type="hidden" name="token" value="{$token}">
				    <td width="100%"><b>{if $action eq "edit"}Edytuj stronę:{else}Dodaj stronę{/if}</b></td>
				  </tr>
				  <tr>
				    <td width="100%">Tytuł<font color="red">*</font>:</td>
				  </tr>
				  <tr>
				    <td width="100%"><input type="text" name="title" value="{$title}" size="60"></td>
				  </tr>
				  <tr>
				    <td width="100%">Link<font color="red">*</font>:</td>
				  </tr>
				  <tr>
				    <td width="100%"><input type="text" name="link" value="{$link}" size="60"></td>
				  </tr>
				  <tr>
				    <td width="100%">Treść<font color="red">*</font>:</td>
				  </tr>
				  <tr>
				    <td width="100%"><textarea name="pageText" id="pageText" rows="10" cols="60">{$pageText}</textarea></td>
				  </tr>
				  <tr>
				    <td width="100%"><input type="submit" value="zapisz" id="save"></td>
				  </tr>
				</tbody>
			      </table>
			</div>
		</div>
	</div>