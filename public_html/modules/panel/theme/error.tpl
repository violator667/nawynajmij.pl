{if $error}
	{foreach $error as $err}
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			<strong>Błąd!</strong> {$err.0}
		</div>
	{/foreach}
{/if}

