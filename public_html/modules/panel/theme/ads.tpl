	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
			<h2>Ogłoszenia</h2>
			<br/>
			<form action="{$site_url}panel/ads" method="post">
			<input type="hidden" name="token" value="{$token}">
				<table class="table">
					<tbody>
						<tr class="success">
							<td width="5%">Ogłoszenia:</td>
							<td width="10%">
								<select name="list">
									{if $list eq "waiting"}
										<option value="waiting" selected>do opłacenia</option>
									{else}
										<option value="waiting">do opłacenia</option>
									{/if}
									{if $list eq "active"}
										<option value="active" selected>aktywne</option>
									{else}
										<option value="active">aktywne</option>
									{/if}
									{if $list eq "fihished"}
										<option value="finished" selected>zakończone</option>
									{else}
										<option value="finished">zakończone</option>
									{/if}
								</select>
							</td>
							<td width="5%">Szukaj:</td>
							<td width="10%"><input type="text" name="search" value="{$search}" size="15"></td>
							<td width="15%">ID Użytkownika:</td>
							<td width="10%"><input type="text" name="user_id" value="{$user_id}" size="15"></td>
							<td width="5%">Sortuj:</td>
							<td width="15%">
                <select name="sort">
                  <option value="1">data malejąco</option>
                  <option value="2">data rosnąco</option>
                  <option value="3">koniec malejąco</option>
                  <option value="4">koniec rosnąco</option>
                  <option value="5">cena malejąco</option>
                  <option value="6">cena rosnąco</option>
                </select>
							</td>
							<td width="20%"></td>
							<td width="5%"><input type="submit" value="wyświetl"></td>
						</tr>
					</tbody>
				</table>
			</form>
			<hr>
			
				<table class="table">
					<tbody>
						<tr class="success">
              <td width="5%"><b>#</b></td>
              <td width="25%"><b>Tytuł</b></td>
              <td width="10%"><b>ID Użyt.</b></td>
              <td width="5%"><b>Wyr.</b></td>
              <td width="5%"><b>Prom.</b></td>
              <td width="5%"><b>Aukcja</b></td>
              <td width="10%"><b>Odsłon</b></td>
              <td width="25%"><b>Dodano / wygasa</b></td>
              <td width="10%"><b>opcje</b></td>
						</tr>
						{foreach from=$adlist item=ad}
              <tr>
                <td width="5%">{$ad.aid}</td>
                <td width="25%">{$ad.title}</td>
                <td width="10%">{$ad.user_id}</td>
                <td width="5%">{if $ad.highlited eq 1}tak{else}nie{/if}</td>
                <td width="5%">{if $ad.positioned eq 1}tak{else}nie{/if}</td>
                <td width="5%">{if $ad.auction eq 1}tak{else}nie{/if}</td>
                <td width="10%">{$ad.ad_views}</td>
                <td width="25%">{$ad.created}<br/>{$ad.expires}</td>
                <td width="10%"><a href="{$site_url}panel/delad/id/{$ad.aid}" onClick="return confirm('Czy usunąć to ogłoszenie?')">[usuń]</a> <a href="{$site_url}panel/editad/id/{$ad.aid}">[edytuj]</a></td>
              </tr>
						{/foreach}
					</tbody>
				</table>
		</div>
	</div>
