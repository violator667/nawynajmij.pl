{function name=catList}
    {foreach $items as $item}
    <option value="{$item['cid']}">{$separator} {$item['name']}</option>
        {if $item['children']}
            {concat 1=$separator 2=$item['name']}
            {call name=catList items=$item['children'] separator={concat 1=$separator 2=$item['name'] 3=" | "}}
        {/if}
    {/foreach}
{/function}
	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
			<h2>Edytuj ogłoszenie</h2>
		</div>
		<div class="col-md-12 column">
			<table class="table">
				<tbody>
					<tr class="success">
						<td width="70%">Edytuj ogłoszenie: {$title}
						</td>
						<td width="15%"><form action="{$site_url}panel/delad/id/{$aid}" method="get">
            <input type="submit" value="usuń ogłoszenie" onClick="return confirm('Czy bezpowrotnie usunąć ogłoszenie?')">
						</form></td>
						<td width="15%">
						{if $auction eq 1}
              <form action="{$site_url}panel/suspendauction/id/{$aid}" method="get">
                <input type="submit" value="zakończ aukcję" onClick="return confirm('Czy zakończyć aukcję z powodu naruszenia regulaminu?')">
              </form>
						{/if}
						</td>
					</tr>
				</tbody>
			</table>
			<br/>
		</div>
		<div class="col-md-12 column">
      <form action="{$site_url}panel/editad/id/{$aid}" method="post">
      <input type="hidden" name="token" value="{$token}">
			<table class="table">
				<tbody>
					<tr>
						<td width="20%"><b>Tytuł:<font color="red">*</font></b></td>
						<td width="80%"><input type="text" name="title" value="{$title}" size="40"></td>
					</tr>
					<tr>
						<td width="20%"><b>Miasto:<font color="red">*</font></b></td>
						<td width="80%"><input type="text" name="city" value="{$city}" size="40"></td>
					</tr>
					<tr>
						<td width="20%"><b>Dzielnica:</b></td>
						<td width="80%"><input type="text" name="city_district" value="{$city_district}" size="40"></td>
					</tr>
					<tr>
						<td width="20%"><b>Email kontaktowy:<font color="red">*</font></b></td>
						<td width="80%"><input type="text" name="email" value="{$email}" size="40"></td>
					</tr>
					<tr>
						<td width="20%"><b>Telefon:<font color="red">*</font></b></td>
						<td width="80%"><input type="text" name="phone_number" value="{$phone_number}" size="40"></td>
					</tr>
					<tr>
						<td width="20%"><b>Cena:<font color="red">*</font></b></td>
						<td width="80%"><input type="text" name="price" value="{$price}" size="40"></td>
					</tr>
					<tr>
						<td width="20%"><b>Nowa kategoria:<font color="red">*</font></b></td>
						<td width="80%"><select name="category"><option value="{$cat_id}"> - nie zmieniaj -</option>{call name=catList items=$category_tree separator=""}</select>
						</td>
					</tr>
					<tr>
            <td width="100%" colspan="2"><b>Treść<font color="red">*</font></b></td>
					</tr>
					<tr>
            <td width="100%" colspan="2"><textarea id="adText" name="adText">{$adText}</textarea></td>
					</tr>
					<tr>
						<td width="20%"><b></b></td>
						<td width="80%"><input type="submit" value="zapisz zmiany"></form></td>
					</tr>
				</tbody>
			</table>
					
</div>
</div>

		<!-- <div class="row clearfix"> -->
    <!-- </div>
	</div> -->
