	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
			<h2>Kategorie</h2>
		</div>
		<div class="col-md-12 column">
			<table class="table">
				<tbody>
					<tr class="success">
						<td width="80%">{if $parent_cid > 0 }
						<a href="javascript:window.history.back(-1);">&laquo; wstecz</a> | 
						{/if}<b>{$parent_name}</b></td>
						<td width="20%">
						<form action="{$site_url}panel/categoryadd/parent/{$parent_cid}" method="get">
						<input type="submit" value="dodaj"></form>
						</td>
					</tr>
				</tbody>
			</table>
			<br/>
			<table class="table">
				<tbody>
					<tr class="success">
						<td width="10%"><b>#</b></td>
						<td width="20%"><b>Nazwa</b></td>
						<td width="10%"><b>Ogłoszeń</b></td>
						<td width="50%"><b>Szablon</b></td>
						<td width="10%"><b>Opcje</b></td>
					</tr>
					{foreach from=$catlist item=cat}
            <tr class="">
              <td width="10%">{$cat.cid}</td>
              <td width="20%"><a href="{$site_url}panel/category/parent/{$cat.cid}">{$cat.name}</a></td>
              <td width="10%">{$cat.ad_num}</td>
              <td width="50%">{$cat.title_format}</td>
              <td width="10%">
                <form action="{$site_url}panel/categoryedit/cat_id/{$cat.cid}" method="get">
                <input type="submit" value="edytuj"></form>
              </td>
					</tr>
					{/foreach}
				</tbody>
			</table>
					
</div>
</div>

		<!-- <div class="row clearfix"> -->
    <!-- </div>
	</div> -->
