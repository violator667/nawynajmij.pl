	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
			<h2>Dodaj kategorię</h2>
		</div>
		<div class="col-md-12 column">
			<table class="table">
				<tbody>
					<tr class="success">
						<td width="80%">Dodaj 
						{if $parent_cid > 0 }
              podkategorię dla: {$parent_name}
						{else}
              kategorię główną
						{/if}
						</td>
						<td width="20%">
						</td>
					</tr>
				</tbody>
			</table>
			<br/>
		</div>
		<div class="col-md-6 column">
      <form action="{$site_url}panel/categoryadd/parent/{$parent_cid}" method="post">
      <input type="hidden" name="token" value="{$token}">
			<table class="table">
				<tbody>
					<tr>
						<td width="40%"><b>Nazwa:<font color="red">*</font></b></td>
						<td width="60%"><input type="text" name="name" value="{$name}"></td>
					</tr>
					<tr>
						<td width="40%"><b>Format tytułu ogłoszenia:<font color="red">*</font></b></td>
						<td width="60%"><input type="text" name="title_format" value="{$title_format}"></td>
					</tr>
					<tr>
						<td width="40%"><b>Url:<font color="red">*</font></b></td>
						<td width="70%"><input type="text" name="link" value="{$link}"></td>
					</tr>
					<tr>
						<td width="40%"><b>Atrybuty:</b></td>
						<td width="70%"></td>
					</tr>
					{foreach from=$attr_list item=attr}
            <tr>
              <td width="40%" style="text-align: right;"><input type="checkbox" name="attr[]" value="{$attr.aid}" id="{$attr.aid}"></td>
              <td width="70%"><label for="{$attr.aid}"><b>{$attr.aid}</b> - {$attr.name} 
              {if $attr.required eq 1}
                <font color="red">[wymagane]</font>
              {/if}
              {if $attr.hide eq 1}
                [ukryte w widoku]
              {/if}
              </label></td>
            </tr>
					{/foreach}
					<tr>
						<td width="40%"><b></b></td>
						<td width="70%"><input type="submit" value="dodaj"></form></td>
					</tr>
				</tbody>
			</table>
					
</div>
</div>

		<!-- <div class="row clearfix"> -->
    <!-- </div>
	</div> -->
