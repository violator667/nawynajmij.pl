	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
{foreach from=$userdet item=userdetail}
<h2>Edycja użytkownika: {$userdetail.email}</h2>
			<div class="col-md-6 column">
				<table class="table">
					<tbody>
						<tr class="success">
							<td width="30%"><b>{$userdetail.email}</b></td>
							<td width="50%"></td>
							<td width="20%"><form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/delete" method="post">
							<input type="hidden" name="token" value="{$token}">
							<input type="submit" value="usuń konto" onClick="return confirm('Czy usunąć użytkownika? Usunięcie użytkownika nie usunie jego ogłoszeń!')">
							</form></td>
						</tr>
						<tr>
							<td width="30%">Email:</td>
							<td width="50%">
							<form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/email" method="post">
							<input type="hidden" name="token" value="{$token}">
							<input type="email" name="email" value="{$userdetail.email}">
							</td>
							<td width="20%"><input type="submit" value="zmień"></form></td>
						</tr>
						<tr>
							<td width="30%">Nowe hasło:</td>
							<td width="50%">
							<form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/password" method="post">
							<input type="hidden" name="token" value="{$token}">
							<input type="text" name="newpass">
							</td>
							<td width="20%"><input type="submit" value="zmień"></form></td>
						</tr>
						<tr>
							<td width="30%">Poziom:</td>
							<td width="50%">
							<form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/role" method="post">
							<input type="hidden" name="token" value="{$token}">
							<select name="role">
							  {if $userdetail.role eq 99}
							    <option value="admin" selected>administrator</option>
							    <option value="user">użytkownik</option>
							  {else}
							    <option value="admin">administrator</option>
							    <option value="user" selected>użytkownik</option>
							  {/if}
							</select>
							</td>
							<td width="20%"><input type="submit" value="zmień"></form></td>
						</tr>
						<tr>
							<td width="30%">Status:</td>
							<td width="50%">
							<form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/status" method="post">
							<input type="hidden" name="token" value="{$token}">
							<select name="status">
							  {if $userdetail.status eq 0}
							    <option value="unconfirmed" selected>nie potwierdzony</option>
							    <option value="confirmed">potwierdzony</option>
							  {else}
							    <option value="unconfirmed">nie potwierdzony</option>
							    <option value="confirmed" selected>potwierdzony</option>
							  {/if}
							</select>
							</td>
							<td width="20%"><input type="submit" value="zmień"></form></td>
						</tr>
						<tr>
							<td width="30%">Abonament:</td>
							<td width="50%">
							<form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/subscription" method="post">
							<input type="hidden" name="token" value="{$token}">
							<select name="subscription">
                {if $userdetail.subscription eq 1}
                  <option value="1" selected>tak</option>
                  <option value="0">nie</option>
                {else}
                  <option value="1">tak</option>
                  <option value="0" selected>nie</option>
                {/if}
              </select>
							</td>
							<td width="20%"><input type="submit" value="zmień"></form></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-6 column">
			  <table class="table">
				<tbody>
				  <tr class="success">
				    <td width="40%"><b>Informacje:</b></td>
				    <td width="60%"></td>
				  </tr>
				  <tr>
				    <td width="40%">Status:</td>
				    <td width="60%">
				      {if $userdetail.status eq 1}
					<font color="green">potwierdzony</font>
				      {else}
					nie potwierdzony
				      {/if}
				    </td>
				  </tr>
				  <tr>
				    <td width="40%">Poziom:</td>
				    <td width="60%">
				      {if $userdetail.role eq 99}
					<font color="orange">administrator</font>
				      {else}
					użytkownik
				      {/if}
				    </td>
				  </tr>
				  <tr>
				    <td width="40%">Rejestracja:</td>
				    <td width="60%">{$userdetail.registration}</td>
				  </tr>
				  <tr>
				    <td width="40%">Ostatnie logowanie:</td>
				    <td width="60%">{$userdetail.last_login}</td>
				  </tr>
				</tbody>
				</table>
				{if $userdetail.subscription eq 1}
				<table class="table">
				<tbody>
				  <tr class="success">
				    <td width="40%"><b>Abonament:</b></td>
				    <td width="50%"></td>
				    <td width="10%"></td>
				  </tr>
				  <tr>
				    <td width="40%">Ważny do:</td>
				    <td width="50%">
              {$userdetail.subscription_end}
				    </td>
				    <td width="10%"></td>
				  </tr>
				  <tr>
				    <td width="40%">Przedłuż o: </td>
				    <td width="50%">
				    <form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/subscription_end" method="post">
						<input type="hidden" name="token" value="{$token}">
				      <select name="subscription_renew">
                <option value="0"></option>
                <option value="1">1 dzień</option>
                <option value="7">7 dni</option>
                <option value="14">14 dni</option>
                <option value="30">30 dni</option>
                <option value="60">60 dni</option>
                <option value="90">90 dni</option>
                <option value="365">365 dni</option>
				      </select>
				    </td>
				    <td width="10%"><input type="submit" value="zmień"></form></td>
				  </tr>
				  <tr>
				    <td width="40%">Ogłoszeń standard:</td>
				    <td width="50%">
				    <form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/free_standard" method="post">
						<input type="hidden" name="token" value="{$token}">
				    <input type="text" name="free_standard" value="{$userdetail.free_standard}" size="6"></td>
				    <td width="10%"><input type="submit" value="zmień"></form></td>
				  </tr>
				   <tr>
				    <td width="40%">Ogłoszeń wyróżnionych:</td>
				    <td width="50%"><form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/free_highlighted" method="post">
						<input type="hidden" name="token" value="{$token}">
				    <input type="text" name="free_highlighted" value="{$userdetail.free_highlighted}" size="6"></td>
				    <td width="10%"><input type="submit" value="zmień"></form></td>
				  </tr>
				   <tr>
				    <td width="40%">Ogłoszeń promowanych:</td>
				    <td width="50%"><form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/free_positioned" method="post">
						<input type="hidden" name="token" value="{$token}">
				    <input type="text" name="free_positioned" value="{$userdetail.free_positioned}" size="6"></td>
				    <td width="10%"><input type="submit" value="zmień"></form></td>
				  </tr>
				  <tr>
				    <td width="40%">Ogłoszeń mix:</td>
				    <td width="50%"><form action="{$site_url}panel/user/userid/{$userdetail.id}/edit/free_mix" method="post">
						<input type="hidden" name="token" value="{$token}">
				    <input type="text" name="free_mix" value="{$userdetail.free_mix}" size="6"></td>
				    <td width="10%"><input type="submit" value="zmień"></form></td>
				  </tr>
				</tbody>
			      </table>
			  {/if}
			</div>
			{/foreach}
		</div>
	</div>
	