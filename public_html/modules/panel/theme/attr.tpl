	<div class="row clearfix">
		<div class="col-md-12 column">
{include file='error.tpl'}
{include file='notice.tpl'}
			<h2>Atrybuty</h2>
			<br/>
			<form action="{$site_url}panel/attributes/addattribute/true" method="post">
			<input type="hidden" name="token" value="{$token}">
				<table class="table">
					<tbody>
						<tr class="success">
							<td width="20%"><b>Dodaj nowy atrybut:</b></td>
							<td width="80%"></td>
						</tr>
						<tr>
							<td width="20%"><b>Identyfikator:</b></td>
							<td width="80%"><input type="text" name="attr_id" value="attr_"> (musi zaczynać się ciągiem znaków: attr_ / nie może zawierać spacji, znaków specjalnych itp.)</td>
						</tr>
						<tr>
							<td width="20%"><b>Nazwa dla użytkownika:</b></td>
							<td width="80%"><input type="text" name="name"> (nazwa wyświetlana użytkownikowi w kategorii)</td>
						</tr>
						<tr>
							<td width="20%"><b>Wymagane :</b></td>
							<td width="80%"><input type="checkbox" name="required" value="1"> (użytkownik będzie musiał wprowadzić dane aby wysłać ogłoszenie)</td>
						</tr>
						<tr>
							<td width="20%"><b>Ukryte w widoku :</b></td>
							<td width="80%"><input type="checkbox" name="hide" value="1"> (widoczne podczas dodawania ogłoszenia, ukrytę w widoku)</td>
						</tr>
						<tr class="active">
							<td width="20%">
								<input type="submit" name="add" value="dodaj"> 
							</td>
							<td width="80%"></td>
						</tr>
					</tbody>
				</table>
			</form>
			<br/>
        <table class="table">
					<tbody>
						<tr class="success">
							<td width="30%"><b>Identyfikator</b></td>
							<td width="30%"><b>Nazwa</b></td>
							<td width="10%"><b>Wymagany</b></td>
							<td width="10%"><b>Ukryty</b></td>
							<td width="20%"><b></b></td>
						</tr>
			{foreach from=$attrlist key=key item=item}
						<form action="{$site_url}panel/attributes/edit/{$item.aid}" method="post">
						<input type="hidden" name="token" value="{$token}">
						<tr class="active">
							<td width="30%">{$item.aid}
							</td>
							<td width="30%"><input type="text" name="name" value="{$item.name}" size="40"></td>
							<td width="10%">
                <select name="required">
                  {if $item.required eq 1}
                    <option value="1" selected>tak</option>
                    <option value="0">nie</option>
                  {else}
                    <option value="1">tak</option>
                    <option value="0" selected>nie</option>
                  {/if}
                </select>
							</td>
							<td width="10%">
                <select name="hide">
                  {if $item.hide eq 1}
                    <option value="1" selected>tak</option>
                    <option value="0">nie</option>
                  {else}
                    <option value="1">tak</option>
                    <option value="0" selected>nie</option>
                  {/if}
                </select>
               </td>
							<td width="20%"><input type="submit" name="edit" value="zmień"></form>
							<form action="{$site_url}panel/attributes/delete/{$item.aid}" method="post">
              <input type="hidden" name="token" value="{$token}">
              <input type="submit" name="edit" value="usuń" onClick="return confirm('Czy bezpowrotnie usunąć atrybut?')"></form>
							</td>
						</tr>
			{/foreach}
			</tbody>
				</table>
		</div>
	</div>
