<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	HomepageView.class.php
 * Version	:	1.0
 *
 * Info		:	View for homepage
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	29.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class PanelView Extends BasicView {
	public function __construct( $input )
	{
		parent::__construct( $input );
		$this->viewMakeDisplay();
	}
  }
?>
 