<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	PanelController.class.php
 * Version	:	1.0
 *
 * Info		:	Panel Module Controller
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	29.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class PanelController Extends BasicController {

  protected $access = 99;	
  protected $access_exceptions = array();
  
	public function __construct() {
		parent::__construct();
	}
	
  public function noAccess()
  {
	registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie masz uprawnień do wyświetlenia tej strony' , '60', '/');
	$this->redirect( registry::getSetting('CNF_SITE_URL') );
  }
  
  public function errorAndNotice()
	{
		/* Cookie Error */
		$cookie_error = registry::getObject( 'cookie' )->get( 'error' ); 
		if( !empty( $cookie_error ) ) {
			registry::getObject( 'error' )->storeError( 'panel', $cookie_error );
			registry::getObject( 'cookie' )->unsetcookie( 'error' );
		}
		/* Cookie Notice */
		$cookie_notice = registry::getObject( 'cookie' )->get( 'notice' ); 
		if( !empty( $cookie_notice ) ) {
			registry::getObject( 'notice' )->storeNotice( 'panel', $cookie_notice );
			registry::getObject( 'cookie' )->unsetcookie( 'notice' );
		}
	}
  
  public function actionIndex()
  {
	/* echo "INDEX-PANEL"; */
	/*
	$this->errorAndNotice();
	$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
	$this->setViewParam( 'templates',array('header.tpl','panel.tpl','footer.tpl') );
	$this->setViewParam( 'active','index' );
	$this->setViewParam( 'orders_waiting',registry::getObject( 'panel' )->countOrders( 'paid-waiting' ) );
	
	$this->loadView( 'Panel' );
	*/
	$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/users/');
  }

  public function actionBackup()
  {
  	$this->errorAndNotice();
	$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
	$this->setViewParam( 'templates',array('header.tpl','backup.tpl','footer.tpl') );
	$this->setViewParam( 'active','settings' );
	###
	$mysqlDatabaseName =DB_NAME;
	$mysqlUserName =DB_USER;
	$mysqlPassword =DB_PASS;
	$mysqlHostName =DB_SERVER;
	$mysqlExportPath = MYSQL_PATH.date("Y-m-d-H:i:s").'.sql';

	$command='/usr/local/mysql/bin/mysqldump --opt -h' .$mysqlHostName .' -u' .$mysqlUserName .' -p' .$mysqlPassword .' ' .$mysqlDatabaseName .' > ' .$mysqlExportPath;
	exec($command,$output=array(),$worked);
	switch($worked){
		case 0:
			$text = 'Baza danych <b>' .$mysqlDatabaseName .'</b> została zapisana w: <b>' .$mysqlExportPath .'</b>';
			break;
		case 1:
			$text = 'Wystąpił problem z zapisem bazy danych <b>' .$mysqlDatabaseName .'</b> do <b>~/' .$mysqlExportPath .'</b>';
			break;
		case 2:
			$text = 'Wystąpił błąd podczas połączenia.';
			break;
	}
	//echo $command;
	###
	$this->setViewParam( 'text',$text );
	$this->loadView( 'Panel' );
  }

  public function actionSettings()
  {
	$this->errorAndNotice();
	$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
	$this->setViewParam( 'templates',array('header.tpl','settings.tpl','footer.tpl') );
	$this->setViewParam( 'active','settings' );
	if( !empty( registry::getObject( 'input' )->post ) ) {
		registry::getObject( 'panel' )->updateSettings();
	}
	
	$this->setViewParam( 'settings_opt',registry::getObject( 'panel' )->getSettings() );
	
	$this->loadView( 'Panel' );
  }
  
  public function actionAddcms()
  {
  		$inp = registry::getObject( 'input' );
  		if( !empty($inp->post[title]) && !empty($inp->post[link]) && !empty($inp->post[pageText])) {
  				registry::getObject( 'panel' )->addCms( $inp->post[title], $inp->post[link], $inp->post[pageText] );
  				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Strona została dodana.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/cms/');
  		}else{
  				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać wszystkie dane.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/cms/');
  		}
  }
  
  public function actionDelcms()
  {
  		$inp = registry::getObject( 'input' );
  		if( !empty($inp->params[id]) ) {
  				registry::getObject( 'panel' )->delCms( $inp->params[id] );
  				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Strona została usunięta.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/cms/');
  		}else{
  				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz wybrać stronę do usunięcia.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/cms/');
  		}
  }
  
  public function actionEditcms()
  {
  		$inp = registry::getObject( 'input' );
  		if( !empty($inp->post[title]) && !empty($inp->post[link]) && !empty($inp->post[pageText])) {
  				registry::getObject( 'panel' )->editCms( $inp->post[title], $inp->post[link], $inp->post[pageText], $inp->params[id] );
  				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Strona została zapisana.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/cms/edit/'.$inp->params[id]);
  		}else{
  				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać wszystkie dane.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/cms/edit/'.$inp->params[id]);
  		}
  }
  
  public function actionCms()
  {
  		$inp = registry::getObject( 'input' );
  		$this->errorAndNotice();
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
		$this->setViewParam( 'templates',array('header.tpl','cms.tpl','footer.tpl') );
		$this->setViewParam( 'active','cms' );
		$this->setViewParam( 'cmsEditor', 'true' );
		if( $inp->params[edit] ) {
				$cms = registry::getObject( 'panel' )->getCms( $inp->params[edit] );
				$this->setViewParam( 'action', 'edit' );
				$this->setViewParam( 'title', $cms[0][title]);
				$this->setViewParam( 'link', $cms[0][link]);
				$this->setViewParam( 'pageText', $cms[0][text]);
				$this->setViewParam( 'id', $cms[0][id]);
		}		
		
		$this->setViewParam( 'cmslist', registry::getObject( 'panel' )->getCmsList() );
		$this->loadView( 'Panel' );
  }
  
  
  
  public function actionPayment()
  {
  		$inp = registry::getObject( 'input' );
  		$this->errorAndNotice();
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
		$this->setViewParam( 'templates',array('header.tpl','pay_edit.tpl','footer.tpl') );
		$this->setViewParam( 'active','payment' );
		
		if( $inp->post[token] && $inp->post[price_id] ) {
			if( !empty( $inp->post[price]) ) {
					registry::getObject( 'panel' )->priceEdit( $inp->post[price_id], $inp->post[price] );
			}else{
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać cenę.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/payment/');
			}
		}		
		
		$this->setViewParam( 'paylist', registry::getObject( 'panel' )->getPrices() );
		$this->loadView( 'Panel' );
  }
  
  public function actionEditad()
  {
  	 	$inp = registry::getObject( 'input' );
  		$this->errorAndNotice();
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
		$this->setViewParam( 'templates',array('header.tpl','ad_edit.tpl','footer.tpl') );
		$this->setViewParam( 'active','ads' );
		$ad = registry::getObject( 'panel' )->getAd( $inp->params[id]);
		$this->setViewParam( 'title', $ad[0][title] );
		$this->setViewParam( 'city', $ad[0][city] );
		$this->setViewParam( 'city_district', $ad[0][city_district] );
		$this->setViewParam( 'phone_number', $ad[0][phone_number] );
		$this->setViewParam( 'email', $ad[0][email] );
		$this->setViewParam( 'price', $ad[0][price] );
		$this->setViewParam( 'adText', $ad[0][text] );
		$this->setViewParam( 'adedit', 'true' );
		$this->setViewParam( 'aid', $ad[0][aid] );
		$this->setViewParam( 'cat_id', $ad[0][category_id]);
	  	$this->setViewParam( 'auction', $ad[0]['auction']);
		$this->setViewParam( 'category_tree', registry::getObject( 'panel')->categoryIterator( 0 ));
		
		
		
		if ( $inp->post[token] ) {
			if( !empty($inp->post[title]) && !empty($inp->post[city]) && !empty($inp->post[phone_number]) && !empty($inp->post[email]) && !empty($inp->post[price]) && !empty($inp->post[adText]) ) {
				$input[city] = $inp->post[city];
				$input[city_district] = $inp->post[city_district];
				$input[title] = $inp->post[title];
				$input[phone_number] = $inp->post[phone_number];
				$input[email] = $inp->post[email];
				$input[price] = $inp->post[price];
				$input[text] = $inp->post[adText];
				$input[id] = $ad[0][aid];
				$input[category] = $inp->post[category];
				
				registry::getObject( 'panel' )->editAd( $input );
			}else{
				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Musisz podać wszystkie wymagane dane.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/editad/id/'.$inp->params[id].'/');
			}
		}
		
		$this->loadView( 'Panel' );
  }

  public function actionSuspendauction()
  {
	  $inp = registry::getObject( 'input' );
	  if( $inp->params[id] ) {
		  registry::storeObject( 'auction', 'auction' );
		  $objAuction = registry::getObject( 'auction' );
		  $objAuction->getAuctionIdByAdId($inp->params[id]);
		  if($objAuction->auction['ended']==1) {
			  registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Aukcja jest już zakończona.', '60', '/');
			  $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/editad/id/'.$inp->params[id]);
		  }else{
			  if($objAuction->suspendAuction()) {
				  registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Aukcja została zawieszona. Właściciel oraz uczestnicy zostali powiadomieni.', '60', '/');
				  $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/editad/id/'.$inp->params[id]);
			  }else{
				  registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wystąpił błąd podczas zakańczania aukcji.', '60', '/');
				  $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/editad/id/'.$inp->params[id]);
			  }
		  }

	  }else{
		  $this->redirect( registry::getSetting('CNF_SITE_URL').'panel');
	  }
  }
  
  public function actionDelad()
  {
  		$inp = registry::getObject( 'input' );
  		if( $inp->params[id] ) {
  				registry::getObject( 'panel' )->delAd( $inp->params[id] );
  				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Ogłoszenie zostało skasowane.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/ads/');
  		}
  }
  
  public function actionAds()
  {
	   $inp = registry::getObject( 'input' );
  		$this->errorAndNotice();
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
		$this->setViewParam( 'templates',array('header.tpl','ads.tpl','footer.tpl') );
		$this->setViewParam( 'active','ads' );
		
		$adlist = registry::getObject( 'panel' )->getAds();
		$this->setViewParam( 'adlist', $adlist );
		$this->setViewParam( 'search', $inp->post[search] );
		$this->setViewParam( 'user_id', $inp->post[user_id] );
		
		$this->loadView( 'Panel' );
  }
  
  public function actionCategorydel()
  {
  		$inp = registry::getObject( 'input' );
  		if( registry::getObject( 'panel' )->delCategory( $inp->params[cat_id] ) ) {
  			registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Kategoria została usunięta.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/category/');
		}else{
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Wystąpił błąd podczas usunięcia kategorii. Wygląda na to, że w kategorii znajdują się podkategorie.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/categoryedit/cat_id/'.$inp->params[cat_id]);
		}
  }
  
  public function actionCategoryedit()
  {
  		$this->errorAndNotice();
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
		$this->setViewParam( 'templates',array('header.tpl','category_edit.tpl','footer.tpl') );
		$this->setViewParam( 'active','category' );
		$inp = registry::getObject( 'input' );	
		
		if( empty( $inp->params[cat_id] ) || $inp->params[cat_id]==0 ) {
			registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie wybrano kategorii do edycji.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/category/');
		}else{
			$catdata = registry::getObject( 'panel' )->getCategoryData( $inp->params[cat_id] );
			$catattrdata =  registry::getObject( 'panel' )->getCategoryAttributes( $inp->params[cat_id] );
			$attrlist = registry::getObject( 'panel' )->getAttrList();
			foreach( $catattrdata as $key => $value ) {
					$catattr[] = $value[attribute];
			}
			foreach( $attrlist as $key => $value ) {
				if( in_array($value[aid], $catattr) ) {
					$attrlist[$key][checked] = "true";
				}
			}
			
			/*
			echo "<pre>";
			print_r($attrlist);
			echo "</pre>";
			echo "<pre>";
			print_r($catattr);
			echo "</pre>";
			*/
			if( $inp->post[token] ) {
				if( !empty($inp->post[name]) && !empty($inp->post[title_format]) && !empty($inp->post[link] )) {
						$input[name] = $inp->post[name];
						$input[title_format] = $inp->post[title_format];
						$input[cat_id] = $inp->params[cat_id];
						$input[link] = $inp->post[link];
						$input[attr] = $inp->post[attr];					
						registry::getObject( 'panel' )->editCategory( $input );
						registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Kategoria została dodana.', '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/categoryedit/cat_id/'.$inp->params[cat_id]);
				}else{
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie podano wszystkich wymaganych danych.', '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/categoryedit/cat_id/'.$inp->params[cat_id]);
				}
			}
			
			$this->setViewParam( 'name', $catdata[0][name] );
			$this->setViewParam( 'cat_cid', $inp->params[cat_id] );
			$this->setViewParam( 'link', $catdata[0][link] );
			$this->setViewParam( 'title_format', $catdata[0][title_format] );
			$this->setViewParam( 'attr_list', $attrlist );
		}
		$this->loadView( 'Panel' );
  }
  
  public function actionCategoryadd()
  {
  		$this->errorAndNotice();
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
		$this->setViewParam( 'templates',array('header.tpl','category_add.tpl','footer.tpl') );
		$this->setViewParam( 'active','category' );
		$inp = registry::getObject( 'input' );	
		
		/*
		echo "<pre>";
		print_r($inp->params);
		echo "</pre>";
		*/
		if( empty( $inp->params[parent] ) || $inp->params[parent]==0 ) {
			$catlist = registry::getObject( 'panel' )->getCategoryList( (int) 0 );
			$parent_name = "";
		}else{
			$catlist = registry::getObject( 'panel' )->getCategoryList( (int) $inp->params[parent] );
			$parent_name = registry::getObject( 'panel' )->getCategoryName( $inp->params[parent] );
		}		
		$this->setViewParam( 'parent_name',$parent_name);
		$this->setViewParam( 'parent_cid',$inp->params[parent]);
		
		$this->setViewParam( 'attr_list', registry::getObject( 'panel' )->getAttrList() );
		/*		
		echo "<pre>";
		print_r($inp->params);
		echo "</pre>";
		*/
		if( $inp->post[token] ) {
				if( !empty($inp->post[name]) && !empty($inp->post[title_format]) && !empty($inp->post[link] )) {
						$input[name] = $inp->post[name];
						$input[title_format] = $inp->post[title_format];
						$input[parent] = $inp->params[parent];
						$input[link] = $inp->post[link];
						$input[attr] = $inp->post[attr];					
						registry::getObject( 'panel' )->addCategory( $input );
						registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Kategoria została dodana.', '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/category/parent/'.$inp->params[parent]);
				}else{
					registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie podano wszystkich wymaganych danych.', '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/categoryadd/parent/'.$inp->params[parent]);
				}
		}
		
		$this->loadView( 'Panel' );
  }
  
  public function actionCategory()
  {
  		$this->errorAndNotice();
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
		$this->setViewParam( 'templates',array('header.tpl','category.tpl','footer.tpl') );
		$this->setViewParam( 'active','category' );
		$inp = registry::getObject( 'input' );	
		
		if( empty( $inp->params[parent] ) || $inp->params[parent]==0 ) {
			$catlist = registry::getObject( 'panel' )->getCategoryList( (int) 0 );
			$parent_name = "Kategorie główne";
		}else{
			$catlist = registry::getObject( 'panel' )->getCategoryList( (int) $inp->params[parent] );
			$parent_name = registry::getObject( 'panel' )->getCategoryName( $inp->params[parent] );
		}		
		$this->setViewParam( 'catlist', $catlist );
		$this->setViewParam( 'parent_name',$parent_name);
		$this->setViewParam( 'parent_cid',$inp->params[parent]);
		$this->loadView( 'Panel' );
  }
  
  public function actionAttributes()
  {
  		$this->errorAndNotice();
		$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
		$this->setViewParam( 'templates',array('header.tpl','attr.tpl','footer.tpl') );
		$this->setViewParam( 'active','attributes' );
		$inp = registry::getObject( 'input' );	
		/* edit */
		
		if( $inp->params[edit] ) {
			if( !empty( $inp->params[edit] ) ) {
					$input[aid] = $inp->params[edit];
					$input[name] = $inp->post[name];
					$input[required] = $inp->post[required];
					$input[hide] = $inp->post[hide];
					registry::getObject( 'panel' )->editAttribute( $input );
					registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Atrybut został zmieniony.', '60', '/');
					$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/attributes/');
			}
		}
		
		/* delete */
		if( $inp->params[delete] ) {
			if( !empty( $inp->params[delete] ) ) {
				registry::getObject( 'panel' )->deleteAttribute( $inp->params[delete] );
				registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Atrybut został usunięty.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/attributes/');
			}
		}
		
		/* add */
		if( $inp->params[addattribute] ) {
			if( !empty( $inp->post[attr_id] ) ) {
					if( !empty( $inp->post[name]) ) {
							$input[aid] = $inp->post[attr_id];
							$input[name] = $inp->post[name];
							$input[required] = $inp->post[required];
							$input[hide] = $inp->post[hide];
							registry::getObject( 'panel' )->addAttribute( $input );
							registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Atrybut został dodany.', '60', '/');
							$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/attributes/');
					}else{
						registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać wszystkie dane.', '60', '/');
						$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/attributes/');
					}
			}else{
				registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać wszystkie dane.', '60', '/');
				$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/attributes/');
			}
		}
		
		$this->setViewParam( 'attrlist',registry::getObject( 'panel' )->getAttrList() );
	
		$this->loadView( 'Panel' );
  }

  public function actionUsers()
  {
	$this->errorAndNotice();
	$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
	$this->setViewParam( 'templates',array('header.tpl','userlist.tpl','footer.tpl') );
	$this->setViewParam( 'active','users' );

	if( registry::getObject('input')->params['filter']=="true" ) {
		$inp = registry::getObject('input')->post;
		$url = registry::getSetting( 'CNF_SITE_URL' )."panel/users/";
		if( !empty($inp[user_id]) ) {
			$url.="user_id/".$inp[user_id]."/";
		}
		if( !empty($inp[user_email]) ) {
			$url.="user_email/".$inp[user_email]."/";
		}
		if( !empty($inp[user_type]) ) {
			if($inp[user_type]!="wszystkie" ) {
				$url.="user_type/".$inp[user_type]."/";
			}
		}
		parent::redirect($url);
		//echo $url;
	}else{
		$user_list = registry::getObject( 'panel' )->getUserList();
		$this->setViewParam( 'userlist',$user_list );
		$inp = registry::getObject('input')->params;
		if( !empty($inp[user_id]) ) {
			$this->setViewParam( 'filter_user_id', $inp[user_id]);
		}
		if( !empty($inp[user_email]) ) {
			$this->setViewParam( 'filter_user_email', $inp[user_email]);
		}
		if( !empty($inp[user_type]) ) {
			if($inp[user_type]!="wybierz" ) {
				if($inp[user_type]=="normal") {
					$this->setViewParam( 'selectNormal',' selected');
				}else{
					$this->setViewParam( 'selectSub',' selected');
				}
			}
		}
		
		if( registry::getObject('input')->post[send_email]=="1" ) {
			$Semail = new Mail();
			//$Semail->SingleTo = true;
			foreach($user_list as $key => $value ) {
				$emc = 0;
				if($value[status]=="1" ) {
					$Semail->AddBCC($value[email]);
					$emc = $emc+1;
				}
			}
			$Semail->Subject = 'Wiadomość z nawynajmij.pl';
			$Semail->AltBody = 'Open in HTML mode';
			$Semail->MsgHTML(registry::getObject('input')->post[message]);
			$Semail->Send();
			$Semail->ClearAddresses();
			
			$nt[0][0] = "Wiadomości email zostały wysłane do ".$emc." potwierdzonych użytkowników";
			$this->setViewParam( 'notice',$nt);
		}
		
		$this->loadView( 'Panel' );
	}
  }
 
  
  public function actionUser()
  {
	$this->errorAndNotice();
	$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
	$this->setViewParam( 'templates',array('header.tpl','user.tpl','footer.tpl') );
	$this->setViewParam( 'active','users' );
	
	if( !empty( registry::getObject( 'input' )->params[userid] ) ) {
	  if( registry::getObject( 'input' )->params[userid]=="1" ) {
	    registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Użytkownika o ID 1 nie można edytować.', '60', '/');
	    $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/users' );
	  }else{
	    /* password edit */
	    if( registry::getObject( 'input' )->params[edit]=="password" ) {
	      if( !empty( registry::getObject( 'input' )->post[newpass] ) ) {
		$new_pass = md5( WDSALT.registry::getObject( 'input' )->post[newpass] );
		registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'password',$new_pass );
		registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Hasło zostało zmienione.', '60', '/');
		$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	      }else{
		 registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać nowe hasło.', '60', '/');
		 $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	      }
	    }
	    
	    /* email edit */
	    if( registry::getObject( 'input' )->params[edit]=="email" ) {
	      if( !empty( registry::getObject( 'input' )->post[email] ) ) {
		$new_email = registry::getObject( 'input' )->post[email];
		registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'email',$new_email );
		registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Adres email został zmieniony.', '60', '/');
		$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	      }else{
		 registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz podać nowy email.', '60', '/');
		 $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	      }
	    }
		
		 /* user delete */
		 if( registry::getObject( 'input' )->params[edit]=="delete" ) {
		 	registry::getObject( 'panel' )->deleteUser( registry::getObject( 'input' )->params[userid] );
	    	registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Użytkownik został usunięty.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/users/' );
	    }
	    
	    /* role edit */
	    if( registry::getObject( 'input' )->params[edit]=="role" ) {
	      if( !empty( registry::getObject( 'input' )->post[role] ) ) {
		if( registry::getObject( 'input' )->post[role]=="admin" ) {
		  $new_role=99;
		}else{
		  $new_role=1;
		}
		
		registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'role',$new_role );
		registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Poziom został zmieniony.', '60', '/');
		$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	      }else{
		 registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz wybrać poziom użytkownika.', '60', '/');
		 $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	      }
	    }
	    
	    /* role status */
	    if( registry::getObject( 'input' )->params[edit]=="status" ) {
	      if( !empty( registry::getObject( 'input' )->post[status] ) ) {
		if( registry::getObject( 'input' )->post[status]=="confirmed" ) {
		  $new_status=1;
		}else{
		  $new_role=0;
		}
		
		registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'status',$new_status );
		registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Status został zmieniony.', '60', '/');
		$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	      }else{
		 registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Musisz wybrać status użytkownika.', '60', '/');
		 $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	      }
	    }
	    
	    /* free ads standard */
	    if( registry::getObject( 'input' )->params[edit]=="free_standard" ) {
		 	$free = registry::getObject( 'input' )->post[free_standard];
		 	
		 	registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'free_standard',$free );
		 	
	    	registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Parametry abonamentu zostały zmienione.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
		 }
		 
		  /* free ads high */
	    if( registry::getObject( 'input' )->params[edit]=="free_highlighted" ) {
		 	$free = registry::getObject( 'input' )->post[free_highlighted];
		 	
		 	registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'free_highlighted',$free );
		 	
	    	registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Parametry abonamentu zostały zmienione.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
		 }
		 
		  /* free ads posit */
	    if( registry::getObject( 'input' )->params[edit]=="free_positioned" ) {
		 	$free = registry::getObject( 'input' )->post[free_positioned];
		 	
		 	registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'free_positioned',$free );
		 	
	    	registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Parametry abonamentu zostały zmienione.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
		 }
		 
		  /* free ads mix */
	    if( registry::getObject( 'input' )->params[edit]=="free_mix" ) {
		 	$free = registry::getObject( 'input' )->post[free_mix];
		 	
		 	registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'free_mix',$free );
		 	
	    	registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Parametry abonamentu zostały zmienione.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
		 }
	    
		 /* subscription extension */
		 if( registry::getObject( 'input' )->params[edit]=="subscription_end" ) {
		 	$subscription = registry::getObject( 'input' )->post[subscription_renew];
		 	if( $subscription !=0 ) {
	    		/* extend subscription end */
	    		registry::getObject( 'panel' )->extendSubscription( registry::getObject( 'input' )->params[userid], $subscription );
	    	}
	    	registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Czas abonamentu został zmieniony.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
		 }	    
	    
	    /* subscription edit */
	    if( registry::getObject( 'input' )->params[edit]=="subscription" ) {
	    	$subscription = registry::getObject( 'input' )->post[subscription];
	    	if( empty( $subscription ) ) {
	    		$subscription = 0;
	    	}
	    	
	    	registry::getObject( 'panel' )->updateUserField( registry::getObject( 'input' )->params[userid],'subscription',$subscription );
			if( $subscription !=0 ) {
	    		/* extend subscription end */
	    		registry::getObject( 'panel' )->extendSubscription( registry::getObject( 'input' )->params[userid], 30 );
	    	}
	    	
			registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Status abonamentu został zmieniony.', '60', '/');
			$this->redirect( registry::getSetting('CNF_SITE_URL').'panel/user/userid/'.registry::getObject( 'input' )->params[userid] );
	    }
	    
	    $this->setViewParam( 'userdet',registry::getObject( 'panel' )->getUserDetails() );
	  }
	}else{
	  registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Nie wybrano użytkownika do edycji', '60', '/');
	  $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/users' );
	}
	
	$this->loadView( 'Panel' );
  }
  
  
  
  public function actionEmail()
  {
	$this->errorAndNotice();
	$this->setViewParam( 'dir', array(ROOT_PATH.'/modules/panel/theme') );
	$this->setViewParam( 'templates',array('header.tpl','email.tpl','footer.tpl') );
	$this->setViewParam( 'active','email' );
	if( registry::getObject( 'input' )->params[edit]=="template" ) {
	  if( ( !empty( registry::getObject( 'input' )->post[template] ) ) && ( !empty( registry::getObject( 'input' )->post[email_body] ) ) ) {
	    registry::getObject( 'panel' )->updateEmail( registry::getObject( 'input' )->post[template], registry::getObject( 'input' )->post[email_body] );
	    registry::getObject( 'cookie' )->extended_setcookie( 'notice', 'Wiadomość została zmieniona.', '60', '/');
	    $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/email/index' );
	  }else{
	    registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Treść wiadomości nie może być pusta.', '60', '/');
	    $this->redirect( registry::getSetting('CNF_SITE_URL').'panel/email/index' );
	  }
	}
	
	$this->setViewParam( 'emaillist',registry::getObject( 'panel' )->getEmails() );
	$this->loadView( 'Panel' );
  }
  
}
?>
