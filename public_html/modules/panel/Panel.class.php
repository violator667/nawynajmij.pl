<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Panel.class.php
 * Version	:	1.0
 *
 * Info		:	Panel Homepage
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	29.09.2013
 * 
 */

  if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class Panel {
  
  public function getSettingSections()
  {
	$query = "SELECT DISTINCT section FROM ".DB_PREFIX."_config";
	$result = Db::getConnection()->getQuery( $query, array(), 0, 1, 1 );
	
	return $result;

  }
  
  public function getSettings()
  {
	$sections = $this->getSettingSections();
	$res = array();
	for( $i=0;$i<count( $sections );$i++ ) {
		//$query = "SELECT * FROM ".DB_PREFIX."_config WHERE section='".$sections[$i][section]."'";
		$query = "SELECT * FROM ".DB_PREFIX."_config WHERE section= :section";
		$params = array(":section" => $sections[$i][section]);
		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1 );
		
		if( $result ) {
			/*
			while( $res=$exec->fetch_array( MYSQL_ASSOC ) ) {
				$result[$sections[$i][section]][] = $result;
			}
			*/
			foreach( $result as $key => $value ) {
				$res[$sections[$i][section]][] = $value;
			}
		} 
	}
	return $res;
  }
  
 
  
  public function updateSettings()
  {
	foreach( registry::getObject( 'input' )->post as $key => $value ) {
		if( $key != "token" ) {
			$query = "UPDATE ".DB_PREFIX."_config SET value = :value WHERE param = :key";
			if( is_numeric($value ) ) {
				$params = array(":value" => array( (int) $value, PDO::PARAM_INT ), ":key" => $key);
			}else{
				$params = array(":value" => $value, ":key" => $key);
			}	
			$exec = Db::getConnection()->putQuery( $query,$params, 0, 1, 1 );
		}
	}
  }
  
  public function getUserList()
  {
  	$query = "SELECT * FROM ".DB_PREFIX."_users WHERE id!=1";
  	$params = array();
  	$inp = registry::getObject('input')->params;
  	/* user_id */
  	if( !empty($inp[user_id]) ) {
  			$query.= " AND id=:user_id";
  			$params[":user_id"] = array( (int) $inp[user_id], PDO::PARAM_INT );
  	}
  	/* email */
  	if( !empty($inp[user_email]) ) {
  			$query.= " AND email=:email";
			$params[":email"] = $inp[user_email];
  	}
  	
  	/* subscription */
  	if( !empty($inp[user_type]) ) {
  				$query.= " AND subscription=:sub";
  			if( $inp[user_type]=="subscription" ) {
  				$params[":sub"] = array( (int) 1, PDO::PARAM_INT );
  			}else{
  				$params[":sub"] = array( (int) 0, PDO::PARAM_INT );
  			}
  	}
  	
  	$result = Db::getConnection()->getQuery($query,$params, 0, 1, 1);
	//echo "<pre>";
	//print_r($result);
	//echo "</pre>";
	if( $result ) {
	   return $result;
	}else{
		return array();
	}
  }

  public function getUserDetails( $user_id = NULL )
  {
  	 if( $user_id == NULL ) {
    	$user_id = registry::getObject( 'input' )->params[userid];
    }
    $query = "SELECT * FROM ".DB_PREFIX."_users WHERE id = :user_id ";
    $params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT ) );
    $result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1 );
	 if( $result ) { 
	  return $result;
	 }else{
	 	return array();
	 }
  }
  
  public function delCms( $id )
  {
  		$query = "DELETE FROM ".DB_PREFIX."_cms WHERE id = :id";
  		$params = array(":id" => array( (int) $id, PDO::PARAM_INT ) );
  		$exec = Db::getConnection()->putQuery($query, $params);
  }
  
  public function editCms($title,$link,$text,$id)
  {
  		$query = "UPDATE ".DB_PREFIX."_cms SET title = :title, link = :link, text = :text WHERE id = :id";
  		$params = array(":id" => array( (int) $id, PDO::PARAM_INT ), ":title" => $title, ":link" => $link, ":text" => $text );
  		$exec = Db::getConnection()->putQuery($query, $params);
  }
  
  public function addCms($title,$link,$text)
  {
  		$query = "INSERT INTO ".DB_PREFIX."_cms (title, link, text) VALUES (:title, :link, :text)";
  		$params = array(":title" => $title, ":link" => $link, ":text" => $text );
  		$exec = Db::getConnection()->putQuery( $query, $params);

  }
  
  public function getCms( $id ) 
  {
  		$query = "SELECT * FROM ".DB_PREFIX."_cms WHERE id = :id ";
  		$params = array(":id" => array( (int) $id, PDO::PARAM_INT ) );
  		$exec = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);

  		return $exec;
  }
  
  public function getCmsList()
  {
  		$query = "SELECT * FROM ".DB_PREFIX."_cms";
  		$exec = Db::getConnection()->getQuery( $query, array(), 0, 1, 1);

  		return $exec;
  }
  
  public function priceEdit( $price_id, $value )
  {
  		$query = "UPDATE ".DB_PREFIX."_ad_price SET price = :price WHERE id = :id";
  		$params = array(":price" => $value, ":id" => array( (int) $price_id, PDO::PARAM_INT ));
  		$exec = Db::getConnection()->putQuery($query,$params);
  }
  
  public function getPrices()
  {
  		$query = "SELECT * FROM ".DB_PREFIX."_ad_price";
  		$exec = Db::getConnection()->getQuery( $query, array(), 0, 1, 1);
  		return $exec;
  }
  
  public function extendSubscription( $user_id, $days )
  {
  	$user = $this->getuserDetails($user_id);
  	if( $user[0][subscription_end] != "0000-00-00 00:00:00" ) {
  		$now = date("Y-m-d H:i:s",time() );
  		if( $now > $user[0][subscription_end] ) {
 			$query = "UPDATE ".DB_PREFIX."_users SET subscription_end = DATE(DATE_ADD(NOW(), INTERVAL ".$days." DAY)) WHERE id = :user_id";
  		}else{
  			$query = "UPDATE ".DB_PREFIX."_users SET subscription_end = DATE(DATE_ADD(subscription_end, INTERVAL ".$days." DAY)) WHERE id = :user_id";
  		}
  	}else{
  		$query = "UPDATE ".DB_PREFIX."_users SET subscription_end = DATE(DATE_ADD(NOW(), INTERVAL ".$days." DAY)) WHERE id = :user_id";
  	}
  	$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT ));
  	$result = Db::getConnection()->putQuery($query,$params);
  }
  
  public function updateUserField( $user_id, $field, $value )
  {
  	 $query = "UPDATE ".DB_PREFIX."_users SET `$field` = :value WHERE id = :user_id";
  	 if( is_numeric( $value ) ) {
  	 	$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT), ":value" => array( (int) $value, PDO::PARAM_INT ) );
  	 }else{
  	 	$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT), ":value" => array( $value, PDO::PARAM_STR ) );
  	 }
    
    $exec = Db::getConnection()->putQuery( $query, $params );
  }
  
  public function deleteUser( $user_id )
  {
  	 	$query = "DELETE FROM ".DB_PREFIX."_users WHERE id = :user_id";
  	 	$params = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT) );
   	$exec = Db::getConnection()->putQuery( $query, $params );
  }
  
  public function getCategoryName( $cid )
  {
  		$query = "SELECT name FROM ".DB_PREFIX."_category WHERE cid = :cid";
  		$params = array(":cid" => array( (int) $cid, PDO::PARAM_INT ) );
  		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
  		
  		if( $result ) {
  			return $result[0][name];
  		}else{
  			return array();
  		}
  } 
  
  public function getCategoryList( $parent_cid )
  {
  		$query = "SELECT * FROM ".DB_PREFIX."_category WHERE parent_cid = :parent_cid";
  		$params = array(":parent_cid" => array( (int) $parent_cid, PDO::PARAM_INT ) );
  		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
  		if( $result ) {
  			return $result;
  		}else{
  			return array();
  		}
  }
  
  public function getCategoryData( $cat_id ) 
  {
  		$query = "SELECT * FROM ".DB_PREFIX."_category WHERE cid = :category";
  		$params = array(":category" => array( (int) $cat_id, PDO::PARAM_INT) );
  		$exec = Db::getConnection()->getQuery($query,$params, 0, 1, 1);
  		
  		return $exec;
 
  }
  
  public function delAd( $aid )
  {
  		$query = "DELETE FROM ".DB_PREFIX."_ad WHERE aid = :aid";
  		$params = array(":aid" => array( (int) $aid, PDO::PARAM_INT ));
  		$exec = Db::getConnection()->putQuery($query,$params);
  		
  		/* delete watch list */
  		$query = "DELETE FROM ".DB_PREFIX."_ad_watch WHERE ad_id = :aid";
  		$params = array(":aid" => array( (int) $aid, PDO::PARAM_INT ));
  		$exec = Db::getConnection()->putQuery($query,$params);
  } 
  
  public function editAd( $input )
  {
	   /* get category info */
	   $c_query = "SELECT category_id FROM ".DB_PREFIX."_ad WHERE aid = :aid";
	   $c_params = array(	":aid" => array( (int) $input[id], PDO::PARAM_INT) );
	   $c_exec = Db::getConnection()->getQuery($c_query,$c_params, 0, 1, 1);
	   if($c_exec[0][category_id]!=$input[category]) {
	   	/* ad was moved to another category - update both category num */
	   	registry::storeObject( 'category','category');
	   	$cat = registry::getObject('category');
	   	$cat->countCategoryAds($c_exec[0][category_id]);
	   	$cat->countCategoryAds($input[category]);
	   }
  		$query = "UPDATE ".DB_PREFIX."_ad SET category_id = :category, title = :title, city = :city, city_district = :city_district, email = :email, phone_number = :phone_number, price = :price, text = :text WHERE aid = :aid";
  		$params = array(	":aid" => array( (int) $input[id], PDO::PARAM_INT),
  								":title" => $input[title],
  								":city" => $input[city],
  								":city_district" => $input[city_district],
  								":email" => $input[email],
  								":phone_number" => $input[phone_number],
  								":price" => $input[price],
  								":text" => $input[text],
  								":category" => $input[category]
  								 );
  		$exec = Db::getConnection()->putQuery( $query, $params );
  }
  
  public function categoryIterator( $parent_cid )
  {
  		$query = "SELECT cid,name FROM ".DB_PREFIX."_category WHERE parent_cid = :parent_cid";
  		$params = array(":parent_cid" => array( (int) $parent_cid, PDO::PARAM_INT ) );
  		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
  		foreach( $result as $key => $value ) {
  			$result[$key][children] = $this->categoryIterator( $value[cid] );
  		}
  		return $result;
  }
  
  public function getAd( $aid )
  {
  		$query = "SELECT * FROM ".DB_PREFIX."_ad WHERE aid = :aid";
  		$params = array(":aid" => array( (int) $aid, PDO::PARAM_INT ));
  		$exec = Db::getConnection()->getQuery($query,$params, 0, 1, 1);
  		return $exec;
  }
  
  public function getAds()
  {
  		$inp = registry::getObject( 'input' );
  		
  		$query = "SELECT * FROM ".DB_PREFIX."_ad WHERE status = :status";
  		/* status */
  		switch ( $inp->post['list'] ) {
    		case waiting:
        		$status = 0;
        		break;
        	case finished:
        		$status = 2;
        		break;
    		default:
    			$status = 1;
		}
		$params[":status"] = array( (int) $status, PDO::PARAM_INT );
		
		if( $inp->post['list'] == "finished" ) {
			$query.= " AND expires < :date_exp";
			$params[":date_exp"] = date("Y-m-d H:i:s",time());
		}
		if( $inp->post['list'] == "active" ) {
			$query.= " AND expires > :date_exp";
			$params[":date_exp"] = date("Y-m-d H:i:s",time());
		}
		if( !empty( $inp->post[search]) ) {
			if( strlen($inp->post[search]) >= registry::getSetting('CNF_SEARCH_MIN_LENGHT') ) {
				$query.= " AND title LIKE :search";
				$params[':search'] = '%'.$inp->post[search].'%';
			}
		}
		if( !empty( $inp->post[user_id] ) ) {
				if( registry::getObject( 'validator' )->isIntPositive( $inp->post[user_id] ) ) {
					$query.= " AND user_id = :user_id";
					$params[':user_id'] = array( (int) $inp->post[user_id], PDO::PARAM_INT );
				}
		}
			$query.= " ORDER BY";
		if( $inp->post[sort] == "1" ) {
			$query.= " created DESC";
		}elseif( $inp->post[sort] == "2" ) {
			$query.= " created ASC";
		}elseif( $inp->post[sort] == "3" ) {
			$query.= " expires DESC";
		}elseif( $inp->post[sort] == "4" ) {
			$query.= " expires ASC";
		}elseif( $inp->post[sort] == "5" ) {
			$query.= " price DESC";
		}elseif( $inp->post[sort] == "5" ) {
			$query.= " price ASC";
		}else{
			$query.= " aid DESC";
		}
			//echo $query;
			$result = Db::getConnection()->getQuery($query,$params, 0, 1, 1);
			
			return $result;
		
  }
  
  public function delCategory( $cat_id )
  {
  		/* check for children */
  		$query = "SELECT cid FROM ".DB_PREFIX."_category WHERE parent_cid = :cat_id";
  		$params = array(":cat_id" => array( (int) $cat_id, PDO::PARAM_INT ) );
  		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
  		if( count($result) < 1 ) {
  			/* delete category */
  			$query = "DELETE FROM ".DB_PREFIX."_category WHERE cid = :cat_id";
  			$params = array(":cat_id" => array( (int) $cat_id, PDO::PARAM_INT ) );
  			$exec = Db::getConnection()->putQuery($query,$params);
  			
  			/* delete all ads in this category */
  			$query = "DELETE FROM ".DB_PREFIX."_ad WHERE category_id = :cat_id";
  			$params = array(":cat_id" => array( (int) $cat_id, PDO::PARAM_INT ) );
  			$exec = Db::getConnection()->putQuery($query,$params);
  			return true;
		}else{
			return false;
		}
  }
  
  public function editCategory( $input )
  {
  		$query = "UPDATE ".DB_PREFIX."_category SET name = :name, link = :link, title_format = :title_format WHERE cid = :category";
  		$params = array(
  								":category" => array( (int) $input[cat_id], PDO::PARAM_INT),
  								":name" => $input[name],
  								":link" => $input[link],
  								":title_format" => $input[title_format]
  								 );
  		$exec = Db::getConnection()->putQuery( $query, $params );
  		/* remove all atte for this category */
  		$query2 = "DELETE FROM ".DB_PREFIX."_attribute_category WHERE category = :category";
  		$params2 = array(":category" => array( (int) $input[cat_id], PDO::PARAM_INT) );
  		$exec = Db::getConnection()->putQuery( $query2, $params2 );
  		/* insert attr */
  		$query3 = "INSERT INTO ".DB_PREFIX."_attribute_category (attribute, category) VALUES (:attribute, :category)";
  		foreach( $input[attr] as $key => $value ) {
  			$params3 = array(":attribute" => $value, ":category" => array( (int) $input[cat_id], PDO::PARAM_INT) );
  			$exec = Db::getConnection()->putQuery( $query3, $params3 );
  		}
  }
  
  
  public function addCategory( $input ) 
  {
  		/*
  		echo "<pre>";
  		print_r($input);
  		echo "</pre>";
  		*/
  		$query = "INSERT INTO ".DB_PREFIX."_category (name, link, parent_cid, title_format) VALUES (:name, :link, :parent_cid, :title_format)";
  		$params = array(
  							":name" => trim($input[name]), 
  							":link" => trim($input[link]), 
  							":parent_cid" => array( (int) $input[parent], PDO::PARAM_INT ), 
  							":title_format" => trim($input[title_format])
  							);
  		$exec = Db::getConnection()->putQuery($query,$params);
  		$cat_id = Db::getConnection()->lastInsertId();
  		
  		$query2 = "INSERT INTO ".DB_PREFIX."_attribute_category (attribute, category) VALUES (:attribute, :category)";
  		foreach( $input[attr] as $key => $value ) {
  			$params = array(":attribute" => $value, ":category" => array( (int) $cat_id, PDO::PARAM_INT) );
  			$exec = Db::getConnection()->putQuery( $query2, $params );
  		}
  }
  
  public function getCategoryAttributes( $cat_id )
  {
  		$query = "SELECT attribute FROM ".DB_PREFIX."_attribute_category WHERE category = :cat_id";
  		$params = array(":cat_id" => array( (int) $cat_id, PDO::PARAM_INT) );
  		$result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
  		if( $result ) {
  			return $result;
  		}else{
  			return array();
  		}
  }
  
  public function deleteAttribute( $attr_id )  
  {
  		$query = "DELETE FROM ".DB_PREFIX."_attribute WHERE aid = :attr_id";
  		$query2 = "DELETE FROM ".DB_PREFIX."_attribute_category WHERE attribute = :attr_id";
  	 	$params = array(":attr_id" => array( $attr_id, PDO::PARAM_STR) );
   	$exec = Db::getConnection()->putQuery( $query, $params );
   	$exec = Db::getConnection()->putQuery( $query2, $params );
  }
  
  public function editAttribute( $input )
  {
  		$query = "UPDATE ".DB_PREFIX."_attribute SET name = :name, required = :required, hide = :hide WHERE aid = :aid";
  		$params = array(
  							":aid" => array( $input[aid], PDO::PARAM_STR),
  							":name" => array( $input[name], PDO::PARAM_STR),
  							":required" => array( $input[required], PDO::PARAM_INT),
  							":hide" => array( $input[hide], PDO::PARAM_INT)
  							); 
  		
  		$exec = Db::getConnection()->putQuery( $query, $params );
  }  
  
  public function addAttribute( $input )
  {
  		$query = "INSERT INTO ".DB_PREFIX."_attribute (aid, name, type, required, hide ) VALUES (:aid, :name, :type, :required, :hide )";
  		if( empty($input[required]) ) {
  				$input[required] = (int) 0;
  		}
  		if( empty($input[hide]) ) {
  				$input[hide] = (int) 0;
  		}
  		$params = array(
  							":aid" => array( $input[aid], PDO::PARAM_STR),
  							":name" => array( $input[name], PDO::PARAM_STR),
  							":type" => array( "text", PDO::PARAM_STR),
  							":required" => array( $input[required], PDO::PARAM_INT),
  							":hide" => array( $input[hide], PDO::PARAM_INT)
  							);
  		$exec = Db::getConnection()->putQuery( $query, $params );
  }  
  
  public function getAttrList()
  {
  		$query = "SELECT * FROM ".DB_PREFIX."_attribute";
  		$result = Db::getConnection()->getQuery($query,array(), 0, 1, 1);
  		if( $result ) {
  			return $result;
  		}else{
  			return array();
  		}
  }  
  
  public function getEmails()
  {
	$query = "SELECT * FROM ".DB_PREFIX."_email_template";
	$result = Db::getConnection()->getQuery( $query, array(), 0, 1, 1 );
	if( $result ) {
	  return $result;
	}else{
		return array();
	}
  }
  
  public function updateEmail( $template,$message )
  {
	$query = "UPDATE ".DB_PREFIX."_email_template SET content='$message' WHERE name='$template'";
	$exec = Db::getConnection()->runQuery( $query );
  }

 
}
?>
