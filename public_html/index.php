<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	  index.php
 * Version	:	  1.0
 *
 * Info		  :	  Single Access Point
 *
 * Author  	: 	Michał‚ Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  31.08.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */

 /*
 *	Check PHP version
 */
 if( version_compare( PHP_VERSION, '5.2', '<' ) ) {
	die( 'This script requires PHP 5.2 to run - current PHP version is: '.PHP_VERSION );
 }
 /*
 *	Check for register_globals
 */
 if( ini_get( 'register_globals' ) ) {
	die( 'PHP register_globals are ON - server is NOT safe - this script will not run!' );
 }
 
 /*
 *	Check install
 */
 if( !file_exists( 'config/config.php' ) ) {
	/*
	*	Script is not installed (config.php missing)
	*/
	die( 'There is no config.php file - this script will not run!' );
 }else{
		DEFINE( 'WHISKY','TRUE' );
		/*
		*	Think WD is installed - include config file
		*/
		require_once( 'config/config.php' );
 }

 /*
 *	Register Autoload & FrontController
 */
 require_once( CORE.'/autoload.php' );
 require_once( CORE.'/FrontController.class.php' );
 
 /**
  * Run registry, make token
 **/
 $registry = registry::instance();
 $registry->storeSetting( 'token',md5( time().rand( 1,20000000 ) ) );
 
 /**
  * Run Input, Cookie, Module & Session manager
 **/
 $logger = Logger::getInstance();
 try{
 	 $registry->storeObject( 'validator','validator' );
	 $registry->storeObject( 'input', 'input' );
	 $registry->storeObject( 'cookie','cookie' );
	 $registry->storeObject( 'appconfig','appconfig' );
	 $registry->storeObject( 'session','session' );
	 $registry->storeObject( 'router','router');
	 $registry->storeObject( 'breadcrumb','breadcrumb' );
	 $registry->getObject( 'breadcrumb' )->addElement( array("link" => "homepage/index", "name" => "Strona główna") );
	
	 /* check token */
	 if( !empty( registry::getObject( 'input' )->post ) ) {
		/* make way to talk with DotPay */
		/* TODO make it more FLEXIBLE ! move IP's into config */
		if( ( registry::getObject( 'input' )->server[ 'REMOTE_ADDR' ] == "217.17.41.5" ) || ( registry::getObject( 'input' )->server[ 'REMOTE_ADDR' ] = "195.150.9.37" ) ) { 
		 
		}else{
		 if( ( registry::getObject( 'session' )->getToken() )==( registry::getObject( 'input' )->post['token'] ) ) {
			registry::getObject( 'input' )->clearPost();
		 }else{
			if( empty( registry::getObject( 'input' )->post['token'] ) ) {
					registry::getObject( 'input' )->clearPost();
					$logger->saveLog( "error",array( 'Code'=>'100','Message'=>'No token passed via form.' ) );
			}else{
				registry::getObject( 'session' )->saveToken( registry::getObject( 'input' )->post['token'] );
			}
		 }
		 
	    }
	 }
	 /* end check token */
	 
      /**
      *	Set up Error & Notice Handle 
      **/
     $registry->storeObject( 'error','error' );
     $registry->storeObject( 'notice','notice' );
	 
	 /**
	  * Set up user module
	  **/
	 $registry::storeObject( 'user','user' );
	 
 }catch( AppException $e ) {
 	$logger->saveLog( $e );
 	die( $e->getMessage() );
 }

	/**
	 *	Handle other modules
	**/
	 
	try{
	
		$frontController = new FrontController();
	
		if( $frontController->getModule()==FALSE ) {
			/**
			 *	No module selected - redirect to default module
			**/
			$frontController->redirect( registry::getSetting( 'CNF_SITE_URL' ).strtolower( registry::getSetting( 'CNF_DEFAULT_MODULE' ) ).'/index' );
		}else{
			if( file_exists( MODULES.'/'.$frontController->getModule().'/'.ucfirst( $frontController->getModule() ).'.class.php' ) ) {
			
				$registry->storeObject( $frontController->getModule(),$frontController->getModule() );
				$controller = $frontController->getController();
				$$controller = new $controller;
				$logger->saveLog( "error",array( 'Code'=>'100','Message'=>'Making the '.$controller ) );
				if( $frontController->getAction()==FALSE ) {
					/**
					 *	No action defined - redirect to actionIndex
					**/
					$$controller->redirect( registry::getSetting( 'CNF_SITE_URL' ).$frontController->getModule().'/index' );
				}else{
					$action = 'action'.ucfirst( $frontController->getAction() );
					if ( method_exists( $$controller, $action ) ) {
						
						if( (( $$controller->getAccess() )<=( registry::getObject( 'session' )->getLogged() )) || ( in_array( $action, $$controller->getAccessExceptions() ) ) ) {
							$$controller->$action();
							/* check for subcontroller */
							if( is_object( $$controller->getSubcontroller() ) ) {
								$$controller = $$controller->getSubcontroller();
							}
						}else{
							/* consider to DELETE IT - MAKE ACL ! */
							$$controller->noAccess();
						}
						/*
						echo "<pre>";
						print_r($$controller);
						echo "</pre>";
						*/
							
						/* Save module/action to use it in session */
						registry::storeSetting( 'presentModule', $frontController->getModule() );
						registry::storeSetting( 'presentAction', $frontController->getAction() );
						registry::getObject( 'session' )->saveModule();
													
						/* get view */
						
						$view = $$controller->getView();
						if( !is_object( $view ) ) {
							$logger->saveLog( "error",array( 'Code'=>'100','Message'=>'#### No View in '.$frontController->getModule() ) );
							/* display 404? or other info page */
						}
													 
					}else{
						$logger->saveLog( "error",array( 'Code'=>'100','Message'=>'Invalid action '.$action.' in '.$controller ) );
						$$controller->redirect( registry::getSetting( 'CNF_SITE_URL' ).$frontController->getModule().'/index' );
					}
	
				}
				
			}else{
				/*
				 *	No valid module selected - redirect to main page.
				 */
				$logger->saveLog( "error",array( 'Code'=>'100','Message'=>'Invalid module '.$frontController->getModule().' request' ) );
				$frontController->redirect( registry::getSetting( 'CNF_SITE_URL' ) );
			}
		}
	}catch( Exception $e ) {
		$logger->saveLog( $e );
		echo $e->getMessage();
	}
 ?>