<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	registry.class.php
 * Version	:	1.0
 *
 * Info		:	Registry - used to store objects to access it from anywhere
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	19.08.2013
 * 
 */
 
 /**
  *		USAGE:
  *		$registry = registry::instance();
  *		$registry->storeSetting( 'param','value' );
  *		$registry->storeObject( 'ClassName', 'ClassNameKey' );
  */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }

      
class registry {

	/*
	*	Array of objects
	*	@access private
	*/
	private static $objects = array();
	
	/*
	*	Array of application settings
	*	@access private
	*/
	private static $settings = array();
	
	
	/*
	*	Instance of registry
	*	@access private
	*/
	private static $_instance;
	
	/*
	*	Constructor
	*	@access private 
	*/
	private function __construct()
	{
	
	}
	
	/*
	*	Singleton implementation 
	*	@access public
	*	@return instance of this object
	*/
	public static function instance()
	{
		if( !isset( self::$_instance ) )  
        {  
            $obj = __CLASS__;  
            self::$_instance = new $obj;  
        }  
          
        return self::$_instance;
	}
	
	/*
	*	Prevent cloning of the object
	*	@access public
	*	@error throw
	*/
	public function __clone()
	{
		Throw new Exception( 'Application error: Cloning of the registry obj is not permitted',1 );
	}
	
	/*
	*	Store object in registry
	*	@access	public
	*	@param String $object 	: 	name of the object you store
	*	@param String $key		:	key for $objects array 
	*	@return void
	*/
	public static function storeObject( $object, $key )
	{
			if( !is_object ( self::$objects[ $key ] ) )  
			{  
				self::$objects[ $key ] = new $object( self::$_instance );
			}
			/*else{
				self::$objects[ $key ] = $object;
			}
			*/
	} 
	
	/*
	*	Get object stored in registry
	*	@access public
	*	@param String $key		:	key for array ( $objects )
	*	@return object
	*	@error 	level			:	20 /medium/
	*/
	public static function getObject( $key )
	{
		if( is_object ( self::$objects[ $key ] ) )  
		{  
            return self::$objects[ $key ];  
        }else{
			Throw new Exception( "No object[key]: $key found in registry.<br/>",20);
        }  
	}
	
	/*
	*	Store setting in registry
	*	@access public
	*	@param String $key
	*	@param String $value
	*	@return void
	*/
	public static function storeSetting( $key, $value )
	{
		self::$settings[ $key ] = $value;
	} 
	
	/*
	*	Get stored setting
	*	@access public
	*	@param String $key
	*	@return String
	*/
	public static function getSetting( $key )
	{
		return self::$settings[ $key ];
	}
}
?>