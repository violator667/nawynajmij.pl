<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Session.class.php
 * Version	:	1.0
 *
 * Info		:	Session manager
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	07.09.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class Session {
  
  protected $php_session_id;
  protected $logged;
  protected $user_id;
  protected $created;
  protected $last_access;
  protected $ip;
  protected $token;
  protected $module;
  protected $action;
  protected $fields;
  protected $lifetime = 900; //15 minut
  function __construct()
  {
	session_set_save_handler(
          array(&$this, '_session_open_method'), 
          array(&$this, '_session_close_method'), 
          array(&$this, '_session_read_method'), 
          array(&$this, '_session_write_method'), 
          array(&$this, '_session_destroy_method'), 
          array(&$this, '_session_gc_method')
		);
	//session_name( "WDSESSID" );
	#session_set_cookie_params(time()+$this->lifetime);	
	//session_set_cookie_params($this->lifetime);	
	session_set_cookie_params(0);	
	
	if( registry::getObject( 'cookie' )->get( "WDSESSID" ) ) {
		$this->php_session_id = registry::getObject( 'cookie' )->get( "WDSESSID" );
	}
	session_start();
	$expired = date("Y-m-d H:i:s", time()-$this->lifetime);
	if( !empty($this->last_access) ) {
		/* last_access is empty on first page view in this session */
		if( $this->last_access<=$expired ) {
			//DB::getConnection()->runQuery( "DELETE FROM ".DB_PREFIX."_session WHERE last_access<='$expired'" );
			Db::getConnection()->runQuery( "DELETE FROM ".DB_PREFIX."_session WHERE ses_id='".$this->php_session_id."'" );
			session_destroy();
			Header( "Location: ".registry::getSetting( 'CNF_SITE_URL' ).'user/index' );
		}
	}
  }
  
  public function getSessionId()
  {
	return $this->php_session_id;
  }
  
  public function getLogged()
  {
	return $this->logged;
  }
  
  public function getUserId()
  {
	if($this->logged>0) {
		return $this->user_id;
	}else{
		return 1;
	}
  }
  
  public function getCreated()
  {
	return $this->created;
  }
  
  public function getLastAccees()
  {
	return $this->last_access;
  }
  
  public function getIp()
  {
	return $this->ip;
  }
  
  public function getToken()
  {
	return $this->token;
  }
  
  
  public function getModuleAction()
  {
	return $this->module.'/'.$this->action;
  }
  
  public function updateUserId( $user_id ) 
  {
	Db::getConnection()->runQuery( "UPDATE ".DB_PREFIX."_session SET user_id='$user_id' WHERE ses_id='".$this->php_session_id."'" );
	$this->user_id = $user_id;
  }
  
  public function setLoggedIn( $access_level )
  {
	Db::getConnection()->runQuery( "UPDATE ".DB_PREFIX."_session SET logged='".$access_level."' WHERE ses_id='".$this->php_session_id."'" );
	$this->logged = $access_level;
  } 
    
  public function _session_open_method()
  {
  } 
    
  public function _session_close_method()
  {
  } 
    
  public function _session_read_method( $id )
  {
		$this->php_session_id = $id;
		$now = date("Y-m-d H:i:s",time());
		$exec = Db::getConnection()->runQuery( "SELECT * FROM ".DB_PREFIX."_session WHERE ses_id='".$this->php_session_id."' " );
		if($exec->rowCount()==0) {
			$ip = registry::getObject( 'input' )->server[ 'REMOTE_ADDR' ];
			Db::getConnection()->runQuery( "INSERT INTO `".DB_PREFIX."_session` 
				(ses_id,
				user_id,
				created,
				last_access,
				ip,
				token,
				fields,
				module) VALUES (
				'".$this->php_session_id."',
				'1',
				'".$now."',
				'".$now."',
				'".$ip."',
				'',
				'',
				'')
				" );
			
		}else{
			$result=$exec->fetch( );
			$this->user_id 	= 	$result[ 'user_id' ];
			$this->logged	=	$result[ 'logged' ];
			$this->created	=	$result[ 'created' ];
			$this->last_access	=	$result[ 'last_access' ];
			$this->ip		=	$result[ 'ip' ];
			$this->token	=	$result[ 'token' ];
			if( unserialize( $result[ 'fields' ] )!=FALSE ) {
			  $this->fields	=	unserialize( $result[ 'fields' ] );
			}else{
			  $this->fields = '';
			}
			$mod = explode("/",$result[ 'module' ]);
			$this->module 	= $mod[0];
			$this->action 	= $mod[1];
			
			$ModAction = $this->module."/".$this->action;
			Db::getConnection()->runQuery( "UPDATE ".DB_PREFIX."_session SET last_access='".$now."' WHERE ses_id='".$this->php_session_id."'" );
			if( $this->user_id > 1)	{		
				if( registry::getSetting( 'CNF_SESSION_IP' )=="TRUE" ) {
			 	 if( $this->ip!=registry::getObject( 'input' )->server[ 'REMOTE_ADDR' ] ) {
			  	  registry::getObject( 'cookie' )->extended_setcookie( 'error', 'Twój adres IP się zmienił, ze względów bezpieczeństwa zostałeś wylogowany.' , '60', '/');
			   	 $this->_session_destroy_method();
			   	 $this->__construct();
			 	 }
				}
			}
			
		}
		return( '' );
  } 
  
  public function addField( $field, $value )
  {
      $this->fields[ $field ] = $value;
      $this->saveFields();
  }
  
  public function returnField( $field )
  {
    if ( isset( $this->fields[ $field ] ) ) {
      return $this->fields[ $field ];
    }else{
      return false;
    }
  }
  
  public function saveFields()
  {
    if( !empty( $this->fields ) ) {
      $serialized = serialize( $this->fields );
      Db::getConnection()->runQuery( "UPDATE ".DB_PREFIX."_session SET fields='".$serialized."' WHERE ses_id='".$this->php_session_id."'" );
    }
  }
  
  public function saveGoogleToken( $google_token )
  {
	Db::getConnection()->runQuery( "UPDATE ".DB_PREFIX."_session SET google_token='".$google_token."' WHERE ses_id='".$this->php_session_id."'" );
  }
  
  public function saveToken( $token )
  {
	Db::getConnection()->runQuery( "UPDATE ".DB_PREFIX."_session SET token='".$token."' WHERE ses_id='".$this->php_session_id."'" );
  }
  
  public function saveModule()
  {
	$module = registry::getSetting( 'presentModule' ).'/'.registry::getSetting( 'presentAction' );
	Db::getConnection()->runQuery( "UPDATE ".DB_PREFIX."_session SET  module='".$module."' WHERE ses_id='".$this->php_session_id."'" );
  }
  
  public function _session_write_method()
  {
  } 
    
  public function _session_destroy_method()
  {
	Db::getConnection()->runQuery( "DELETE FROM ".DB_PREFIX."_session WHERE ses_id='".$this->php_session_id."'" );
	session_destroy();
	Header( "Location: ".registry::getSetting( 'CNF_SITE_URL' ) );
  } 
    
  public function _session_gc_method()
  {
	$expired = date("Y-m-d H:i:s", time()-$this->lifetime);
	DB::getConnection()->runQuery( "DELETE FROM ".DB_PREFIX."_session WHERE last_access<='$expired'" );
  } 
  
  }