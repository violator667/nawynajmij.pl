<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	Router.class.php  
 * Version	:	  1.0
 *
 * Info		  : Very Simple Pseudo Routing
 *
 * Author  	: 	Michał‚ Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  01.09.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */
 if(!defined('WHISKY')) {
	die( 'Out of WHISKY SAP - exit!' );
 }
 
 class Router {
 	
 	public function __construct(  )
	{
		
	}
	
	public function linkTo( $param ) 
	{
		$module = $param['module'];
		$module = strtolower( trim( $module ) );
		$link = $link = registry::getSetting( 'CNF_SITE_URL' ).$module.'/';
		foreach( $param as $key => $value ) {
			if( $key != "module" ) {
				if( $key != "action" ) {
					$link.=trim( $key );
					$link.='/';
				}
				$link.=trim( $value );
				$link.='/';
			
			}
		}
		return $link;
	}
	
	public function makeLink($phrase, $maxLength=100000000000000)
	{
	 $replace = array(  
				 'ą' => 'a', 'Ą' => 'A', 'ę' => 'e', 'Ę' => 'E',  
				 'ć' => 'c', 'Ć' => 'C', 'ń' => 'n', 'Ń' => 'N', 'ł' => 'l',  
				 'Ł' => 'L', 'ś' => 's', 'Ś' => 'S', 'ż' => 'z',  
				 'Ż' => 'Z', 'ź' => 'z', 'Ź' => 'Z', 'ó' => 'o', 'Ó' => 'o');  
	 $phrase = str_replace(array_keys($replace), array_values($replace), $phrase);
	 $result = strtolower($phrase);
    $result = preg_replace("/[^A-Za-z0-9\s-._\/]/", "", $result);
    $result = trim(preg_replace("/[\s-]+/", " ", $result));
    $result = trim(substr($result, 0, $maxLength));
    $result = preg_replace("/\s/", "-", $result);
    
    return $result;
	}
	
	public function linkTo2( $param ) {
		$module = $param['module'];
		$module = strtolower( trim( $module ) );
		$link = registry::getSetting( 'CNF_SITE_URL' ).$module;
		
		if( !empty( $param[action] ) ) {
				$link.='/'.$param[action];
		}
		if( !empty( $param[id] ) ) {
				if( empty( $param[idName] ) ) {
						$link.='/id/'.$param[id];
				}else{
						$link.='/'.$param[idName].'/'.$param[id];
				}
		}
		if( !empty( $param[page] ) ) {
				$link.='/page/'.$param[page];
		}
		if( !empty( $param[region] ) ) {
				$link.='/region/'.$param[region];
		}
		if( !empty( $param[city] ) ) {
				$link.='/city/'.$param[city];
		}
		if( !empty( $param[cityDistrict]) ) {
				$link.='/cityDistrict/'.$param[cityDistrict];
		}
		if( !empty( $param[link] ) ) {
				$link.='/'.$param[link];
		}
		
		return $link;
	}
	
 }
?>