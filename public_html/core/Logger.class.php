<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Logger.class.php
 * Version	:	1.0
 *
 * Info		:	Log errors 
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	07.09.2013
 * 
 */

 /*
  *	LOG LEVELS:
  *		100 - 	debug
  *		75	-	info
  *		50	-	notice
  *		25	-	warning
  *		10	-	error
  *		1	-	critical
  */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
   
 
 class Logger {
 
  /*
  const LOG_LEVEL=LOG_LEVEL;
  const LOG_ERRORS=LOG_ERRORS;
  const ROOT_PATH=ROOT_PATH;
  const DEBUG=DEBUG;
  */
  private static $_instance;
	
 private function __construct()
 {
	
 }
 
	
 /*
 *	Prevent cloning of the object
 *	@access public
 */
 public function __clone()
 {
	Throw new AppException( 'Application error: Cloning of the Logger obj is not permitted',50 );
 }
   
 public static function getInstance(){  
  if (is_null(self::$_instance)){  
   $obj = __CLASS__;  
   self::$_instance = new $obj;   
  }  
  return self::$_instance;  
 }  
	
 /**
  *	Save excveption in log file
  */
 public static function saveLog( $error, $error_info = NULL )
 {
	if( is_object( $error ) ) {
		$err[ 'getCode' ] 		= $error->getCode();
		$err[ 'getMessage' ] 	= $error->getMessage();
		$err[ 'getFile' ] 		= $error->getFile();
		$err[ 'getLine' ] 		= $error->getLine();
	}else{
		$err[ 'getCode' ] 		= $error_info[ 'Code' ];
		$err[ 'getMessage' ] 	= $error_info[ 'Message' ];
		$err[ 'getFile' ] 		= $error_info[ 'File' ];
		$err[ 'getLine' ] 		= $error_info[ 'Line' ];
	}
	if( registry::getSetting( 'CNF_LOG_ERRORS' )=="TRUE" ) {
		/** Log only msgs with code equal or greater than defined **/
		if($err[ 'getCode' ]>=registry::getSetting( 'CNF_LOG_LEVEL' ) ) {
			$log_file = fopen(ROOT_PATH.'/logs/system-'.date("d-m-Y").'.log','a');
			fwrite( $log_file,date("d-m-Y H:i:s").'		' );
			fwrite($log_file,' ['.registry::getObject( 'input' )->server[ 'REMOTE_ADDR' ].']	');
			fwrite($log_file,'LEVEL:'.$err[ 'getCode' ].'	');
			fwrite($log_file,'MSG:'.$err[ 'getMessage' ].'	');
			if( !empty( $err[ 'getFile' ] ) ) {
				fwrite($log_file,'[file: '.$err[ 'getFile' ].', line: '.$err[ 'getLine' ].']
');
			}else{
				fwrite($log_file,'
');
			}
			fclose($log_file);
		}
	}
 } 

 } //end class