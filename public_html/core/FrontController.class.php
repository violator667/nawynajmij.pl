<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	FrontController.class.php
 * Version	:	1.0
 *
 * Info		:	Front Controller Class
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	19.08.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 class FrontController Extends BasicController {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function getModule()
	{
		if( !empty( $this->module ) ) {
			return strtolower( $this->module );
		}else{
			return FALSE;
		}
	}
	
	public function getAction()
	{
		return strtolower( $this->action );
	}
	
	public function getController()
	{
		$controller = ucfirst( strtolower( $this->module ) ).'Controller';
		return $controller;
	}
 }