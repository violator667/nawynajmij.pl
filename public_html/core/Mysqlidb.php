<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	db.class.php
 * Version	:	1.1
 *
 * Info		:	PHP5 MYSQLi DB Connection Class Singleton
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	19.08.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }

     
class Mysqlidb {  
  
 const DB_SERVER=DB_SERVER;   
 const DB_USER=DB_USER;  
 const DB_PASS=DB_PASS;  
 const DB_NAME=DB_NAME;  
   
 private $_connection;  
 private static $_dbinstance;  
  
  
 private function __construct(){  
 /* 
 * 	Private constructor to make sure that instance can be made only in this class
 */
  try{  
   $this->_connection= @new mysqli( self::DB_SERVER,  
                                  self::DB_USER,  
                                  self::DB_PASS,  
                                  self::DB_NAME );
    $this->runQuery("SET NAMES utf8");
    /*
	*	Check version of PHP to make sure that any connection errors are handeled
	*/         
	if ( version_compare( PHP_VERSION, '5.3.0', '<' ) ) {
		if( mysqli_connect_error() ) {
			Throw new AppException( 'Database connection error: '.mysqli_connect_error(), 1 );
		}
	}else{
		if( $this->_connection->connect_error ) {
			Throw new AppException( 'Database connection error: '.$this->_connection->connect_error, 1 );
		}
	}                     
  }catch( Exception $err ){
	Throw new AppException( $err->getMessage(), 1 ); 
  }  
    
 }
 
 /*
 *	Prevent cloning of the object
 *	@access public
 *	@error throw
 */
 public function __clone()
 {
	Throw new AppException( 'Application error: Cloning of the Db obj is not permitted',10 );
 }
   
 public static function getConnection(){  
  if ( is_null( self::$_dbinstance ) ){  
   $obj = __CLASS__;  
   self::$_dbinstance = new $obj;   
  }  
  return self::$_dbinstance;  
 }  
 
 public function secureQuery( $input )
 {
	/*
	*	Securing input to prevent SQL Injection
	*/
	$input = $this->_connection->real_escape_string( $input );
	return $input;
 } //end secureQuery
 
 public function runQuery( $query ) {  
	/* 
	*	Execute a SQL query and return the results
	*/  
	if(DEBUG==TRUE) {
		logger::saveLog( "error",array( 'Code'=>'100','Message'=>$query ) );
	}
  if( $res=$this->_connection->query( $query ) ) {
	return $res;   
  }else{
	Throw new AppException( 'DB Error: '.$this->_connection->error,10 );
  }
 }  
}
?>