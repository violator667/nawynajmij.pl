<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Auction.class.php
 * Version	:	1.1
 *
 * Info		:	Auction
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	03.01.2015
 * 
 */

if( !defined( 'WHISKY' ) ) {
    die( 'Out of WHISKY SAP - exit!' );
}

class Auction
{

    /* represents a_auction teblale */
    public $auction = array();

    /* represents a_bid table */
    public $bids = array();

    public $ad_id;

    public $ad_expiration;

    public $ad_status;

    public $ad_owner = 0;

    public $ad_title;

    public $owner_email;

    public $auction_id = 0;

    public $objValidator;

    function __construct()
    {
        registry::storeObject('validator', 'validator');
        $this->objValidator = registry::getObject('validator');
    }

    public function getAdData()
    {
        $query = "SELECT status,expires,user_id,title FROM ".DB_PREFIX."_ad WHERE aid = :aid";
        $params = array(":aid" => array( (int) $this->ad_id));
        $result = Db::getConnection()->getQuery($query, $params, 0,1,1);
        $this->getOwnerEmail();
        if(!empty($result[0])) {
            $this->ad_expiration = $result[0]['expires'];
            $this->ad_status = $result[0]['status'];
            $this->ad_owner = $result[0]['user_id'];
            $this->ad_title = $result[0]['title'];
        }else{
            $this->ad_expiration = date("Y-m-d H:i:s", 0);
            $this->ad_status = 0;
            $this->ad_owner = 0;
            $this->ad_title = '';
        }
    }


    public function getAuctionData()
    {
        $query = "SELECT * FROM " . DB_PREFIX . "_auction WHERE id = :id";
        $params = array(":id" => array((int)$this->auction_id, PDO::PARAM_INT));
        $result = Db::getConnection()->getQuery($query, $params, 0, 1, 1);

        $this->auction = $result[0];

        $this->getBidsData();


//        echo "<pre>";
//        print_r($this->auction);
//        echo "</pre>";

//        echo "<pre>";
//        print_r($this->bids);
//        echo "</pre>";


    }

    public function hideEmail($email = NULL)
    {
        if($email!=NULL) {
            $e = explode('@',$email);
            return $e[0].'@';
        }

    }

    public function getBidsData()
    {
        $query = "SELECT * FROM " . DB_PREFIX . "_bid WHERE auction_id = :id ORDER BY max_bid DESC";
        $params = array(":id" => array((int)$this->auction_id, PDO::PARAM_INT));
        $result = Db::getConnection()->getQuery($query, $params, 0, 1, 1);

        if (!empty($result[0])) {
            foreach($result as $key => $value) {
                $result[$key]['email'] = $this->hideEmail($result[$key]['bidder_email']);
            }
            $this->bids = $result;
        }
    }

    public function endAuction()
    {
        $query = "UPDATE ".DB_PREFIX."_auction SET ended = :ended, end_date = NOW() WHERE id = :auction_id";
        $params = array(":ended" => array( (int) 1, PDO::PARAM_INT), ":auction_id" => array( (int) $this->auction_id));
        Db::getConnection()->putQuery( $query, $params );


        if($this->auction['current_bid_id']=="0") {
            //nie było ofert AUCTION_END_OWNER_NO
            $markers = array("{{title}}");
            $change = array($this->ad_title);
            $this->sendEmail($this->owner_email, 'AUCTION_END_OWNER_NO', $markers, $change, 'Zakończenie aukcji '.$this->ad_title.' - nie było ofert');
        }else{
            //były oferty
            if($this->auction['min_price']=="0") {
                $min_price_info = 'Cena minimalna nie była ustawiona.';
            }else{
                if($this->auction['min_prive']>=$this->auction['current_bid']) {
                    $min_price_info = 'Cena minimalna ('.$this->auction['min_price'].' zł) została osiągnięta.';
                }else{
                    $min_price_info = 'Cena minimalna ('.$this->auction['min_price'].' zł) nie została osiągnięta.';
                }
            }
            //powiadomienie dla wlasciciela AUCTION_END_OWNER
            $winner = $this->getWinner();
            $markers = array("{{title}}",
                            "{{offer}}",
                            "{{bidder_email}}",
                            "{{minimal_price_info}}");
            $change = array($this->ad_title,
                            $this->auction['current_bid'],
                            $winner['bidder_email'],
                            $min_price_info);
            $this->sendEmail($this->owner_email, 'AUCTION_END_OWNER', $markers, $change, 'Zakończenie Twojej aukcji: '.$this->ad_title);

            //powiadomienie dla wszystkich licytujacych AUCTION_END_BIDDERS
            $bidders = $this->getBidders();
            foreach( $bidders as $key => $value ) {
                $markers = array("{{title}}");
                $change = array($this->ad_title);
                $this->sendEmail($value['bidder_email'], 'AUCTION_END_BIDDERS', $markers, $change, 'Zakończenie aukcji: '.$this->ad_title);
            }

            //powiadomienie dla zwyciezcy AUCTION_END_WINNER
            $markers = array("{{title}}",
                            "{{offer}}");
            $change = array($this->ad_title,
                            $this->auction['current_bid']);
            $this->sendEmail($winner['bidder_email'], 'AUCTION_END_WINNER', $markers, $change, 'Twoja oferta jest najwyższa: '.$this->ad_title);
        }
    }

    public function getBidders()
    {
        $query = "SELECT DISTINCT bidder_email FROM ".DB_PREFIX."_bid WHERE auction_id = :auction_id";
        $param = array(":auction_id" => array( (int) $this->auction_id, PDO::PARAM_INT));
        $result = Db::getConnection()->getQuery( $query, $param );

        return $result;
    }

    public function suspendAuction()
    {
        //sprawdz czy admin
        if(registry::getObject( 'user' )->role == '99') {
            $query = "UPDATE ".DB_PREFIX."_auction SET ended = :ended, end_date = NOW() WHERE id = :auction_id";
            $params = array(":ended" => array( (int) 1, PDO::PARAM_INT), ":auction_id" => array( (int) $this->auction_id, PDO::PARAM_INT));
            Db::getConnection()->putQuery( $query, $params );

            //powiadomienie dla wlasciciela AUCTION_SUSPEND
            $markers = array("{{title}}");
            $change = array($this->ad_title);
            $this->sendEmail($this->owner_email, 'AUCTION_SUSPEND', $markers, $change, 'Zawieszenie Twojej aukcji: '.$this->ad_title);

            //powiadomienie dla wszystkich licytujacych AUCTION_SUSPEND
            $bidders = $this->getBidders();
            foreach( $bidders as $key => $value ) {
                $markers = array("{{title}}");
                $change = array($this->ad_title);
                $this->sendEmail($value['bidder_email'], 'AUCTION_SUSPEND', $markers, $change, 'Zawieszenie aukcji: '.$this->ad_title);
            }
            return true;
        }else{
            return false;
        }
    }

    public function updateAuctionBid($bid,$bid_id)
    {
        $query = "UPDATE ".DB_PREFIX."_auction SET current_bid = :bid, current_bid_id = :bid_id WHERE id = :auction_id";
        $params = array(":auction_id" => array( (int) $this->auction_id, PDO::PARAM_INT),
                        ":bid" => array( (int) $bid, PDO::PARAM_INT),
                        ":bid_id" => array( (int) $bid_id, PDO::PARAM_INT));
        Db::getConnection()->putQuery($query,$params, 0, 1, 1);

        $this->sendNotificationToOwner($bid);
    }

    public function getOwnerEmail()
    {
        $user_email_query = "SELECT email FROM ".DB_PREFIX."_users WHERE id= :user_id";
        $user_email_params = array(":user_id" => array( (int) $this->auction['user_id']));
        $user_email = Db::getConnection()->getQuery( $user_email_query, $user_email_params);
        $user_email = $user_email[0]['email'];
        $this->owner_email = $user_email;
    }

    public function sendNotificationToOwner($bid)
    {

        $markers = array("{{title}}",
                         "{{offer}}");
        $change = array($this->ad_title,
                        $bid);
        $this->sendEmail($this->owner_email, 'AUCTION_NEW_OFFER', $markers, $change, 'Nowa oferta w aukcji');
    }

    public function sendEmail($email, $template_name, $markers, $change, $subject)
    {
//        $markers = array("{{email}}","{{site}}","{{verif_link}}");
//        $change = array($email,registry::getSetting( 'CNF_SITE_URL' ),registry::getSetting( 'CNF_SITE_URL' ).'user/emailchangeverification/hash/'.$hash.'/address/'.$email);
        // send email

        $get_email_body_query = "SELECT * FROM ".DB_PREFIX."_email_template WHERE name= :email_template_name";
        $get_email_body_params = array(":email_template_name" => $template_name);
        $get_email_body = Db::getConnection()->getQuery( $get_email_body_query, $get_email_body_params);

        $email_body = str_replace($markers,$change,$get_email_body[0]['content']);
        $Semail = new Mail();
        $Semail->SingleTo = true;
        $Semail->addAddress($email);
        $Semail->Subject = $subject;
        $Semail->AltBody = 'Open in HTML mode';
        $Semail->MsgHTML($email_body);
        $Semail->Send();
        $Semail->ClearAddresses();
    }

    public function bid($amount)
    {
//        echo 'poprzednia oferta: '.$this->auction['current_bid'].' ta oferta: '.$amount;
        if(($this->ad_status=="1") && ($this->ad_expiration > date("Y-m-d H:i:s",time())) && ($this->ad_owner!=registry::getObject('user')->id) && ($this->auction['ended']!=1)) {

            if ($this->objValidator->isNum( array("item" => $amount) )) {
                if ($amount <= $this->auction['current_bid']) {
//                echo 'kwota nie może być mniejsza niż oferowana dotychczas.';
                    return array('result' => 'error', 'msg' => 'Twoja oferta nie może być mniejsza niż obecna stawka licytacji.');
                } else {

                    //sprawdz najwyzsza oferte zlozona do tej porty
                    $best_bid = $this->getBestBid();
                    if($best_bid['max_bid']>=$amount) {
                        //wczesniej zlozono taka sama lub lepsza oferte - wygrywa wczesniejsza
                        $this->updateAuctionBid($amount,$best_bid['id']);
                        $this->saveBid($amount);
                        return array('result' => 'ok', 'msg' => 'Wcześniej złożono taką samą lub lepszą ofertę. Kontunuuj licytację.');
                    }else{
                        //ta oferta jest lepsza
                        if($best_bid['max_bid']==0) {
                            //pierwsza oferta
                            if($amount>=$this->auction['start_price']) {
                                $bid_id = $this->saveBid($amount);
                                if($amount>=$this->auction['min_price']) {
                                    if($this->auction['min_price']!=0) {
                                        //jesli uzytkownik od razu przewyzsza cene minimalna to jedziemy od ceny minimalnej
                                        $this->updateAuctionBid($this->auction['min_price'],$bid_id);
                                        return array('result' => 'ok', 'msg' => 'Twoja oferta jest najwyższa.');
                                    }else{
                                        //cena minimalna nie jest ustawiona - startujemy od początkowej
                                        $this->updateAuctionBid($this->auction['start_price'],$bid_id);
                                        return array('result' => 'ok', 'msg' => 'Twoja oferta jest najwyższa.');
                                    }

                                }else{
                                    //$this->updateAuctionBid($this->auction['start_price'],$bid_id);
                                    $this->updateAuctionBid($amount,$bid_id);
                                    return array('result' => 'ok', 'msg' => 'Twoja oferta jest najwyższa jednak nie osiągnęła ceny minimalnej.');
                                }

                            }else{
//                            echo 'nie mozesz zlozyc oferty ponizej progu licytacji';
                                return array('result' => 'error', 'msg' => 'Nie możesz złożyć oferty poniżej progu licytacji.');
                            }
                        }else{
                            //kolejna oferta
                            $bid_id = $this->saveBid($amount);
                            if($amount>=$this->auction['min_price']) {
                                if($this->auction['min_price']!=0) {
                                    if($this->auction['current_bid']>=$this->auction['min_price']) {
                                        $this->updateAuctionBid($this->auction['current_bid']+1, $bid_id);
                                        return array('result' => 'ok', 'msg' => 'Twoja oferta jest najwyższa.');
                                    }else{
                                        $this->updateAuctionBid($this->auction['min_price'], $bid_id);
                                        return array('result' => 'ok', 'msg' => 'Twoja oferta jest najwyższa.');
                                    }

                                }else{
                                    $this->updateAuctionBid($this->auction['current_bid']+1, $bid_id);
                                    return array('result' => 'ok', 'msg' => 'Twoja oferta jest najwyższa.');
                                }
                            }else{
                                //kwota mniejsza niz cena minimalna
                                $this->updateAuctionBid($amount, $bid_id);
                                return array('result' => 'ok', 'msg' => 'Twoja oferta jest najwyższa jednak nie osiągnęła ceny minimalnej.');
                            }

                        }

                    }

                }
            } else {
//            echo "<pre>";
//            var_dump($amount);
//            echo "</pre>";
                return array('result' => 'error', 'msg' => 'Musisz podać liczbę.');
            }
        }else{
            return array('result' => 'error', 'msg' => 'Nie możesz składać ofert dla tego ogłoszenia.');
        }

    }

    public function getWinner()
    {
        $query = "SELECT * FROM ".DB_PREFIX."_bid WHERE id = :bid_id";
        $params = array(":bid_id" => array( (int) $this->auction['current_bid_id'], PDO::PARAM_INT));
        $result = Db::getConnection()->getQuery($query,$params, 0,1,1);
        if(!empty($result[0])) {
            $result[0]['email'] = $this->hideEmail($result[0]['bidder_email']);
            return $result[0];
        }else{
            return array();
        }
    }

    public function getBestBid()
    {
        $query = "SELECT * FROM ".DB_PREFIX."_bid WHERE auction_id = :auction_id ORDER BY max_bid DESC LIMIT 0,1";
        $params = array(":auction_id" => array( (int) $this->auction_id, PDO::PARAM_INT));
        $result = Db::getConnection()->getQuery($query,$params, 0,1,1);
        if(!empty($result[0])) {
            return $result[0];
        }else{
            return array('max_bid' => 0);
        }
    }

    public function getUserLastBidAmount($user)
    {
        $query = "SELECT max_bid FROM ".DB_PREFIX."_bid WHERE bidder_id = :user AND auction_id = :auction_id ORDER BY id DESC LIMIT 0,1";
        $params = array(":user" => array( (int) $user, PDO::PARAM_INT), ":auction_id" => array( (int) $this->auction_id, PDO::PARAM_INT));
        $result = Db::getConnection()->getQuery($query,$params, 0, 1, 1);
        if(!empty($result[0])) {
            return $result[0]['max_bid'];
        }else{
            return 0;
        }
    }

    public function makeNewAuction($ad_id, $user_id, $start_price, $min_price = 0)
    {
        $query = "INSERT INTO ".DB_PREFIX."_auction (ad_id, user_id, start_price, min_price) VALUES (:ad_id, :user_id, :start_price, :min_price)";
        $params = array(":ad_id" => array( (int) $ad_id, PDO::PARAM_INT),
                        ":user_id" => array( (int) $user_id, PDO::PARAM_INT),
                        ":start_price" => array( (int) $start_price, PDO::PARAM_INT),
                        ":min_price" => array( (int) $min_price, PDO::PARAM_INT));
        Db::getConnection()->putQuery( $query, $params );
    }

    public function saveBid($amount)
    {
        $user = registry::getObject('user');
        if( $amount > $this->getUserLastBidAmount($user->id) ) {
            $query = "INSERT INTO " . DB_PREFIX . "_bid  (auction_id, bidder_email, bidder_id, max_bid, bid_date) VALUES (:auction_id, :bidder_email, :bidder_id, :max_bid, NOW())";
            $params = array(":auction_id" => array((int)$this->auction_id, PDO::PARAM_INT), ":bidder_email" => $user->email, ":bidder_id" => array((int)$user->id, PDO::PARAM_INT), ":max_bid" => array((int)$amount, PDO::PARAM_INT));
            $bid_id = Db::getConnection()->putQuery($query, $params);

            return $bid_id;
        }
    }

    public function getAuctionIdByAdId($ad_id)
    {
        $this->ad_id = $ad_id;
        $query = "SELECT id FROM ".DB_PREFIX."_auction WHERE ad_id = :ad_id ORDER BY id DESC LIMIT 0,1";
        $params = array(":ad_id" => array( (int) $ad_id, PDO::PARAM_INT ));
        $result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);

        if(!empty($result[0])) {
            $this->auction_id = $result[0]['id'];
            $this->getAuctionData();
            $this->getAdData();
            return true;
        }else{
            return false;
        }
    }

}