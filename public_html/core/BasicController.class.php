<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	BacisController.class.php
 * Version	:	1.0
 *
 * Info		:	Abstract Basic Controller Class
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	19.08.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 abstract class BasicController {
	/*
	*	Stores all URL values
	*	@access protected
	*/
	protected $urlvalues;
	
	/*
	*	Stores Module name
	*	@access protected
	*/
	protected $module;
	
	/*
	*	Stores Action for module
	*	@acces protected
	*/
	protected $action;
	
	/*
	*	Defines access to module
	*	$access protected
	*	0	:	all
	*	1	:	logged
	*/
	protected $access = 0;
	/*
	*	Stores all request param in array
	*	@access protected
	*/
	protected $params = array();
	
	/*
	*	Stores all actions that can be accessed for unlogged users
	*	even if access parameter is set to higher value;
	*/
	protected $access_exceptions = array();
	
	
	/*
	*	Stores all error
	*	@access proctected
	*/
	protected $errors = array();
	
	/*
	*	Stores info about usage of module subcontroller
	*	@access proctected
	*/
	protected $subcontroller;
	
	/*
	*	Stores OBJ View
	*	@access protected
	*/
	protected $view;
	
	/*
	*	Stores View settings
	*	@access protected
	*/
	protected $view_array = array();
	
	/*
	*	Reads $_GET
	*/
	public function __construct()
	{
		/*
		* URL will look: http://dummyservername.pl/module/action/param/value/param/value
		* This is the non SEO value: http://dummyservername.pl/index.php?param=module/action/param/value/param/value
		*/
		$this->urlvalues = registry::getObject( 'input' )->get[ 'param' ];
		
		$param = explode( '/',$this->urlvalues );
		$param_count = count($param);
		$this->module = $param[0];
		$this->action = $param[1];
		/*
		* Now make pair param / value
		*/
		for( $i = 2; $i<$param_count; $i++ ) {
			if( $i % 2 == 0 ) {
				$this->params[ $param[ $i ] ] = $param[ $i+1 ];
			}
		}
		$this->setViewParam( 'module',$this->module );
		
	}
	
	/*
	* get desired param
	* @access public
	*/
	public function getParam( $param  )
	{
		return $this->params[$param];
	}
	
	/*
	*	Returns access_exceptions
	*/
	public function getAccessExceptions()
	{
		return $this->access_exceptions;
	}
	
	/*
	*	Returns access to module
	*	@access public
	*/
	public function getAccess()
	{
		return $this->access;
	}
	
	/*
	*  redirect to given url
	*  @access public
	*
	*  TODO:
	*  Make sure that $url contain valid URL address 
	*  Make sure that $url isn't a external address
	*/
	public function redirect( $url = NULL )
	{
		if( $url !=NULL ) {
			Header( "Location: ".$url );
		}else{
			Header( "Location: ".registry::getSetting( 'CNF_SITE_URL' ) );
		}
	}
	
	/*
	*  loads subcontroller for used action
	*  @access public
	*
	*/
	public function loadActionController()
	{
		if( file_exists( MODULES.'/'.strtolower($this->module).'/'.ucfirst( $this->module ).ucfirst( $this->action ).'Controller.class.php' ) ) {
			require_once( MODULES.'/'.strtolower($this->module).'/'.ucfirst( $this->module ).ucfirst( $this->action ).'Controller.class.php' );
						
			$actionControllerName = ucfirst( $this->module ).ucfirst( $this->action ).'Controller';
			$actionController = new $actionControllerName;
			return $actionController;
		}
	}
	
	public function setSubcontroller( $object )
	{
		if( is_object( $object ) ) {
			$this->subcontroller = $object;
		}else{
			Throw new AppException( 'SubController must be an object.' );
		}
	}
	
	public function getSubcontroller()
	{
		return $this->subcontroller;
	}
	
	public function noAccess()
	{
		registry::getObject( 'error' )->storeError( $this->module,'Access Denied' );
	}
	
	public function getView()
	{
		return $this->view;
	}
	
	public function loadView( $view )
	{
		if( file_exists( ROOT_PATH.'/modules/'.$this->module.'/'.$view.'View.class.php') ) {
			require_once( ROOT_PATH.'/modules/'.$this->module.'/'.$view.'View.class.php' );
			$name = $view.'View';
			$this->view = new $name( $this->view_array );
		}
	}
	
	public function setViewParam( $param,$value )
	{
		$this->view_array[ $param ] = $value;
	}
	
	/* Cookie Error */
	public function cookieError( $module ) {
		$cookie_error = registry::getObject( 'cookie' )->get( 'error' ); 
		if( !empty( $cookie_error ) ) {
			registry::getObject( 'error' )->storeError( $module , $cookie_error );
			registry::getObject( 'cookie' )->unsetcookie( 'error' );
		}
	}
	
	/* Cookie Notice */
	public function cookieNotice( $module ) {
		$cookie_notice = registry::getObject( 'cookie' )->get( 'notice' ); 
		if( !empty( $cookie_notice ) ) {
			registry::getObject( 'notice' )->storeNotice( $module , $cookie_notice );
			registry::getObject( 'cookie' )->unsetcookie( 'notice' );
		}
	}
	
	
 }