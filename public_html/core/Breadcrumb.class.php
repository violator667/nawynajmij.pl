<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	Breadcrumb.class.php  
 * Version	:	  1.0
 *
 * Info		  :	Breadcrumb   
 *
 * Author  	: 	Micha�� Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  08.09.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */
 
 if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
  
 Class Breadcrumb {
 	
 	/* stores all breadcrumbs */
 	public $breadcrumb = array(  );
 	
 	/* how to separate items in string representation */
 	public $separator = ' &raquo; ';
 	
 	/* max items in display before cuting the middle */
 	public $display_depth = 8;
 	
 	/* make breadcrub array 
 	*  array[0][link][homepage/index]
 	*				[name][Strona g�owna]
 	*	array[1][link][category/id/{id}/mieszkania/]
 	*				[name][Mieszkania]
 	*	array[2][link][ogloszenie/id/{id}/apartament-warszawa-150m]	
 	*				[name][Apartament Warszawa 150m]
 	*/
 	
 	/* ads element to main breadcrumb array
 	*	example: addElement( array[link][value] )
 	*/
 	public function addElement( array $array ) {
 		array_push( $this->breadcrumb, $array );
 	}
 	
 	/* returns string version of breadcrumb */
 	
 	public function returnString( ) {
 		$string = '';
 		$lenght = count($this->breadcrumb);
 		$last = $lenght-1;
 		$before_last = $last-1;
 		$separator_lenght = strlen( $this->separator );
 		if( $lenght > $this->display_depth ) {
 			
 			/* array is longer than we could display in one line
 			*	so we display 0 elemnt than penultimate alnd the last one
 			*/
 			$string.= "<li><a href=".registry::getSetting( 'CNF_PAGE_URL' ).$this->breadcrumb[0]['link'].">".$this->breadcrumb[0]['name']."</a></li>";
 			$string.= $this->separator;
 			$string.= "<li><a href=".registry::getSetting( 'CNF_PAGE_URL' ).$this->breadcrumb[$before_last]['link'].">".$this->breadcrumb[$before_last]['name']."</a></li>";
			$string.= $this->separator; 			
 			$string.= "<li><a href=".registry::getSetting( 'CNF_PAGE_URL' ).$this->breadcrumb[$last]['link'].">".$this->breadcrumb[$last]['name']."</a></li>";
 		}else{
 			for( $i=0; $i<$lenght; $i++ ) {
 				$string.= "<li><a href=".registry::getSetting( 'CNF_PAGE_URL' ).$this->breadcrumb[$i]['link'].">".$this->breadcrumb[$i]['name']."</a></li></li>";
 				$string.= $this->separator;
 			}
 			/* cut last separator */
 			$string = substr( $string, 0, -$separator_lenght );
 		}
 		return $string;
 	}
 } //end class
?>