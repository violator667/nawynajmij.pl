<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	SimpleXml.class.php
 * Version	:	1.0
 *
 * Info		:	Simple XML
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class SimpleXml {
 
  protected $file;
  protected $xml;
  
  public function __construct( )
  {
  }
  
  public function setFile( $file )
  {
	$this->file = trim( $file );
  }
  
  public function checkFile(  )
  {
	if( file_exists( $this->file ) ) {
		return true;
	}else{
		return false;
	}
  }
  
  public function loadFile()
  {
	$this->xml = simplexml_load_file( $this->file );
  }
  
  public function getXml( $file )
  {
	$this->setFile( $file );
	echo $this->file;
	if( $this->checkFile()==true ) {
		$this->loadFile();
		return $this->xml;
	}else{
		return false;
	}
  }
  
 }