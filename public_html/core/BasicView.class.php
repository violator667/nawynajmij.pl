<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	BasicView.class.php
 * Version	:	1.0
 *
 * Info		:	View
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	28.09.2013
 * 
 */
  
  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
  require_once(ROOT_PATH.'/libs/Smarty.class.php');
  
  class BasicView Extends Smarty {
  
  protected $view_settings = array();
  protected $view_user = array();
  protected $view_templates_files = array();
  protected $view_templates_dirs = array();
  
  public function __construct( $input )
  {
	parent::__construct();
	if( is_array( $input ) ) {
		$this->view_settings = $input;
	}
	if( DEBUG == "false" ) {
	  $this->debugging = false;
	}else{
	  $this->debugging = true;
	}
	//$this->debugging = false;
	if( $this->debugging == false ) {
		$this->caching 		= registry::getSetting( 'CNF_CACHE' );
		$this->cache_lifetime 	= registry::getSetting( 'CNF_CACHE_TIME' );
	}else{
		$this->caching 		= FALSE;
	}
	#set up user 
	$user = $this->viewGetObjectVars( registry::getObject( 'user' ) );
	if( $user!=FALSE ) {
		foreach( $user as $key => $value ) {
			$this->view_user[$key] = $value;
		}
		/* assign email as username if login method is set to email */
		if( registry::getSetting( 'CNF_LOGIN_METHOD' )=='email' ) {
			$this->view_user['username'] = $this->view_user['email'];
		}
		$this->assign( 'user',$this->view_user );
	}
	
	$this->assign( 'token',registry::getSetting( 'token' ) );
	$this->assign( 'site_url',registry::getSetting( 'CNF_SITE_URL' ) );
	$this->assign( 'wdname',WDNAME );
	$this->assign( 'wdversion',WDVERSION );
	$this->assign( 'appversion',APPVERSION );
	$this->assign( 'error',$this->getErrors( $this->view_settings['module'] ) );
	$this->assign( 'notice',$this->getNotices( $this->view_settings['module'] ) );
	/* very simple router */
	$this->registerObject( 'router', registry::getObject( 'router' ), array( 'linkTo' )  );
	$this->viewAddTemplateDir( ROOT_PATH.'/themes/'.registry::getSetting( 'CNF_THEME' ) );
	
	$this->assign( 'breadcrumb', registry::getObject( 'breadcrumb' )->returnString() );
	
	/* assign vars */
	if( count( $this->view_settings )>0 ) {
		foreach( $this->view_settings as $key => $value ) {
			if( ( $key != "dir") && ( $key != "templates" ) ) {
				$this->assign( $key,$value );
			}
		}
	}
	
	/* assign configs */
	if( count( registry::getObject( 'appconfig' )->returnParamArray() )>0 ) {
		foreach( registry::getObject( 'appconfig' )->returnParamArray() as $value ) {
			$this->assign( $value,registry::getSetting( $value ) );
		}
	}
	
	/* templates dir */
	if( count( $this->view_settings['dir'] )>0 ) {
		foreach( $this->view_settings['dir'] as $dir ) {
			$this->viewAddTemplateDir( $dir );
		}
	}
	/* templates */
	foreach( $this->view_settings['templates'] as $template ) {
		$this->viewAddTemplate( $template );
	}
		
  }
  
  public function viewMakeDisplay()
  {
	/* templates DIR */
	$this->setTemplateDir( $this->view_templates_dirs );
	
	foreach( $this->view_templates_files as $template ) {
		$this->display( $template );
	}
  }
  
  public function viewAddTemplateDir( $dir )
  {
	array_unshift( $this->view_templates_dirs,$dir );
  }
  
  public function viewAddTemplate( $file )
  {
	/* Adds template file to the end of array */
	array_push($this->view_templates_files, $file);
  }
  
  
  public function viewGetObjectVars( $object )
  {
	
	 $objVars = get_object_vars( $object );
	 if( is_array( $objVars ) ) {
		return $objVars;
	 }else{
		return FALSE;
	 }
  }
  
  public function getSetting( $key = NULL )
  {
	if( $key!=NULL ) {
		return $this->view_settings[ $key ];
	}else{
		return $this->view_settings;
	}
	
  }
  
  public function getErrors( $module )
  {
	if( registry::getObject( 'error' )->getError( $module )!=FALSE ) {
		return registry::getObject( 'error' )->getError( $module );
	}
	registry::getObject( 'error' )->clear( $module );
  }
  
  
   public function getNotices( $module )
  {
	if( registry::getObject( 'notice' )->getNotice( $module )!=FALSE ) {
		return registry::getObject( 'notice' )->getNotice( $module );
	}
	registry::getObject( 'notice' )->clear( $module );
  }
  
  }