<?php
 /* PHP WHISKY DRINKER
 * 
 * File		:	autoload.php
 * Version	:	1.0
 *
 * Info		:	Definition of autoload core, module, helper files
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	19.08.2013
 * 
 */


  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 function loadCore( $class ) {
	if( file_exists( CORE.'/'.ucfirst( $class ).'.class.php' ) ) {
		require_once( CORE.'/'.ucfirst( $class ).'.class.php' );
	}
 }
 
 function loadModule( $class ) {
	if( file_exists( MODULES.'/'.$class.'/'.ucfirst( $class ).'.class.php' ) ) {
		require_once( MODULES.'/'.$class.'/'.ucfirst( $class ).'.class.php' );
		if( file_exists( MODULES.'/'.$class.'/'.ucfirst( $class ).'Controller.class.php' ) ) {
			require_once( MODULES.'/'.$class.'/'.ucfirst( $class ).'Controller.class.php' );
		}
	}
 }
 
 function loadHelper( $class ) {
	if( file_exists( HELPERS.'/'.ucfirst( $class ).'.class.php' ) ) {
		require_once( HELPERS.'/'.ucfirst( $class ).'class.php' );
	}
 }
 
 function loadOther( $class ) {
	if( file_exists( HELPERS.'/class.'.strtolower($class).'.php' ) ) {
		require_once( HELPERS.'/class.'.strtolower($class).'.php' );
	}
 }
 /*
 * Set loaders
 */
 spl_autoload_register('loadCore');
 spl_autoload_register('loadModule');
 spl_autoload_register('loadHelper');
 spl_autoload_register('loadOther');
 ?>