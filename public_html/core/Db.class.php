<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	  Db.class.php
 * Version	:	  1.0
 *
 * Info		  :	  PDO connection
 *
 * Author  	: 	Michał‚ Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  31.08.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }

class Db {  
  
 const DB_SERVER=DB_SERVER;   
 const DB_USER=DB_USER;  
 const DB_PASS=DB_PASS;  
 const DB_NAME=DB_NAME;  
   
 private static $_instance;
 private $_pdo;
 public $memcache;

 private function __construct() {
	 	  try {
        #$this->_pdo = new PDO( "mysql:host=localhost;dbname=klienci_playgr;charset=utf8", "klienci_playgr", "oP7kCdWW" );
        $this->_pdo = new PDO( "mysql:host=".DB_SERVER.";dbname=".DB_NAME.";charset=utf8", "".DB_USER."", "".DB_PASS."" );
        $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->_pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        
         if( USECACHE==TRUE ) {
		 	 $this->memcache = new Memcache();
		 	 $this->memcache->connect( CACHEHOST, CACHEPORT );
			}
		  }catch(PDOException $e) {
		  			logger::saveLog( $e );
 					Throw new AppException( 'Wystąpił niespodziewany błąd. kod błędu: ' .$e->getCode());
		  }
 }
    
 public static function getConnection() {
 	if (self::$_instance === null) {
 		self::$_instance = new Db();
   }
   return self::$_instance;
 }
 public function lastInsertId()
 {
 	return $this->_pdo->lastInsertId();
 } 
 public function __clone() {
        return false;
 }

 public function __wakeup() {
        return false;
 }
 /* use this function to put something in DB */
 public function putQuery( $query, $params = array() , $id = 'id')
 {
 		logger::saveLog( "error",array( 'Code'=>'100','Message'=>'### Start PDO putQuery ' ) );
 		logger::saveLog( "error",array( 'Code'=>'100','Message'=>'QUERY: '.$query ) );
 		/* convert $params to sring to pass them into log */
 		foreach( $params as $key => $value ) {
 			
 			$log_query.= " [".$key."] ";	
 				if( is_array( $value ) ) {
 						$log_query.= " => array ";
 						foreach( $value as $k => $v ) {
 								$log_query.= " [".$k."] => ".$v." ";
 						}
				}else{
					$log_query.= " => ".$value." ";
				}
 		}
 		
 			try {
 				$sth = $this->_pdo->prepare( $query );
 			
 			foreach( $params as $key => $value ) {
 				if( is_array( $value ) ) {
 					$sth->bindValue( $key, $value[0], $value[1] );
 				}else{
 					$sth->bindValue( $key, $value );
 				}
 			}
 			
 			$sth->execute();
 			//return $sth;
				return $this->_pdo->lastInsertId();
 			}catch( PDOException $e ) {
 				if( DEBUG==true ) {
 					logger::saveLog( $e );
 					Throw new AppException( $e->getMessage() );
 				}else{
 					logger::saveLog( $e );
 					Throw new AppException( 'Wystąpił niespodziewany błąd' );
 				}
 			}
 		
 		
 } //end putQuery
 
 /* use this function to manage prepere queries and memcache 
 *
 *	str $query - example: "SELECT id FROM example WHERE name = :name"
 * array $params - example: array(":name" => "value")
 * int $expiration - the expiration time in seconds
 * int $force_mysql - if set to 1 skips memcache
 * int $dont_cache - if set result will not be stored in memcache
 */
 public function getQuery( $query, $params = array(), $expiration = 0, $force_mysql = 0, $dont_cache = 0 )
 {

 		logger::saveLog( "error",array( 'Code'=>'100','Message'=>'### Start PDO getQuery exp:'.$expiration.' force_mysql:'.$force_mysql.' dont_cache:'.$dont_cache ) );
 		logger::saveLog( "error",array( 'Code'=>'100','Message'=>'QUERY: '.$query ) );
 		/* convert $params to sring to pass them into log */
 		foreach( $params as $key => $value ) {
 			
 			$log_query.= " [".$key."] ";	
 				if( is_array( $value ) ) {
 						$log_query.= " => array ";
 						foreach( $value as $k => $v ) {
 								$log_query.= " [".$k."] => ".$v." ";
 						}
				}else{
					$log_query.= " => ".$value." ";
				}
 		}
 		logger::saveLog( "error",array( 'Code'=>'100','Message'=>'PARAMS: '.$log_query ) );
 		/* check memcache usage in global config */
 		if( ( USECACHE==TRUE ) && ( $force_mysql==0 ) ) {
 			/* use memcache */
 			logger::saveLog( "error",array( 'Code'=>'100','Message'=>'Use Memcache' ) );
 			//$memcache = registry::getObject( 'memcache' );
 			$memcache = $this->memcache;
 			$cache_key = md5( $query . serialize( $params ) );
 			$result = $memcache->get( $cache_key );
 		}
 		
 		if( !$result ) {
 			/* no results in memcache - get it from MySQL */
 			logger::saveLog( "error",array( 'Code'=>'100','Message'=>'No results in Memcache' ) );
 			
 			try {
 			$sth = $this->_pdo->prepare( $query );
 			
 			foreach( $params as $key => $value ) {
 				if( is_array( $value ) ) {
 					$sth->bindValue( $key, $value[0], $value[1] );
 				}else{
 					$sth->bindValue( $key, $value );
 				}
 			}
 			
 			$sth->execute();
 			$result_array = array();

 			while( $row = $sth->fetch() ) {
 				array_push($result_array,$row);
 			}
 			$result = $result_array;
 				
 			}catch( PDOException $e ) {
 				if( DEBUG==true ) {
 					logger::saveLog( $e );
 					Throw new AppException( $e->getMessage() );
 				}else{
 					logger::saveLog( $e );
 					Throw new AppException( 'Wystąpił niespodziewany błąd' );
 				}
 			}
 			if( $dont_cache==0 ) {
 				/* store in memcache */
 				logger::saveLog( "error",array( 'Code'=>'100','Message'=>'Store Memcache' ) );
 				$cache_key = md5( $query . serialize( $params ) );
 			
 				/* check cache expiration time */
 				if( $expiration==0 ) {
 					/* use expiration time from config /DB/ */
 					$expiration = registry::getSetting( 'CNF_MEMCACHE_TIME' );
 				}
 			
 				$memcache->set( $cache_key, serialize( $result ), MEMCACHE_COMPRESSED, $expiration );
 			}

 		}else{
			/* results are in memcache */ 		
			logger::saveLog( "error",array( 'Code'=>'100','Message'=>'Results found in Memcache' ) );
			$result = unserialize( $result );
 		}
 		logger::saveLog( "error",array( 'Code'=>'100','Message'=>'### End PDO getQuery' ) );
 		return $result;
 }
 
 /* simple function to keep downward compatibility */
 public function runQuery( $query ) {   
	try {
		if( $result = $this->_pdo->query( $query ) ) {
			return $result;
		}else{
			return false;
		}
	}catch( PDOException $e ) {
		Throw new Exception( $e->getMessage() );
	}
 }

}
?>