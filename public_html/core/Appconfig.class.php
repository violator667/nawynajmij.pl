<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Appconfig.class.php
 * Version	:	1.0
 *
 * Info		:	Config
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	22.11.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 class Appconfig {
  protected $param_array = array();
  public function __construct( )
  {
    $exec = Db::getConnection()->runQuery( "SELECT param,value FROM ".DB_PREFIX."_config" );
    if( $exec->rowCount()!=0 ) {
      while( $result = $exec->fetch( ) ) {
	registry::storeSetting( 'CNF_'.$result[ 'param' ] , $result[ 'value' ] );
	$this->param_array[] = 'CNF_'.$result[ 'param' ];
      }
    }
  }
  
  public function returnParamArray()
  {
    return $this->param_array;
  }
 }