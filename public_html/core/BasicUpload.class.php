<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	BacisUpload.class.php
 * Version	:	1.0
 *
 * Info		:	Basic Upload
 *
 * Author  	:	Michał Zielonka-Majka 
 * Build	:	24.11.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 class BasicUpload {
 protected $upload_dir = 'upload'; //upload dir without ending slash
 protected $upload_path;
 protected $allowed_extensions = array();
 protected $max_filesize;
 protected $form_name = 'file'; //name of the form used to upload file
 protected $file;
 protected $tmp_name;
 protected $file_name;
 protected $module_used = ''; //name of the module witch uses this class
  public function __construct(  )
  {
    /* make a path */
    $this->upload_path = ROOT_PATH.'/'.$this->upload_dir.'/';
    
    /* set max upload size */
    if( registry::getSetting( 'CNF_UPLOAD_MAX_FILESIZE' ) != NULL ) {
      $this->setMaxUploadSize( registry::getSetting( 'CNF_UPLOAD_MAX_FILESIZE' ) );
    }else{
      $this->setMaxUploadSize( 1 );
    }
  }
  
  public function getFileName()
  {
	return $this->file_name;
  }
  
  public function setModule( $module )
  {
	$this->module_used = trim( $module );
  }
  
  public function setMaxUploadSize( $size_mb )
  {
    $this->max_filesize = $size_mb*1024;
  }
  
  public function addAllowedExtension( $extension )
  {
    /* extension example: jpeg !!not .jpeg !! */
    $extension = trim( $extension );
    array_push( $this->allowed_extensions,$extension );
  }
  
  public function isExtensionAllowed( $extension )
  {
    /* extension example: jpeg !!not .jpeg !! */
    $extension = trim( $extension );
    if( in_array( $extension,$this->allowed_extensions ) ) {
      return TRUE;
    }else{
      return FALSE;
    }
  }
  

  public function makeMiniature( $file, $max_width, $max_height, $directory )
  {
	if( !empty( $file ) ) {
		if( !file_exists(  ROOT_PATH.'/upload/'.$directory.'/'.$file ) ) {
			$fotofile = ROOT_PATH.'/upload/'.$file;
			$miniature = ROOT_PATH.'/upload/'.$directory.'/'.$file;
			/* get image information */
			$fotoinfo = GetImageSize( $fotofile );
			$width = $fotoinfo[0];
			$height	= $fotoinfo[1];
			$mime =	$fotoinfo[2];
			/* get factor */
			if( $width>$height ) {
                $factor = $width/$height;
                $x_scale = $max_width;
                $y_scale= floor( $x_scale/$factor );                  
            }else{
                $factor = $height/$width;
                $y_scale = $max_height;
                $x_scale= floor( $y_scale/$factor );
            }
            
            /* switch mime */
            switch($mime) {
				case 1:
					$im = ImageCreateFromGif( $fotofile );
				break;
				case 2:
					$im = ImageCreateFromJpeg( $fotofile );
				break;
				case 3:
					$im = ImageCreateFromPng( $fotofile );
				break;
			}
			/*  */
			$minifile = ImageCreateTrueColor( $x_scale,$y_scale );
			ImageCopyResampled( $minifile, $im, 0, 0, 0, 0, $x_scale, $y_scale, $width, $height );
			
			switch($mime) {
				case 1:
					ImageGIF( $minifile, $miniature );
				break;
				case 2:
					ImageJPEG( $minifile, $miniature, 100 );
				break;
				case 3:
					ImagePNG( $minifile, $miniature, 2 );
				break;
			}
			/* done */
			imagedestroy( $im );
			imagedestroy( $minifile );
			
		}
	}else{
		Throw new Exception( 'no file passed to make miniature',100 );
	}
  }
  
  public function upload()
  {
    if( empty( $_FILES ) ) {
      Throw new Exception( 'No file detected',100 );
      //Throw new Exception( language::lang('core','BasicUpload','noFile'),10 );
    }else{
      $this->file = registry::getObject( 'input' )->files[ $this->form_name ];
      $this->tmp_name = md5( WDSALT.time() );
      
      $uploaded = urldecode( $this->file[name] );
      $uploaded = $this->upload_path.$this->tmp_name;
      
      $explode = explode( ".",$this->file[name] );
      $explode = array_reverse( $explode );
      $this->file[size] = $this->file[size]/1024;
		if ( (int) $this->file[size] > (int) $this->max_filesize ) {
				//Throw new Exception( $this->file[size].'  '.$this->max_filesize.' za duży plik',100);
				Throw new Exception(' Za duży plik - załaduj zdjęcie poniżej 1MB',100);
		}   
      
      if( $this->isExtensionAllowed( $explode[0] ) ) {
		$uploaded = $uploaded.'.'.$explode[0];
		$this->tmp_name = $this->tmp_name.'.'.$explode[0];
		if( move_uploaded_file( $this->file['tmp_name'], $uploaded ) ) {
		  $this->file_name = $this->tmp_name;
		  try {
		  	$this->makeMiniature( $this->file_name, 60, 60, 'mini' );
		  	$this->makeMiniature( $this->file_name, 180, 180, 'thumb' );
		  	$this->makeMiniature( $this->file_name, 435, 435, 'medium' );
		  }catch(Exception $e) {
		  		echo $e->getMessage();
		  }
		  echo json_encode(array("error" => "null","file" => $this->file_name));
		  return true;
		}else{
		  Throw new Exception( 'There was a problem with your upload.',100 );
		  //Throw new Exception( language::lang('core','BasicUpload','uploadProblem'),10 );
		}
      }else{
			Throw new Exception( '.'.$explode[0].' rozszerzenie nie jest dozwolone',100 );
      }
      
    }
  }
    
 }