<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Cookie.class.php
 * Version	:	1.0
 *
 * Info		:	Cookie manipulation
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	07.09.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class Cookie {
  
  
  /**
   * 	clean cookies
  **/
  public function clean()
  {
	foreach( $_COOKIE as $cookie => $value ) {
		setcookie( $cookie, '', time() - 3600);
	}
  }
  
  /**
   *	get cookie
  **/
  public function get( $cookie )
  {
	if( isset( registry::getObject( 'input' )->cookie[ $cookie ] ) ) {
		return registry::getObject( 'input' )->cookie[ $cookie ];
	}else{
		return null;
	}
  }
  
  /**
   * 	basic set cookie method
  **/
  public function setcookie( $cookie, $value ) 
  {
	if( $value != '' ) {
		setcookie( $cookie, $value);
	}else{
		/* Empty $value so unset this cookie */
		$this->unsetcookie( $cookie );
	}
  }
  
  /**
   * 	Extended set cookie method
  **/
  public function extended_setcookie( $cookie, $value , $expire = 0, $path = '', $domain = '', $secure = 'false', $httponly = 'false' )
  {
	setcookie( $cookie, $value, ( time() + intval( $expire ) ), $path, $domain );
	#setcookie( $cookie, $value, ( time() + intval( $expire ) ), $path, $domain, $secure, $httponly );
  }
  
  /**
   *	Unset a cookie
  **/
  public function unsetcookie( $cookie )
  {
	$expired = time()-3600;
	setcookie( $cookie, '', $expired, '/', '' );
  }
  
  }