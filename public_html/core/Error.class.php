<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Error.class.php
 * Version	:	1.0
 *
 * Info		:	Error management
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	11.09.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class Error {
  
  protected $errors = array();
  
  /*
   *	Store $error msgs
   */
  public function storeError( $class, $error_txt )
  {
	$class = strtolower( $class );
	$class = str_replace( 'controller','',$class );
	$this->errors[ $class ][] = array( $error_txt );
  }

  /*
   *	getError
   */
  public function getError( $class = NULL )
  {
	/*
	 *  If exists remove 'controller' to get real module name
	 */
	 $class = strtolower( $class );
	 
	if( count( $this->errors )>0 ) {
		if( $class!=NULL ) {
			return $this->errors[ $class ];
		}else{
			return $this->errors;
		}
	}else{
		return FALSE;
	}
  }
  
  public function clear( $class = NULL )
  {
	$class = strtolower( $class );
	$class = str_replace( 'controller','',$class );
	
	if( $class==NULL ) {
		unset( $this->errors );
	}else{
		unset( $this->errors[ $class ] );
	}
  }
  
 }