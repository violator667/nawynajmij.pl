<?php
/*
 * NAWYNAJMIJ.PL  
 *
 * PHP WHISKY DRINKER FRAMEWORK
 * 
 * File		  :	Validator.class.php  
 * Version	:	  1.0
 *
 * Info		  :	Class to validate all data	  
 *
 * Author  	: 	Micha�� Zielonka-Majka 
 * Email    :   violator667@gmail.com
 * Build	  :	  16.09.2014
 * 
 * Licence  :   Commercial, for details ask ernes.pl
 */
 
 if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 class Validator
 {
 	/* all validators $input array must look like this:
 		
		$input = array("item" => "the item to validate", "param" => "value") 		
 		
		All validators must return true if it's OK - or false when item isn;t valid 		
 		
 	/* check for email */
 	/* return true if it is a valid email address */
 	public function isEmail( $input )
 	{
 			if( filter_var( $input['item'] , FILTER_VALIDATE_EMAIL )==false ) {
 				return false;
 			}else{
 				return true;
 			}
 	}
 	
 	/* check if given $input is integer */
 	public function isInt( $input )
 	{
 			if( filter_var( $input['item'] , FILTER_VALIDATE_INT )==false ) {
 				return false;
 			}else{
 				return true;
 			}
 	}
 	
 	/* check if given $input is integer and it is > 0 */
 	public function isIntPositive( $input )
 	{
 			$options = array(
    							'options' => array(
       						 'default' => false,
        						 'min_range' => 0
   		 					)
   		 					);
	 		if( filter_var( $input['item'] , FILTER_VALIDATE_INT, $options )==false ) {
 				return false;
 			}else{
 				return true;
 			}
 	}
 	public function isIntNegative( $input )
 	{
 			$options = array(
    							'options' => array(
       						 'default' => false,
        						 'max_range' => 0
   		 					)
   		 					);
	 		if( filter_var( $input['item'] , FILTER_VALIDATE_INT, $options )==false ) {
 				return false;
 			}else{
 				return true;
 			}
 	}
 	
 	/* returns true if is only text */
 	public function isText( $input )
 	{
   	if( !preg_match ("/^[a-zA-Z\s]+$/",$input['item'])) {
     		return false;
   	}else{
     	 	return true;
   	}
 	}

	 /* returns true if is numeric */
	 public function isNum( $input )
	 {
		 if( !preg_match ("/^[0-9\s]+$/",$input['item'])) {
			 return false;
		 }else{
			 return true;
		 }
	 }
 	
 	/* returns true if is alphanumeric */
 	public function isAlphanumeric( $input )
 	{
 		if( !preg_match ("/^[a-zA-Z0-9\s]+$/",$input['item'])) {
     		return false;
   	}else{
     	 	return true;
   	}
 	}
 	
 	/* returns true if lenght is shorter than maxLenght */
 	public function maxLenght( $input )
 	{
 			if( strlen( $input['item'] ) <= $input['maxLenght'] ) {
	 			return true;
 			}else{
 				return false;
 			}
 	}
 	
 	/* returns true if lenght is longer or equal minLenght */
 	public function minLenght( $input )
 	{
 			if( strlen( $input['item'] ) >= $input['minLenght'] ) {
	 			return true;
 			}else{
 				return false;
 			}
 	}
 
 } //end class
?>