<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Notice.class.php
 * Version	:	1.0
 *
 * Info		:	Notice management
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	11.09.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class Notice {
  
  protected $notices = array();
  
  /*
   *	Store $error msgs
   */
  public function storeNotice( $class, $notice_txt )
  {
	$class = strtolower( $class );
	$class = str_replace( 'controller','',$class );
	$this->notices[ $class ][] = array( $notice_txt );
  }

  /*
   *	getError
   */
  public function getNotice( $class = NULL )
  {
	/*
	 *  If exists remove 'controller' to get real module name
	 */
	 $class = strtolower( $class );
	 
	if( count( $this->notices )>0 ) {
		if( $class!=NULL ) {
			return $this->notices[ $class ];
		}else{
			return $this->notices;
		}
	}else{
		return FALSE;
	}
  }
  
  public function clear( $class = NULL )
  {
	$class = strtolower( $class );
	$class = str_replace( 'controller','',$class );
	
	if( $class==NULL ) {
		unset( $this->notices );
	}else{
		unset( $this->notices[ $class ] );
	}
  }
  
 }