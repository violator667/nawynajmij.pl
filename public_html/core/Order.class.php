<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	BacisOrder.class.php
 * Version	:	1.0
 *
 * Info		:	Abstract Basic Order
 *
 * Author  	:	Michał Zielonka-Majka 
 * Build	:	07.01.2014
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 class Order Extends BasicOrder
 {

	 public function setUserId($user_id)
	 {
		 $this->user_id = $user_id;
	 }


	 public function amountFormat()
	 {
		 $this->amount_to_pay = round($this->amount_to_pay, 2);
	 }

	 public function getOrderAdDetail()
	 {
		 $query = "SELECT * FROM " . DB_PREFIX . "_order_detail WHERE type = :type AND order_id = :id";
		 $params = array(":id" => array((int)$this->returnOrderId(), PDO::PARAM_INT),
			 ":type" => $this->type);
		 $result = Db::getConnection()->getQuery($query, $params);
		 return $result;
	 }

	 public function getAdExpiration($ad_id)
	 {
		 $query  = "SELECT expires FROM ".DB_PREFIX."_ad WHERE aid = :aid";
		 $params  = array(":aid" => array( (int) $ad_id, PDO::PARAM_INT ));
		 $result = Db::getConnection()->getQuery($query, $params);
		 return $result;
	 }
 
  public function getAdDetail() 
  {
  		$query = "SELECT * FROM ".DB_PREFIX."_order_detail WHERE order_id = :order_id";
  		$params = array(":order_id" => array( (int) $this->returnOrderId(), PDO::PARAM_INT ));
  		$result = Db::getConnection()->getQuery($query, $params);
  		return $result;
  }
 
  public function updateStatus( $status )
  {
    parent::updateStatus( $status );
    Logger::saveLog( "error",array( 'Code'=>'101','Message'=>'#DotPay### status: '.$status.' for order: '.$this->returnOrderId() ) ); 
    if( $status == 2 ) {
    		
			$det = $this->getAdDetail();
			$date = strtotime("+ ".$det[0][days]." DAYS", time());
			$date = date("Y-m-d H:i:s", $date );
			$days = $det[0][days];
			$high = $det[0][highlighted];
			$posit = $det[0][positioned];
			$ad_id = $det[0][ad_id];
			$ad_detail = $this->getAdExpiration($ad_id);

			$today = date("Y-m-d H:i:s");

			#$query = "UPDATE ".DB_PREFIX."_ad SET expires = :date_exp, highlighted = :highlighted, highlighted_expires = :high_exp, positioned = :positioned, positioned_expires = :posit_exp, status = :status WHERE aid = :ad_id";
			#$query = "UPDATE ".DB_PREFIX."_ad SET expires = DATE_ADD(expires, INTERVAL ".$days." DAY), highlighted = :highlighted, highlighted_expires = DATE_ADD(highlighted_expires, INTERVAL ".$days." DAY), positioned = :positioned, positioned_expires = DATE_ADD(positioned_expires, INTERVAL ".$days." DAY), status = :status WHERE aid = :ad_id";
			if($ad_detail['0']['expires']<$today) {
				$query = "UPDATE " . DB_PREFIX . "_ad SET expires = DATE_ADD(NOW(), INTERVAL " . $days . " DAY), highlighted = :highlighted, highlighted_expires = DATE_ADD(NOW(), INTERVAL " . $days . " DAY), positioned = :positioned, positioned_expires = DATE_ADD(NOW(), INTERVAL " . $days . " DAY), status = :status WHERE aid = :ad_id";
			}else{
				$query = "UPDATE ".DB_PREFIX."_ad SET expires = DATE_ADD(expires, INTERVAL ".$days." DAY), highlighted = :highlighted, highlighted_expires = DATE_ADD(highlighted_expires, INTERVAL ".$days." DAY), positioned = :positioned, positioned_expires = DATE_ADD(positioned_expires, INTERVAL ".$days." DAY), status = :status WHERE aid = :ad_id";
			}

			$params = array(
								//":date_exp" => $date, 
								":highlighted" => array( (int) $high, PDO::PARAM_INT ),
								//":high_exp" => $date, 
								":positioned" => array( (int) $posit, PDO::PARAM_INT ), 
								//":posit_exp" => $date,
								":status" => array( (int) 1, PDO::PARAM_INT ),
								":ad_id" => array( (int) $ad_id, PDO::PARAM_INT )
								);
			$res = Db::getConnection()->putQuery($query,$params);
			
			/* get ad category and recount */
			$cat_query = "SELECT category_id FROM ".DB_PREFIX."_ad WHERE aid = :ad_id";
			$cat_params = array(":ad_id" => array( (int) $ad_id, PDO::PARAM_INT ) );
			$cat_result = Db::getConnection()->getQuery($cat_query,$cat_params);
			$cat_id = $cat_result[0][category_id];
			registry::storeObject( 'category','category' );
			registry::getObject( 'category' )->countCategoryAds( $cat_id );
			
    }elseif( $status==3 ){
    		
    }elseif( $status==5 ){
    		
    }else{
    		
    }
    
  }
  
  public function setOrderType( $type )
  {
  		$this->type = $type;
  }
  
  public function placeOrder( )
  {
  		$input = registry::getObject( 'input' )->post;
		$ad_id = $input[aid];
		if( $input[payment_method]!="subscription" ) {
			$pay_id = registry::getObject( 'user' )->getPaymentMethod( $input[payment_method] );
			$price  = $pay_id[0][price];
			$high = $pay_id[0][highlighted];
			$posit = $pay_id[0][positioned];
			$auction = $pay_id[0][auction];
			$days = $pay_id[0][days];
		}else{
			$price = 0;
			$days = 30;
			$high = $input[high];
			$posit = $input[posit];
			$auction = $input[auction];
		}
		$user_id = registry::getObject( 'user' )->id;
		$secret = md5( WDSALT.rand( 0, 40000000 ) );
		
		$query_one = "INSERT INTO ".DB_PREFIX."_order (user_id, status, type, amount_to_pay, secret, order_placed ) VALUES (:user_id, :status, :type, :amount_to_pay, :secret, :order_placed )";
		$params_one = array(":user_id" => array( (int) $user_id, PDO::PARAM_INT ), 
									":status" => array( (int) 1, PDO::PARAM_INT ),
									":type" => $this->type, 
									":amount_to_pay" => $price,
									":secret" => $secret,
									":order_placed" => date("Y-m-d H:i:s",time()) );
		
		$place = Db::getConnection()->putQuery($query_one,$params_one);
		$last  = Db::getConnection()->lastInsertId();
		
		$query_two = "INSERT INTO ".DB_PREFIX."_order_detail (order_id, ad_id, days, highlighted, positioned, auction ) VALUES (:order_id, :ad_id, :days, :highlighted, :positioned, :auction)";
		$params_two = array(":order_id" => array( (int) $last, PDO::PARAM_INT ),
									 ":ad_id" => array( (int) $ad_id, PDO::PARAM_INT ),
									 ":days" => array( (int) $days, PDO::PARAM_INT ),
									 ":highlighted" => array( (int) $high, PDO::PARAM_INT ),
									 ":positioned" => array( (int) $posit, PDO::PARAM_INT ),
									 ":auction" => array( (int) $auction, PDO::PARAM_INT ) );
		$detail = Db::getConnection()->putQuery($query_two, $params_two);
		return $last;
  }
  
  
 
 }