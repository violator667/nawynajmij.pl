<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	BacisOrder.class.php
 * Version	:	1.0
 *
 * Info		:	Abstract Basic Order
 *
 * Author  	:	Michał Zielonka-Majka 
 * Build	:	22.11.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 abstract class BasicOrder {
  protected $order_id;
  protected $user_id;
  protected $status;
  protected $amount_to_pay;
  protected $secret;
  protected $order_placed;
  protected $order_edited;
  protected $order_ended;
  protected $type = 'transfer';
  
  public function setOrderId( $order_id ) {
    $this->order_id = $order_id;
  }
  
  public function returnOrderId()
  {
    return $this->order_id;
  }
  
  public function returnUserId()
  {
    return $this->user_id;
  }
  
  public function returnPayGateway()
  {
    return $this->pay_gateway;
  }
  
  public function returnStatus()
  {
    return $this->status;
  }
  
  public function returnAmountToPay()
  {
    return $this->amount_to_pay;
  }
  
  public function returnSecret()
  {
    return $this->secret;
  }
  
  public function returnOrderPlaced()
  {
    return $this->order_placed;
  }
  
  public function returnOrderEdited()
  {
    return $this->order_edited;
  }
  
  public function returnOrderEnded()
  {
    return $this->order_ended;
  }
  
  public function getOrderData() {
  	 $query = "SELECT * FROM ".DB_PREFIX."_order WHERE order_id = :order_id LIMIT 0,1";
  	 $params = array(":order_id" => array( (int) $this->order_id, PDO::PARAM_INT ) );
    $result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
    if( $result ) {
      $res = $result[0];
		$this->user_id 		= $res['user_id'];
		$this->status 		= $res['status'];
		$this->amount_to_pay 	= $res['amount_to_pay'];
		$this->secret 		= $res['secret'];
		$this->order_placed 	= $res['order_placed'];
		$this->order_edited 	= $res['order_edited'];
		$this->order_ended 	= $res['order_edited'];
    }else{
      Throw new Exception( 'Order not found '.$this->order_id ,100 );
    }
  }
  
  public function getOrderBySecret( $secret )
  {
  	 $query = "SELECT * FROM ".DB_PREFIX."_order WHERE secret = :secret LIMIT 0,1";
  	 $params = array(":secret" => $secret );
    $result = Db::getConnection()->getQuery( $query, $params, 0, 1, 1);
   
    if( $result ) {
      $res = $result[0];
			$this->order_id = $res['order_id'];
			$this->getOrderData();
      
    }else{
      Throw new Exception( 'Order not found',100 );
    }
  }

  
  public function updateStatus( $status )
  {
    /*
    *	1 - order placed,
    *	2 - order paid - waiting for delivery
    *	3 - order cancelled
    *	4 - order refunded
    *	5 - order paid - finished 
    */
    $date = date( "Y-m-d H:i:s",time() );
    if( $status==1 ) {
      $query = "UPDATE `".DB_PREFIX."_order` SET status='1',order_placed='$date',order_edited='$date' WHERE order_id='$this->order_id'";
    }elseif( $status==2|| $status==3 || $status==4 || $status==5){
      $query = "UPDATE `".DB_PREFIX."_order` SET status='$status',order_edited='$date',order_ended='$date' WHERE order_id='$this->order_id'";
    }else{
      Throw new Exception( 'Unknown status '.$status.' for order '.$this->order_id,10 );
    }
    
    $exec = Db::getConnection()->runQuery( $query );
  
  }
 
 }
 
 