<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	Mail.class.php
 * Version	:	1.0
 *
 * Info		:	Mail
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	23.11.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
 class Mail Extends PHPMailer {
  
  public function __construct( )
  {
    
  
    //$this->SMTPDebug = 2;
    $this->SMTPDebug = 0;
    
    if( registry::getSetting( 'CNF_MAIL_USE_SMTP' )==TRUE ) {
      $this->IsSMTP();
      $this->SMTPAuth	= true;//registry::getSetting( 'CNF_MAIL_SMTP_AUTH' );
      $this->Host		= registry::getSetting( 'CNF_MAIL_SMTP_HOST' );                   
      $this->Port		= registry::getSetting( 'CNF_MAIL_SMTP_PORT' );        
      $this->Username	= registry::getSetting( 'CNF_MAIL_SMTP_USERNAME' ); 
      $this->Password	= registry::getSetting( 'CNF_MAIL_SMTP_PASSWORD' );
      $this->XMailer	= WDNAME.' Mailer ver. '.WDVERSION;
    }else{
		//echo "Not SMTP";
    }
   
    
    $this->CharSet	= 'utf-8';
    //$this->AddReplyTo( registry::getSetting( 'CNF_ADMIN_EMAIL' ), registry::getSetting( 'CNF_MAIL_SMTP_FROM_NAME' ) );
    $this->SetFrom( registry::getSetting( 'CNF_MAIL_SMTP_FROM_ADDRESS' ), registry::getSetting( 'CNF_MAIL_SMTP_FROM_NAME' ) );
    //$this->ClearAddresses();
  }
  

 }