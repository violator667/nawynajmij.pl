<?php
/* PHP WHISKY DRINKER
 * 
 * File		:	BasicUser.class.php
 * Version	:	1.0
 *
 * Info		:	Basic User
 *
 * Author  	: 	Michał Zielonka-Majka 
 * Build	:	07.09.2013
 * 
 */

  if( !defined( 'WHISKY' ) ) {
	die( 'Out of WHISKY SAP - exit!' );
  }
 
  class BasicUser {
  
  /**
   *	ID of user 
  **/
  public $id;
  
  /**
   *	Username 
  **/
  public $username;
  
  /**
   *	Email address
  **/
  public $email;
  
  /**
   *	Password HASH
   *	md5(SALT.GIVENPASSWORD);
  **/
  public $password;
  
  /**
   *	Status:
   *	0 : email not confirmed
   *	1 :	email confirmed
  **/
  public $status;
  
  /**
   *	Role:
   *	1 		: normal user
   *	2 - 98 	: saved for future use
   *	99 		: admin
  **/
  public $role;
  
  /**
   * 	Banned:
   *	0 : user isn't banned
   *	1 : user is banned 
  **/
  public $banned;
  
  /**
   *	Registration date
  **/
  public $registration;
  
  /**
   *	Hash - random hash 
   *	example usage: to confirm email address
  **/
  public $hash;
  
	public function __construct(  )
	{
		
	}
	/**
	 *  setId
	**/
	public function setId( $id ) {
		$this->id = $id;
	}
	/**
	 * 	getUser - gets user data from DB
	**/
	public function getUser()
	{
		$exec = Db::getConnection()->runQuery( "SELECT * FROM `".DB_PREFIX."_users` WHERE id='$this->id' LIMIT 0,1" );
		if( $exec->rowCount()!=0 ) {
			$result = $exec->fetch( );
			foreach( $result as $key => $value ) {
				$this->$key	= $value;
			}
		}else{
			Throw new AppException( 'Nie znaleziono użytkownika.' , 10 );
		}
	}

	public function registerUser( $username, $email, $password, $status, $role, $banned, $registration, $hash)
	{
			/*
			*	Registers user - this function DON'T check anything!
			*/
			$exec = Db::getConnection()->runQuery( "INSERT INTO ".DB_PREFIX."_users 
			(username,
			email,
			password,
			status,
			role,
			banned,
			registration,
			hash) VALUES 
			('$username',
			'$email',
			'$password',
			'$status',
			'$role',
			'$banned',
			'$registration',
			'$hash')" );		
	}
 }